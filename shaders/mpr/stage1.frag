// fragment shader
//

uniform vec2 uViewportSize;
uniform sampler2D uPreviousStage;

void main() {
  vec4 previous = PREVIOUS_FRAG_COLOR;
  float alpha = previous.z;
  float intensity = previous.a;

  gl_FragColor = vec4(intensity, intensity, intensity, alpha);
}
