// fragment shader
//

uniform vec2 uViewportSize;
uniform sampler2D uPreviousStage;

uniform sampler2D uLabelMapLUT;
uniform float uLabelMapLUTlength;

void main() {
  vec2 segIndex = LABEL_PREVIOUS_FRAG_COLOR.xy;
  vec4 segColor
    = lutRGBA(uLabelMapLUT, uLabelMapLUTlength, indexVToIndex(segIndex));

  gl_FragColor = vec4(segColor.a * segColor.rgb, segColor.a);
}
