/*  Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

varying vec3 vColor;

uniform sampler2D uFrontCoords;
uniform vec2 uViewportSize;

uniform float uSlice; // slice position [0.0, 1.0]

uniform SAMPLER_3D uVolume;
uniform SAMPLER_3D uLabelMap;

float linePlaneIntersection(vec3 planeNormal, vec3 planePoint,
    vec3 lineDirection, vec3 linePoint)
{
  return dot((planePoint - linePoint), planeNormal)
    / dot(lineDirection, planeNormal);
}

uniform vec3 uCutPlaneNormal;
const vec3 cutPlanePoint = vec3(0.5);

//FIXME non isotropic
vec4 mpr(SAMPLER_3D texture, vec3 origin, vec3 direction, float slicePosition) {
  vec3 n = normalize(uCutPlaneNormal);
  /* scaling such that the plane always intersects the volume, but the corners
   * are not reachable since slicePosition is not scaled (e.g. sqrt(3.0)) to
   * reach the maximal distance, but the minimal.
   * (Nobody wants to see the corners anyway) */
  float d = linePlaneIntersection(n, cutPlanePoint + n*(slicePosition - 0.5),
      direction, origin);

  if (0.0 <= d && d <= 1.0) {
    vec3 samplePoint = origin + d * direction;
    float intensity = TEXTURE_3D(texture, samplePoint).r;
    vec2 segIndex = colorToIndexV(texture3DNearest(uLabelMap, samplePoint));

    return vec4(segIndex, 1, intensityToColor(intensity).r);
  }

  return vec4(0.0);
}

void main() {
  vec3 start = FRONT_COORDS;
  vec3 end = vColor;
  vec3 direction = end - start;

  gl_FragColor = mpr(uVolume, start, direction, uSlice);
}
