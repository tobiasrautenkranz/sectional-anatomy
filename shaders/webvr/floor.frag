// fragment shader
varying mediump vec4 worldSpaceCoords;

precision mediump float;

void main(void) {
  mediump vec2 w = worldSpaceCoords.xz / worldSpaceCoords.w;
  mediump vec2 p = fract(w);
  const float stepSize = 2.0;
  if (fract(stepSize*0.5*p.x) <= 0.05 && fract(stepSize*0.5*p.y) <= 0.05) {
    gl_FragColor = vec4(0.8, 0.8, 0.8, 1.0);
  } else if (fract(stepSize*p.x) <= 0.05 || fract(stepSize*p.y) <= 0.05) {
    gl_FragColor = vec4(0.2, 0.2, 0.2, 1.0);
  } else {
    gl_FragColor = vec4(0.2, 0.0, 0.0, 1.0);
  }
}
