// Vertex shader
attribute vec3 aVertexPosition;
attribute vec2 aTextureCoord;

uniform mat4 uPMatrix;
uniform mat4 uVMatrix;

varying highp vec2 vTextureCoord;

void main(void) {
  gl_Position = uPMatrix * uVMatrix * vec4(aVertexPosition, 1.0);
  vTextureCoord = aTextureCoord;
}
