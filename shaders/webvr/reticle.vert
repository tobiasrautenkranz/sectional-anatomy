// Vertex shader
attribute vec3 aVertexPosition;

uniform mat4 uPMatrix;
uniform mat4 uVMatrix;

uniform vec3 center;

varying mediump float vDistance;
varying mediump vec2 vPosition;

void main(void) {
  gl_Position = uPMatrix * uVMatrix * vec4(aVertexPosition, 1.0);

  vDistance = length(aVertexPosition.xy);
  vPosition = aVertexPosition.xy;
}
