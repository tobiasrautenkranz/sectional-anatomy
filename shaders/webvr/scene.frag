// fragment shader
precision mediump float;

uniform vec3 uLightPosition;

varying lowp vec3 vColor;
varying vec3 vNormal;
varying vec3 vVertexPosition;

const float shininess = 10.0;

const float ambientColorFactor = 0.1;
const float diffuseColorFactor = 0.9;
const float specluarFactor = 0.3;

void main(void) {
  vec3 lightDirection = normalize(uLightPosition - vVertexPosition);

  vec3 normal = normalize(vNormal);

  float diffuseFactor = clamp(dot(normal, lightDirection), 0.0, 1.0);

  vec3 viewDirection = normalize(-vVertexPosition);
  vec3 halfway = normalize(lightDirection + viewDirection);
  float specular = pow(clamp(dot(normal, halfway), 0.0, 1.0), shininess);

  gl_FragColor = vec4(ambientColorFactor * vColor
      + diffuseFactor * diffuseColorFactor * vColor
      + specluarFactor * specular, 1.0);
}
