// Vertex shader
attribute vec3 aVertexPosition;
attribute vec3 aVertexColor;
attribute vec3 aVertexNormal;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

varying lowp vec3 vColor;
varying mediump vec3 vNormal;
varying mediump vec3 vVertexPosition;


void main(void) {
  vec4 position = uMVMatrix * vec4(aVertexPosition, 1.0);
  gl_Position = uPMatrix * position;

  vColor = aVertexColor;

  vVertexPosition = position.xyz / position.w;
  vNormal = vec3(uMVMatrix * vec4(aVertexNormal, 0.0));
}
