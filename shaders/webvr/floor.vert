// Vertex shader
attribute vec4 aVertexPosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

varying mediump vec4 worldSpaceCoords;

void main(void) {
  gl_Position = uPMatrix * uMVMatrix * aVertexPosition;

  worldSpaceCoords = aVertexPosition;
}
