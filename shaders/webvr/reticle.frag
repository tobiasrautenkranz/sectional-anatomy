// fragment shader
precision mediump float;

uniform float uRadius;
uniform float uWidth;

varying float vDistance;
varying vec2 vPosition;

const vec4 color = vec4(0.5, 0.5, 0.8, 0.5);

void main(void) {
  float dist = length(vPosition);
  if (dist  <= uRadius && dist >= (uRadius - uWidth)) {
    gl_FragColor = color;
  } else {
    discard;
  }
}
