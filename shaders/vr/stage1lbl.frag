// fragment shader
//

uniform vec2 uViewportSize;
uniform sampler2D uPreviousStage;

void main() {
  gl_FragColor = vec4(LABEL_PREVIOUS_FRAG_COLOR.rgb, 1.0);
}
