// fragment shader
//

uniform vec2 uViewportSize;
uniform sampler2D uPreviousStage;

void main() {
#ifdef USE_DRAW_BUFFERS_EXT
  gl_FragColor = PREVIOUS_FRAG_COLOR;
#else
  float intensity = PREVIOUS_FRAG_COLOR.a;

  gl_FragColor = vec4(intensity, intensity, intensity, 1.0);
#endif
}
