/*  Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

varying vec3 vColor;
uniform sampler2D uFrontCoords;
uniform vec2 uViewportSize;

uniform SAMPLER_3D uVolume;
uniform SAMPLER_3D uLabelMap;

uniform sampler2D uLabelMapLUT;
uniform float uLabelMapLUTlength;

uniform vec3 uToLight;


const float referenceSteps = 64.0;
float opacityForSteps(float opacity, float numSteps) {
  return 1.0 - pow(1.0 - opacity, referenceSteps/numSteps);
}
float stepCorrectedOpacity(float opacity) {
  return opacityForSteps(opacity, float(STEPS));
}


#ifdef HIGH_QUALITY
#define AMBIENT_OCCLUSION
#endif

#ifdef AMBIENT_OCCLUSION

// -----------------
// local Ambient occlusion
//



float ambientOcclusion(highp vec3 position) {
  initBBS(position);

  const int ambientOcclusionRays = STEPS/16;
  const int ambientOcclusionSteps = STEPS/32;

  // ratio of steps inside the volume (guestimate)
  const float proximityFactor = 0.8;

  float occlusionFactor = 0.0;
  float stepSize = 0.3 * sqrt(3.0) / float(ambientOcclusionSteps);

  for (int i=0; i<ambientOcclusionRays; i++) {
    highp vec3 rayIncrement = randomUnitVector();
    // only half sphere to light. negate because faceforward ~ facebackward.
    rayIncrement = -faceforward(rayIncrement, uToLight, rayIncrement);
    rayIncrement *= stepSize;
    /* move at least stepSize to prevent sampling of voxels skipped when finding
     * this position. */
    highp vec3 ray = position + rayIncrement;

    float opacityAcc = 0.0;

    for (int j=0; j<ambientOcclusionSteps; j++) {
      if (any(lessThan(ray, vec3(0.0))) ||
          any(greaterThan(ray, vec3(1.0)))) {
        break;
      }

      float opacity
        = opacityForSteps(intensityToColor(texture3DNearest(uVolume, ray).r).a,
            float(ambientOcclusionSteps) / proximityFactor);
      opacityAcc += (1.0 - opacityAcc) * opacity;

      if (opacityAcc >= 0.99) {
        break;
      }

      ray += rayIncrement;
    }
    occlusionFactor += opacityAcc;
  }
  return (1.0 - occlusionFactor/float(ambientOcclusionRays));
}
#endif

void volumeRendering(SAMPLER_3D texture, highp vec3 start, highp vec3 end) {
#ifdef GL_EXT_draw_buffers
  vec3 colorAcc = vec3(0.0);
#else
  float colorAcc = 0.0;
#endif
  float opacityAcc = 0.0;
  vec3 labelMapAcc = vec3(0.0);

  highp vec3 direction = normalize(end - start);
  highp vec3 rayIncrement = sqrt(3.0)/float(STEPS) * direction;
  highp vec3 ray = start + rand(start)*rayIncrement;
  int lastStep = int(distance(start, end) / length(rayIncrement))-1;

  for (int i=0; i<STEPS; i++) {
    float intensity = texture3D(texture, ray).r;
    vec4 color = intensityToColor(intensity);

    vec4 segColor
      = texture3DNearestLUT(uLabelMap, ray, uLabelMapLUT, uLabelMapLUTlength);

#ifdef AMBIENT_OCCLUSION
    const float iAmbient = 0.2;
    const float iDiffuse = 0.6;
    color.rgb *= iAmbient + iDiffuse*ambientOcclusion(ray);
#endif
    float opacity = stepCorrectedOpacity(color.a);
    float segOpacity = stepCorrectedOpacity(color.a * segColor.a);


    labelMapAcc += (1.0 - opacityAcc) * segColor.rgb * segOpacity;
#ifdef GL_EXT_draw_buffers
    colorAcc += (1.0 - opacityAcc) * color.rgb * opacity;
#else
    colorAcc += (1.0 - opacityAcc) * color.r * opacity;
#endif
    opacityAcc += (1.0 - opacityAcc) * opacity;

    if (i >= lastStep) {
      // WTF angle problem? break does not work with last step????????
     // opacityAcc = 1.0;
     break;
    }
    if (opacityAcc >= 0.99)
    {
      break;
    }

    ray += rayIncrement;
  }

#ifdef GL_EXT_draw_buffers
  gl_FragData[0] = vec4(colorAcc * opacityAcc, opacityAcc);
  gl_FragData[1] = vec4(labelMapAcc, opacityAcc);
#else
  gl_FragColor = vec4(labelMapAcc, colorAcc);
#endif
}

void main() {
  vec3 start = FRONT_COORDS;
  vec3 end = vColor;
  volumeRendering(uVolume, start, end);
}

