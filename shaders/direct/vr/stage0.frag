/*  Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

varying vec3 vColor;
uniform sampler2D uFrontCoords;
uniform vec2 uViewportSize;

uniform SAMPLER_3D uVolume;
uniform SAMPLER_3D uLabelMap;

uniform sampler2D uLabelMapLUT;
uniform float uLabelMapLUTlength;

uniform float uLabelOppacity;

const float referenceSteps = 64.0;
float opacityForSteps(float opacity, float numSteps) {
  return 1.0 - pow(1.0 - opacity, referenceSteps/numSteps);
}
float stepCorrectedOpacity(float opacity) {
  return opacityForSteps(opacity, float(STEPS));
}

vec4 volumeRendering(SAMPLER_3D texture, highp vec3 start, highp vec3 end) {
  vec3 colorAcc = vec3(0.0);
  float opacityAcc = 0.0;

  highp vec3 direction = normalize(end - start);
  highp vec3 rayIncrement = sqrt(3.0)/float(STEPS) * direction;
  highp vec3 ray = start + rand(start)*rayIncrement;
  int lastStep = int(distance(start, end) / length(rayIncrement))-1;

  for (int i=0; i<STEPS; i++) {
    float intensity = texture3D(texture, ray).r;
    vec4 color = intensityToColor(intensity);

    float opacity = stepCorrectedOpacity(color.a);

    colorAcc += (1.0 - opacityAcc) * color.rgb * opacity;
    opacityAcc += (1.0 - opacityAcc) * opacity;

    if (i >= lastStep) {
      // WTF angle problem? break does not work with last step????????
     // opacityAcc = 1.0;
     break;
    }
    if (opacityAcc >= 0.99)
    {
      break;
    }

    ray += rayIncrement;
  }

  return vec4(colorAcc * opacityAcc, opacityAcc);
}

vec4 colorFromLabels(SAMPLER_3D labelMap, highp vec3 start, highp vec3 end) {
  highp vec3 direction = normalize(end - start);
  highp vec3 rayIncrement = sqrt(3.0)/float(STEPS) * direction;
  highp vec3 ray = start + rand(start)*rayIncrement;
  int lastStep = int(distance(start, end) / length(rayIncrement))-1;

  const float alphaThreshold = 0.3;

  for (int i=0; i<STEPS; i++) {
    vec4 color
      = texture3DNearestLUT(uLabelMap, ray, uLabelMapLUT, uLabelMapLUTlength);

     if (color.a >= alphaThreshold) {
      return color;
     }

     if (i >= lastStep) {
       break;
     }

     ray += rayIncrement;
  }

  return vec4(0.0);
}

void main() {
  vec3 start = FRONT_COORDS;
  vec3 end = vColor;

  vec4 vrColor = volumeRendering(uVolume, start, end);
  vec4 lblColor = colorFromLabels(uLabelMap, start, end);

  gl_FragColor = mix(vrColor, lblColor, lblColor.a * uLabelOppacity);
}

