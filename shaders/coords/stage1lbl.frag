// fragment shader
//
varying vec3 vColor;

uniform vec2 uViewportSize;
uniform sampler2D uPreviousStage;
uniform sampler2D uFrontCoords;

void main() {
  vec3 start = LABEL_FRONT_COORDS;
  vec3 end = vColor;
  vec3 prev = LABEL_PREVIOUS_FRAG_COLOR.rgb;

  //gl_FragColor = vec4(end, 1.0);
  gl_FragColor = vec4(0.5 + 0.5*(end - start), 1.0);
}
