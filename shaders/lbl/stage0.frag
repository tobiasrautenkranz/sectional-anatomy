/*  Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

varying vec3 vColor;
uniform sampler2D uFrontCoords;
uniform vec2 uViewportSize;

uniform sampler2D uLabelMapLUT;
uniform float uLabelMapLUTlength;

uniform SAMPLER_3D uLabelMap;

float labelAlphaTreshold = 0.98 * 200.0 / 255.0;

/** Find the first selected label */
vec4 lbliso(vec3 start, vec3 direction) {
  vec3 rayIncrement = 1.0/float(STEPS) * direction;
  vec3 ray = start + rand(start)*rayIncrement;

  // --------------------
  // Find Surface
  vec2 labelId = vec2(0.0);
  bool hasLabel = false;

  for (int i=0; i<STEPS; i++) {
    vec2 label = colorToIndexV(texture3DNearest(uLabelMap, ray));
    float alpha
      = lutRGBA(uLabelMapLUT, uLabelMapLUTlength, indexVToIndex(label)).a;

    if (alpha > labelAlphaTreshold) {
      labelId = label;
      hasLabel = true;
      break;
    }
    ray += rayIncrement;
  }


  if (hasLabel) {
    // ---------------------------
    // binary search for surface
    rayIncrement /= 2.0;
    vec3 testRay = ray - rayIncrement;

    // worst case: 64 steps sqrt(3) * 256 = 440
    // 7 voxel/step
    // ln(7) = 3
    // => max 3 steps
    const int subSteps = 2;
    for (int i=0; i<subSteps; i++) {
      vec2 label = colorToIndexV(texture3DNearest(uLabelMap, ray));
      float alpha
        = lutRGBA(uLabelMapLUT, uLabelMapLUTlength, indexVToIndex(label)).a;

      rayIncrement /= 2.0;
      if (alpha > labelAlphaTreshold) {
        ray = testRay;
        labelId = label;

        testRay -= rayIncrement;
      } else {
        testRay += rayIncrement;
      }
    }

    float depth = distance(start, ray) / length(direction);
    return vec4(labelId, depth, 0.0);
  } else {
    return vec4(0, 0, 1, 0);
  }
}

void main() {
  vec3 start = FRONT_COORDS;
  vec3 end = vColor;
  vec3 direction = end - start;

  gl_FragColor = lbliso(start, direction);
}
