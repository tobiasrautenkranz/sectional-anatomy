/*  Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
varying vec3 vColor;
uniform sampler2D uFrontCoords;
uniform sampler2D uPreviousStage;
uniform vec2 uViewportSize;

uniform sampler2D uLabelMapLUT;
uniform float uLabelMapLUTlength;

uniform SAMPLER_3D uLabelMap;
uniform SAMPLER_3D uVolume;


vec4 getlblavg(vec3 start, vec3 end, float stopLengthRatio) {
  vec3 ray = start;
  vec3 direction = end - start;
  float stopLength = stopLengthRatio * length(direction);
  vec3 rayIncrement = sqrt(3.0)/float(STEPS) * normalize(direction);
  int lastStep = int(stopLength / length(rayIncrement));

  vec4 avg = vec4(0, 0, 0, 0);
  for (int i=0; i<STEPS; i++) {
    avg += texture3DNearestLUT(uLabelMap, ray, uLabelMapLUT, uLabelMapLUTlength);
    ray += rayIncrement;

    if (i >= lastStep) {
      break;
    }
  }
  avg /= float(STEPS);

  return avg;
}


const float unselectedLabelAlpha = 100.0/255.0;
const float maxFillingFactor = 0.4;
void main() {
  vec4 prev = LABEL_PREVIOUS_FRAG_COLOR;
  vec3 start = LABEL_FRONT_COORDS;
  vec3 end = vColor;
  vec3 direction = end - start;

  float depth = prev.b;
  vec4 lblavg = getlblavg(start, end, depth);
  float alpha = lblavg.a * depth;
  alpha /= unselectedLabelAlpha; // scale to 1.0 alpha for labels;
  alpha /= maxFillingFactor; // make maxFillingFactor full opaque;
  if (depth < 0.9) {
    // sample inside the label volume to prevent blending with non label voxels.
    vec3 voxelStep = 2.0 * normalize(direction) * 1.0 / uVolumeSize;

    vec3 volumeColor
      = intensityToColor(texture3D(uVolume, start + depth*direction + voxelStep).r).rgb;

    gl_FragColor = vec4(mix(volumeColor, lblavg.rgb, alpha), 1);
  } else {
    gl_FragColor = vec4(alpha * lblavg.rgb, alpha);
  }
}

