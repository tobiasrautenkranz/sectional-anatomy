/*  Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

varying vec3 vColor;
uniform sampler2D uFrontCoords;
uniform sampler2D uPreviousStage;
uniform vec2 uViewportSize;

uniform float uScale;

uniform SAMPLER_3D uVolume;
uniform SAMPLER_3D uLabelMap;

uniform sampler2D uLabelMapLUT;
uniform float uLabelMapLUTlength;

uniform vec3 uToLight;

float avg(SAMPLER_3D volume, vec3 start, vec3 end, float stopLengthRatio) {
  vec3 ray = start;
  vec3 direction = end - start;
  float stopLength = stopLengthRatio * length(direction);
  vec3 rayIncrement = sqrt(3.0)/float(STEPS) * normalize(direction);
  int lastStep = int(stopLength / length(rayIncrement));

  float avg = 0.0;
  for (int i=0; i<STEPS; i++) {
    avg += texture3D(volume, ray).r;

    if (i >= lastStep) {
      break;
    }
    ray += rayIncrement;
  }
  avg /= float(STEPS);

  return avg;
}

float labelAlphaTreshold = 0.98 * 200.0 / 255.0;
float texture3DSelectedLabel(vec3 position) {
  float index
    = texture3DNearestLUT(uLabelMap, position, uLabelMapLUT, uLabelMapLUTlength).a;

  return float(index > labelAlphaTreshold);
}
const float gradSmoothing = 1.0;
vec3 grad(SAMPLER_3D volume, vec3 position) {
  vec3 grad_epsilon = gradSmoothing/uVolumeSize;
  vec3 delta = vec3(
      texture3D(volume, position + vec3(grad_epsilon.x, 0, 0)).r
      - texture3D(volume, position - vec3(grad_epsilon.x, 0, 0)).r,

      texture3D(volume, position + vec3(0, grad_epsilon.y, 0)).r
      - texture3D(volume, position - vec3(0, grad_epsilon.y, 0)).r,

      texture3D(volume, position + vec3(0, 0, grad_epsilon.z)).r
      - texture3D(volume, position - vec3(0, 0, grad_epsilon.z)).r
      );

  return delta / (2.0 * grad_epsilon);
}

vec3 normalSelectedLabels(vec3 position) {
  vec3 grad_epsilon = gradSmoothing/uVolumeSize;
  vec3 grad = vec3(
      texture3DSelectedLabel(position + vec3(grad_epsilon.x, 0, 0))
      - texture3DSelectedLabel(position - vec3(grad_epsilon.x, 0, 0)),

      texture3DSelectedLabel(position + vec3(0, grad_epsilon.y, 0))
      - texture3DSelectedLabel(position - vec3(0, grad_epsilon.y, 0)),

      texture3DSelectedLabel(position + vec3(0, 0, grad_epsilon.z))
      - texture3DSelectedLabel(position - vec3(0, 0, grad_epsilon.z))
      );

  return normalize(-grad / (2.0 * grad_epsilon));
}

vec3 normalSelectedLabelsUsingImageVolumeGrad(vec3 position) {
  vec3 volumeGrad = grad(uVolume, position);
  const float minValidGradientLength2 = 0.1*0.1;
  if (dot(volumeGrad,volumeGrad) >= minValidGradientLength2) { // FIXME better decision
    return -normalize(volumeGrad);
  } else {
    return normalSelectedLabels(position);
  }
}

#if STEPS >= 64
  #define CEL_SHADING 1
#endif

#ifdef CEL_SHADING
const float outlineFactor = 0.5;

float edgeDelta = 0.12;
bool outlineFromNeighbour(float depth1, vec2 pos2, float labelId) {
  vec4 p = texture2D(uPreviousStage, pos2);
  float labelId2 = indexVToIndex(p.rg);
  float depth2 = p.b;

  // outline for label next to other label
  if (labelId != labelId2) {
    // check if HL label is in front, or nearly
    return depth2 - depth1 > -edgeDelta;
  }

  // outline for edge
  return ((depth2 - depth1) > edgeDelta);
}

bool outline(sampler2D previousStage, vec2 position, vec3 direction,
    float depth, float labelId)
{
  vec2 d = vec2(0.01,0.01)/uScale;
  float realDepthFactor = length(direction);

  // works only for d -> 0, but removes edges drawn at the volume edge.
  edgeDelta = 0.12 / realDepthFactor;

  if (outlineFromNeighbour(depth, position + vec2(0, d.y), labelId)) {
    return true;
  }
  if (outlineFromNeighbour(depth, position + vec2(0, -d.y), labelId)) {
    return true;
  }
  if (outlineFromNeighbour(depth, position + vec2(d.x, 0), labelId)) {
    return true;
  }
  if (outlineFromNeighbour(depth, position + vec2(-d.x, 0), labelId)) {
    return true;
  }

  return false;
}
#endif

const float iAmbient = 0.4;
const float iDiffuse = 0.4;
float shading(vec3 normal, vec3 direction, vec2 viewportCoord, float depth,
    float labelId, bool selectedLabel)
{
  // ---------------
  // Phong lighting
  float diffuse = iDiffuse * max(0.0, dot(normal, uToLight));

  float intensity;
#ifdef CEL_SHADING
  intensity = iAmbient + diffuse;

  // abs(dot(n, normalize(direction))) < 0.2 || // Does not work sufficiently
  if (outline(uPreviousStage, viewportCoord, direction, depth, labelId)) {
    if (selectedLabel) {
      intensity /= outlineFactor;
    } else {
      intensity *= outlineFactor;
    }
  }
#else
  intensity = iAmbient + diffuse;
#endif

  return intensity;
}

vec4 labelColorSmooth(vec4 color) {
  vec2 position = FB_TEXTURE_COORDS;
  // FIXME sampling image space, but should jump a voxel (normally several pixels)
  vec2 delta = 1.0 / uViewportSize * 3.0;

  color *= 2.0;
  color += lutRGBA(uLabelMapLUT, uLabelMapLUTlength,
      indexVToIndex(texture2D(uPreviousStage, position + vec2(delta.x, 0)).rg));
  color += lutRGBA(uLabelMapLUT, uLabelMapLUTlength,
      indexVToIndex(texture2D(uPreviousStage, position - vec2(delta.x, 0)).rg));

  color += lutRGBA(uLabelMapLUT, uLabelMapLUTlength,
      indexVToIndex(texture2D(uPreviousStage, position + vec2(0, delta.y)).rg));
  color += lutRGBA(uLabelMapLUT, uLabelMapLUTlength,
      indexVToIndex(texture2D(uPreviousStage, position - vec2(0, delta.y)).rg));

  return color/6.0;
}

void main() {
  vec2 viewportCoord = FB_TEXTURE_COORDS;
  vec4 prev = texture2D(uPreviousStage, viewportCoord);
  vec3 start = texture2D(uFrontCoords, viewportCoord).xyz;
  vec3 end = vColor;
  vec3 direction = end - start;

  float labelId = indexVToIndex(prev.rg);
  float depth = prev.b;
  vec4 volumeColor = intensityToColor(avg(uVolume, start, end, depth));
  float alpha = depth * volumeColor.a;

  if (labelId != 0.0) {
    vec3 normal
      = normalSelectedLabelsUsingImageVolumeGrad(start + direction * depth);

    vec4 labelColor = lutRGBA(uLabelMapLUT, uLabelMapLUTlength, labelId);

    float light
      = shading(normal, direction, viewportCoord, depth, labelId, labelColor.a == 1.0);
    //#ifdef HIGH_QUALITY // FIXME less blur
    //   labelColor = labelColorSmooth(labelColor);
    //#endif
    float depthCue = (1.0 - 0.5 * depth * length(direction));
    gl_FragColor
      = vec4(mix(depthCue * labelColor.a * light * labelColor.rgb, volumeColor.rgb, alpha), 1);
  } else {
    gl_FragColor = vec4(alpha * volumeColor.rgb, alpha);
#ifdef CEL_SHADING
    if (outline(uPreviousStage, viewportCoord, direction, depth, labelId)) {
      const float ocolor = 0.1;
      gl_FragColor = vec4(ocolor, ocolor, ocolor, 1) + gl_FragColor;
    }
#endif
  }
}
