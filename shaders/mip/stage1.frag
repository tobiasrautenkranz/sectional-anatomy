// fragment shader
//

uniform vec2 uViewportSize;
uniform sampler2D uPreviousStage;

void main() {
  float intensity = PREVIOUS_FRAG_COLOR.a;
  gl_FragColor = vec4(intensity, intensity, intensity, 1.0);
}
