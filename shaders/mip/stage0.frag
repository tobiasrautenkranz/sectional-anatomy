/*  Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

varying vec3 vColor;
varying vec4 vPosition;
uniform sampler2D uFrontCoords;
uniform vec2 uViewportSize;

uniform SAMPLER_3D uVolume;
uniform SAMPLER_3D uLabelMap;

uniform float uThreshold;

vec4 mip(SAMPLER_3D texture, vec3 origin, vec3 direction, float threshold) {
  highp vec3 rayIncrement = 1.0/float(STEPS) * direction;
  highp vec3 ray = origin + rand(origin) * rayIncrement;

  float maxVal = 0.0;
  vec3 maxPosition = vec3(ray);

  for (int i=0; i<STEPS; i++) {
    float value = TEXTURE_3D(texture, ray).r;
    if (maxVal > threshold && value < maxVal) {
      // LMIP
      break;
    }
    if (value > maxVal) {
        maxVal = value;
        maxPosition = vec3(ray);
    }
    ray += rayIncrement;
  }

  vec2 segIndex = colorToIndexV(texture3DNearest(uLabelMap, maxPosition));
  return vec4(segIndex, 1.0, intensityToColor(maxVal).r);
}

vec4 avg(SAMPLER_3D texture, vec3 origin, vec3 direction, float threshold) {
  highp vec3 rayIncrement = 1.0/float(STEPS) * direction;
  highp vec3 ray = origin + rand(origin)*rayIncrement;

  float acc = 0.0;
  float steps = 0.0;
  for (int i=0; i<STEPS; i++) {
    float value = texture3D(texture, ray).r;
    acc += value;
    steps++;
    ray += rayIncrement;
  }

  return vec4(intensityToColor(acc/steps).r, 0.0, 1.0, 1.0);
}

void main() {
  vec3 start = FRONT_COORDS;
  vec3 end = vColor;
  vec3 direction = end - start;

  gl_FragColor = mip(uVolume, start, direction, uThreshold);
}
