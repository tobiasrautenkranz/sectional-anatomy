// fragment shader
//

uniform vec2 uViewportSize;
uniform sampler2D uPreviousStage;

uniform sampler2D uLabelMapLUT;
uniform float uLabelMapLUTlength;

// make image less dark
const float luminosityBoost = 1.5;

void main() {
  vec4 prev = LABEL_PREVIOUS_FRAG_COLOR;
  vec2 light = prev.rg;
  vec2 segIndex = prev.ba;

  if (!all(equal(segIndex, vec2(0, 0)))) {
    vec4 segColor
      = lutRGBA(uLabelMapLUT, uLabelMapLUTlength, indexVToIndex(segIndex));

    gl_FragColor
      = vec4(light.x * segColor.rgb * luminosityBoost * segColor.a + light.yyy, 1);
  } else {
    if (light.x == 0.0) {
      gl_FragColor = vec4(0, 0, 0, 0);
    } else {
      gl_FragColor = vec4(light.yyy, 1);
    }
  }
}
