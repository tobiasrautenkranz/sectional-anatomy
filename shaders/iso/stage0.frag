/*  Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

varying vec3 vColor;
uniform sampler2D uFrontCoords;
uniform vec2 uViewportSize;

uniform sampler2D uLabelMapLUT;
uniform float uLabelMapLUTlength;

uniform SAMPLER_3D uVolume;
uniform SAMPLER_3D uLabelMap;

uniform float uThreshold;

uniform vec3 uToLight;

/*
 * Adopting a physically based shading model
 * Sébastien Lagarde
 *
 * August 17, 2011 25 Comments
 * Version : 1.31 – Living blog – First version was 2 August 2011
 *
 * https://seblagarde.wordpress.com/2011/08/17/hello-world/
 *
 * additional references:
 * https://en.wikipedia.org/wiki/Schlick%27s_approximation
 * https://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_shading_model
 * http://blog.selfshadow.com/publications/s2012-shading-course/#course_content
 */

float fresnelSchlick(float specularColor, vec3 E, vec3 H)
{
  // R = R(0) + (1 -R(0))(1- cos(θ))
  return specularColor +
    (1.0 - specularColor) * pow(1.0 - max((dot(E, H)), 0.0), 5.0);
}

struct material {
  float ambientDiffuseColor;
  float diffuseColor;
  float specularColor;
  float specularPower;
};

const material defaultMaterial = material(
  0.3,
  0.5,
  0.5,
  0.17
);
const float lightIntensity = 0.8;

float specularL(const material m, float intensity,
    vec3 n, vec3 toLight, vec3 halfway) {
  float fresnel = fresnelSchlick(m.specularColor, toLight, halfway);
  return fresnel * ((m.specularPower + 2.0) / 8.0 )
    * pow(max(0.0,dot(n, halfway)), m.specularPower) * dot(n, toLight);
}

float phongDiffuse(const material m, float intensity, vec3 n, vec3 toLight) {
  return m.diffuseColor * clamp(dot(n, toLight), 0.0, 1.0) * intensity;
}

#if STEPS >= 64
  #define AMBIENT_OCCLUSION 1
#endif
#ifdef HIGH_QUALITY
  #define SHADOW 1
#endif

vec3 normal(SAMPLER_3D volume, vec3 position) {
  vec3 grad_epsilon = 1.0/uVolumeSize;
  vec3 delta = vec3(
      texture3D(volume, position + vec3(grad_epsilon.x, 0, 0)).r
      - texture3D(volume, position - vec3(grad_epsilon.x, 0, 0)).r,

      texture3D(volume, position + vec3(0, grad_epsilon.y, 0)).r
      - texture3D(volume, position - vec3(0, grad_epsilon.y, 0)).r,

      texture3D(volume, position + vec3(0, 0, grad_epsilon.z)).r
      - texture3D(volume, position - vec3(0, 0, grad_epsilon.z)).r
      );
  vec3 grad = delta / (2.0*grad_epsilon);

  // gradient points to higher value but the normal shall point outwards
  return normalize(-grad);
}

vec4 iso(SAMPLER_3D texture, vec3 origin, highp vec3 direction, float threshold) {
  highp vec3 rayIncrement = 1.0/float(STEPS) * direction;

#ifdef HIGH_QUALITY
  highp vec3 ray = origin;
#else
  // prevent banding for low step sizes
  highp vec3 ray = origin + rand(origin)*rayIncrement;
#endif

  bool hasSurface = false;

  // --------------------
  // Find Surface

  for (int i=0; i<STEPS; i++) {
    float sample = texture3D(texture, ray).r;
    if (sample > threshold) {
      hasSurface = true;
      /*
       * We should not do the texture lookup here,
       * because some drivers (D3D) try to unroll these
       * and compilation takes really long.
       *
       * [1] http://code.google.com/p/angleproject/issues/detail?id=146#c4
       */
      break;
    }
    ray += rayIncrement;
  }


  if (hasSurface) {
    vec2 labelIndex
      = colorToIndexV(texture3DNearest(uLabelMap, ray + 0.5*rayIncrement));

    if (all(equal(labelIndex, vec2(0, 0)))) {
      labelIndex
        = colorToIndexV(texture3DNearest(uLabelMap, ray + 1.5*rayIncrement));
    }

    // ---------------------------
    // binary search for surface
    // worst case: 64 steps sqrt(3) * 256 = 440
    // 7 voxel/step
    // ln(7) = 3
    // => max 3 steps
    rayIncrement /= 2.0;
    vec3 testRay = ray - rayIncrement;

    for (int i=0; i<4; i++) {
      float s = texture3D(texture, testRay).r;
      rayIncrement /= 2.0;
      if (s > threshold) {
        testRay -= rayIncrement;
        ray = testRay;
      } else {
        testRay += rayIncrement;
      }
    }
    if (all(equal(labelIndex, vec2(0, 0)))) {
      labelIndex = colorToIndexV(texture3DNearest(uLabelMap, ray));
    }

    // -----
    // self shadowing
#ifdef SHADOW
    const int shadowSteps = 32;
    vec3 shadowRay = uToLight * 0.5 / float(shadowSteps);
    vec3 spos = ray + shadowRay;
    bool shadowed = false;
    for (int i=0; i<shadowSteps; i++) {
        if (any(lessThan(spos, vec3(0.0))) ||
            any(greaterThan(spos, vec3(1.0)))) {
          break;
        }

        float value = texture3DNearest(uVolume, spos).r;
        if (value > threshold) {
            shadowed = true;
          break;
        }
        spos += shadowRay;
    }
#endif

    vec3 n = normal(texture, ray);
    float specular = 0.0;
    float diffuse = 0.0;


#ifdef SHADOW
      if (!shadowed) {
#endif
        vec3 viewDir = -normalize(direction);
        vec3 halfway = normalize(uToLight + viewDir);
        specular = specularL(defaultMaterial, lightIntensity, n, uToLight, halfway);
        diffuse = phongDiffuse(defaultMaterial, lightIntensity, n, uToLight);
#ifdef SHADOW
      } else {
        /* don't make shadow to dark
         * (e.g. the ribs can make a distracting shadow) */
        diffuse = 0.2 * phongDiffuse(defaultMaterial, lightIntensity, n, uToLight);
      }
#endif


    // -----------------
    // Ambient occlusion
#ifdef AMBIENT_OCCLUSION
    initBBS(5.0); // initialize static to achieve more uniform look.
    //initBBS(ray);

    const int ambientOcclusionRays = STEPS/16;
    const int ambientOcclusionSteps = STEPS/4;
    /* Don't look to far. (On average we start in the middle,
     * i.e. only have to go half as far. */
    const float proximityFactor = 0.2;
    float occlusionCount = 0.0;

    for (int i=0; i<ambientOcclusionRays; i++) {
      vec3 aoray = randomUnitVector();
      aoray = -faceforward(aoray, n, aoray);
      if (dot(aoray, uToLight) < 0.0) {
        occlusionCount++;
        continue;
      }
      aoray *= proximityFactor * sqrt(3.0)/float(ambientOcclusionSteps);
      vec3 pos = ray + aoray;
      for (int j=0; j<ambientOcclusionSteps; j++) {
        if (any(lessThan(pos, vec3(0.0))) || any(greaterThan(pos, vec3(1.0)))) {
          break;
        }

        float value = texture3D(uVolume, pos).r;
        if (value > threshold) {
          occlusionCount++;
          break;
        }

        pos += aoray;
      }
    }
    float ambientOccludedLight = 1.0 - occlusionCount/float(ambientOcclusionRays);

#else
    float ambientOccludedLight = 0.8;
#endif
    float intensity = diffuse +
      defaultMaterial.ambientDiffuseColor * ambientOccludedLight;

    return vec4(intensity, specular, labelIndex);
  } else {
    return vec4(0.0);
  }
}

void main() {
  vec3 start = FRONT_COORDS;
  vec3 end = vColor;
  highp vec3 direction = end - start;

  gl_FragColor = iso(uVolume, start, direction, uThreshold);
}
