// fragment shader
//

uniform vec2 uViewportSize;
uniform sampler2D uPreviousStage;

const vec3 specularColor = vec3(1.0, 1.0, 1.0);
const vec3 diffuseColor = vec3(1.0, 1.0, 1.0);

void main() {
  vec2 light = PREVIOUS_FRAG_COLOR.xy;
  float specular = light.y;
  float intensity = light.x;

  vec3 color = diffuseColor * intensity + specularColor * specular;

  gl_FragColor = vec4(color, 1.0);
}
