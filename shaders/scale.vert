// Vertex shader

// vertex.xy -1 or 1 => fullscreen quad
attribute vec2 aVertexPosition;

// scale by that much. i.e. sample that fraction of the uTile texture width and height.
// e.g., when the scale is 2, the texture coordinates will be [0, 0.5]
uniform float uViewportScale;

varying vec2 vTexCoord;

void main(void) {
  gl_Position = vec4(aVertexPosition, -1.0, 1.0);
  vTexCoord = (0.5 + 0.5*aVertexPosition.xy)/uViewportScale; // [0, 1/scale]
}
