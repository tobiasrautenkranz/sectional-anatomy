/*  Sectional Anatomy Viewer
 * 
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

varying vec3 vColor;
uniform sampler2D uFrontCoords;
uniform vec2 uViewportSize;

uniform SAMPLER_3D uVolume;
uniform SAMPLER_3D uLabelMap;

void main() {
  vec3 start = FRONT_COORDS;
  vec3 end = vColor;

  gl_FragColor = vec4(end, 1.0);
}
