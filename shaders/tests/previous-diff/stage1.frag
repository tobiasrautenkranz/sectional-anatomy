// fragment shader
//
varying vec3 vColor;

uniform vec2 uViewportSize;
uniform sampler2D uPreviousStage;
uniform sampler2D uFrontCoords;

void main() {
  vec3 start = FRONT_COORDS;
  vec3 end = vColor;
  vec3 prev = PREVIOUS_FRAG_COLOR.rgb;

  vec3 delta = end - prev;

  gl_FragColor = vec4(abs(delta), 1.0);
}
