/*  Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef GL_EXT_draw_buffers
#extension GL_EXT_draw_buffers : enable
#endif

precision mediump float;
//precision highp float; // using highp (since mediump is hard to test for)
// (see https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/WebGL_best_practices)

// Texture3D Uniforms
uniform vec2 uVolumeTilingSize;
uniform vec3 uVolumeSize;


#ifndef QUALITY
  #define QUALITY 0
#endif

#if 0 == QUALITY
  #define STEPS 32
  #define LOW_QUALITY 1
#elif 1 == QUALITY
  #define STEPS 64
#elif 2 == QUALITY
  #define STEPS 128
  #define HIGH_QUALITY 1
#elif 3 == QUALITY
  #define STEPS 265
  #define HIGH_QUALITY 1
#else
  #error Invalid value for QUALITY
#endif

#if 8 == LABEL_DEPTH
  #define LABEL_MAX 255.0
  float colorToIndex(vec4 color) {
    return color.r * LABEL_MAX;
  }
  vec2 colorToIndexV(vec4 color) {
    return vec2(color.r, 0);
  }
  float indexVToIndex(vec2 index) {
    return index.x * LABEL_MAX;
  }
#elif 16 == LABEL_DEPTH
  #define LABEL_MAX 65535.0
  float colorToIndex(vec4 color) {
    return color.r * 255.0 + floor(color.a * 256.0) * 256.0;
  }
  vec2 colorToIndexV(vec4 color) {
    return vec2(color.r, color.a);
  }
  float indexVToIndex(vec2 index) {
    return index.x * 255.0 + floor(index.y * 256.0) * 256.0;
  }
#else
  #error Unknown LABEL_DEPTH
#endif

// -------------------------
// Random Number Generators
// -------------------------

// FIXME Why?
highp float rand(highp vec2 co){
  return fract(sin(dot(co.xy, vec2(12.9898, 78.233))) * 43758.5453);
}
// http://lumina.svn.sourceforge.net/viewvc/lumina/plugins/noise.js?revision=4&view=markup
highp float rand(highp vec3 co){
  return fract(sin(dot(co, vec3(12.9898, 78.233, 34.897))) * 43758.5453);
}

/* ---------------------
 * Blum Blum Shub
 *
 * http://briansharpe.wordpress.com/2011/10/01/gpu-texture-free-noise/
 * Olano, Marc. "Modified Noise for Evaluation on Graphics Hardware".
 * Proceedings of Graphics Hardware 2005, Eurographics/ACM SIGGRAPH, July 2005.
 * http://www.csee.umbc.edu/~olano/papers/index.html#mNoise
 */
highp float BBSstate;
//use getShaderPrecisionFormat?
// BBSM = p * q  with p,q primes such that: p % 4 = 3 and q % 4 = 3
// highp int max: 2^16, int precision needed up to BBSM^2
//(mediump 2^10 => BBSM=61 TODO GL_FRAGMENT_PRECISION_HIGH)
const highp float BBSM = 11.0 * 19.0;
//const float BBSM = 61.0;
float BBShash(highp float x) {
  return mod(x * x, BBSM);
}
void initBBS(highp float seed) {
  BBSstate = 3.0 + floor(mod(seed, BBSM-4.0));
}
void initBBS(highp vec3 seed) {
  initBBS(floor(BBSM*rand(seed)));
}

float BBS() {
  BBSstate = BBShash(BBSstate);
  return BBSstate / BBSM;
}

vec3 randomUnitVector() {
  const highp float tau = 6.283185307179586; // = 2 pi
  // Lambert cylindrical equal area projection
  //
  // R:
  // library(scatterplot3d)
  // x <- runif(1000,0,2*3.1415926)
  // y <- runif(1000,-1,1)
  // scatterplot3d(sqrt(1-y*y)*cos(x), sqrt(1-y*y)*sin(x),y)
  highp float x = tau * BBS(); // [0,360°]
  highp float y = 2.0 * BBS() - 1.0; // [-1,1]

  highp float rXY = sqrt(1.0 - y*y);
  return vec3(rXY * cos(x), rXY * sin(x), y);
}


vec4 lutRGBA(sampler2D lut, float length, float index) {
  return texture2D(lut, vec2((index + 0.5)/length, 0.5));
}

uniform sampler2D uIntensityLUT;
vec4 intensityToColor(float intensity) {
  // first pixel is 0.5/length etc.; account for that.
  const float lutLength = 256.0;
  return texture2D(uIntensityLUT,
      vec2((1.0-1.0/lutLength) * intensity + 0.5/lutLength, 0.5));
}

//------------------------------
// Access to previous stages
//------------------------------

// gl_FragCoord
//
// By default, gl_FragCoord assumes a lower-left origin for window coordinates
// and assumes pixel centers are located at half-pixel centers. For example,
// the (x, y) location (0.5, 0.5) is returned for the lower-left-most pixel in
// a window.
//
// https://www.opengl.org/sdk/docs/man4/html/gl_FragCoord.xhtml
uniform vec2 uViewportOrigin;
#define FB_TEXTURE_COORDS ((gl_FragCoord.xy - uViewportOrigin) / uViewportSize)
#define FRONT_COORDS texture2D(uFrontCoords, FB_TEXTURE_COORDS).xyz
#define PREVIOUS_FRAG_COLOR texture2D(uPreviousStage, FB_TEXTURE_COORDS)

#define LABEL_FRAG_COORD FB_TEXTURE_COORDS
#define LABEL_PREVIOUS_FRAG_COLOR PREVIOUS_FRAG_COLOR
#define LABEL_FRONT_COORDS FRONT_COORDS


//-------------------------------
// SAMPLER_3D: 3D Texture implementation using sampler2D
// Only needed for WebGL 1
//------------------------------
#define SAMPLER_3D sampler2D

// Note: slice should be an int, but a float is needed for the texture access,
// thus skip the to - from int conversion.
highp vec2 tileCoord(highp float slice) {
  highp vec2 sliceCoord;
  slice += 0.5; // adjust for float imprecision.
  sliceCoord.y = floor(slice / uVolumeTilingSize.x);
  sliceCoord.x = floor(slice - uVolumeTilingSize.x * sliceCoord.y);

  return sliceCoord;
}

float texelCoord(float coord, float textureSize) {
  float texelSize = 1.0/textureSize;
  return (coord - 0.5*texelSize) * textureSize;
}

lowp vec4 texture3DNearest(SAMPLER_3D texture, highp vec3 coord) {
  highp float slice = floor(texelCoord(coord.z, uVolumeSize.z) + 0.5);

  return texture2D(texture, (tileCoord(slice) + coord.xy) / uVolumeTilingSize);
}

lowp vec4 texture3D(SAMPLER_3D texture, highp vec3 coord) {
  vec3 voxelSize = 1.0/uVolumeSize;
  vec3 clampMax = 1.0 - 0.5*voxelSize;
  vec3 clampMin = 0.5*voxelSize;
  coord = clamp(coord, clampMin, clampMax); // clamp to edge;
  // prevents blending with adjacent tiles.

  highp float slice = texelCoord(coord.z, uVolumeSize.z);
  highp float slice1 = floor(slice);
  vec4 sliceColor1 = texture2D(texture, (tileCoord(slice1) + coord.xy) / uVolumeTilingSize);

  highp float slice2 = slice1 + 1.0;
  vec4 sliceColor2 = texture2D(texture, (tileCoord(slice2) + coord.xy) / uVolumeTilingSize);

  return mix(sliceColor1, sliceColor2, fract(slice));
}
#ifdef HIGH_QUALITY
  #define TEXTURE_3D_LUT texture3DLinearLUT
#else
  #define TEXTURE_3D_LUT texture3DNearestLUT
#endif

#ifdef LOW_QUALITY
  #define TEXTURE_3D texture3DNearest
#else
  #define TEXTURE_3D texture3D
#endif

vec4 texture3DNearestLUT(SAMPLER_3D texture, vec3 coord, sampler2D LUT,
    float LUTLength)
{
  float index = colorToIndex(texture3DNearest(texture, coord));
  return lutRGBA(LUT, LUTLength, index);
}

vec4 texture3DLinearLUT(SAMPLER_3D texture, vec3 coord, sampler2D LUT,
    float LUTLength)
{
  vec3 pixelPosition = (uVolumeSize - 1.0)*coord;
  vec3 low = (floor(pixelPosition) + 0.5)/uVolumeSize;
  vec3 high = (ceil(pixelPosition) + 0.5)/uVolumeSize;

  mediump vec4 lowColor = texture3DNearestLUT(texture, low, LUT, LUTLength);

  vec4 color = mix(lowColor,
      texture3DNearestLUT(texture, vec3(high.x, low.yz), LUT, LUTLength),
      fract(pixelPosition.x));
  color += mix(lowColor,
      texture3DNearestLUT(texture, vec3(low.x, high.y, low.z), LUT, LUTLength),
      fract(pixelPosition.y));
  color += mix(lowColor,
      texture3DNearestLUT(texture, vec3(low.xy, high.z), LUT, LUTLength),
      fract(pixelPosition.z));

  return color/3.0;
}
