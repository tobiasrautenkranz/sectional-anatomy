#!/bin/sh
#
# Minify GLSL shader programs
#
# Copyright (C) 2012 - 2016 Tobias Rautenkranz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -eu

if [ "$#" -ne 2 ] ; then
  echo "usage: $0 SHADER_INPUT OUT_FILE" >&2
  exit 1
fi

SHADER="$1"
OUT_FILE="$2"

TMP=$(mktemp || exit 1)

REMOVE_LEADING_WHITE_SPACE='s/^[ \t]*//g'
REMOVE_EMPTY_LINES='/^$/d'
JOIN_LINES=':a;/[;{,(]$/{N;s/\n[ \t]*//;ba}'
FIX_JOINED_DEFINES='s/\([;{(,]\)#/\1\n#/g'
REMOVE_OPERATOR_WHITE_SPACE='s/[ \t]*\([<>!=,/*+-]\)[ \t]*/\1/g'

stripcomments() {
# http://stackoverflow.com/a/13062682
sed 's/a/aA/g;s/__/aB/g;s/#/aC/g' | # escape since e.g. #extension is no recognized
gcc -fpreprocessed -dD -E -P - | # strip comments
sed 's/aC/#/g;s/aB/__/g;s/aA/a/g' # unescape
}

stripcomments < "$SHADER" | sed  \
  -e "$REMOVE_LEADING_WHITE_SPACE" \
  -e "$REMOVE_EMPTY_LINES" \
  -e "$JOIN_LINES" \
  -e "$FIX_JOINED_DEFINES" \
  -e "$REMOVE_OPERATOR_WHITE_SPACE" \
  > "$TMP"

cp "$TMP" "$OUT_FILE"
rm -f -- "$TMP"
