({
    appDir: "../",
    baseUrl: "js/",
    dir: "../../sectionalanatomy-build",

    optimize: "uglify2",
    preserveLicenseComments: false,
    generateSourceMaps: true,

    fileExclusionRegExp: /^bower_components/,

    modules: [
        {
          name: "util/utils"
        },
        {
            name: "studies-main",
            exclude: ["util/utils"]
        },
        {
            name: "viewer-main",
            exclude: ["util/utils"]
        },
        {
          name: "webgl",
          exclude: ["util/utils", "lib/gl-matrix", "viewer-main"]
        }
    ]
})
