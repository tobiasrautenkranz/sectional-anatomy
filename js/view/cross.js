/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "ui/animate"],
  function (utils, animate) {
  "use strict";

  function makeBar(type) {
    var bar = document.createElement("div");
    bar.style.position = "absolute";
    bar.style.top = 0;
    bar.style.left = 0;
    if ("horizontal" === type) {
      bar.className = "cross-h";
    } else if ("vertical" === type) {
      bar.className = "cross-v";
    } else {
      throw new Error("Unknown bar type: " + type);
    }

    return bar;
  }

  var scrollAction = {
    none: 0,
    hbar: 1,
    vbar: 2
  };

  // Draw a cross at the current position in the volume.
  var Cross = function (parentElement) {
    this.action = scrollAction.none;
    var t = this;

    this.hbar = [makeBar("horizontal"), makeBar("horizontal")];
    parentElement.appendChild(this.hbar[0]);
    parentElement.appendChild(this.hbar[1]);

    var hbarMove = function () {
      t.action = t.action | scrollAction.hbar;
    };
    this.hbar[0].addEventListener("mousedown", hbarMove);
    this.hbar[1].addEventListener("mousedown", hbarMove);


    this.vbar = [makeBar("vertical"), makeBar("vertical")];
    parentElement.appendChild(this.vbar[0]);
    parentElement.appendChild(this.vbar[1]);

    var vbarMove = function () {
      t.action = t.action | scrollAction.vbar;
    };
    this.vbar[0].addEventListener("mousedown", vbarMove);
    this.vbar[1].addEventListener("mousedown", vbarMove);


    this.cross = document.createElement("div");
    var s = this.cross.style;
    s.position = "absolute";
    s.height = "2em";
    s.width = "2em";
    s.backgroundColor = "rgba(0,0,0,0)";
    s.cursor = "move";
    s.marginTop = "-1em";
    s.marginLeft = "-1em";
    parentElement.appendChild(this.cross);

    this.cross.addEventListener("mousedown", function () {
      t.action = t.action |
        scrollAction.hbar | scrollAction.vbar;
    });

    this.response = document.createElement("span");
    this.response.className = "response";
    parentElement.appendChild(this.response);

  };

  Cross.prototype.setPosition = function (position) {
    function setBarVisible(bar, visible) {
      for (var i=0; i<bar.length; i++) {
        bar[i].style.display = visible ? "" : "none";
      }
    }

    var parentElement = this.cross.parentNode;
    var parentSize = [parentElement.clientWidth, parentElement.clientHeight];

    var clearSize = [this.cross.clientWidth, this.cross.clientHeight];
    var bars = [this.vbar, this.hbar];
    for (var i=0; i<bars.length; i++) {
      if (position[i] < 0) {
        setBarVisible(bars[i], false);
        clearSize[i] = 0;
      }
      else if (position[i] > parentSize[i]) {
        setBarVisible(bars[i], false);
        clearSize[i] = 0;
      } else {
        setBarVisible(bars[i], true);
      }
    }

    position[0] = utils.clamp(position[0], 0, parentSize[0]);
    position[1] = utils.clamp(position[1], 0, parentSize[1]);

    var areaWidth = this.cross.parentElement.clientWidth;
    var areaHeight = this.cross.parentElement.clientHeight;

    this.hbar[0].style.top = position[1] + "px";
    this.hbar[0].style.width = position[0] - clearSize[0]/2 + "px";
    this.hbar[1].style.top = position[1] + "px";
    this.hbar[1].style.left = position[0] + clearSize[0]/2 + "px";
    this.hbar[1].style.width =
      areaWidth - (position[0] + clearSize[0]/2) + "px";


    this.vbar[0].style.left = position[0] + "px";
    this.vbar[0].style.height = position[1] - clearSize[1]/2 + "px";
    this.vbar[1].style.left = position[0] + "px";
    this.vbar[1].style.top = position[1] + clearSize[1]/2 + "px";
    this.vbar[1].style.height =
      areaHeight - (position[1] + clearSize[1]/2) + "px";

    this.cross.style.left = position[0] + "px";
    this.cross.style.top = position[1] + "px";


    this.response.style.left = position[0] + "px";
    this.response.style.top = position[1] + "px";

    animate.setAnimationProperty(this.response, "");
    if (this.drawResponse) {
      this.drawResponse = false;
      var response = this.response;
      window.setTimeout(function () {
        response.style.visibility = "hidden";
        animate.setAnimationProperty(response, "response 0.5s");
      }, 0);
    }
  };

  Cross.action = scrollAction;

  Cross.prototype.dragEnd = function () {
    this.action = Cross.action.none;
  };


  return Cross;

  });
