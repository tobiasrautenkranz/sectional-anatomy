/* Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["./slice-view", "util/utils", "./view-utils",
    "./redraw", "view/mouse-action",
     "volume/ppvolume", "volume/axis-numbers", "lib/gl-matrix",
    "./view-basis", "./axis-labels", "./cross", "./overscroll"],
  function (slice, utils, viewUtils, draw, mouseAction, pp, AxisNumbers, glm, viewBasis,
    axisLabels, Cross, Overscroll) {
  "use strict";

  var vec3 = glm.vec3;
  var vec2 = glm.vec2;

  // 2D view of a volume
  var View = function (parentElement, index, options) {
    this.parentElement = parentElement;

    options = options || {};
    this.viewer = options.viewer;
    this.interactive = (options.interactive === false) ?
      false : true;  // undefined => true;

    this.overlayDiv = document.createElement("div");
    this.overlayDiv.className = "overlay";
    this.parentElement.appendChild(this.overlayDiv);

    this.title = document.createElement("p");
    this.title.className = "view-title";
    this.overlayDiv.appendChild(this.title);

    this.transformation =  {
      scale: 1,
      translation: [0, 0] // x and y translation ratio.
                          // e.g.: 0: no translation; 1: move width/height pixels
    };


    if (this.interactive) {
      this.sliceIndex = document.createElement("div");
      this.sliceIndex.className = "slice-index";
      this.overlayDiv.appendChild(this.sliceIndex);

      this.cross = new Cross(this.overlayDiv);

      this.axisLabels = axisLabels.make(this.overlayDiv);

      var view = this;
      var scroll = function (e) {
        var clicks = Math.floor(utils.mouseWheelClicks(e));
        if (e.ctrlKey || e.shiftKey) {
          var index = view.mouseCoords2Volume(e.clientX, e.clientY);
          view.setZoom(view.transformation.scale + clicks/-4,
              [index[0], index[1]]);
        } else {
          viewUtils.scrollIndex(view.index, view.getVolume(), -clicks);
        }
        draw.requestRedraw();

        e.preventDefault();
      };
      // attach to parent element because IE.
      this.overlayDiv.parentElement.addEventListener(utils.mouseWheelEvent,
          scroll);

      this.overscroll = new Overscroll(function (delta) {
        viewUtils.scrollIndex(this.index, this.getVolume(), delta / 5);
      }.bind(this));
    }

    this.sliceViews = [];
    this.index = index;
  };

  View.prototype.setVolumes = function (volumes) {
    var parentElement = this.parentElement;

    this.sliceViews.map(function(v) { v.remove(); });
    parentElement.removeChild(this.overlayDiv);

    this.sliceViews = volumes.map(function(volume) {
      var v = new slice.SliceView(parentElement);
      v.setVolume(volume);
      return v;
    });

    for (var i=1; i<this.sliceViews.length; i++) {
      this.sliceViews[i].canvas.style.opacity = "0.5";
    }

    parentElement.appendChild(this.overlayDiv);
    if (this.axisLabels) {
      this.axisLabels.update(this.getVolume());
      this.axisLabels.setVisible(true);
    }
  };

  var maxZoom = 5;
  View.prototype.setZoom = function (zoom, center) {
    var size = this.sliceViews[0].volume.getSize();
    var zoomOrigin = utils.map(function (x) { return x/2;}, size);

    var scale = this.transformation.scale;
    var oldPos = utils.map(function (center, origin, translation, size) {
      return (center - origin - translation*size) * scale;
    }, center, zoomOrigin, this.transformation.translation, size);

    scale = utils.clamp(zoom, 1, maxZoom);
    this.transformation.scale = scale;
    if (center) {
      // center has to remain invariant, thus: oldPos == newPos
      // solving for offset:
      this.transformation.translation =
        utils.map(function (oldPos, center, origin, size) {
          return (oldPos/scale - center + origin) / -size;
        }, oldPos, center, zoomOrigin, size);
    }
    this.clampTranslation();

    for (var i=0; i<this.sliceViews.length; i++) {
      this.sliceViews[i].setMagnification(this.transformation.scale);
    }
    if (this.linkedView) {
      var sliceViews = this.linkedView.sliceViews;
      for (i=0; i<sliceViews.length; i++) {
        sliceViews[i].setMagnification(this.transformation.scale);
      }
    }
  };

  View.prototype.clampTranslation = function () {
    var scale = this.transformation.scale;
    this.transformation.translation =
      utils.map(function (x) {
        return utils.clamp(x, -0.5+0.5/scale, 0.5-0.5/scale);
      }, this.transformation.translation);
  };

  View.prototype.onZoom = function (scale, center, translation, scaleAdditive) {
    function updateTranslation(transformation, translation, scale) {
      transformation.translation[0] -= translation[0] * scale[0];
      transformation.translation[1] -= translation[1] * scale[1];
    }

    var size = [this.parentElement.clientWidth, this.parentElement.clientHeight];
    var oldScale = this.transformation.scale;
    var voxelSign = utils.map(utils.sign,
        viewBasis.getScaleTransform(this.getVolume()));
    var d = vec2.fromValues(voxelSign[0] / size[0] / oldScale,
                            voxelSign[1] / size[1] / oldScale);

    if (scale) {
      if (scaleAdditive) {
        this.setZoom(this.transformation.scale + scale, center);
      } else {
        this.setZoom(this.transformation.scale * scale, center);
      }

      updateTranslation(this.transformation, translation, d);
    } else {
      updateTranslation(this.transformation, translation, d);
      this.clampTranslation();
    }

    this.didScroll = true;

    draw.requestRedraw();
  };

  View.prototype.linkView = function (dependantView) {
    dependantView.transformation = this.transformation;
    this.linkedView = dependantView;
    if (!dependantView.linkedView) {
      dependantView.linkedView = this;
    }
  };

  View.prototype.getNativeSize = function () {
    if (this.sliceViews.length > 0) {
      return this.sliceViews[0].getNativeSize();
    } else {
      // throw new Error("View has no volumes.");
      return [0,0];
    }
  };

  View.prototype.onscroll = function (mouse, gv) {
    if (0 === this.sliceViews.length) {
      return;
    }
    var volume = this.getVolume();
    if (!this.didScroll) {
      this.scrollLength = (this.scrollLength || 0) +
        mouse.delta[0]*mouse.delta[0] + mouse.delta[1]*mouse.delta[1];
    }
    if (this.scrollLength > 2) {
      this.didScroll = true;
    }

    var index = this.mouseCoords2Volume(mouse.clientX, mouse.clientY);

    if (this.cross.action) {
      var coords = volume.getBasis().to(vec3.create(), this.index);

      if (Cross.action.hbar & this.cross.action) {
        coords[1] = index[1];
      }
      if (Cross.action.vbar & this.cross.action) {
        coords[0] = index[0];
      }

      var volPosition = this.volumeCoords2World(coords);
      this.index[0] = volPosition[0];
      this.index[1] = volPosition[1];
      this.index[2] = volPosition[2];
      viewUtils.clampIndex(volume, this.index, this.index);
    } else {
      if (mouseAction.isChangeWindow(mouse)) {
        // FIXME update w/c slider in the view tab.
        if (volume.isLabelMap()) {
          if (!this.linkedView) {
            // no applicable view found.
            return;
          }
          volume = this.linkedView.getVolume();
          utils.assert(!volume.isLabelMap());
        }

        mouseAction.setCursor("changeWindow");
        var stepSize = mouseAction.stepSize(200);
        var dWidth  =  mouse.delta[0]/stepSize;
        var dCenter = -mouse.delta[1]/stepSize;
        if (volume.getLUT().getWidth() < 0) {
          dWidth *= -1;
          dCenter *= -1;
        }

        var lut = gv.getSelectedView().getVolume().getLUT();
        var center = lut.getCenter() + dCenter;
        var width = lut.getWidth() + dWidth;
        center = utils.clamp(center, 0, 255);
        if (lut.getWidth() > 0) {
          width = utils.clamp(width, 1, 255);
        } else {
          width = utils.clamp(width, -255, -1);
        }
        lut.setWindow(center, width);
      } else if (mouseAction.isZoom(mouse)) {
        mouseAction.setCursor("zoom");
        var downIndex = this.mouseCoords2Volume(mouse.mouseDown.client[0],
                                                mouse.mouseDown.client[1]);
        this.onZoom(mouse.delta[1]/mouseAction.stepSize(7),
            [downIndex[0], downIndex[1]], [0,0], true);
      } else if (mouseAction.isPan(mouse)) {
        mouseAction.setCursor("pan");
        this.onZoom(null, null, mouseDelta(mouse.delta));
      } else { // scroll through slices
        viewUtils.scrollIndex(this.index, volume,
            mouse.delta[1]/mouseAction.stepSize(110));

        this.overscroll.update(mouse);

        var sliceIndex = this.getSliceIndex();
        if (0 === sliceIndex) {
          mouseAction.setCursor("scrollDown");
        } else if (volume.getSize()[2]-1 === sliceIndex) {
          mouseAction.setCursor("scrollUp");
        } else {
          mouseAction.setCursor("scroll");
        }
      }
      draw.requestRedraw();
    }
  };

  View.prototype.volumeCoords2World = function (coords) {
      var volume = this.getVolume();

      var vec = vec3.create();
      volume.getBasis().to(vec, this.index);
      vec[0] = coords[0];
      vec[1] = coords[1];

      var volPosition = volume.getBasis().from(vec, vec);

      return volPosition;
  };

  View.prototype.scrollEnd = function (mouseCoords) {
    if (0 === this.sliceViews.length) {
      return;
    }

    this.overscroll.end();

    this.cross.dragEnd();
    mouseAction.setCursor();

    if (!this.didScroll) {
      var coords = this.mouseCoords2Volume(mouseCoords.clientX, mouseCoords.clientY);
      var labelmap;
      if (this.getVolume().isLabelMap()) {
        labelmap = this.getVolume();
        if (!labelmap.getAnatomyId) {
          labelmap = labelmap.getSourceVolume();
        }
      } else {
        labelmap = this.getVolume().getLabelMap();
      }

      var volPosition = this.volumeCoords2World(coords);
      vec3.copy(this.index, volPosition);

      var labelMapIndex = labelmap.getBasis().to(vec3.create(), this.index);
      var viewer = this.viewer;
      labelmap.getAnatomyId(function (anatomyId) {
        viewer.anatomyTools.selectAnatomy(anatomyId);
      }, utils.roundArray(labelMapIndex, labelMapIndex));

      this.drawResponse = true;
    }

    this.didScroll = false;
    this.scrollLength = 0;
  };

  View.prototype.draw = function () {
    var completed = true;
    var offset = this.getImageOffset();
    for (var i=0; i<this.sliceViews.length; i++) {
      var sliceView = this.sliceViews[i];

      completed &= sliceView.draw(offset, this.getSliceIndex());
    }

    if (this.interactive && this.sliceViews.length > 0) {
      // set cross last due to canvas size change from zoom
      var crossPosition = volumeCoords2Pixels(this.index,
          this.sliceViews[0].canvas, this.sliceViews[0].volume, this);
      this.cross.setPosition(crossPosition);

      this.sliceIndex.innerHTML =
        "" + (this.getSliceIndex() + 1) + " / " + this.getVolume().getSize()[2];
    }
    return completed;
  };

  View.prototype.resize = function (zoomFactor) {
    if (this.sliceViews.length > 0) {
      for (var i=0; i< this.sliceViews.length; i++) {
        this.sliceViews[i].fit(zoomFactor);
      }
      if (this.interactive) {
        var crossPosition = volumeCoords2Pixels(this.index,
            this.sliceViews[0].canvas, this.sliceViews[0].volume, this);
        this.cross.setPosition(crossPosition);
      }
    }
  };

  View.prototype.getVolume = function () {
    return this.sliceViews[0].volume;
  };

  View.prototype.keydown = function (e) {
    function reformatVolumes(view, axis) {
      var deps = view.viewer.gridView.getDependantViews(view, true);
      for (var i=0; i<deps.length; i++) {
        deps[i].setVolumes([pp.reformatVolume(deps[i].getVolume(), axis)]);
      }
      view.setVolumes([pp.reformatVolume(view.getVolume(), axis)]);
    }
    function switchVolumes(view, direction) {
      var volume = view.getVolume();
      var index = view.viewer.study.volumes.indexOf(volume.getSourceVolume());

      if (index != -1) {
        var deps = view.viewer.gridView.getDependantViews(view, true);
        // ignore labelmap (FIXME move labelmap)
        var length = view.viewer.study.volumes.length-1;
        index = (index+direction+length) % length;

        deps.push(view);
        for (var i=0; i<deps.length; i++) {
          var v = deps[i].getVolume();
          if (v.getAxisNumber() && v.getSourceVolume().type !== "labelmap") {
            var rVol = pp.reformatVolume(view.viewer.study.volumes[index],
                v.getAxisNumber());
            deps[i].setVolumes([rVol]);
          }
        }
      }
    }
    if (!this.sliceViews || !this.sliceViews.length) {
      return;
    }

    if (e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
      return;
    }

    var handleKey = true;
    switch(e.keyCode) {
      case 38: // up
      case 39: // right
      case 78: // N // pinnacle
      case 98: // num2 // siemens
      case 74: // J // VIM
        viewUtils.scrollIndex(this.index, this.getVolume(), -1);
        e.preventDefault();
        break;
      case 40: // down
      case 37: // left
      case 80: // P
      case 97: // num1
      case 75: // K
        viewUtils.scrollIndex(this.index, this.getVolume(), 1);
        e.preventDefault();
        break;
      case 33: // pageUp
        viewUtils.scrollIndex(this.index, this.getVolume(), -10);
        e.preventDefault();
        break;
      case 34: // pageDown
        viewUtils.scrollIndex(this.index, this.getVolume(), 10);
        e.preventDefault();
        break;

      case 72: // H
      case 100: // num4
        switchVolumes(this, -1);
        break;
      case 76: // L
      case 101: // num5
        switchVolumes(this, 1);
        break;

      case 84: // T
      case 65: // A
        reformatVolumes(this, AxisNumbers.tra);
        break;
      case 67: // C
        reformatVolumes(this, AxisNumbers.cor);
        break;
      case 83: // S
        reformatVolumes(this, AxisNumbers.sag);
        break;

      case 79: // O
        var axis = this.getVolume().getSourceVolume().getAxisNumber();
        if (AxisNumbers.tra === axis) {  // oblique is only supported for tra
          var direction = 0;
          if (this.getVolume() instanceof pp.ObliqueMPRVolume &&
              this.getVolume().direction === 0) {
            direction = 1;
          }
          var deps = this.viewer.gridView.getDependantViews(this, true);
          for (var i=0; i<deps.length; i++) {
            deps[i].setVolumes(
                [new pp.ObliqueMPRVolume(deps[i].getVolume().getSourceVolume(),
                  direction)]);
          }
          this.setVolumes(
              [new pp.ObliqueMPRVolume(this.getVolume().getSourceVolume(),
                direction)]);
        }
        break;

      default:
        handleKey = false;
        break;
    }
    viewUtils.clampIndex(this.getVolume(), this.index, this.index);

    if (handleKey) {
      draw.requestRedraw();
    }
  };

  /** Returns the coordinates of index in CSS pixels on the screen. */
  var volumeCoords2Pixels = function(index, targetCanvas, targetVolume, view) {
    // the inverse of mouseCoords2Volume()
    var canvasPosition = viewUtils.getSliceViewPosition(targetCanvas);

    var voxelSign = viewBasis.getScaleTransform(view.getVolume()).map(utils.sign);
    var volumeIndex = targetVolume.getBasis().to(vec3.create(), index);
    var parentOffset = utils.getAbsoluteOffset(targetCanvas.parentElement);

    var coords = vec2.create();
    var offset = view.getImageOffset();
    for (var i=0; i<2; i++) {
      var cindex = volumeIndex[i] - offset[i] + voxelSign[i]*0.5;
      coords[i] = cindex*canvasPosition.zoom[i] + canvasPosition.origin[i];

      // for positioning relative to parent.
      coords[i] -= parentOffset[i];
    }
    return coords;
  };

  /** Returns the offset in pixels where the image is placed on the canvas. */
  View.prototype.getImageOffset = function () {
    // the user supplied offset
    var sliceView = this.sliceViews[0];
    var size = sliceView.volume.getSize();
    var offset = [size[0] * this.transformation.translation[0],
                  size[1] * this.transformation.translation[1]];

    // shift the image to the center of the canvas
    offset[0] += (size[0] - sliceView.canvas.width)/2;
    offset[1] += (size[1] - sliceView.canvas.height)/2;

    return offset.map(Math.round);
  };

  function mouseDelta(delta) {
    // FIXME left <-> right translation is not 1:1
    return [delta[0], delta[1]];
  }

  View.prototype.mouseCoords2Volume = function (x, y) {
    var offset = this.getImageOffset();
    var canvasPosition = viewUtils.getSliceViewPosition(this.sliceViews[0].canvas);

    var mouseCoords = vec2.fromValues(x, y);
    var coords = vec2.create();
    var voxelSize = viewBasis.getScaleTransform(this.getVolume());
    for (var i=0; i<2; i++) {
      coords[i] = (mouseCoords[i] - canvasPosition.origin[i]) /
        canvasPosition.zoom[i];
      // consider zoom/offset
      // zoom is included in CSS transform and thus already handled.
      coords[i] += offset[i] - Math.abs(voxelSize[i]) * 0.5;
    }

    return coords;
  };

  View.prototype.getSliceIndex = function () {
    return viewUtils.coordinatesToSliceIndex(this.index, this.getVolume());
  };

  return View;
  });

