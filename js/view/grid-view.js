/* Sectional Anatomy Viewer
 *
 *  Copyright (C) 2012 - 2018 Tobias Rautenkranz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["./view", "./redraw", "util/utils", "volume/volume"],
  function (View, draw, utils, vol) {
    "use strict";

    // A grid of multiple different views
    var GridView = function (parentElement, viewer) {
      this.table = document.createElement("table");
      this.table.style.width = "100%";
      this.table.style.height = "100%";
      this.table.style.borderSpacing = "0px";
      this.table.tabindex = "0"; //focusable for key input

      this.table.style.touchAction = "none";

      parentElement.appendChild(this.table);
      this.parentElement = parentElement;

      this.index = [0,0,0];
      this.viewer = viewer;
    };

    GridView.prototype.getSelectedView = function () {
      var view = this.activeView;
      if (view.getVolume().getSourceVolume().type === "labelmap") {
        var lblVolume = view.getVolume();

        for (var r=0; r<this.cells.length; r++) {
          for (var c=0; c<this.cells[r].length; c++) {
            var volume = this.cells[r][c].getVolume();
            if (volume.getSourceVolume().type !== "labelmap" &&
                vol.isSameGeometry(lblVolume, this.cells[r][c].getVolume())) {
              return this.cells[r][c];
            }
          }
        }
        throw new Error("no view found for labelmap view: " + view);
      } else {
        return view;
      }
    };

    // FIXME ugly hack
    GridView.prototype.getDependantViews = function (view, noFilter) {
      var deps = [];
      var orgVolume = view.getVolume();
      for (var r=0; r<this.cells.length; r++) {
        for (var c=0; c<this.cells[r].length; c++) {
          var volume = this.cells[r][c].getVolume();
          if ((noFilter || volume.getSourceVolume().type === "labelmap") &&
              vol.isSameGeometry(orgVolume, volume)) {
            deps.push(this.cells[r][c]);
          }
        }
      }

      return deps;
    };

    GridView.prototype.setupEventHandlers = function () {
      var gridView = this;

      window.addEventListener("resize", function () {gridView.resize();});

      var mouseDown;
      var lastMouse;

      var onscroll = function (e, buttons) {
        gridView.viewer.anatomyInfo.show(false);
        if (mouseDown) {
          var mouse = {
            mouseDown: mouseDown,
            downButtons: buttons,
            buttons: e.buttons,
            which: e.which,
            delta: [e.screenX - lastMouse.screen[0],
                    e.screenY - lastMouse.screen[1]],

            clientX: e.clientX, clientY: e.clientY,
            screenX: e.screenX, screenY: e.screenY,

            altKey: e.altKey,
            shiftKey: e.shiftKey,
            ctrlKey: e.ctrlKey
          };
          if (gridView.activeView.interactive) {
            gridView.activeView.onscroll(mouse, gridView);
          }
          lastMouse.screen = [e.screenX, e.screenY];
          lastMouse.client = [e.clientX, e.clientY];
        }
      };

      // -- mouse ----

      var mouseMove = function (e) {
        draw.begin();
        onscroll(e, mouseDown.buttons);

        e.preventDefault();
      };

      var mouseUp = function (e) {
        window.removeEventListener("mousemove", mouseMove);
        window.removeEventListener("mouseup", mouseUp);

        var mouse = {
          clientX: e.clientX,
          clientY: e.clientY,
          delta: [e.screenX - mouseDown.screen[0],
                  e.screenY - mouseDown.screen[1]]
        };
        if (gridView.activeView.interactive &&
            gridView.activeView.scrollEnd) {
            gridView.activeView.scrollEnd(mouse);
        }
        mouseDown = null;
        draw.end();

        e.preventDefault();
      }.bind(this);

      var onMouseDown = function (e) {
        mouseDown = mouseDown || {};
        mouseDown.buttons = e.buttons;
        mouseDown.screen = [e.screenX, e.screenY];
        mouseDown.client = [e.screenX, e.screenY];
        mouseDown.isRightClick = utils.isRightClick(e);
        lastMouse = mouseDown;

        if (gridView.activeView.interactive && gridView.activeView.onmousedown) {
          gridView.activeView.onmousedown(e);
        }

        window.addEventListener("mousemove", mouseMove);
        window.addEventListener("mouseup", mouseUp);

        e.preventDefault();
      }.bind(this);

      this.table.addEventListener("mousedown", onMouseDown);

      this.table.addEventListener("contextmenu", function (e) {
        e.preventDefault();
      });

      // -- touch -------------

      var lastDistance;
      var lastCenter;
      var touchMove = function (e) {
        gridView.viewer.anatomyInfo.show(false);
        var numTouches = e.touches.length;
        switch (numTouches) {
          case 1:
            draw.begin();

            mouseDown.zoom = false;

            onscroll(e.touches[0], false);
            break;

          case 2:
            draw.begin();

            mouseDown.zoom = true;

            var touch1 = e.touches[0];
            var touch2 = e.touches[1];

            var distance =
              Math.sqrt(Math.pow(touch1.screenX - touch2.screenX, 2) +
                        Math.pow(touch1.screenY - touch2.screenY, 2));

            // center(a,b) = a + 1/2* (b-a) = 1/2 a + 1/2 b
            var center =  [0.5 * (touch1.screenX + touch2.screenX),
                           0.5 * (touch1.screenY + touch2.screenY)];

            if (!lastDistance) {
              lastDistance = distance;
              lastCenter = center;
            }

            var scaleUnderscaleFactor = 1.0;
            var scale = 1 + scaleUnderscaleFactor * (distance/lastDistance - 1);
            var offset = [center[0] - lastCenter[0],
            center[1] - lastCenter[1]];

            lastDistance = distance;
            lastCenter = center;

            var view = gridView.activeView;
            if (view.interactive && view.onZoom) {
              var index;
              if (view.mouseCoords2Volume) {
                index = view.mouseCoords2Volume(center[0], center[1]);
              } else {
                index = center;
              }
              view.onZoom(scale, index, offset);
            }

            break;
        }
        e.preventDefault();
      };

      var touchEnd = function (e) {
        window.removeEventListener("touchmove", touchMove);
        window.removeEventListener("touchend", touchEnd);
        if (mouseDown) {
          var mouse;
          if (e.changedTouches.length > 0) {
            var t = e.changedTouches[0];
            mouse = {
              clientX: t.clientX,
              clientY: t.clientY,
              delta: [t.screenX - mouseDown.screen[0],
                      t.screenY - mouseDown.screen[1]]
            };
          } else {
            if (utils.debug) console.log("no changed touches!!");
            mouse = {
              delta:  [mouseDown.screen[0] - lastMouse.screen[0],
                       mouseDown.screen[1] - lastMouse.screen[1]]
            };
          }

          if (gridView.activeView.interactive && gridView.activeView.scrollEnd &&
              !mouseDown.zoom) {
            gridView.activeView.scrollEnd(mouse);
          }
          mouseDown = null;

          draw.end();
        }
        lastDistance = null;
        lastCenter = null;
      }.bind(this);

      var touchStart =  function (e) {
        gridView.viewer.anatomyInfo.show(false);
        if (e.changedTouches.length >= 1) {
          mouseDown = {};
          mouseDown.screen = [e.changedTouches[0].screenX,
                              e.changedTouches[0].screenY];
        }
        lastMouse = mouseDown;
        window.addEventListener("touchmove", touchMove);
        window.addEventListener("touchend", touchEnd);
      }.bind(this);

      this.table.addEventListener("touchstart", touchStart);
    };

    GridView.prototype.setSize =
      function (rows, columns, options, ViewConstructor) {
        function mkSetActiveView(gridView, view) {
          return function (e) {gridView.activeView = view; };
        }
        this.table.innerHTML = ""; // remove all
        ViewConstructor = ViewConstructor || View;

        options = options || {};
        options.viewer = this.viewer;

        this.cells = new Array(rows);
        this.tds = new Array(rows);
        var c,r;

        var divs = new Array(rows*columns);

        for (r=0; r<rows; r++) {
          this.cells[r] = new Array(columns);
          this.tds[r] = new Array(columns);
          var row = document.createElement("tr");
          row.style.width = "100%";
          row.style.height = 100/rows + "%";
          this.table.appendChild(row);
          for (c=0; c<columns; c++) {
            var cell = document.createElement("td");
            cell.style.width = 100/columns + "%";
            var tdHack = document.createElement("div");
            tdHack.style.position = "relative";
            tdHack.style.width = "100%";
            tdHack.style.height = "100%";
            divs[r*columns + c] = tdHack;
            cell.appendChild(tdHack);
            this.tds[r][c] = cell;
            row.appendChild(cell);
          }
        }

        for (r=0; r<rows; r++) {
          for (c=0; c<columns; c++) {
            var div = divs[r*columns + c];
            var view = new ViewConstructor(div, this.index, options);
            var setActive = mkSetActiveView(this, view);
            // use div not overlaydiv because IE
            div.addEventListener("mousedown", setActive);
            div.addEventListener("touchstart", setActive);
            this.cells[r][c] = view;
          }
        }

        this.activeView = this.cells[0][0];
      };

    GridView.prototype.getSize = function () {
      return [this.cells.length, this.cells[0].length];
    };

    GridView.prototype.resize = function () {
      var zoomFactor = this.adjustCellSize();
      for (var r=0; r<this.cells.length; r++) {
        for (var c=0; c<this.cells[r].length; c++) {
          this.cells[r][c].resize(zoomFactor);
        }
      }
    };

    GridView.prototype.adjustCellSize = function () {
      if (this.cells.length === 0) {
        return 0;
      }
      var length = this.cells[0].map(function (cell) {
        return Math.abs(cell.getNativeSize()[0]);
      }).reduce(function(a,b) {return a+b;});


      var maxHeight = 0;
      for (var r=0; r<this.tds.length; r++) {
        for (var c=0; c<this.tds[r].length; c++) {
          var td = this.tds[r][c];
          var cell = this.cells[r][c];
          td.style.width = cell.getNativeSize()[0]/length*100 + "%";
          td.style.height = 100/this.cells.length + "%";

          maxHeight = Math.max(maxHeight, cell.getNativeSize()[1]);
        }
      }

      var zoomFactorWidth = this.table.clientWidth / length;
      var zoomFactorHeight = this.table.clientHeight /
        this.cells.length / maxHeight;
      return Math.min(zoomFactorWidth, zoomFactorHeight);
    };

    GridView.prototype.getView = function (row, column) {
      return this.cells[row][column];
    };

    GridView.prototype.draw = function (highQuality) {
      var completed = true;
      for (var r=0; r<this.cells.length; r++) {
        for (var c=0; c<this.cells[r].length; c++) {
          if (this.cells[r][c]) {
            completed = this.cells[r][c].draw(highQuality) && completed;
          }
        }
      }
      return completed;
    };

    return GridView;
  });
