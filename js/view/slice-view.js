/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "ui/transform", "./view-basis"],
  function (utils, transform, viewBasis) {
    "use strict";

    // 2D view of a slice of the volume
    var SliceView = function (parentElement) {
      this.canvas = document.createElement("canvas");
      this.context = this.canvas.getContext("2d",
          {alpha: true /* for label highlighting (default)*/});

      // drawing should be 1:1, thus theoretically not needed. (maybe faster)
      utils.setContext2DImageSmoothing(this.context, false);

      this.canvas.style.position = "absolute";
      this.canvas.style.cssFloat = "left";
      this.canvas.style.pointerEvents = "none";

      parentElement.appendChild(this.canvas);
    };

    SliceView.prototype.setVolume = function (volume) {
      this.volume = volume;
      this.setMagnification(1);
      this.fit();
    };


    SliceView.prototype.fit = function (zoomFactor) {
      var voxelSize = viewBasis.getScaleTransform(this.volume);
      var zoomX, zoomY;

      var size = this.volume.getSize();

      if (zoomFactor) {
        //FIXME
        zoomX = zoomFactor * voxelSize[0];
        zoomY = zoomFactor * voxelSize[1];
      } else {
        var pWidth = this.canvas.parentNode.clientWidth;
        var pHeight = this.canvas.parentNode.clientHeight;

        if (0 === pWidth || 0 === pHeight) {
          pWidth = this.canvas.parentNode.parentNode.clientWidth;
          pHeight = this.canvas.parentNode.parentNode.clientHeight;
        }

        var aspectRatio = Math.abs(size[0]/size[1]*voxelSize[0]/voxelSize[1]);
        var parentAspectRatio = pWidth/pHeight;

        if (parentAspectRatio > aspectRatio) {
          // e.g. 16:9 > 4:3
          zoomY = pHeight/this.canvas.height;
          if (voxelSize[1] < 0)  {
            zoomY = -zoomY;
          }
          zoomX = zoomY * voxelSize[0]/voxelSize[1];

          this.canvas.width = Math.max(1, pWidth/Math.abs(zoomX));
        } else {
          zoomX = pWidth/this.canvas.width;
          if (voxelSize[0] < 0)  {
            zoomX = -zoomX;
          }
          zoomY = zoomX * voxelSize[1]/voxelSize[0];

          this.canvas.height = Math.max(1, pHeight/Math.abs(zoomY));
        }
      }
      // use transform instead of canvas.syle.width to allow for negative zoom
      // value (flip image)
      transform.scaleElement(this.canvas, [zoomX, zoomY]);

      var centerX = this.canvas.parentNode.clientWidth / 2;
      var centerY = this.canvas.parentNode.clientHeight / 2;
      this.canvas.style.left = centerX - this.canvas.width / 2 + "px";
      this.canvas.style.top  = centerY - this.canvas.height / 2 + "px";
    };

    SliceView.prototype.getNativeSize = function () {
      var voxelSize = this.volume.getVoxelSize();
      return [this.volume.getSize()[0]*voxelSize[0],
             this.volume.getSize()[1]*voxelSize[1]].map(Math.abs);
    };

    SliceView.prototype.remove = function () {
      if (this.canvas.parentNode) {
        this.canvas.parentNode.removeChild(this.canvas);
      }
      this.canvas = undefined;
      this.context = undefined;
      this.volume = undefined;
    };

    function drawLoading(context, maxWidth) {
      context.textAlign = "center";
      context.font = "bold 25px sans-serif";
      context.textBaseline = "middle";

      context.lineWidth = 3;
      context.strokeText("Loading...", 0, 0, maxWidth);

      context.fillStyle = "#ddd";
      context.fillText("Loading...", 0, 0, maxWidth);
    }

    // draw slice at index with offset.
    //
    // The offset is the translation in pixels from the canvas origin
    // to the slice center. There is a 1 to 1 mapping from slice pixels to
    // canvas pixels. The zoom is achieved by setting the canvas width and height
    // while keeping its CSS dimensions unchanged.
    SliceView.prototype.draw = function (offset, sliceIndex) {
      var slice = this.volume.getSlice(null, sliceIndex);

      if (0 === slice.length) {
        // slice not yet loaded
        // Draw loading... over the last drawn image

        var context = this.context;
        var width = this.canvas.width;
        var height = this.canvas.height;

        // compensate for CSS transform and center
        context.translate(width/2, height/2);
        var zoom = transform.getElementZoom(this.canvas);
        context.scale(utils.sign(zoom[0]), utils.sign(zoom[1]));

        drawLoading(context, width/3);

        context.setTransform(1, 0, 0, 1, 0, 0);

        return false;
      }

      var size = this.volume.getSize();

      var imageData = createImageData(this.context, size[0], size[1]);

      this.volume.getLUT().toPixels(slice, imageData.data);
      this.context.putImageData(imageData,
          // set the dirty rectangle such that it covers the whole canvas.
          // Such that we we draw a canvas sized rectangle of the imageData with
          // the given offset covering the whole canvas.
          -offset[0], -offset[1], // output origin
           offset[0], offset[1], // dirty position (relative to output origin)
          this.canvas.width, this.canvas.height);

      return this.volume.hasCompleteSlice(sliceIndex);
    };

    SliceView.prototype.setMagnification = function (zoom) {
      this.canvas.width = this.volume.getSize()[0]/zoom;
      this.canvas.height = this.volume.getSize()[1]/zoom;
      this.zoom = zoom;
      if (zoom > 1) {
        this.fit();
      } else {
        this.fit();
      }
    };

    var imageData = null;
    /** returns a possibly cached image data object */
    function createImageData(context, width, height) {
      if (null === imageData ||
          imageData.width !== width || imageData.height !== height) {
        imageData = context.createImageData(width, height);
      }
      return imageData;
    }

    return {
      SliceView: SliceView
    };

  });
