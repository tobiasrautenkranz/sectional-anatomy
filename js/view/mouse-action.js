/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
define(["util/utils"],
  function (utils) {
  "use strict";

  var buttons = {
    left: 1,
    right: 2,
    middle: 4,
  };

  function mouseButton(e) {
    // buttons pressed on mousedown event.
    // workaround for brocken e.buttons on mousemove
    if (e.downButtons !== undefined) {
      return e.downButtons;
    }
    if (e.buttons !== undefined) {
      return e.buttons;
    }
    if (e.which) {
      // fallback Webkit
      // No support for multiple buttons
      switch(e.which) {
        case 1:
          return buttons.left;
        case 2:
          return buttons.middle;
        case 3:
          return buttons.right;
      }
    }

    return 0;
  }

  var cursors = {
    changeWindow: "crosshair",
    zoom: "row-resize",
    pan: "move",
    scroll: "ns-resize",
    scrollUp: "n-resize",
    scrollDown: "s-resize"
  };

  return {
    isChangeWindow: function (mouse) {
      var button = mouseButton(mouse);
      if (button) {
        return buttons.middle === button ||
          (mouse.altKey && buttons.left === button) ||
          (mouse.metaKey && buttons.left === button);
      }

      return false;
    },

    isZoom: function (mouse) {
      var button = mouseButton(mouse);
      if (button) {
        return buttons.right === button || (mouse.shiftKey && buttons.left === button);
      }

      return false;
    },

    isPan: function (mouse) {
      var button = mouseButton(mouse);
      if (button) {
        return (buttons.left + buttons.right) == button ||
          mouse.ctrlKey && buttons.left === button;
      }

      return false;
    },

    stepSize: function (factor) {
      var screenHeight = window.innerHeight;
      return Math.max(5.0, screenHeight/factor);
    },

    setCursor: function (action) {
      var element = document.documentElement;
      if (!action) {
        element.style.cursor = "";
      } else {
        element.style.cursor = cursors[action];
      }
    }
  };

  });
