/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["ui/animate", "volume/volume", "lib/gl-matrix", "./view-basis"],
  function (animate, volUtils, glm, viewBasis) {
  "use strict";

  var vec3 = glm.vec3;

  // add axis labels to a element.
  function makeAxisLabels(parentElement) {
    var xlabel = document.createElement("p");
    xlabel.style.position = "absolute";
    xlabel.style.right = "0";
    xlabel.style.top = "50%";
    xlabel.style.marginTop = "-1em";
    xlabel.style.height = "2em";
    xlabel.style.width = "2em";
    xlabel.style.fontSize = "1.5em";
    xlabel.style.lineHeight = "2em";
    xlabel.className = "axis-label";
    parentElement.appendChild(xlabel);

    var ylabel = document.createElement("p");
    ylabel.style.position = "absolute";
    ylabel.style.top = "0";
    ylabel.style.left = "50%";
    ylabel.style.marginLeft = "-1em";
    ylabel.style.height = "2em";
    ylabel.style.width = "2em";
    ylabel.style.fontSize = "1.5em";
    ylabel.className = "axis-label";
    parentElement.appendChild(ylabel);

    return {
      update: function (volume) {
        var voxelSize = viewBasis.getScaleTransform(volume);
        var axes = volume.getAxes();

        var xAxis = vec3.scale(vec3.create(), axes[0], voxelSize[0]);
        var yAxis = vec3.scale(vec3.create(), axes[1], voxelSize[1]);

        xlabel.textContent = volUtils.labelForAxis(xAxis).end;
        ylabel.textContent = volUtils.labelForAxis(yAxis).start;
      },
      setVisible: function (visible) {
        if (!visible) {
          xlabel.style.visibility = "hidden";
          ylabel.style.visibility = "hidden";
        } else {
          if (!animate.supported) {
            xlabel.style.visibility = undefined;
            ylabel.style.visibility = undefined;
          } else {
            xlabel.style.visibility = "hidden";
            ylabel.style.visibility = "hidden";
            animate.setAnimationProperty(ylabel, "axisLabelFade 6s");
            animate.setAnimationProperty(xlabel, "axisLabelFade 6s");
          }
        }
      }

    };
  }
  return {
    make: makeAxisLabels
  };
  });

