/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils"], function (utils) {
  "use strict";

  var requestAnimationFrame = window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function requestAnimationFrameFallback (callback) {
      return window.setTimeout(function () { callback(Date.now()); }, 20);
    };

  var cancelAnimationFrame = window.cancelAnimationFrame ||
    window.mozCancelAnimationFrame ||
    function cancelAnimationFrameFallback (request) {
      window.clearTimeout(request);
    };

  var timestampFallback;

  function makeFrameDuration() {
    var delta;
    var lastRedrawTime = null;
    var valid = false;
    return {
      step: function (timestamp) {
        if (undefined === timestampFallback) {
          timestampFallback = !timestamp || timestamp > 1e12;
          if (timestampFallback && utils.debug) {
            console.warn("using requestAnimationFrame timestamp fallback");
          }
        }

        if (timestampFallback) {
          timestamp = Date.now();
        }

        var redrawTime= timestamp;

        if (lastRedrawTime) {
          var d = redrawTime - lastRedrawTime; // milliseconds
          if (d > 10) {
            // only update when not on the same frame as redraw and  the last
            // frame was a recent one.
            delta = d;
            valid = true;
          } else {
            valid = false;
          }
          valid = true;
        }
        lastRedrawTime = redrawTime;
      },
      end: function (timestamp) {
        this.step(timestamp);
        lastRedrawTime = null;
      },
      fps: function () {
        return Math.round(1000/delta);
      },
      get: function () {
        return delta;
      },
      valid: function () {
        return valid;
      }
    };
  }

  // we can't reliably record the frame duration of a single frame.
  // Thus when only one frame is drawn, queue on more requestAnimationFrame
  // only to record the duration to this next frame.
  function makeMeassureFrameDuration(frameDuration) {
    var request = null;
    var record = function (timestamp) {
      request = null;

      frameDuration.end(timestamp);
    };

    var timer = null;
    var requestRecord = function () {
      timer = null;
      request = requestAnimationFrame(record);
    };

    return {
      cancel: function () {
        if (timer) {
          window.clearTimeout(timer);
          timer = null;
        }
        if (request) {
          cancelAnimationFrame(request);
          request = null;
        }
      },
      record: function () {
        if (!timer) {
          // ensure we get the next frame
          timer = window.setTimeout(requestRecord, 1);
        }
      },
      pending: function () {
        return timer || request;
      }
    };
  }

  function makeRequest () {
    var timer = null;
    var animationFrame = null;

    return {
      clearTimer: function () {
        if (timer) {
          window.clearTimeout(timer);
          timer = null;
        }
      },
      later: function (redraw) {
        this.clearTimer();
        timer = window.setTimeout(redraw, 500);
      },

      set: function (redraw) {
        animationFrame = requestAnimationFrame(redraw);
      },
      isSet: function () {
        return !!animationFrame;
      },

      done: function () {
        animationFrame = null;
      },

      highQuality: false
    };
  }

  function makeSampleFrames () {
    return {
      count: 0,
      onComplete: null,
      continious: false,

      step: function () {
        if (this.count > 0) {
          this.count--;
        }
      },
      isSampleNext: function () {
        return this.count > 0 || this.continious;
      },
      stepCompleted: function () {
        if (this.count === 0 && this.onComplete) {
          this.onComplete();
          this.onComplete = null;
        }
      }
    };
  }

  function makeRedraw() {
    var request = makeRequest();

    var frameDuration = makeFrameDuration();
    var meassureFrameDuration = makeMeassureFrameDuration(frameDuration);

    var sample = makeSampleFrames();

    var drawObjects = [];
    var redraw = function (timestamp) {
      request.done();

      frameDuration.step(timestamp);
      sample.step();

      if (sample.isSampleNext()) {
        request.highQuality = false;
      }

      var completed = true;
      for (var i=0; i<drawObjects.length; i++) {
        completed = drawObjects[i].draw(request.highQuality, timestamp) && completed;
      }

      sample.stepCompleted();

      if (!completed) {
        request.highQuality = false;
        request.later(redraw);
      }


      if (sample.isSampleNext()) {
        request.set(redraw);
      } else if (!meassureFrameDuration.pending()) {
        meassureFrameDuration.record();
      }
    };

    var requestRedraw = function (hq) {
      request.highQuality = hq || false;

      request.clearTimer();
      if (!request.isSet()) {
        meassureFrameDuration.cancel();

        request.set(redraw);
      }
    };

    return {
      drawObjects: drawObjects,
      requestRedraw: requestRedraw,
      hasPendingRedraw: function () {
        return !!(request.isSet() || sample.isSampleNext());
      },
      requestRedrawHQ: function () {
        requestRedraw(true);
      },
      requestNRedraws: function (n, cont) {
        sample.count = n;
        sample.onComplete = cont;
        requestRedraw();
      },

      begin: function () {
        if (!sample.continious) {
          requestRedraw();
        }
        sample.continious = true;
      },
      end: function () {
        if (!sample.continious) {
          // end called without begin (=> draw once)
          requestRedraw();
        }
        request.highQuality = true;
        sample.continious = false;
      },

      fps: frameDuration.fps,
      frameDuration: frameDuration.get,
      validFrameDuration: frameDuration.valid
    };
  }

  return makeRedraw();
});
