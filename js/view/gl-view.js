/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils", "ui/progress", "./redraw",
    "volume/volume", "volume/axis-numbers", "vr/vr-canvas", "vr/arcball",
    "ui/animate", "view/mouse-action", "lib/gl-matrix", "./view-basis",
    "./orientation-cube", "ui/fps-display"],
  function (utils, glUtils, progress, draw, vol, AxisNumbers, VRCanvas, ArcBall,
    animate, mouseAction, glm, viewBasis, orientationCube, makeFPSDisplay)  {
    "use strict";

    var vec2 = glm.vec2;
    var vec3 = glm.vec3;
    var mat4 = glm.mat4;
    var quat = glm.quat;

    var debug = utils.debug;
    if ((debug || document.debug) && !document.getElementById("qunit-fixture")) {
      var fpsDisplay;

      if (!document.getElementById("fpsElement")) {
        var fpsElement = document.createElement("div");
        fpsElement.id = "fpsElement";
        fpsElement.style.position = "absolute";
        fpsElement.style.top = "0";
        fpsElement.style.left = "50%";
        fpsDisplay = makeFPSDisplay(fpsElement);

        document.documentElement.appendChild(fpsElement);
      }
    }


    // Volume Rendering view (using WebGL)
    var GLView = function (parentElement, index, options) {
      this.parentElement = parentElement;
      this.index = index;
      this.viewTools = options.viewTools;

      this.loading = true;
      this.light = [0,1,1];
      this.showAnatomy = true;
      vec3.normalize(this.light, this.light);

      this.response = document.createElement("span");
      this.response.className = "response";
      parentElement.appendChild(this.response);

      var canvas = document.createElement("canvas");
      canvas.style.width = "100%";
      canvas.style.height = "100%";
      canvas.style.position = "absolute";
      parentElement.appendChild(canvas);
      this.canvas = canvas;
      this.vrcanvas = new VRCanvas(canvas, {constantFPS: true});

      if ((debug || document.debug) && fpsDisplay) {
        fpsDisplay.extraInfoCallback = function () {
          return "sub: " +
            Math.round(100*this.vrcanvas.vr.interactiveSubSample)/100;
        }.bind(this);
      }

      this.invalidateCache();

      this.cubeDistance = -0.5;
      var arcBall = new ArcBall();
      this.arcBall = arcBall;
      this.volumeInitialRotation = quat.create();

      var vrcanvas = this.vrcanvas;
      var view = this;
      vrcanvas.init(function () {
        vrcanvas.setLUT(function () {
          return view.viewTools.lut.toArray();
        });
        view.requestRedrawHQ();
      });

      var fadeCube = function () {
        this.cube.style.visibility = "hidden";
        animate.setAnimationProperty(this.cube, "axisLabelFade 6s");
      }.bind(this);

      var fadeTimeout;

      vrcanvas.setUpdateMVMatrix(function (MVMatrix) {
        var currentRotation = this.arcBall.rotation();
        var volumeRotation = quat.create();
        quat.multiply(volumeRotation, currentRotation, this.volumeInitialRotation);

        mat4.fromRotationTranslation(MVMatrix, volumeRotation,
            vec3.fromValues(0, 0, this.cubeDistance));

        orientationCube.rotate(this.cube, currentRotation);
        this.cube.style.visibility = "";
        animate.setAnimationProperty(this.cube, "");

        // retrigger animation
        if (fadeTimeout) clearTimeout(fadeTimeout);
        fadeTimeout = setTimeout(fadeCube, 50);
      }.bind(this));

      var scroll = function (e) {
        var clicks = utils.mouseWheelClicks(e);
        this.onZoom(1.0-clicks/20);

        e.preventDefault();
      }.bind(this);
      this.parentElement.addEventListener(utils.mouseWheelEvent, scroll);
      this.dragStartVector = vec3.create();

      this.canvas.addEventListener("touchstart", function (e) {
        view.mouseAction = MouseActions.rotateOrPick;
        var coords = view.getArcballCoords(e.touches[0]);

        arcBall.setStartPosition(coords);
      });

      this.interactive = true;
      this.sliceViews = [{canvas: canvas}];

      this.cube = orientationCube.make(parentElement);
      this.cube.style.right = "4%";
      this.cube.style.bottom = "4%";

    };

    GLView.prototype.testContextLost = function () {
      var l = glUtils.getExtension(this.vrcanvas.gl, "WEBGL_lose_context");
      if (l) {
        l.loseContext();
        window.setTimeout(function() {
          l.restoreContext();
        }, 500);
      }
    };

    GLView.prototype.setRotation = function (toAxis) {

      var toPlane = quat.create();
      switch (toAxis) {
        case AxisNumbers.tra:
          quat.rotateX(toPlane, toPlane, -Math.PI/2);
          //quat.identity(toPlane);
          break;
        case AxisNumbers.cor:
          quat.identity(toPlane);
          break;
        case AxisNumbers.sag:
          quat.rotateY(toPlane, toPlane, -Math.PI/2);
          break;
      }
      this.arcBall.setRotation(toPlane);
    };

    function rotationToWorld(basis) {
      var rotation = quat.create();
      var corAxes =  [
        vec3.fromValues(0,1,0), // viewing direction
        vec3.fromValues(1,0,0), // right
        vec3.fromValues(0,0,1), // up (see gl-matrix docs)
      ];
      utils.map(function(axis) {
        basis.to(axis, axis);
        return vec3.normalize(axis, axis);
      }, corAxes);

      quat.setAxes(rotation, corAxes[0], corAxes[1], corAxes[2]);
      return rotation;
    }
    function initialRotation(volume) {
      // rotate volume to coronal position
      var rotation = rotationToWorld(volume.getBasis());
      if (!vol.isRightHandCoordinateSystem(volume)) {
        var rot = quat.create();
        // FIXME
        // only a hack to make it work (mostly)
        if (volume.getAxisNumber() == AxisNumbers.sag) {
          quat.rotateZ(rot, rot, Math.PI);
          quat.rotateY(rot, rot, Math.PI/2);
        } else {
          quat.rotateX(rot, rot, -Math.PI/2);
        }
        quat.multiply(rotation, rot, rotation);
      }
      return rotation;
    }

    GLView.prototype.setVolumes = function (volumes) {
      var volume = volumes[0].getSourceVolume();
      this.vrcanvas.setVolume(volume);

      this.volumeInitialRotation = initialRotation(volume);
      this.setRotation(1);
    };

    GLView.prototype.getNativeSize = function () {
      return [1, 1];
    };

    GLView.prototype.getSelectedView = function (mouseCoords) {
      var offset = utils.getAbsoluteOffset(this.canvas);
      var x = mouseCoords.clientX - offset[0];

      var activeView = 0;
      if (this.showAnatomy) {
        var width = this.canvas.clientWidth;
        width /= 2;
        activeView = Math.floor(x / width);
      }

      return activeView;
    };

    // square "normalized device coordinates", i.e.: [-1;1] for the smaller axis.
    // the center is [0,0] and the lower left corner is [-a; -1] or [-1; -b].
    GLView.prototype.getArcballCoords = function (mouseEvent, selectedView) {
      var offset = utils.getAbsoluteOffset(this.canvas);

      var p = vec2.fromValues(mouseEvent.clientX - offset[0],
                                   mouseEvent.clientY - offset[1]);

      var size = vec2.fromValues(this.canvas.clientWidth,
                                 this.canvas.clientHeight);

      // p[0] relative to selected view
      if (this.showAnatomy) {
        size[0] /= 2;
      }

      if (undefined === selectedView) {
        if (this.showAnatomy) {
          selectedView = Math.floor(p[0] / size[0]);
        } else {
          selectedView = 0;
        }
      }
      p[0] -= selectedView * size[0];

      // recenter [-size; size]
      p[0] = 2*p[0] - size[0];
      p[1] = 2*p[1] - size[1];

      p[1] = -p[1]; // flip for canvas to GL axis.

      var shortAxisLength = Math.min(size[0], size[1]);
      vec2.scale(p, p, 1/shortAxisLength);

      return p;
    };

    var MouseActions = {
      rotateOrPick: 0,
      zoom:         1,
      changeWindow: 2,
      change1f:     3, // change shader parameter
    };
    function getMouseAction(e) {
      if (mouseAction.isZoom(e)) {
        return MouseActions.zoom;
      }
      if (mouseAction.isChangeWindow(e)) {
        return MouseActions.changeWindow;
      }
      if (mouseAction.isPan(e)) {
        return MouseActions.change1f;
      }

      return MouseActions.rotateOrPick;
    }

    GLView.prototype.onmousedown = function (e) {
      this.dragStartView = this.getSelectedView(e);

      var action = getMouseAction(e);

      if (action == MouseActions.change1f && !this.viewTools.update1f) {
        action = MouseActions.zoom;
      } else if (action == MouseActions.rotateOrPick) {
        var coords = this.getArcballCoords(e);
        this.arcBall.setStartPosition(coords);
      }
      this.mouseAction = action;
    };

    GLView.prototype.onscroll = function (mouseCoords) {
      switch (this.mouseAction) {
        case MouseActions.change1f:
          mouseAction.setCursor("scroll"); // not scroll but same cursor
          var value = this.viewTools.get1f() + 0.001*mouseCoords.delta[1];
          this.viewTools.update1f(value);
          break;

        case MouseActions.changeWindow:
          mouseAction.setCursor("changeWindow");
          var d = 0.001;
          this.viewTools.lut.changeWindow(-d*mouseCoords.delta[1],
              d*mouseCoords.delta[0]);
          break;

        case MouseActions.rotateOrPick:
          // rotate
          var coords = this.getArcballCoords(mouseCoords, this.dragStartView);
          this.arcBall.setPosition(coords);
          break;
        case MouseActions.zoom:
          mouseAction.setCursor("zoom");
          this.onZoom(mouseCoords.delta[1] / mouseAction.stepSize(7), true);
          break;

        default:
          utils.assert(false, "Unsupported mouseAction ", this.mouseAction);
      }
    };

    GLView.prototype.onZoom = function (scale, scaleAdditive) {
      if (scaleAdditive === true) {
        this.cubeDistance += scale;
      } else {
        this.cubeDistance /= scale;
      }
      this.cubeDistance = utils.clamp(this.cubeDistance, -20, -0.1);
      this.requestRedrawHQ(4);
    };


    GLView.prototype.scrollEnd = function (mouseCoords) {
      mouseAction.setCursor();

      var rotationThreshold = 1e-5;
      if (this.arcBall.dragDistance2 > rotationThreshold) {
        this.arcBall.setRotation(this.arcBall.rotation());
        this.arcBall.endDrag();
      } else if (MouseActions.rotateOrPick == this.mouseAction) {
        // not rotated => pick
        this.arcBall.endDrag();
        var offset = utils.getAbsoluteOffset(this.canvas);
        var x = mouseCoords.clientX - offset[0];
        var y = mouseCoords.clientY - offset[1];

        var response = this.response;
        response.style.left = x + "px";
        response.style.top = y + "px";
        animate.setAnimationProperty(response, "");
        window.setTimeout(function () {
          response.style.visibility = "hidden";
          animate.setAnimationProperty(response, "response 0.5s");
        }, 0);

        this.vrcanvas.pick(x, y);
      }

      this.dragStartView = undefined;
    };

    GLView.prototype.requestRedrawHQ = function (n) {
      this.vrcanvas.vr.endHQDraw(); // cancel when in progress
      if (n) {
        draw.requestNRedraws(n, draw.requestRedrawHQ);
      } else {
        draw.requestRedrawHQ();
      }
    };

    GLView.prototype.draw = function (highQuality) {
      var vr = this.vrcanvas.vr;
      if (!highQuality) {
        vr.endHQDraw();
      } else if (highQuality && !vr.isHQDraw()) {
        vr.beginHQDraw();
      }

      this.vrcanvas.updateLUT();
      this.vrcanvas.draw();

      if (highQuality) {
        if (vr.isHQDraw()) {
          draw.requestRedrawHQ();
        } else {
          vr.endHQDraw();
        }
      }

      if (this.volumeLoaded) {
        return true;
      } else {
        var volume = this.vrcanvas.volume;

        var isComplete = volume.isComplete() && volume.getLabelMap().isComplete();

        if (isComplete) {
          this.volumeLoaded = true;
          this.requestRedrawHQ();
        }

        return isComplete;
      }
    };

    GLView.prototype.resize = function () {
      this.requestRedrawHQ();
    };

    GLView.prototype.getVolume = function () {
      return this.vrcanvas.volume;
    };
    GLView.prototype.keydown = function (e) {
      if (e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
        return;
      }

      var rotate = function (angle, axis) {
        this.arcBall.rotate(quat.setAxisAngle(quat.create(), axis, angle));
        e.preventDefault();
      }.bind(this);

      var delta = 0.05;
      var handleKey = true;

      switch(e.keyCode) {
        // Rotations
        case 37: // left
          rotate(-delta, [0,1,0]);
          break;
        case 39: // right
          rotate(delta, [0,1,0]);
          break;
        case 40: // down
          rotate(delta, [1,0,0]);
          break;
        case 38: // up
          rotate(-delta, [1,0,0]);
          break;

        case 81: // q counter clock wise rotation
          rotate(delta, [0,0,1]);
          break;
        case 69: // e clock wise rotation
          rotate(-delta, [0,0,1]);
          break;

          // predefined planes
        case 83: // s sagittal
          this.setRotation(0);
          break;
        case 67: // c coronal
          this.setRotation(1);
          break;
        case 84: // t transverse
        case 65: // a axial
          this.setRotation(2);
          break;

        default:
          handleKey = false;
          break;
      }
      if (handleKey) {
        this.requestRedrawHQ();
      }
    };

    GLView.prototype.setAnatomyHidden = function (isHidden) {
      this.vrcanvas.setAnatomyVisible(!isHidden);
      if (this.showAnatomy == isHidden) {
        this.requestRedrawHQ();
      }
      this.showAnatomy = !isHidden;
    };
    GLView.prototype.isAnatomyVisible = function () {
      return this.showAnatomy;

    };
    GLView.prototype.setShaderQuality = function (quality) {
      this.vrcanvas.setQuality(quality, function () {
        this.requestRedrawHQ(32);
      }.bind(this));
    };

    GLView.prototype.invalidateCache = function () {
      this.rendererCache = {};
    };

    GLView.prototype.setRenderer = function (rendererDescription, cont) {
      progress.update("loading shaders");
      this.vrcanvas.setRenderer(rendererDescription,
          function () {
            progress.done();
            this.viewTools.setRenderer(rendererDescription);
            this.requestRedrawHQ(32);
            if (cont) cont();
          }.bind(this));
    };

    return GLView;
  });
