/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "volume/basis-transform", "volume/axis-numbers", "lib/gl-matrix"],
  function (utils, BasisTransform, AxisNumbers, glm) {
  "use strict";

  var vec3 = glm.vec3;

  // how the volume is oriented in the 2D slice view.
  // That is:
  // left - posterior - inferior
  // note the use of inferior to make cor and sag stand with superior on top
  var basis = new BasisTransform(
      [
      vec3.fromValues(1,0,0),
      vec3.fromValues(0,1,0),
      vec3.fromValues(0,0,-1),
      ]
      );


  var axis = vec3.create();

  basis.getScaleTransform = function (volume, allAxes) {
    var axes = volume.getAxes();
    var result = new Array(allAxes ? 3 : 2);
    for (var i=0; i<result.length; i++) {
      basis.to(axis, axes[i]);
      var scale = vec3.length(axis);
      var sign = utils.sign(vec3.dot(vec3.fromValues(1,1,1), axis));

      result[i] = sign * scale;
    }

    return result;
  };

  return basis;
  });
