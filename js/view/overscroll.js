/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(function () {
  "use strict";

  var Overscroll = function (onscroll) {
    this.timer = null;
    this.onscroll = function () {
      onscroll(this.delta);
    }.bind(this);
  };

  Overscroll.prototype.begin = function () {
    if (!this.timer) {
      this.timer = window.setInterval(this.onscroll, 10);
    }
  };

  Overscroll.prototype.update = function (mouseEvent) {
    if (mouseEvent.screenY === 0) {
      this.delta = -1;
      this.begin();
    } else if (mouseEvent.screenY === window.screen.height-1) {
      this.delta = 1;
      this.begin();
    } else {
      this.end();
    }
  };

  Overscroll.prototype.end = function () {
    if (this.timer) {
      window.clearInterval(this.timer);
      this.timer = null;
    }
  };

  return Overscroll;
});
