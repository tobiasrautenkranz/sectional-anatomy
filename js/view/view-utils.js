/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "ui/transform", "volume/volume",
    "lib/gl-matrix", "./view-basis"],
  function (utils, transform, volUtils, glm, viewBasis) {
  "use strict";

  var vec3 = glm.vec3;
  var vec2 = glm.vec2;

  function coordinatesToSliceIndex  (coordinates, volume) {
    var index = volume.getBasis().to(vec3.create(), coordinates)[2];
    var maxSlice = volume.getSize()[2]-1;
    return Math.round(utils.clamp(index, 0, maxSlice));
  }

  /** Clamps index such that it is within volume. */
  function clampIndex (volume, index, out) {
    out = out || vec3.create();
    var v = volume.getSourceVolume();

    var range = volUtils.volumeRangeInWorldCoordinates(v);

    for (var i=0; i<3; i++) {
      out[i] = utils.clampToRange(index[i], range.begin[i], range.end[i]-1);
    }
    return out;
  }
  function scrollIndex(index, volume, steps) {
    var axis = volume.getAxes()[2];

    vec3.scaleAndAdd(index, index, axis, steps);
    clampIndex(volume, index, index);
  }

  function getSliceViewPosition (canvas) {
    var canvasOffsetPx = utils.getAbsoluteOffset(canvas);
    var zoom = transform.getElementZoom(canvas);
    var canvasCenterPx =  [
      canvasOffsetPx[0] + canvas.width/2,
      canvasOffsetPx[1] + canvas.height/2
    ];
    // The origin in absolute coordinates as displayed (taking zoom into account)
    var canvasOriginPx =  [
      canvasCenterPx[0] - zoom[0]*canvas.width/2,
      canvasCenterPx[1] - zoom[1]*canvas.height/2,
    ];

    return {
      center: canvasCenterPx,
      origin: canvasOriginPx,
      // CSS transform property does not figure into this.
      zoom: zoom
    };
  }

  return {
    getSliceViewPosition: getSliceViewPosition,
    coordinatesToSliceIndex: coordinatesToSliceIndex,
    clampIndex: clampIndex,
    scrollIndex: scrollIndex
  };

  });
