/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "lib/gl-matrix"],
  function (utils, glm) {
  "use strict";

  var quat = glm.quat;
  var vec3 = glm.vec3;

  var faceNames = [
    "I", "S",
    "R", "L",
    "A", "P",
    ];

  var sideLength = 60;

  // Draw a cube with axis labels in the same orientation as the volume
  function makeCube(parentElement, rotationOffset) {
    var container = document.createElement("div");
    container.style.perspective = 5*sideLength + "px";
    container.style.position = "absolute";
    container.style.width = sideLength + "px";
    container.style.height = sideLength + "px";


    var div = document.createElement("div");
    div.className = "orientationCube";
    container.appendChild(div);

    var faces = [];
    for (var i=0; i<6; i++) {
      var face = document.createElement("p");
      face.appendChild(document.createTextNode(faceNames[i]));
      face.className = "face" + faceNames[i];

      div.appendChild(face);

      faces[i] = face;
    }

    if (parentElement) {
      parentElement.appendChild(container);
    }

    container.rotationOffset = rotationOffset || quat.create();

    return container;
  }

  var axis = vec3.create();
  function rotationStr(rotation) {
    var angle = quat.getAxisAngle(axis, rotation);
    angle += 0.001; // Firefox 48 flicker bug workaround!?
    return "rotate3d(" + axis[0] + "," + axis[1] + "," + axis[2] + "," +
      angle + "rad)";
  }

  function rotate(container, rotation) {
    container.firstChild.style.transform = "translateZ(-" + sideLength + "px) " +
      "scaleY(-1) " + // flip Y axis for GL coords
      rotationStr(rotation) +
      " scaleY(-1)";
  }

  return {
    make: makeCube,
    rotate: rotate
  };
  });

