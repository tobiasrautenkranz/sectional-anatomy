/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

requirejs.config({
  baseUrl: "/js",

  paths: {
    "QUnit": "lib/qunit/qunit",
    "blanket": "lib/blanket.min"
  },

  shim: {
    "QUnit" : {
      exports: "QUnit",
      init: function() {
        QUnit.config.autostart = false;
      }
    },
    "blanket": {
      exports: "blanket",
      deps: ["QUnit"],
      init: function () {
        blanket.options({
          filter: "//js\/.+/",
          antifilter: "//tests|lib/",
          /* module: js/DIRECTORY or "js" when not in a directory */
          modulePattern: /(?:\/js\/(?![^\/]+$)|\/)([^.\/]+)/
        });
      }
    }
  }
});

(function () {
  var coverage = false;
  //>>includeStart("debugInclude", pragmas.debug);
  coverage = true;
  //>>includeEnd("debugInclude");

  // require blanket first to allow it to track loaded/required js files
  requirejs(coverage ? ["blanket"] : [], function () {
    requirejs(["util/utils", "QUnit", "tests/qunit-ext"],
        function (utils, QUnit) {
          "use strict";

          function testPath(test) {
            return "tests/" + test;
          }

          var tests = ["utils", "cont", "studies",
          "volume", "lut", "mpr", "mip",
          "view", "views", "redraw", "transform", "widgets",
          "fuzzy-search",
          "arcball"];

          var testsGL = ["vr-canvas", "gradient-editor",
          "program-cache", "gl-view", "shaders", "webvr"];

          if (utils.hasWebGL()) {
            Array.prototype.push.apply(tests, testsGL);
          } else {
            testsGL.map(function (test) {
              QUnit.skip("WebGL Test: " + test);
            });
          }

          var modules = tests.map(testPath);

          if (coverage) {
            /* load all sources for complete coverage report. i.e. also sources
             * not loaded by unit tests. */
            var allSources = ["studies-browser", "image-changer",
            "viewer"];
            if (utils.hasWebGL()) {
              allSources.push("webgl");
            }
            Array.prototype.push.apply(modules, allSources);
          }

          require(modules, function () {
            QUnit.config.testTimeout = 10 * 1000;
            QUnit.start();
          });
        });
  });
})();
