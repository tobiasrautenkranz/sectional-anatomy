/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils"],
  function (utils, makeRangePicker) {
    "use strict";

    function makeColor (r, g ,b, a) {
      if ("string" === typeof r) {
        var c = r;

        r = parseInt(c.slice(1,3), 16);
        g = parseInt(c.slice(3,5), 16);
        b = parseInt(c.slice(5,7), 16);
      } else if ("object" === typeof r) {
        a = r.a;
        b = r.b;
        g = r.g;
        r = r.r;
      } else {
        if (undefined === g) {
          g = r;
        }
        if (undefined === b) {
          b = r;
        }
      }

      var color = {
        r: r,
        g: g,
        b: b
      };

      if (a !== undefined) {
        color.a = a;
      }

      return color;
    }

    function insertStop(stops, stop) {
      var insPos;
      for (insPos=0; insPos < stops.length; insPos++) {
        if (stops[insPos].position > stop.position) {
          break;
        }
      }
      stops.splice(insPos, 0, stop);

      return insPos;
    }

    var GradientCanvas = function (canvas) {
      canvas.width = 256;
      canvas.height = 1;

      this.context = canvas.getContext("2d");

      this.imageData = this.context.createImageData(canvas.width, canvas.height);
      this.colorStops = [];
      this.alphaStops = [];
    };

    function mix (x, y, a) {
      return x * (1-a) + y * a;
    }
    function mixc (x, y, a) {
      return [
        x.r * (1-a) + y.r * a,
        x.g * (1-a) + y.g * a,
        x.b * (1-a) + y.b * a,
      ];
    }

    GradientCanvas.prototype.updateImageData = function () {
      utils.assert(this.colorStops.length >= 2);

      var alphaStops = this.alphaStops;
      if (0 === alphaStops.length) {
        alphaStops = [{position: 0, color: {a:1}},
                      {position: 1, color: {a:1}}];
      } else if (1 === alphaStops.length) {
        alphaStops = [
        {position: alphaStops[0].position, color: alphaStops[0].color},
        {position: 1,                      color: {a: 1}}];
      }
      utils.assert(alphaStops.length >= 2);

      var cIndex = 1;
      var aIndex = 1;
      var cPrev = this.colorStops[0];
      var aPrev = alphaStops[0];

      var cNext = this.colorStops[1];
      var aNext = alphaStops[1];

      for (var i=0; i<256*4; i+=4) {
        var position = i/255/4;
        // color
        var dc = cNext.position - cPrev.position;
        var rcpos = utils.clamp((position - cPrev.position) / dc, 0, 1);
        var c = mixc(cPrev.color, cNext.color, rcpos);

        // alpha
        var da = aNext.position - aPrev.position;
        var rapos = utils.clamp((position - aPrev.position) / da, 0, 1);
        var a = mix(aPrev.color.a, aNext.color.a, rapos);

        this.imageData.data[i  ] = c[0];
        this.imageData.data[i+1] = c[1];
        this.imageData.data[i+2] = c[2];
        this.imageData.data[i+3] = 255*a;

        if (position > cNext.position &&
            cIndex+1 < this.colorStops.length) {
          cPrev = cNext;
          cIndex++;
          cNext = this.colorStops[cIndex];
        }
        if (position > aNext.position &&
            aIndex+1 < alphaStops.length) {
          aPrev = aNext;
          aIndex++;
          aNext = alphaStops[aIndex];
        }
      }

    };

    GradientCanvas.prototype.draw = function () {
      this.updateImageData();
      return this.context.putImageData(this.imageData, 0, 0);
    };

    GradientCanvas.prototype.getArray = function () {
      this.updateImageData();
      return this.imageData.data;
    };
    GradientCanvas.prototype.color = function (position) {
      this.updateImageData();
      var d = this.imageData.data;
      var p = 4*Math.round(255*position);
      return makeColor(d[p], d[p+1], d[p+2], d[p+3]/255);
    };

    GradientCanvas.prototype.addStop = function (position, value) {
      insertStop(this.stopsForValue(value), value);
    };

    GradientCanvas.prototype.stopsForValue = function (value) {
      if (value.color.r !== undefined) {
        return this.colorStops;
      }

      return this.alphaStops;
    };

    GradientCanvas.prototype.stopID = function (stop, stops) {
      stops = stops || this.stopsForValue(stop);
      var p;
      // FIXME !O(N) but array is sorted
      for (p=0; p < stops.length; p++) {
        if (stops[p] == stop) {
          break;
        }
      }
      utils.assert(p < stops.length, "stopID: stop not found. stop: " + stop + " in " + stops);

      return p;
    };

    GradientCanvas.prototype.removeStop = function (value) {
      var stops = this.stopsForValue(value);

      stops.splice(this.stopID(value, stops), 1);
    };

    GradientCanvas.prototype.changeWindow = function (deltaCenter, deltaWidth) {
      function avgPos(a) {
        var avg = 0;
        var n = 0;
        for (var i=0; i<a.length; i++) {
          var b = a[i];
          for (var j=0; j<b.length; j++) {
            avg += b[j].position;
          }
          n += b.length;
        }
        return avg/n;
      }
      var caStops = [this.colorStops, this.alphaStops];

      var center = avgPos(caStops);

      caStops.forEach(function (stops) {
        for (var i = 0; i < stops.length; i++) {
          var stop = stops[i];
          stop.position = (1 + deltaWidth) * (stop.position - center) + center;
          stop.position += deltaCenter;
          this.stopClampPosition(stops, i);
        }
      }.bind(this));
    };

    GradientCanvas.prototype.stopClampPosition = function (stops, id) {
      var lower = 0;
      var upper = 1;

      if (id-1 >= 0) {
        lower = stops[id-1].position;
      }
      if (id + 1 < stops.length) {
        upper = stops[id+1].position;
      }

      stops[id].position = utils.clamp(stops[id].position, lower, upper);
    };

    GradientCanvas.prototype.save = function () {
      function copy (stop) {
        return {
          position: stop.position,
          color: makeColor(stop.color),
        };
      }

      return {
        colorStops: this.colorStops.map(copy),
        alphaStops: this.alphaStops.map(copy)
      };
    };

    return GradientCanvas;
  });

