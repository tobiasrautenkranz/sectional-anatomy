/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils"],
  function (utils) {
    "use strict";

    function mkButton(text, onclick, repeat) {
      var button = document.createElement("button");	
      button.type = "button";
      setButtonText(button, text);

      if (onclick) {
        if (repeat) {
          var timer;
          button.onmousedown = function (e) {
            onclick.call(this);
            timer = setInterval(onclick, 100, e);
          };
          button.onmouseout = function () {
            clearInterval(timer);
          };
          button.onmouseup = function () {
            clearInterval(timer);
          };
        } else {
          button.onclick = onclick;
        }
      }
      return button;
    }

    function setButtonText(button, text) {
      if (text instanceof Array) {
        button.textContent = text[0];
        button.title = text[1];
      } else {
        button.textContent = text;
      }
    }

    function makeSlider(onchange, numSliders) {
      numSliders = numSliders || 1;
      utils.assert(numSliders >= 1);
      var container = document.createElement("div");

      var bg = document.createElement("div");
      bg.className = "slider-bg";
      container.appendChild(bg);

      var fill = document.createElement("div");
      fill.className = "slider-fill";
      bg.appendChild(fill);

      var sliders = [];
      var values = [];
      var valueStep = numSliders > 1 ? 1/(numSliders-1) : 0;
      for (var i=0; i<numSliders; i++) {
        values[i] = i * valueStep;

        var slider = document.createElement("button");
        slider.style.left = (values[i]*100) + "%";
        slider.className = "slider";

        bg.appendChild(slider);
        sliders[i] = slider;
      }

      var updateFill = function () {
        fill.style.width = (values[0]*100) + "%";
      };

      container.className = "slider";
      if (2 === sliders.length) {
        container.className = "interval-slider";
        updateFill = function () {
          fill.style.left = (values[0]*100) + "%";
          fill.style.width = ((values[1]-values[0])*100) + "%";
        };
      }

      var positionSlider = function positionSlider (slider) {
        sliders[slider].style.left = (values[slider] * 100) + "%";
        updateFill();
      };

      var activeSlider = 0;
      var dragStartX;
      var initialValue;
      var positionEvent = function (e) {
        var x = e.clientX - dragStartX;

        var length = bg.clientWidth;
        x = x / length + initialValue;
        values[activeSlider] = utils.clamp(x, 0, 1);
        for (var i=0; i<activeSlider; i++) {
          values[i] = utils.clamp(values[i], 0, values[activeSlider]);
          positionSlider(i);
        }
        positionSlider(activeSlider);
        for (var i=activeSlider+1; i < sliders.length; i++) {
          values[i] = utils.clamp(values[i], values[activeSlider], 1);
          positionSlider(i);
        }
        onchange(1 === values.length  ? values[0]: values);
        e.preventDefault();
      };

      var makeSliderDragStartListener = function (sliderIndex) {
        return function (e) {
          activeSlider = sliderIndex;
          dragStartX = e.clientX;
          initialValue = values[sliderIndex];
        };
      };

      for (var i=0; i<numSliders; i++) {
        utils.makeDragEventListener(sliders[i], makeSliderDragStartListener(i),
            positionEvent);
      }

      if (1 === sliders.length) {
        // prevent click position event when the slider is clicked.
        // Otherwise the slider jumps a small offset after a drag event.
        // Due to the click event (absoluteOffset is kind of buggy, but
        // good enough)
        sliders[0].addEventListener("click",
            function preventClick (e) { e.stopPropagation(); });

        container.addEventListener("click", function (e) {
          var width = sliders[0].clientWidth;
          var length = bg.clientWidth;
          var x = (e.screenX - utils.getAbsoluteOffset(bg)[0] -width) / length;
          values[0] = utils.clamp(x, 0, 1);
          positionSlider(0);
          onchange(1 === values.length  ? values[0]: values);
        });

        container.addEventListener(utils.mouseWheelEvent, function (e) {
          container.updateValue(container.values[0] +
              utils.mouseWheelClicks(e) / 500);
        });
      }

      container.values = values;
      container.setValue = function (value) {
        if (!Array.isArray(value)) {
          value = [value];
        }
        utils.assert(sliders.length === value.length);

        var lastValue = 0;
        for (var i=0; i<sliders.length; i++) {
          var v = utils.clamp(value[i], lastValue, 1);
          container.values[i] = v;
          positionSlider(i);

          lastValue = v;
        }
      };
      container.updateValue = function (value) {
        container.setValue(value);
        onchange(1 === container.values.length ?
            container.values[0] : container.values);
      };
      container.getValue = function (index) {
        if (index) {
          return container.values[index];
        } else if (1 === container.values.length) {
          return container.values[0];
        } else {
          return container.values;
        }
      };

      if (2 === sliders.length) {
        container.setInterval = function (low, high) {
          container.setValue([low, high]);
        };
      }

      return container;
    }

    function makeIntervalSlider(onchange) {
      return makeSlider(onchange, 2);
    }

    return {
      makeButton: mkButton,
      setButtonText: setButtonText,
      makeSlider: makeSlider,
      makeIntervalSlider: makeIntervalSlider
    };
  });

