/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["ui/widgets", "ui/animate"],
  function (widgets, animate) {
    "use strict";

    // display description of selected anatomy
    var AnatomyInfo = function (parentElement, study, viewer) {
      this.study = study;

      var div = document.createElement("div");

      div.id = "anatomy-info";
      div.style.visibility = "hidden";	

      var p = document.createElement("p");

      this.colorBox = document.createElement("span");
      this.colorBox.className = "color-box";
      p.appendChild(this.colorBox);

      this.textContent = document.createTextNode("");
      p.appendChild(this.textContent);
      this.content = p;

      div.appendChild(p);


      var anatomyInfo = this;
      var showBtn = widgets.makeButton(["^","show/hide anatomy description"],
          function () {
            var show = (div.style.visibility === "hidden");
            anatomyInfo.show(show);
          });
      showBtn.style.display = "none";
      showBtn.className = "show-hide";
      this.showBtn = showBtn;
      parentElement.appendChild(showBtn);

      var unselect = widgets.makeButton(["-", "unselect anatomy [delete]"],
          function () {
            viewer.anatomyTools.unselectAnatomy(anatomyInfo.anatomyID);
          });
      unselect.style.display = "none";
      unselect.className = "anatomy-info";
      parentElement.appendChild(unselect);

      var buttons = [];
      buttons.push(unselect);
      this.buttons = buttons;

      this.element = div;
      parentElement.appendChild(this.element);
    };

    AnatomyInfo.collapse = function (showButton, buttons, hidden) {
      if (hidden) {
        showButton.textContent = "\u2303";
        showButton.style.marginBottom = "0";
      } else {
        showButton.textContent = "\u2304";
        showButton.style.marginBottom = "0.5em";
      }
      for (var i=0; i<buttons.length; i++) {
        buttons[i].style.display = hidden ? "none" : "";
      }
    };
    AnatomyInfo.prototype.show = function (visible) {
      AnatomyInfo.collapse(this.showBtn, this.buttons, !visible);
      if (visible) {
        this.element.style.visibility = "";
        animate.setAnimationProperty(this.element, "anatomyInfoShow 0.3s");
      } else {
        this.element.style.visibility = "hidden";
        animate.setAnimationProperty(this.element, "anatomyInfoHide 0.3s");
      }
    };

    AnatomyInfo.prototype.setAnatomyID = function (anatomyID) {
      if (anatomyID) {
        this.study.labelMap.getLUT().getArray()[4*anatomyID+3] = 255;
      }
      this.anatomyID = anatomyID;
      this.updateText();
    };

    function cssRGB(r, g, b) {
      return "rgb(" + r + "," + g + "," + b + ")";
    }

    AnatomyInfo.prototype.updateText = function () {
      if (this.anatomyID) {
        AnatomyInfo.collapse(this.showBtn, this.buttons, false);

        this.textContent.data = this.study.labelMap.labels[this.anatomyID];
        var colors = this.study.labelMap.getLUT().getArray();
        var cIndex = 4 * this.anatomyID;
        this.colorBox.style.backgroundColor =
          cssRGB(colors[cIndex], colors[cIndex+1], colors[cIndex+2]);
        encodeURIComponent(this.study.labelMap.labels[this.anatomyID]);
        this.show(true);
        this.showBtn.style.display = "";
      } else {
        this.show(false);
        this.showBtn.style.display = "none";
      }
    };

    return AnatomyInfo;
  });
