/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "./tabs", "./widgets", "view/redraw",
    "volume/remote-label-map"],
  function (utils, tabs, widgets, draw, LabelMap) {
    "use strict";

    // Setting in relation to the anatomy / segmentation.
    // Mostly to allow searching and selecting anatomy labels.
    var AnatomyTools = function (parentElement, anatomyInfo, study, gridView) {
      tabs.Overlay.call(this, parentElement);
      this.getElement().id = "anatomy-tab";

      this.study = study;
      findSlice.study = study; //FIXME hack
      this.anatomyInfo = anatomyInfo;

      var anatomyTools = this;
      this.onAnatomySelect = function onAnatomySelect (e) {
        var id = e.currentTarget.dataId;
        if (anatomyTools.getLabels()[id].className === "selected") {
          anatomyTools.unselectAnatomy(id);
        } else  {
          findSlice(id, function (index) {
            if (index) {
              gridView.index[0] = index[0];
              gridView.index[1] = index[1];
              gridView.index[2] = index[2];
            }
            draw.requestRedraw();
          });
          anatomyTools.selectAnatomy(id);
        }

        e.preventDefault();
      };
      var searchDiv = document.createElement("div");
      searchDiv.className = "anatomy-search";


      var search = document.createElement("input");
      searchDiv.appendChild(search);
      search.placeholder = "filter";
      var t = this;
      search.addEventListener("input", function () {
        AnatomyTools.searchAnatomy(search.value, t);
      });
      this.searchInput = search;

      var select = widgets.makeButton(["+", "add filtered"],
          function () {
            var ids = [];
            for(var i=1; i<anatomyTools.labels.length; i++) {
              if (anatomyTools.labels[i].style.display !== "none") {
                ids.push(i);
              }
            }
            anatomyTools.selectAnatomy(ids, "add");
            draw.requestRedraw();
          }
          );
      searchDiv.appendChild(select);
      var unselect = widgets.makeButton(["-", "remove filtered"],
          function () {
            var ids = [];
            for(var i=1; i<anatomyTools.labels.length; i++) {
              if (anatomyTools.labels[i].style.display !== "none") {
                ids.push(i);
              }
            }
            anatomyTools.unselectAnatomy(ids);
            draw.requestRedraw();
          }
          );
      searchDiv.appendChild(unselect);

      var showAnatomy = widgets.makeButton(["show/hide", "show / hide anatomy"],
          function () {
            if (gridView.isAnatomyVisible) {
              if (gridView.isAnatomyVisible()) {
                gridView.setAnatomyHidden(true);
                widgets.setButtonText(showAnatomy, "show");
              } else {
                gridView.setAnatomyHidden(false);
                widgets.setButtonText(showAnatomy, "hide");
              }
            }
          }
          );
      this.updateShowAnatomy = function () {
        if (gridView.isAnatomyVisible) {
          if (gridView.isAnatomyVisible()) {
            widgets.setButtonText(showAnatomy, "hide");
          } else {
            widgets.setButtonText(showAnatomy, "show");
          }
        }
      };
      searchDiv.appendChild(showAnatomy);

      this.getElement().appendChild(searchDiv);

      var anatomy = document.createElement("div");
      anatomy.className = "anatomy-results";

      var results = document.createElement("section");

      this.anatomyList = document.createElement("ul");
      results.appendChild(this.anatomyList);

      anatomy.appendChild(results);
      this.getElement().appendChild(anatomy);


      this.getLabels();
    };
    AnatomyTools.prototype = new tabs.Overlay();


    AnatomyTools.prototype.getLabels = function () {
      if (!this.labels) {
        var labels = this.study.labelMap.labels;
        this.labels = [];
        var colors = this.study.labelMap.getLUT().getArray();
        for (var i=1; i < labels.length; i++) {
          var label = document.createElement("li");
          var link = document.createElement("a");
          link.dataId = i;
          link.href = "#";
          link.addEventListener("click", this.onAnatomySelect);
          this.labels[i] = label;
          label.appendChild(link);
          var color = document.createElement("span");
          color.className = "color-box";
          var cIndex = 4 * i;
          color.style.backgroundColor = "rgb(" + colors[cIndex] + "," +
            colors[cIndex+1] + "," + colors[cIndex+2] + ")";
          link.appendChild(color);

          link.appendChild(document.createTextNode(labels[i]));

          this.anatomyList.appendChild(label);

        }
        this.anatomyList.lang = this.study.labels.languages[this.study.language];
      }

      return this.labels;
    };

    AnatomyTools.searchAnatomy = function (text, t) {
      var labels = t.getLabels();
      if (text.trim()) {
        var ids = findAnatomyId(t.study, text);
        for (var i=1; i<labels.length; i++) {
          labels[i].style.display = "none";
        }
        for (var i=0; i<ids.length; i++) {
          labels[ids[i]].style.display = "";
        }
      } else {
        for (var i=1; i<labels.length; i++) {
          labels[i].style.display = "";
        }
      }
    };

    AnatomyTools.prototype.search = function (searchString) {
      this.searchInput.value = searchString;
      this.setVisible();
      AnatomyTools.searchAnatomy(searchString, this);
    };


    function findAnatomyId(study, name) {
      return utils.findSearchString(utils.splitSearchString(name),
          study.labelMap.labels).ids;
    }

    function findMidPoint(slice, width, id, start) {

      var first = [start % width, Math.floor(start/width)];
      var i;
      for (i=start; i<slice.length; i+= width) {
        if (slice[i] !== id) {
          break;
        }
      }
      var last = [i % width, Math.floor(i/width)];
      var middle = [last[0], Math.floor((last[1]+first[1])/2)];

      for (i=middle[0]+middle[1]*width; i<(middle[1]+1)*width; i++) {
        if (slice[i] !== id) {
          break;
        }
      }
      last = [i % width, middle[1]];
      for (i=middle[0]+middle[1]*width; i>middle[1]*width; i--) {
        if (slice[i] !== id) {
          break;
        }
      }
      first = [i % width, middle[1]];
      middle = [Math.floor((last[0]+first[0])/2), first[1]];
      return middle;
    }

    function findSlice(id, cont, startSlice, steps) {
      // try not to test every slice
      var step=10;
      steps = steps || [0,6,1,4,9,3,7,2,8,5];
      startSlice = startSlice || 0;
      var volume = findSlice.study.labelMap; //FIXME hack
      if (startSlice >= volume.getSize()[2]) {
        var startStep = steps.pop();
        if (startStep) {
          return findSlice(id, cont, startStep, steps);
        }
        return cont(null);
      }

      var find = function (slice) {
        for (var j=0, l=slice.length; j<l; j++) {
          if (slice[j] === id) {
            var pos = findMidPoint(slice, volume.getSize()[0], id, j);
            var coords = [pos[0], pos[1], startSlice];
            return cont(volume.getBasis().from(coords, coords),
                  volume.getAxes(), volume.getOrigin());
          }
        }
        findSlice(id, cont, startSlice+step, steps);
      };
      volume.getSlice(find, startSlice);
    }


    AnatomyTools.prototype.setVisible = function () {
      this.getElement().style.opacity = "0.8";
      tabs.Overlay.prototype.setVisible.call(this);
      this.updateShowAnatomy();
    };

    AnatomyTools.prototype.updateLabels = function () {
      for (var i=1; i < this.labels.length; i++) {
        var link = this.labels[i].children[0];
        var text = link.childNodes[1];
        link.replaceChild(document.createTextNode(this.study.labelMap.labels[i]),
            text);
      }
      this.anatomyList.lang = this.study.labels.languages[this.study.language];
    };

    AnatomyTools.prototype.getSelectedAnatomy = function () {
      var selected = [];
      for (var i=1; i < this.labels.length; i++) {
        if (this.labels[i].className === "selected") {
          selected.push(i);
        }
      }
    };

    AnatomyTools.prototype.unselectAnatomy = function (anatomyIds) {
      this.selectAnatomy(anatomyIds, "remove");
    };

    AnatomyTools.prototype.selectAnatomy = function (anatomyIds, mode) {
      if (!Array.isArray(anatomyIds)) {
        if (anatomyIds) {
          anatomyIds = [anatomyIds];
        } else {
          anatomyIds = [];
        }
      }
      var labels = this.getLabels();
      mode = mode || this.selectionMode || "replace";
      utils.assert("replace" === mode || "add" === mode || "remove" === mode,
          "unknown mode: " + mode);

      if ("replace" === mode) {
        for (var id=1; id < labels.length; id++) {
          labels[id].className = "";
          this.study.labelMap.getLUT().getArray()[4*id+3] =
            LabelMap.unselectedAlpha;
        }
      }
      if ("remove" === mode) {
        for (var i=0; i < anatomyIds.length; i++) {
          var id = anatomyIds[i];
          labels[id].className = "";
          this.study.labelMap.getLUT().getArray()[4*id+3] =
            LabelMap.unselectedAlpha;
        }
        this.anatomyInfo.setAnatomyID();
      } else {
        for (var i=0; i < anatomyIds.length; i++) {
          var id = anatomyIds[i];
          labels[id].className = "selected";
          this.study.labelMap.getLUT().getArray()[4*id+3] =
            LabelMap.multiselectAlpha;
        }
        if ("replace" === mode) {
          this.anatomyInfo.setAnatomyID(anatomyIds[0]);
        } else if (1 === anatomyIds.length || !this.anatomyInfo.anatomyID) {
          if (this.anatomyInfo.anatomyID) {
            var lutIndex = 4*this.anatomyInfo.anatomyID+3;
            this.study.labelMap.getLUT().getArray()[lutIndex] =
              LabelMap.multiselectAlpha;
          }
          this.anatomyInfo.setAnatomyID(anatomyIds[0]);
        }
      }

      draw.requestRedraw();
    };

    return {
      AnatomyTools: AnatomyTools,
      findSlice: findSlice,
    };
});
