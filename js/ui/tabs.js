/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2015 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["./transform", "./animate"],
  function (transform, animate) {
    "use strict";

    // Allows access to the various settings / tabs
    var ToolBox = function (parentElement) {
      this.tabsElement = document.createElement("nav");
      this.tabsElement.className = "tabs";

      this.ul = document.createElement("ul");
      this.tabsElement.appendChild(this.ul);

      parentElement.appendChild(this.tabsElement);

      this.buttons = [];

      this.overlays = [];

      var tb = this;
      window.addEventListener("resize", function() {tb.resize();});
    };

    ToolBox.prototype.addOverlay = function (overlay, button) {
      this.overlays.push(overlay);
      if (button) {
        overlay.tabButton = button;
      }
    };

    ToolBox.prototype.addTab = function (title, overlay, onclick) {
      return this.addButton(title, onclick, overlay);
    };
    ToolBox.prototype.showOverlay = function (overlay) {
      for (var i=0; i<this.overlays.length; i++) {
        var o = this.overlays[i];
        o.setHidden();
        if (o.tabButton) {
          o.tabButton.parentElement.removeAttribute("selected");
        }
      }
      if (overlay) {
        overlay.setVisible();
      }
    };
    ToolBox.prototype.addButton = function (title, onclick, overlay) {
      function makeOnclickFkt (toolBox, onclick, button, overlay) {
        return function (e) {
          if (overlay) {
            if (overlay.isVisible()) {
              overlay.setHidden(true);
            } else {
              toolBox.showOverlay(overlay);

            }
          }
          if (onclick) {
            onclick(e);
          }
          var li = button.parentElement;
          if (li.getAttribute("selected", "true")) {
            li.removeAttribute("selected");
          } else {
            li.setAttribute("selected", "true");
          }
        };
      }

      var button = document.createElement("button");
      button.type = "button";
      button.style.position="relative";
      button.addEventListener("click",
          makeOnclickFkt(this, onclick, button, overlay));
      button.style.height = "3em";
      button.style.width = "8em";
      button.style[transform.elementTransformProperty] = "rotate(90deg)";

      if (overlay) {
        this.addOverlay(overlay, button);
      }

      var text = document.createElement("p");
      text.style.padding = "0";
      text.style.margin = "0";
      text.innerHTML = title;
      text.style.left = "0em";
      button.appendChild(text);

      var li = document.createElement("li");
      this.buttons.push(button);
      li.style.width = "3em";
      li.appendChild(button);
      this.ul.appendChild(li);

      return button;
    };

    ToolBox.prototype.resize = function () {
      function fitFontSize(string, width, height) {
        // Guestimate
        var fontAspectRatio = 0.8;
        return Math.min(1/fontAspectRatio * width/string.length, 0.7*height);
      }
      var elementHeight = this.tabsElement.parentElement.clientHeight /
        this.buttons.length;
      var elementWidth = this.tabsElement.parentElement.clientWidth;
      var fontPx = fitFontSize("# overview", elementHeight, elementWidth);
      this.tabsElement.style.fontSize = fontPx + "px";
      var styleProperties = {
        width:  (elementHeight + "px"),
        height: (elementWidth + "px"),
        marginTop: elementHeight/2 - elementWidth/2 + "px",
        marginLeft: -elementHeight/2 + elementWidth/2 + "px"
      };
      for (var i=0; i<this.buttons.length; i++) {
        for (var p in styleProperties) {
          this.buttons[i].style[p] = styleProperties[p];
        }
        this.buttons[i].parentElement.style.height =
          this.tabsElement.parentElement.clientHeight/this.buttons.length + "px";
      }
    };

    // A Tab container for settings
    var TabOverlay = function (parentElement) {
      this.overlayElement = document.createElement("div");
      if (parentElement) {
        parentElement.appendChild(this.overlayElement);

        this.overlayElement.className = "tab-overlay";
        this.overlayElement.style.visibility = "hidden";
      }
    };

    TabOverlay.prototype.setVisible = function () {
      this.overlayElement.style.visibility = "";
      this.visible = true;
      animate.setAnimationProperty(this.overlayElement, "slideIn 0.2s");
    };
    TabOverlay.prototype.setHidden = function (doAnimation) {
      this.overlayElement.style.visibility = "hidden";
      this.visible = false;
      if (doAnimation) {
        animate.setAnimation(this.overlayElement, {
          name: "slideOut",
          duration: "0.2s",
          fillMode: "forwards" // prevent flicker on animation end for chromium
        });
      } else {
        animate.setAnimationProperty(this.overlayElement, "");
      }
    };
    TabOverlay.prototype.isVisible = function () {
      return this.visible;
    };
    TabOverlay.prototype.getElement = function () {
      return this.overlayElement;
    };

    return {
      Tabs: ToolBox,
      Overlay: TabOverlay
    };
});
