/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2016 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
define(["view/redraw"],
  function (draw) {
    "use strict";
    function fpsToColor(fps) {
        return "hsl(" + (fps/(2*60)*240) + ",100%,50%)"; // 60fps = green
    }


    var makeFPS = function (parentElement) {
      var fpsElement = document.createElement("p");
      fpsElement.innerHTML = "FPS: ";
      fpsElement.style.color = "white";
      parentElement.appendChild(fpsElement);

      var graph = document.createElement("canvas");
      graph.width = 150;
      graph.height = 100;
      graph.style.border = "solid gray 2px";
      parentElement.appendChild(graph);

      var context = graph.getContext("2d");
      var graphPos = 0;
      var fpsMax = 80;

      var avgFPS = 0;
      var fpsToY = function (fps) {
        return graph.height * (1 - fps/fpsMax);
      };

      var fpsDisplay = {
        extraInfoCallback: null
      };

      var w = 1;
      var lastFPS = 0;

      draw.drawObjects.push({draw: function () {
        var fps = draw.fps();

        w = Math.max(1, Math.round(60/fps));

        // bars
        var height = graph.height * fps/fpsMax;
        var y = graph.height - height;

        context.beginPath();
        context.clearRect(graphPos, 0, w+1, graph.height); // clear +1 to indicate current
        context.lineWidth = 1.5;

        try {
          var gradient = context.createLinearGradient(graphPos-w, fpsToY(lastFPS),
              graphPos, fpsToY(fps));
          gradient.addColorStop(0, fpsToColor(lastFPS));
          gradient.addColorStop(1, fpsToColor(fps));
          context.strokeStyle = gradient;
        } catch (a) {
          // konqueror
          context.strokeStyle = fpsToColor(fps);
        }

        context.moveTo(graphPos-w, fpsToY(lastFPS));
        context.lineTo(graphPos, fpsToY(fps));
        context.stroke();
        lastFPS = fps;

        // 60 fps line
        context.fillStyle = "rgba(180,180,180,0.8)";
        context.fillRect(graphPos, fpsToY(60), w, 1);

        graphPos = (graphPos + w) % graph.width;

        avgFPS = 0.5 * (avgFPS + fps);
        fpsElement.innerHTML = "FPS: " + Math.round(avgFPS);
        if (fpsDisplay.extraInfoCallback) {
          fpsElement.innerHTML += " " + fpsDisplay.extraInfoCallback();
        }

        return true;
      }});

      return fpsDisplay;
    };

    return makeFPS;
  });
