/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["require", "util/utils", "volume/mpr", "volume/oblique", "view/redraw",
    "./animate", "./progress", "volume/volume"],
  function (require, utils, MPR, ObliqueMPRVolume, draw, animate, progress,
    volUtils) {
    "use strict";

    function setViewProperties(view, properties) {
      view.setAnatomyHidden = properties.setAnatomyHidden || null;
      view.isAnatomyVisible = properties.isAnatomyVisible || null;
      view.setSmallScreen = properties.setSmallScreen || null;

      if (!view.setSmallScreen && view.setAnatomyHidden) {
        view.setSmallScreen = view.setAnatomyHidden;
      }

      view.onResize = properties.onResize ||
        function () {
          if (view.setSmallScreen && isSmallScreen()) {
            view.setSmallScreen(true);
          } else {
            view.setSmallScreen(false);
          }
        };
    }

    function isSmallScreen () {
      return document.body.clientWidth < 500;
    }
    function showOverview(viewer, onSelect, smallScreen) {
      var setSmallScreenO = function (small) {
        if (small) {
          showOverview(viewer, onSelect, true);
        } else {
          showOverview(viewer, onSelect, false);
        }
      };
      setViewProperties(viewer.gridView, {setSmallScreen: setSmallScreenO});
      if (undefined === smallScreen) {
        smallScreen = isSmallScreen();
      }

      viewer.anatomy.selectionMode = null;
      viewer.anatomyInfo.show(false);
      var gridView = viewer.gridView;
      var study = viewer.study;
      var viewsPerSequences = study.volumes[0].getAxisNumber() === 2 ? 5 : 3;
      if (smallScreen) {
        viewsPerSequences = 3;
      }

      var home = document.createElement("div");

      home.id = "home-button";

      var a = document.createElement("a");
      a.href = "/" + study.labels.languages[study.language] + "/";
      a.addEventListener("click", function (e) { e.stopPropagation(); });
      //a.appendChild(document.createTextNode("\u2302 Home"));
      a.appendChild(document.createTextNode("home"));
      home.appendChild(a);

      gridView.setSize(study.volumes.length-1, viewsPerSequences,
          {interactive: false});

      gridView.table.childNodes[0].appendChild(home);

      var mprClick = function (e) {
        showMPRView(gridView, gridView.getSelectedView().getVolume());
        if (onSelect) onSelect();
        e.stopPropagation();
      };
      var mkMPRHL = function (mprs) {
        return function () {
          for (var i=0; i<mprs.length; i++) {
            mprs[i].setAttribute("hl","true");
          }
        };
      };
      var mkMPRunHL = function (mprs) {
        return function () {
          for (var i=0; i<mprs.length; i++) {
            mprs[i].removeAttribute("hl");
          }
        };
      };

      var row = 0;
      for (var v=0; v<study.volumes.length; v++) {
        var volume = study.volumes[v];
        if (volume.type === "labelmap") {
          continue;
        }
        gridView.getView(row,0).setVolumes([MPR.reformatVolume(volume,2)]);
        gridView.getView(row,1).setVolumes([MPR.reformatVolume(volume,1)]);
        gridView.getView(row,2).setVolumes([MPR.reformatVolume(volume,0)]);


        var mpr = document.createElement("div");
        var mprl = document.createElement("div");
        var mprr = document.createElement("div");
        var mprs = [mprl, mpr, mprr];
        var hlMPR = mkMPRHL([mpr,mprl,mprr]);
        var unhlMPR = mkMPRunHL([mpr,mprl,mprr]);
        for (var i=0; i<mprs.length; i++) {
          mprs[i].className = "mpr-button";

          mprs[i].addEventListener("click", mprClick);
          mprs[i].addEventListener("mouseover", hlMPR);
          mprs[i].addEventListener("mouseout", unhlMPR);

          gridView.getView(row,i).overlayDiv.appendChild(mprs[i]);
        }
        mpr.appendChild(document.createTextNode("MPR"));
        mprl.appendChild(document.createTextNode("3"));

        var hasWebgl = utils.hasFastWebGL();

        if (hasWebgl) {
          var showWebGLView = function (e) {
            if (onSelect) onSelect();
            progress.update("loading 3D");
            require(["webgl"],
                function(webgl) {
                  webgl.showGLView(viewer);
                  draw.requestRedraw();
                });
            e.preventDefault();
            e.stopPropagation();
          };
          var webgl = document.createElement("div");
          webgl.className = "webgl-button";
          //webgl.appendChild(document.createTextNode("\u2B21 3D"));
          webgl.appendChild(document.createTextNode("3D"));
          webgl.addEventListener("click", showWebGLView);
          gridView.getView(row,0).overlayDiv.appendChild(webgl);
        }


        if (5 === viewsPerSequences) {
          gridView.getView(row,3).setVolumes([new ObliqueMPRVolume(volume)]);
          gridView.getView(row,4).setVolumes(
              [new ObliqueMPRVolume(volume, "Oblique2")]);
        }

        gridView.table.addEventListener("click", function selectView () {
          if (gridView.getSize()[1] === viewsPerSequences &&
              gridView.getView(0,0).getVolume().getSourceVolume().type !== "labelmap") {
            showSingleView(gridView, gridView.getSelectedView().getVolume());
            if (onSelect) onSelect();
          }
          this.removeEventListener("click", selectView);
        });


        for (i=0; i<viewsPerSequences; i++) {
          var view = gridView.getView(row,i);
          view.title.style.visibility = "";
          view.title.innerHTML = view.getVolume().getSourceVolume().name +
            "<br>" + volUtils.getAxisName(view.getVolume());
          view.overlayDiv.className = "view-button";
        }
        row++;
      }
      gridView.resize();
      draw.requestRedraw();
      animate.setTransition(gridView.table, "opacity 0.5s ease-in");

      gridView.table.style.opacity = 1.0;
    }

    function showSingleView(gridView, volume, hideAnatomy) {
      var setAnatomyHidden = function (small) {
        if (small) {
          showSingleView(gridView, volume, true);
        } else {
          showSingleView(gridView, volume, false);
        }
      };
      setViewProperties(gridView, {
        setAnatomyHidden: setAnatomyHidden,
        isAnatomyVisible: function () {return 2 === gridView.getSize()[1];}
      });
      if (undefined === hideAnatomy) {
        hideAnatomy = isSmallScreen();
      }

      animate.setTransition(gridView.table, "");
      gridView.table.style.opacity = 0.0;

      if (hideAnatomy) {
        gridView.setSize(1,1);
      } else {
        gridView.setSize(1,2);
      }
      gridView.getView(0,0).setVolumes([volume]);

      if (!hideAnatomy) {
        gridView.getView(0,1).setVolumes([volume.getReformatedLabelMap()]);
        gridView.getView(0,0).linkView(gridView.getView(0,1));
      }
      gridView.resize();
      draw.requestRedraw();

      animate.setTransition(gridView.table, "opacity 0.5s ease-in");

      gridView.table.style.opacity = 1.0;
    }
    function showMPRView(gridView, volume, hideAnatomy) {
      var setAnatomyHidden = function (small) {
        if (small) {
          showMPRView(gridView, volume, true);
        } else {
          showMPRView(gridView, volume, false);
        }
      };
      setViewProperties(gridView, {
        setAnatomyHidden: setAnatomyHidden,
        isAnatomyVisible: function () {return 3 === gridView.getSize()[1];}
      });
      if (undefined === hideAnatomy) {
        hideAnatomy = isSmallScreen();
      }

      animate.setTransition(gridView.table, "");
      gridView.table.style.opacity = 0.0;

      var gV = gridView;
      if (!hideAnatomy) {
        gV.setSize(2,3);

        gV.getView(1,0).setVolumes([MPR.reformatVolume(volume.getLabelMap(),2)]);
        gV.getView(1,1).setVolumes([MPR.reformatVolume(volume,2)]);
        gV.getView(1,1).linkView(gV.getView(1,0));

        gV.getView(0,1).setVolumes([MPR.reformatVolume(volume, 1)]);
        gV.getView(0,0).setVolumes([MPR.reformatVolume(volume.getLabelMap(),1)]);
        gV.getView(0,1).linkView(gV.getView(0,0));

        gV.getView(0,2).setVolumes([MPR.reformatVolume(volume,0)]);
        gV.getView(1,2).setVolumes([MPR.reformatVolume(volume.getLabelMap(), 0)]);
        gV.getView(0,2).linkView(gV.getView(1,2));
      } else {
        gV.setSize(2,2);
        gV.getView(0,0).setVolumes([MPR.reformatVolume(volume, 1)]);
        gV.getView(0,1).setVolumes([MPR.reformatVolume(volume,0)]);

        gV.getView(1,0).setVolumes([MPR.reformatVolume(volume,2)]);
      }


      gridView.resize();
      draw.requestRedraw();

      animate.setTransition(gridView.table, "opacity 0.5s ease-in");

      gridView.table.style.opacity=1.0;
    }

    return {
      showSingleView: showSingleView,
      showMPRView: showMPRView,
      showOverview: showOverview,

      setViewProperties: setViewProperties
    };
  });
