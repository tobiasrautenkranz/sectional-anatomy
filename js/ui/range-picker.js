/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2018 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["ui/widgets"],
  function (widgets) {
    "use strict";

    function showRangePicker (value, element, parentElement,
        onupdate, onchange) {

      var div = document.createElement("div");
      div.className = "range-picker";
      div.style.top = "-3.2em";
      var offset = element.offsetLeft;
      if (offset < parentElement.clientWidth/2) {
        div.style.left = offset + 1.2*element.clientWidth + "px";
        div.style.borderRadius = "1em 1em 1em 0";
      } else {
        div.style.right = parentElement.clientWidth - offset  + "px";
        div.style.borderRadius = "1em 1em 0 1em";
      }

      div.style.touchAction = "none";

      // prevent stuff from happening "below" the div
      div.addEventListener("click", function (e) {
        e.stopPropagation();
      });
      div.addEventListener("mousedown", function (e) {
        e.stopPropagation();
      });

      var keepInWindow = function () {
        var left = div.offsetLeft;
        var right = left + div.clientWidth;
        if (left < 0) {
          left = 0;
        } else if (right > parentElement.clientWidth) {
          left = left - element.clientWidth;
        }
        div.style.left = left + "px";
        div.style.right = "";
      };

      var done = function () {
        parentElement.removeChild(div);
        window.removeEventListener("mousedown", done);
        onchange();
      };
      window.addEventListener("mousedown", done, { passive: false });

      var close = widgets.makeButton("X");
      close.addEventListener("click", done);
      close.style.background = "#222";
      close.style.float = "right";
      div.appendChild(close);

      var slider = widgets.makeSlider(onupdate);
      slider.style.width = "85%";
      slider.setValue(value);
      div.appendChild(slider);

      parentElement.appendChild(div);
      keepInWindow();
    }

    // UI popup to select a floating point value between 0 and 1.
    function makeRangePicker (element, parentElement, onupdate, onchange) {
      var button = document.createElement("input");
      button.type = "button";
      button.style.touchAction = "none";

      var _onupdate = function(v) {
        button.value = v;
        onupdate(v);
      };
      var show = false;
      var _onchange = function () {
        show = false;
        onchange();
      };
      var onclick = function () {
        if (!show) {
          show = true;
          showRangePicker(button.value||0, element, parentElement,
              _onupdate, _onchange);
        }
      };
      button.addEventListener("click", onclick);
      return button;
    }

    return makeRangePicker;
  });
