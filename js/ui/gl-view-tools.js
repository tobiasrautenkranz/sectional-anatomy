/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2012 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "view/redraw", "./tabs", "./widgets",
 "vr/volume-rendering", "ui/gradient-editor"],
  function (utils, draw, tabs, widgets, vr, GradientEditor) {
    "use strict";

    // Volume rendering settings
    var GLViewTools = function (parentElement, viewer) {
      tabs.Overlay.call(this, parentElement);

      var gridView = viewer.gridView;
      this.gridView = gridView;
      var anatomy = viewer.anatomyTools;
      this.anatomy = anatomy; // hack for broken vr

      this.shaderSettings = document.createElement("div");

      this.rendererConfig = document.createElement("div");

      this.renderers = vr.getRenderers(function (id) {
        anatomy.selectAnatomy(id);
      });

      var overlayElement = this.overlayElement;

      // Shaders
      var shaders = document.createElement("ul");
      var makeSetRenderFunktion = function (renderer) {
        return function () {
          var glView = gridView.getView(0,0);
          glView.setRenderer(renderer);
          anatomy.selectionMode = renderer.pickMode;
        };
      };
      for (var i=0; i<this.renderers.length; i++) {
        var button = widgets.makeButton(this.renderers[i].name,
            makeSetRenderFunktion(this.renderers[i]));
        var li = document.createElement("li");
        li.appendChild(button);
        li.style.display = "inline";
        shaders.appendChild(li);
      }

      overlayElement.appendChild(shaders);


      // Shader Quality
      var shaderQuality = document.createElement("ul");
      var mkSetShaderQuality = function (quality) {
        return function () {
          // FIXME catch link fail in continuation (shader complexity)
          gridView.getView(0,0).setShaderQuality(quality);
        };
      };

      var shaderQualities = vr.getShaderQualities();
      for (var i=0; i<shaderQualities.length; i++) {
        var li = document.createElement("li");
        li.style.display = "inline";
        li.appendChild(widgets.makeButton(shaderQualities[i].name,
              mkSetShaderQuality(shaderQualities[i].quality)));
        shaderQuality.appendChild(li);
      }
      overlayElement.appendChild(document.createTextNode("quality"));
      overlayElement.appendChild(shaderQuality);

      this.shaderSettings.appendChild(this.rendererConfig);

      // LUT / Transfer Function
      this.lutDiv = document.createElement("div");
      this.lutDiv.appendChild(document.createTextNode("transfer function"));
      this.lutPresets = document.createElement("div");
      this.lutDiv.appendChild(this.lutPresets);

      this.lut = new GradientEditor(this.lutDiv, "RGBA");
      this.lut.onChange(function () {gridView.getView(0,0).requestRedrawHQ();});
      this.lut.onUpdate(function () {draw.requestRedraw();});

      this.shaderSettings.appendChild(this.lutDiv);

      // Bounding Box
      this.shaderSettings.appendChild(document.createTextNode("bounding box"));
      var rangeChanged = function(view) {
        view.vrcanvas.vr.cube.updateRange(view.vrcanvas.vr.gl);

        draw.requestRedraw();
      };
      var makeRangeChangeFunction = function (index) {
        return function (interval) {
          var glView = gridView.getView(0,0);
          glView.vrcanvas.vr.cube.range[index] = interval;
          rangeChanged(glView);
          draw.requestRedraw();
        };
      };

      for (var i=0; i<3; i++) {
        var rangeSlider = widgets.makeIntervalSlider(makeRangeChangeFunction(i));
        this.shaderSettings.appendChild(rangeSlider);
      }


      //Add divs
      overlayElement.appendChild(this.shaderSettings);

      if (utils.debug) {
      overlayElement.appendChild(widgets.makeButton("lose context",
            function () {
              viewer.gridView.getView(0,0).testContextLost();
            }));
      }
    };
    GLViewTools.prototype = Object.create(tabs.Overlay.prototype);

    GLViewTools.prototype.setRenderer = function (renderer) {
      var glView = this.gridView.getView(0,0);
      var lutChannels = renderer.lut && renderer.lut.channels;

      if (this.renderer && this.renderer.lut) {
        this.renderer.lut.current = this.lut.save();
      }
      this.renderer = renderer;

      var lut = null;
      if (renderer.lut) {
        lut = renderer.lut.current;
      }

      if (lutChannels) {
        var hasDrawBuffersExt = glView.vrcanvas.vr.drawBuffersExt;
        if ("RGBA" === lutChannels && !hasDrawBuffersExt) {
          lutChannels = "LA";
        }
        this.lut.setChannels(lutChannels, lut);
        this.lutDiv.style.display = "";
      } else {
        this.lutDiv.style.display = "none";
      }

      this.lutPresets.innerHTML = "";
      var volume = glView.getVolume();
      if (renderer.lut && renderer.lut.presetsCT && volume.HUMinMax) {
        this.lutPresets.appendChild(document.createTextNode("presets"));
        var presets = renderer.lut.presetsCT;

        var myLUT = this.lut;
        var mkSet = function (p) {
          return function set () {
            myLUT.load(vr.getLUTPresetHU(presets[p], volume.HUMinMax));
            glView.requestRedrawHQ();
          };
        };

        for (var p in presets) {
          var button = widgets.makeButton(p, mkSet(p));
          this.lutPresets.appendChild(button);
        }

        if (utils.debug) {
          var toHU = function (p) {
            return Math.round((volume.HUMinMax[1] - volume.HUMinMax[0])*p +
                volume.HUMinMax[0]);
          };
          if (!volume.HUMinMax) {
            toHU = function (p) {
              return p;
            };
          }
          this.lutPresets.appendChild(widgets.makeButton("print",
                function () {
                  var s = this.lut.save();
                  var c = s.colorStops.map(function (x) {
                    return {
                      color: [x.color.r, x.color.g, x.color.b],
                      HU: toHU(x.position)
                    };
                  });
                  var a = s.alphaStops.map(function (x) {
                    return {
                      alpha: x.color.a,
                      HU: toHU(x.position)
                    };
                  });
                  console.log("LUT",
                      JSON.stringify({colorStops: c, alphaStops: a}));
                }.bind(this)));
        }
      }

      var config = renderer.config;

      var configDiv = this.rendererConfig;
      configDiv.innerHTML = "";

      var t = this;
      var mkUpdateFunktion = function (property) {
        return function (value) {
          var cfg = {};
          cfg[property] = value;
          var glView = t.gridView.getView(0,0);
          glView.vrcanvas.vr.updateRenderer(cfg);
          glView.requestRedrawHQ();

          config[property] = value;
        };
      };

      this.update1f = null;
      var get1fValue = function (slider) {
        return function () {
          return slider.getValue();
        };
      };
      var set1fValue = function (slider) {
        return function (value) {
          slider.updateValue(value);
        };
      };

      for (var p in config) {
        var c = config[p];
        if ("normal" !== c.type) {
          var value = c.value || c;
          if (c.valueCT && volume.HUMinMax) {
            value = vr.fromHU(c.valueCT, volume.HUMinMax);
            mkUpdateFunktion(p)(value);
          }
          var name = p;

          configDiv.appendChild(document.createTextNode(name));
          var slider = widgets.makeSlider(mkUpdateFunktion(p));
          slider.setValue(value);
          configDiv.appendChild(slider);

          if (null === this.update1f) {
            this.update1f = set1fValue(slider);
            this.get1f = get1fValue(slider);
          }
        }
      }
    };

    return GLViewTools;
  });
