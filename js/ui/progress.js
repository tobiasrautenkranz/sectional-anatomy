/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(function () {
  "use strict";

  function makeProgress() {
    var div = document.createElement("div");
    div.id = "progress";

    var p = document.createElement("p");
    p.style.marginTop = "auto";
    p.style.marginBottom = "auto";
    div.appendChild(p);

    var loading = new Image();
    loading.src = "/img/loading.gif";
    div.appendChild(loading);

    document.body.appendChild(div);

    return {div: div, p: p};
  }

  var progress;
  // display a loading progress message (overwriting any previous message)
  function updateProgress(message) {
    progress = progress || makeProgress();
    progress.p.innerHTML = message;
  }

  // hide any progress message
  function progressDone () {
    if (progress) {
      document.body.removeChild(progress.div);
      progress = undefined;
    }
  }

  return {
    update: updateProgress,
      done: progressDone
  };
});
