/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2018 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "./tabs", "./widgets", "volume/volume", "volume/lut"],
  function (utils, tabs, widgets, volumes) {
    "use strict";

    var Settings = function (parentElement, viewer) {
      tabs.Overlay.call(this, parentElement);
      this.study = viewer.study;
      this.viewer = viewer;
    };

    Settings.prototype = new tabs.Overlay();

    var languageCodes = {
      de: "Deutsch",
      en: "English",
      la: "Latin"
    };
    function getLanguageName(code) {
      var name = languageCodes[code];
      if (name) {
        return name;
      }

      return code;
    }

    Settings.prototype.setVisible = function () {
      var study = this.study;


      if (!this.langList) {
        var anatomyInfo = this.viewer.anatomyInfo;
        var anatomyTools = this.viewer.anatomy;
        var updateLabels = function () {
          anatomyInfo.updateText(); anatomyTools.updateLabels();
        };
        this.langList = document.createElement("ul");
        this.langList.className = "languages";
        var setLanguage = function () {
                study.language = this.dataID;
                study.labelMap.loadLabels(updateLabels, study);
        };
        for (var i=0; i<study.labels.languages.length; i++) {
          var li = document.createElement("li");
          var button =
            widgets.makeButton(getLanguageName(study.labels.languages[i]),
              setLanguage);
          button.dataID = i;
          li.appendChild(button);
          this.langList.appendChild(li);
        }
        var language = document.createElement("h2");
        language.textContent = "Anatomy Language";
        this.getElement().appendChild(language);
        this.getElement().appendChild(this.langList);

        var about = document.createElement("h2");
        about.textContent = "About";
        this.getElement().appendChild(about);
        var aboutP = document.createElement("p");
        aboutP.innerHTML =
          'Sectional Anatomy Viewer ' + utils.versionNumber +
          ' Copyright (C) 2012 - 2018 ' +
          '<a href="&#109ailto:&#109ail&#x40;tobias.rautenkranz&#46;c&#104;">' +
          'Tobias Rautenkranz</a>' +
          "<br>This program comes with ABSOLUTELY NO WARRANTY; " +
          'This is free software, and you are welcome to redistribute it ' +
          'under certain conditions; see <a target="_blank" href="/js/LICENSE">'+
          'LICENSE</a> for details. '+
          '<br><br><a target="_blank" href="/dev.html">source code</a>';
        this.getElement().appendChild(aboutP);

        var data = document.createElement("div");
        var dataH = document.createElement("h3");
        dataH.appendChild(document.createTextNode("data"));
        data.appendChild(dataH);
        data.appendChild(document.getElementById("data-info").childNodes[0].cloneNode());
        data.appendChild(document.createElement("br"));
        data.appendChild(document.createTextNode("(see bottom left corner)"));
        this.getElement().appendChild(data);

        var hdiv = document.createElement("div");
        hdiv.appendChild(document.createElement("br"));
        var home = document.createElement("a");
        home.href = "/";
        home.innerHTML = "&#x2302; Home";
        hdiv.appendChild(home);
        this.getElement().appendChild(hdiv);

        var features = document.createElement("div");
        features.innerHTML = '<p>For 3D Rendering see: '+
          '<a href="http://get.webgl.org" target="_blank">WebGL</a>';

        this.getElement().appendChild(features);
      }
      tabs.Overlay.prototype.setVisible.call(this);
    };

    return Settings;
  });
