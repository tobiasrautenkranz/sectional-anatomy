/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "ui/range-picker", "ui/gradient-canvas"],
  function (utils, makeRangePicker, GradientCanvas) {
    "use strict";

    function makeColor (r, g ,b, a) {
      if ("string" === typeof r) {
        var c = r;

        r = parseInt(c.slice(1,3), 16);
        g = parseInt(c.slice(3,5), 16);
        b = parseInt(c.slice(5,7), 16);
      } else if ("object" === typeof r) {
        a = r.a;
        b = r.b;
        g = r.g;
        r = r.r;
      } else {
        if (undefined === g) {
          g = r;
        }
        if (undefined === b) {
          b = r;
        }
      }

      var color = {
        r: r,
        g: g,
        b: b
      };

      if (a !== undefined) {
        color.a = a;
      }

      return color;
    }

    function toHex2 (channel) {
      var h = channel.toString(16);
      if (1 == h.length) {
        h = "0" + h;
      }

      return h;
    }

    function colorToString (color) {
      return "#" + toHex2(color.r) + toHex2(color.g) + toHex2(color.b);
    }
    function alphaToColorString (alpha) {
      var p = Math.round(100 * (1 - alpha));
      return "hsl(0,0%," + p + "%)";
    }

    function toPercentage (ratio) {
      return Math.round(ratio * 100) + "%";
    }

    var CachingGradientCanvas = function (canvas) {
      GradientCanvas.call(this, canvas);
      this.needsUpdate = true;
    };
    CachingGradientCanvas.prototype = Object.create(GradientCanvas.prototype);

    CachingGradientCanvas.prototype.modified = function () {
      this.needsUpdate = true;
    };
    CachingGradientCanvas.prototype.updateImageData = function () {
      if (this.needsUpdate) {
        GradientCanvas.prototype.updateImageData.call(this);
      }
      this.needsUpdate = false;
    };

    // UI to edit a gradient (color or gray scale):
    var GE = function (parentElement, channels) {
      this.div = document.createElement("div");



      this.canvas = document.createElement("canvas");
      this.canvas.style.width = "100%";
      this.canvas.style.height = "100%";
      this.canvas.style.position = "absolute";
      this.gradientCanvas = new CachingGradientCanvas(this.canvas);
      this.div.appendChild(this.canvas);

      this.setChannels(channels);
      /*
      // FIXME track / preview on mousedown
      this.div.addEventListener("mousedown", function (e) {
      var position = (e.pageX - utils.getAbsoluteOffset(this.div)[0]-this.div.clientLeft) /
      this.div.clientWidth;
      position = utils.clamp(position, 0, 1);

      var stop = this.makeStop(position);
      var yPos =  (e.pageY - utils.getAbsoluteOffset(this.div)[1]);
      if (!this.noAlpha && yPos - this.div.clientHeight/2 < 0) {
      stop.className = "gradient-editor-drop-top";
      }
      hideStop({element: stop}, false);

      this.div.appendChild(stop);
      var track = function (e) {
      var position = (e.pageX - utils.getAbsoluteOffset(this.div)[0]-this.div.clientLeft) /
      this.div.clientWidth;

      var yPos =  (e.pageY - utils.getAbsoluteOffset(this.div)[1]);

      if (position < 0 || 1 < position ||
      yPos < 0 || this.div.clientHeight < yPos) {
      hideStop({element: stop}, true);
      } else {
      hideStop({element: stop}, false);
      }

      if (!this.noAlpha) {
      if (yPos - this.div.clientHeight/2 < 0) {
      stop.className = "gradient-editor-drop-top";
      } else {
      stop.className = "gradient-editor-drop";
      }
      }
      stop.style.left = toPercentage(position);
      }.bind(this);
      window.addEventListener("mousemove", track);
      var done = function (e) {
      this.div.removeChild(stop);
      window.removeEventListener("mousemove", track);
      window.removeEventListener("mouseup", done, true);
      }.bind(this);
      window.addEventListener("mouseup", done, true);

      }.bind(this));
      */

      this.div.addEventListener("click", function (e) {
        var position = (e.pageX - utils.getAbsoluteOffset(this.div)[0] -
            this.div.clientLeft) / this.div.clientWidth;
        position = utils.clamp(position, 0, 1);

        var yPos =  (e.pageY - utils.getAbsoluteOffset(this.div)[1]);
        if (!this.noAlpha && yPos - this.div.clientHeight/2 < 0) {
          this.addAlphaStop(position);
        } else {
          this.addColorStop(position);
        }
        this.needsUpdateStops();
      }.bind(this));

      parentElement.appendChild(this.div);
    };

    GE.prototype.setChannels = function (channels, data) {
      if ("RGB" === channels) {
        this.noAlpha = true;
        this.rgb = true;
      } else if ("L" === channels) {
        this.noAlpha = true;
        this.rgb = false;
      } else if ("RGBA" === channels) {
        this.noAlpha = false;
        this.rgb = true;
      } else if ("LA" === channels) {
        this.noAlpha = false;
        this.rgb = false;
      }

      if (this.noAlpha) {
        this.div.className = "gradient-editor";
      } else {
        this.div.className = "gradient-editor gradient-editor-drop";
      }

      if (!data) {
        data = {
          colorStops: [
          {position: 0, color: makeColor(0)},
          {position: 1, color: makeColor(255)},
          ]
        };
        if (this.noAlpha) {
          data.alphaStops = [];
        } else {
          data.alphaStops = [
          {position: 0, color: makeColor(0,0,0,0)},
          {position: 1, color: makeColor(255)}];
        }
      }
      this.load(data);
    };


    GE.prototype.addColorStop = function (position, color) {
      if (!color) {
        color = this.gradientCanvas.color(position);
      }

      var colorStop = this.addStop(position, color);
      colorStop.element.style.backgroundColor = colorToString(colorStop.color);

      var getValue = function () { return colorPicker.value; };
      var change =  function () {
        colorStop.color = makeColor(getValue());
        colorStop.element.style.backgroundColor = colorToString(colorStop.color);
        this.requestUpdate();

      }.bind(this);

      var done = function () {
        if (this.onchange) {
          this.onchange();
        }
      }.bind(this);

      var colorPicker;
      if (this.rgb) {
        colorPicker = document.createElement("input");
        try {
          colorPicker.type = "color";
        } catch (e) {
          // FIXME color not supported (e.g. throws in IE)
        }
        colorPicker.addEventListener("change", function () {change(); done();});
        colorPicker.value = colorToString(color);
      } else {
        // input converts value to string!
        getValue = function () {
          return Math.round(parseFloat(colorPicker.value)*255);
        };
        colorPicker = makeRangePicker(colorStop.element, this.div, change, done);
        colorPicker.className = "grandient-editor";
        colorPicker.value = color.r/255;
      }
      colorStop.element.appendChild(colorPicker);

      return colorStop;
    };
    GE.prototype.makeStop = function (position) {
      var stop = document.createElement("span");
      if (!this.noAlpha) {
        stop.className = "gradient-editor-drop";
      }
      stop.style.left = toPercentage(position);

      return stop;
    };

    GE.prototype.addStop = function (position, color) {
      var stop = this.makeStop(position);

      var c = {
        element: stop,
        color: color,
        position: position,
      };

      this.gradientCanvas.addStop(position, c);

      var moved = false;
      stop.addEventListener("click", function (e) {
        if (moved) {
          e.preventDefault();
          e.stopPropagation();
        }
      }.bind(this), true);
      stop.addEventListener("click", function (e) {
        // stop bubbling up
        e.stopPropagation();
      }.bind(this));

      var removed = false;
      var xStart;
      var positionStart;
      var id;
      var stops = this.gradientCanvas.stopsForValue(c);
      var start = function (e) {
        e.stopPropagation();
        moved = false;
        xStart = e.clientX;
        positionStart = c.position;
        id = this.gradientCanvas.stopID(c);
      }.bind(this);
      var track =  function (e) {
        moved = true;
        var delta = (e.clientY - utils.getAbsoluteOffset(this.div)[1]);
        var height = this.div.clientHeight;
        if ((delta < - 0.4*height || delta > 1.4*height) &&
            stops.length > 2) {
          if (!removed) {
            removed = true;
            hideStop(c, true);
            this.requestUpdate();
          }
          return;
        } else if (removed) {
          removed = false;
          hideStop(c, false);
        }

        var position = positionStart + (e.clientX - xStart) /
          this.div.clientWidth;
        c.position = position;
        this.gradientCanvas.stopClampPosition(stops, id);
        stop.style.left = toPercentage(c.position);
        this.requestUpdate();
      }.bind(this);
      var end = function () {
        if (removed) {
          this.removeStop(c);
          removed = false;
        }
        if (this.onchange) {
          this.onchange();
        }
      }.bind(this);
      utils.makeDragEventListener(stop, start, track, end);

      this.div.appendChild(stop);

      return c;
    };
    GE.prototype.addAlphaStop = function (position, alpha) {
      var color = {a: alpha};
      if (undefined === alpha) {
        color.a = this.gradientCanvas.color(position).a;
      }

      var stop = this.addStop(position, color);
      stop.element.className = "gradient-editor-drop-top";
      stop.element.style.backgroundColor = alphaToColorString(color.a);

      var picker = makeRangePicker(stop.element, this.div,
          function (value) {
            stop.color.a = value;
            stop.element.style.backgroundColor = alphaToColorString(color.a);
            this.requestUpdate();
          }.bind(this),
          function onchange () {
            if (this.onchange) {
              this.onchange();
            }
          }.bind(this));
      picker.value = color.a;
      picker.className = "grandient-editor";
      stop.element.appendChild(picker);

      return stop;
    };

    function hideStop (stop, hide) {
      if (hide) {
        stop.element.style.visibility = "hidden";
      } else {
        stop.element.style.visibility = "";
      }
    }

    GE.prototype.removeStop = function (stop) {
      this.div.removeChild(stop.element);
      this.gradientCanvas.removeStop(stop);
    };


    GE.prototype.requestUpdate = function () {
      this.needsUpdateStops();
      if (this.onupdate) {
        this.onupdate();
      }

      if (!this.pendingUpdate) {
        this.pendingUpdate = true;
        window.requestAnimationFrame(function () {
          this.pendingUpdate = false;
          this.updateGradient();
        }.bind(this));
      }
    };

    GE.prototype.changed = function () {
      this.needsUpdateStops();
      this.updateGradient();

      if (this.onchange) {
        this.onchange();
      }
    };


    GE.prototype.needsUpdateStops = function () {
      this.gradientCanvas.modified();
    };

    GE.prototype.toArray = function () {
      return new Uint8Array(this.gradientCanvas.getArray());
    };


    GE.prototype.updateGradient = function () {
      this.gradientCanvas.draw();
    };


    /** when the update is finished */
    GE.prototype.onChange = function (onchange) {
      this.onchange = onchange;
    };
    /** interactive  */
    GE.prototype.onUpdate = function (onupdate) {
      this.onupdate = onupdate;
    };

    GE.prototype.save = function () {
      return this.gradientCanvas.save();
    };

    function toGray(color) {
      var gray = Math.round((color.r + color.g + color.b) / 3);
      color.r = gray;
      color.g = gray;
      color.b = gray;

      return color;
    }

    GE.prototype.load = function (data) {
      var remove = function (stop) {
        this.div.removeChild(stop.element);
      }.bind(this);

      if (this.gradientCanvas.colorStops) {
        this.gradientCanvas.colorStops.forEach(remove);
        this.gradientCanvas.alphaStops.forEach(remove);
      }

      this.gradientCanvas.colorStops = [];
      this.gradientCanvas.alphaStops = [];

      var i, c;

      for (i=0; i<data.colorStops.length; i++) {
        c = data.colorStops[i];
        if (!this.rgb) {
          c.color = toGray(c.color);
        }
        this.addColorStop(c.position, c.color);
      }

      for (i=0; i<data.alphaStops.length; i++) {
        c = data.alphaStops[i];
        this.addAlphaStop(c.position, c.color.a);
      }

      this.changed();
    };


    GE.prototype.changeWindow = function (deltaCenter, deltaWidth) {
      this.gradientCanvas.changeWindow(deltaCenter, deltaWidth);

      var caStops = [this.gradientCanvas.colorStops, this.gradientCanvas.alphaStops];
      caStops.forEach(function (stops) {
        for (var i = 0; i < stops.length; i++) {
          var stop = stops[i];
          stop.element.style.left = toPercentage(stop.position);
        }
      });

      this.needsUpdateStops();
      this.updateGradient();
    };


    return GE;
  });

