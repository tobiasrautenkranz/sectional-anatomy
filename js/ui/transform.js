/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(function () {
  "use strict";

  /** Scales ELEMENT by the factor ZOOM. */
  function getElementTransformProperty(element) {
    var properties = ["transform",
      "WebkitTransform",
      "msTransform",
      "MozTransform",
      "OTransform"];
    var p;
    while ((p = properties.shift())) {
      if (typeof element.style[p] != "undefined") {
        return p;
      }
    }
    return undefined;
  }
  var elementTransformProperty =
    getElementTransformProperty(document.createElement("img"));

  function scaleElement(element, zoom) {
    element.style[elementTransformProperty] = "scale(" + zoom[0] + ", " +
                                                         zoom[1] + ")";
  }

  function getElementZoom(element) {
    var transform =  element.style[elementTransformProperty];
    if (transform) {
      var scale = transform.substr("scale(".length,transform.length-1).split(",");
      scale =  scale.map(parseFloat);
      if (scale.length === 1) {
        return [scale[0], scale[0]];
      } else {
        return scale;
      }
    } else {
      return [1.0, 1.0];
    }
  }

  return {
    elementTransformProperty: elementTransformProperty,
    scaleElement: scaleElement,
    getElementZoom: getElementZoom
  };

});
