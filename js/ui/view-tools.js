/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "./tabs", "./widgets", "view/redraw", "volume/ppvolume",
    "view/view", "volume/lut"],
  function (utils, tabs, widgets, draw, pp, View, luts) {
    "use strict";

    // Settings for the 2D slice view.
    // e.g.: LUT & slice thickness
    var ViewTools = function (parentElement, viewer) {
      tabs.Overlay.call(this, parentElement);

      var study = viewer.study;
      var gridView = viewer.gridView;
      this.gridView = gridView;

      var definedLUTs = [
      {title: "default", getWindow:
        function (center, width, volume) {return volume.defaultWindow;} },
      {title: "min-max", getWindow:
        function (center, width) {return [255/2, width >=0 ? 255 : -255];} },
      {title: "invert",  getWindow:
        function (center, width) {return [center, -width];} }
      ];

      if (study.volumes[0].type === "CT" && study.volumes[0].HUMinMax) {
        var windowFromHU = function (volume, center, width) {
          var HUmin = volume.HUMinMax[0];
          var HUmax = volume.HUMinMax[1];

          center = 255*(center - HUmin)/(HUmax - HUmin);
          width = 255/(HUmax - HUmin)*width;
          return [center, width];
        };
        var makeHUWindowFunktion = function (center, width) {
          return function (c, w, volume) {
            return windowFromHU(volume, center, width);
          };
        };
        definedLUTs = [
        {title: "abdomen", getWindow: makeHUWindowFunktion(50,   350)},
        {title: "lung",    getWindow: makeHUWindowFunktion(-600, 1500)},
        {title: "bone",    getWindow: makeHUWindowFunktion(300,  1500)},
          // {title: "brain",   getWindow: makeHUWindowFunktion(35,   100)},
        {title: "invert",  getWindow:
          function(center, width) {return [center, -width];}}
        ];
      }

      this.LUTs = [];
      var windowList = document.createElement("ul");
      windowList.className = "lut-window-list";
      this.views = [];
      var setLut = function(e) {
        gridView.getSelectedView().getVolume().getLUT().setWindow(
            e.currentTarget.lut.getCenter(), e.currentTarget.lut.getWidth());
        t.updateLUTs();
        draw.requestRedraw();
      };

      for (var i=0; i<definedLUTs.length; i++) {
        var defLUT = definedLUTs[i];
        var volume = study.volumes[0];
        var lut;

        lut = new luts.GrayLUT();
        lut.getWindow = defLUT.getWindow;
        this.LUTs[i] = lut;
        volume = new pp.ExtraLUTVolume(study.volumes[0], lut);
        var t = this;
        var btn = widgets.makeButton("", setLut);
        btn.lut = lut;

        var div = document.createElement("div");
        div.style.height = "5em";
        //div.style.height = this.getElement().clientHeight/6+"px";
        div.style.width = div.style.height;
        btn.appendChild(div);

        var txtDiv = document.createElement("div");
        var txt = document.createElement("p");
        txt.innerHTML= definedLUTs[i].title;
        txtDiv.appendChild(txt);
        btn.appendChild(txtDiv);

        var lview = new View(div, gridView.index, {interactive: false});
        lview.setVolumes([volume]);
        this.views[i] = lview;


        var li = document.createElement("li");
        li.appendChild(btn);
        windowList.appendChild(li);
      }

      this.getElement().appendChild(windowList);

      this.getElement().appendChild(document.createTextNode("window min/max "));

      this.windowSlider = widgets.makeIntervalSlider(function(interval) {
        var lut = gridView.getSelectedView().getVolume().getLUT();
        var sign = lut.getWidth() < 0 ? -1 : 1;
        var width = 256*(interval[1] - interval[0]);
        var center = 256*(interval[1] + interval[0])/2;
        lut.setWindow(center, sign * width);
        t.updateLUTs();
        draw.requestRedraw();
      });
      this.getElement().appendChild(this.windowSlider);

      var projectionMethod = "MIP";
      var sliceThickness = widgets.makeSlider(
          function(value) {
            var thicknessRatio = parseFloat(value);
            if (!thicknessRatio) {
              thicknessRatio = 0;
            }
            thicknessRatio = Math.max(0,Math.min(thicknessRatio, 1));

            var activeVolume = gridView.getSelectedView().getVolume();
            var slices = Math.round(thicknessRatio * activeVolume.getSize()[2]);
            if (slices <= 1.0) {
              if (activeVolume.thickness) {
                gridView.getSelectedView().setVolumes([activeVolume.baseVolume]);
              }
            } else {
              if (activeVolume.thickness) {
                activeVolume.setThickness(slices);
              } else {
                if (activeVolume instanceof pp.ObliqueMPRVolume) {
                  sliceThickness.setValue(0);
                } else {
                  gridView.getSelectedView().setVolumes(
                      [new pp.MIPVolume(activeVolume, slices, projectionMethod)]);
                }
              }
            }
            var deps = gridView.getDependantViews(gridView.getSelectedView());
            var newLabelMap =
              gridView.getSelectedView().getVolume().getReformatedLabelMap();
            for (var i=0; i<deps.length; i++) {
              deps[i].setVolumes([newLabelMap]);
            }
            draw.requestRedraw();
          });
      var ppDiv = document.createElement("div");

      ppDiv.appendChild(document.createTextNode("thickness"));
      ppDiv.appendChild(sliceThickness);
      this.sliceThickness = sliceThickness;
      var setProjection = function (method) {
        projectionMethod = method;

        var thicknessRatio =
          utils.clamp(parseFloat(sliceThickness.getValue()), 0, 1);
        var activeVolume = gridView.getSelectedView().getVolume();
        var slices = Math.round(thicknessRatio * activeVolume.getSize()[2]);
        if (slices > 1.0) {
          if (activeVolume.thickness) {
            activeVolume = activeVolume.baseVolume;
          }
          var pVolume;
          if ("AvgIP" == projectionMethod) {
            pVolume = new pp.AVGVolume(activeVolume, slices);
          } else {
            pVolume = new pp.MIPVolume(activeVolume, slices, projectionMethod);
          }
          gridView.getSelectedView().setVolumes([pVolume]);
          var deps = gridView.getDependantViews(gridView.getSelectedView());
          var newLabelMap =
            gridView.getSelectedView().getVolume().getReformatedLabelMap();
          for (var i=0; i<deps.length; i++) {
            deps[i].setVolumes([newLabelMap]);
          }
          draw.requestRedraw();
        }
      };
      ppDiv.appendChild(widgets.makeButton(
            ["MIP", "Maximum Intensity Projection"],
            function () { setProjection("MIP"); }));
      ppDiv.appendChild(widgets.makeButton(
            ["MinIP", "Minimum Intensity Projection"],
            function () { setProjection("MinIP"); }));
      ppDiv.appendChild(widgets.makeButton(["AvgIP","Average"],
            function () { setProjection("AvgIP"); }));

      this.getElement().appendChild(ppDiv);
    };
    ViewTools.prototype = new tabs.Overlay();

    ViewTools.prototype.updateLUTs = function () {
      var volume = this.gridView.getSelectedView().getVolume().getSourceVolume();
      var currentLUT = volume.getLUT();
      for (var i=0; i<this.LUTs.length; i++) {
        var lut = this.LUTs[i];
        if (lut.getWindow) {
          var w = lut.getWindow(currentLUT.getCenter(), currentLUT.getWidth(),
              volume);
          lut.setWindow(w[0], w[1]);
        }
      }

      var center = currentLUT.getCenter();
      var width = Math.abs(currentLUT.getWidth());
      var min = center - width/2;
      var max = center + width/2;
      this.windowSlider.setInterval(min/256, max/256);
    };

    ViewTools.prototype.setVisible = function () {
      tabs.Overlay.prototype.setVisible.call(this);
      for (var i=0; i<this.views.length; i++) {
        var volume =
          new pp.ExtraLUTVolume(this.gridView.getSelectedView().getVolume(),
              this.LUTs[i]);
        this.views[i].setVolumes([volume]);
      }
      this.updateLUTs();

      var sliceThickness = this.gridView.getSelectedView().getVolume().thickness;
      if (sliceThickness) {
        this.sliceThickness.setValue(sliceThickness/
            this.gridView.getSelectedView().getVolume().getSize()[2]);
      } else {
        this.sliceThickness.setValue(0);
      }

      this.draw();
    };

    ViewTools.prototype.draw = function () {
      if (this.isVisible()) {
        var completed = true;
        for (var i=0; i<this.views.length; i++) {
          completed = this.views[i].draw() && completed;
        }
        return completed;
      } else {
        return true;
      }
    };

    return ViewTools;
  });
