/* Sectional Anatomy
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "ui/transform"],
  function(QUnit, transform) {
    "use strict";

    QUnit.module("ui", function () {
        QUnit.test("transform-scale", function (assert) {
          var element = document.getElementById("qunit-fixture");

          assert.deepEqual(transform.getElementZoom(element), [1.0, 1.0], "no zoom");

          var zoom = [2.2, 3.5];
          transform.scaleElement(element, zoom);
          assert.deepEqual(transform.getElementZoom(element), zoom, "zoomed");
        });
    });
  });
