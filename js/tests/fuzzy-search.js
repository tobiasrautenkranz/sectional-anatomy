/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "util/string-distance"],
  function(QUnit, StringDistance) {
    "use strict";

    QUnit.module("util");

    QUnit.test("string-distance", function (assert) {
      var d = new StringDistance("kitten", 4);
      assert.equal(d.distance("kitten"), 0, "same");
      assert.equal(d.distance("sitting"), 3, "kitten => sitting");
      assert.equal(d.distance("foobar"), 4, "max distance");
      assert.equal(d.distance(""), 6, "empty");

      assert.equal(StringDistance.distance("sitting", "kitten"), 3, "kitten <= sitting");
      assert.equal(StringDistance.distance("", "bar"), 3, "empty lhs");
    });
  });
