/* Sectional Anatomy
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "view/redraw"],
    function(QUnit, draw) {
      "use strict";

      QUnit.module("redraw");

      QUnit.test("redraw", function (assert) {
        var done = assert.async();
        var drawCount = 0;

        var onDraw = {draw: function (highQuality) {
          drawCount++;

          assert.equal(drawCount, 1, "draw once");
          assert.equal(highQuality, false, "highQuality off");
          assert.equal(draw.hasPendingRedraw(), false, "redraw done");

          assert.equal(draw.drawObjects.pop(), onDraw);

          done();

          return true;
        }};
        draw.drawObjects.push(onDraw);

        draw.requestRedraw();
      });

      QUnit.test("redraw HQ", function (assert) {
        var done = assert.async();

        var onDraw = {draw: function (highQuality) {
          assert.equal(highQuality, true, "highQuality on");
          assert.equal(draw.hasPendingRedraw(), false, "redraw done");

          assert.equal(draw.drawObjects.pop(), onDraw);
          done();

          return true;
        }};
        draw.drawObjects.push(onDraw);

        draw.requestRedrawHQ();
      });

      QUnit.test("requestNRedraws", function (assert) {
        var drawCount = 0;
        var done = assert.async();

        draw.drawObjects.push({draw: function (highQuality) {
          drawCount++;

          return true;
        }});

        draw.requestNRedraws(7, function () {
          assert.equal(drawCount, 7, "draw 7 times");

          assert.ok(draw.validFrameDuration(), "has valid frame duration");
          assert.ok(draw.frameDuration() > 0, "frame duration > 0");

          var fps = draw.fps();
          // not that near because of debugger
          assert.ok( 10 < fps && fps < 75, "fps should be near 60; is: " + fps);

          assert.approxEqual(fps, 1000/draw.frameDuration(),
              "fps agrees with frameDuration", 0.5);

          window.setTimeout(function () {
            assert.equal(draw.hasPendingRedraw(), false, "redraw done");
            done();
          }, 10);

        });

      });

      QUnit.test("begin-end-redraw", function (assert) {
        var drawCount = 0;
        var done = assert.async();

        draw.drawObjects.push({draw: function (hq) {
          drawCount++;
          if (5 == drawCount) {
            draw.end();
            window.setTimeout(function () {
              assert.equal(drawCount, 5, "draw 5x");
              done();
            }, 200);
          }
          return true;
        }});

        draw.begin();
      });

      QUnit.test("end-redraw", function (assert) {
        var drawCount = 0;
        var done = assert.async();

        draw.drawObjects.push({draw: function (hq) {
          drawCount++;
          return true;
        }});

        draw.end();
        window.setTimeout(function () {
          assert.equal(drawCount, 1, "draw once");
          done();
        }, 200);
      });

    });
