/* Sectional Anatomy
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "vr/arcball", "lib/gl-matrix"],
    function(QUnit, ArcBall, glm) {
      "use strict";

      var vec2 = glm.vec2;
      var vec3 = glm.vec3;
      var quat = glm.quat;

      QUnit.module("vr");

      QUnit.test("ArcBall", function (assert) {
        var arcBall = new ArcBall();
        var axis = vec3.create();

        assert.equal(arcBall.isRotating(), false, "not rotating");
        arcBall.setStartPosition(vec2.fromValues(0, 0));
        assert.equal(arcBall.isRotating(), true, "rotating");
        arcBall.setPosition(vec2.fromValues(0, 0));

        assert.deepEqual(arcBall.rotation(), [0,0,0,1], "zero rotation");
        assert.equal(arcBall.dragDistance2, 0.0, "dragDistance = 0");

        arcBall.setPosition(vec2.fromValues(1, 0));
        var angle = quat.getAxisAngle(axis, arcBall.rotation());
        assert.equal(angle, Math.PI, "180° y axis");
        assert.deepEqual(axis, [0,1,0], "y axis");

        arcBall.setPosition(vec2.fromValues(Math.SQRT1_2, Math.SQRT1_2));
        angle = quat.getAxisAngle(axis, arcBall.rotation());
        assert.approxEqual(angle, Math.PI, "180° diagonal", 1e-3);
        assert.floatArrayEqual(axis, [-Math.SQRT1_2, Math.SQRT1_2, 0], "diagonal");

        assert.ok(arcBall.dragDistance2 > 0.0, "dragDistance > 0");

        arcBall.setStartPosition(vec2.fromValues(1, 0));
        arcBall.setPosition(vec2.fromValues(0, 1));
        angle = quat.getAxisAngle(axis, arcBall.rotation());
        assert.equal(angle, Math.PI, "180° z");
        assert.deepEqual(axis, [0,0,1], "180° z axis");

        arcBall.setStartPosition(vec2.fromValues(0, 0));
        arcBall.setPosition(vec2.fromValues(0, 1.5));
        angle = quat.getAxisAngle(axis, arcBall.rotation());
        assert.approxEqual(angle, 1.5*Math.PI, "180° overscroll");
        assert.floatArrayEqual(axis, [-1,0,0], "180° axis");

        var rotation = arcBall.rotation();
        arcBall.setRotation(rotation);
        arcBall.endDrag();
        assert.floatArrayEqual(arcBall.rotation(), rotation, "setRotation");
      });
    });
