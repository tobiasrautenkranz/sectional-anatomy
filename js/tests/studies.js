/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "studies"],
    function(QUnit, sstudies) {
      "use strict";

      QUnit.module("studies");

      var makeStudiesList = function () {
        var studiesList = document.createElement("ul");
        var study = document.createElement("li");
        study.innerHTML = '<a href="mra-brain/">' +
          '<h2>MRA Brain vascular</h2>' +
          '<ul class="sequences">' +
          '<li><img src="mra-brain/tof.jpeg" width=64 height=64 /></li>' +
          '<li><img src="mra-brain/a.png" width=64 height=64 /></li>' +
          '</ul>' +
          '</a>';
        studiesList.appendChild(study);
        return studiesList;
      };

      var origin = window.location.origin ||
        (window.location.protocol + "//" + window.location.hostname); // < ie11

      QUnit.test("load", function (assert) {
        var studiesList = makeStudiesList();
        var studies = sstudies.getStudies(studiesList);
        assert.equal(studies.length, 1, "studies.length");

        var study = studies[0];
        assert.equal(study.url, origin + "/mra-brain/", "studies url");
        assert.equal(study.name, "MRA Brain vascular", "studies name");
        assert.equal(study.id, 0, "id");
      });



      QUnit.test("getLabels", function (assert) {
        var study = { url: origin + "/mra-brain",
          name: "MRA Brain vascular"};

        var done = assert.async();

        sstudies.getLabels(study, "en", function(labels) {
          assert.ok(labels.length > 0, "labels.length");
          done();
        });
      });

      QUnit.test("find", function (assert) {
        var studies = [{ url: origin + "/mra-brain",
          name: "MRA Brain vascular"}];

        var s = sstudies.findStudies(studies, "brain");
        assert.equal(s.length, 1, "length");
        assert.equal(s[0], 0, "brain");

        s = sstudies.findStudies(studies, "NOPE");
        assert.equal(s.length, 0, "NOPE");
      });

      QUnit.test("filter", function (assert) {
        var studies = [{
          url: "/mra-brain/",
          name: "MRA BRAIN",
          id: 0
        }];
        var done = assert.async();

        sstudies.filterStudies("foobar", studies, "en",
            function process (searchStrings, searchResults, study, labels) {
              assert.equal(searchResults.ids.length, 0, "ids.length");
              assert.equal(searchStrings, "foobar", "searchstrings");
              assert.equal(study, studies[0], "study");
              assert.notEqual(labels.length, 0, "labels");
            },
            function doneFilter () {
              done();
            });
      });

    });
