/* Sectional Anatomy
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["require", "QUnit", "util/utils"],
function(require, QUnit, utils) {
  "use strict";

  QUnit.module("util", function () {
    QUnit.module("utils", function () {
      QUnit.test("clamp", function (assert) {
        assert.equal(utils.clamp(0.1, -1, 2), 0.1, "clamp noop");
        assert.equal(utils.clamp(-10, -2, 3), -2, "clamp -");
        assert.equal(utils.clamp(12, -2, 3), 3, "clamp +");
      });

      QUnit.test("version", function (assert) {
        assert.equal(utils.majorVersion("3.14"), "3", "major");
        assert.equal(utils.minorVersion("3.14"), "14", "minor");
      });

      if (!console || !console.assert) { // can't catch console.assert
        QUnit.test("assert", function (assert) {
          utils.assert(true, "all is well");

          assert.throws(function () {
            utils.assert(false, "OMFG");
          },
          new Error("assert: OMFG"),
          "assert fail");
        });
      }

      QUnit.test("map", function (assert) {
        var a = [1, 2, 3];
        var b = [1, 1, 1];
        var c = utils.map(function add(a, b) { return a + b; }, a, b);

        assert.equal(c.length, a.length, "result.length");
        for (var i=0; i<c.length; i++) {
          assert.equal(c[i], a[i]+b[i], "result: " + i);
        }
      });

      QUnit.test("sign", function (assert) {
        assert.equal(utils.sign(37), 1, "37");
        assert.equal(utils.sign(-10), -1, "-10");
        assert.equal(utils.sign(0), 1, "0");
      });

      QUnit.module("WebGL", function () {
        QUnit.test("hasWebGL", function (assert) {
          var hasWebGL = utils.hasWebGL();
          assert.ok(hasWebGL === true || hasWebGL === false, "check boolean");

          var hasFastWebGL = utils.hasFastWebGL();
          if (hasWebGL) {
            assert.ok(hasFastWebGL === true || hasFastWebGL === false,
                "hasFastWebGL check boolean");
          } else {
            assert.equal(hasFastWebGL, false, "hasFastWebGL false");
          }
        });

        function testWebGL (assert) {
          var done = assert.async();
          require(["util/gl-utils"], function (glu) {

            var canvas = document.createElement("canvas");
            var gl = glu.getContext(canvas);
            assert.notEqual(gl, null, "gl context");

            glu.checkGLError(gl, "WebGL test");

            done();
          });
        }
        var hasWebGL = utils.hasWebGL();
        if (hasWebGL) {
          QUnit.test("getContext", testWebGL);
        } else {
          QUnit.skip("getContext", testWebGL);
        }
      });

    });
  });
});
