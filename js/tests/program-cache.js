/* Sectional Anatomy
 *
 * Copyright (C) 2015 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "vr/program-cache"],
  function(QUnit, ProgramCache) {
    "use strict";

    QUnit.test("ProgramCache", function (assert) {
      var updateCount = 0;

      var programCache = new ProgramCache(function (renderer, cont) {
        updateCount++;
        cont(renderer); // identity (for testing)
      });

      var renderer1 = {directory: "1"};

      programCache.get(renderer1, function (p) {
        assert.equal(p, renderer1, "get program");
        assert.equal(updateCount, 1, "get program");

        programCache.get(renderer1, function (p) {
          assert.equal(p, renderer1, "get program");
          assert.equal(updateCount, 1, "get program");
        });
      });


    });
  });
