/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "volume/study", "volume/volume", "volume/axis-numbers"],
    function(QUnit, studyLoader, volUtils, AxisNumbers) {
      "use strict";

      QUnit.module("volume", function () {
        QUnit.test("load study", function (assert) {
          var done = assert.async();
          var progressCount = 0;

          var onStudyLoad = function (study) {
            assert.ok(progressCount > 0, "progressCount");

            assert.equal(study.volumes.length, 4, "volumes.length");

            var volume = study.volumes[0];
            assert.equal(volUtils.getAxisName(volume), "transverse", "axis name");
            assert.deepEqual(volume.getAxisNumber(), AxisNumbers.tra, "axis");
            assert.equal(volume.getDepth(), 8, "depth");
            assert.notOk(volume.isLabelMap(), "!isLabelMap");
            assert.notOk(volume.isComplete(), "complete");


            var doneSliceLoad = assert.async();
            var sliceIndex = 0;
            var sliceLoaded = function (slice) {
              assert.ok(slice, "slice aviable");
              assert.ok(volume.hasCompleteSlice(sliceIndex), "slice loaded");
              assert.equal(slice.length,
                  volume.getSize()[0] * volume.getSize()[1], "slice size");

              doneSliceLoad();
            };
            assert.notOk(volume.hasCompleteSlice(sliceIndex),
                "slice not yet loaded");
            volume.getSlice(sliceLoaded, sliceIndex);

            var labelMap = volume.getReformatedLabelMap();
            assert.equal(labelMap, study.labelMap, "labelMap");
            assert.ok(labelMap.isLabelMap(), "isLabelMap");

            var lut = volume.getLUT();
            assert.ok(lut, "LUT");

            done();
          };

          var onProgress = function () {
            progressCount++;
          };

          // FIXME add test study to use
          studyLoader.loadStudy(onStudyLoad, onProgress, "en", "/mra-brain/");
        });
      });
    });
