/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "view/gl-view", "tests/volume-mock"],
    function(QUnit, GLView, mock) {
      "use strict";

      QUnit.module("views");

      QUnit.test("gl-view", function (assert) {
        var fixture = document.getElementById("qunit-fixture");
        fixture.style.width = "1000px";
        fixture.style.height = "500px";

        var div = document.createElement("div");
        div.style.width = "100%";
        div.style.height = "100%";
        fixture.appendChild(div);

        var volume = mock.makeTestVolume(10,10,10);
        var lut = {toArray: function () {return new Uint8Array(256);}};
        var glViewTools = {lut: lut};
        var index = [0,0,0];

        var glView = new GLView(div, index, {viewTools: glViewTools});
        glView.setVolumes([volume]);
        assert.equal(glView.getVolume(), volume, "get/set Volume");

        glView.resize();

        var fps = document.getElementById("fpsElement");
        if (fps) {
          document.documentElement.removeChild(fps);
        }

        assert.equal(glView.isAnatomyVisible(), true, "anatomy visible");
        glView.setAnatomyHidden(true);
        assert.equal(glView.isAnatomyVisible(), false, "anatomy hidden");

        assert.equal(glView.draw(), true, "draw complete");
      });

    });

