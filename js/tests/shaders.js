/* Sectional Anatomy
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "vr/volume-rendering"],
  function(QUnit, vr) {
    "use strict";

    var selected;
    var selectionCount = 0;
    function selectAnatomy(id) {
      selected = id;
      selectionCount++;
    }

    function testRenderer(renderer) {
      QUnit.test(renderer.name, function (assert) {
        assert.ok(renderer.name, "renderer.name defined");
        assert.ok(renderer.directory, "shader directory defined");

        if (renderer.pickFunction) {
          var lastSelectionCount = selectionCount;
          var picked = renderer.pickFunction([0,0,0,0]);

          if (renderer.pickFunctionJs) {
            assert.equal(picked, false, "nothing picked");
          }

          assert.equal(selected, 0, "empty label");
          assert.equal(selectionCount, lastSelectionCount+1, "selectAnatomy called");
        }
      });
    }

    QUnit.module("vr", function () {
      QUnit.module("renderers", function () {
        QUnit.test("getRenderers", function (assert) {
          var renderers = vr.getRenderers();
          assert.ok(renderers);
        });

        var renderers = vr.getRenderers(selectAnatomy);
        for (var i=0; i<renderers.length; i++) {
          testRenderer(renderers[i]);
        }
      });
    });
  });
