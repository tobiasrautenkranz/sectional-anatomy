/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

WebVRConfig = {
  FORCE_ENABLE_VR: true,
  DIRTY_SUBMIT_FRAME_BINDINGS: true // Default: false.
};

requirejs.config({
  baseUrl: "/js",
});

requirejs(["vr/webvr-canvas",
    "util/utils", "tests/volume-mock",
    "lib/gl-matrix"],
    function(VRCanvas, utils, mock, glm) {
      "use strict";

      var vec3 = glm.vec3;

      function makeVRCanvas (done) {
        var canvas = document.createElement("canvas");
        var fixture = document.getElementById("qunit-fixture");
        fixture.appendChild(canvas);
        canvas.style.width = "512px";
        canvas.style.height = "256px";

        var vrcanvas = new VRCanvas(canvas);

        var setupVR = function () {
          var volume = mock.makeTestVolume(10, 10, 10);
          vrcanvas.setVolume(volume);
          vrcanvas.setRenderer({
            directory: "/shaders/direct/coords/",
            isDirect: true},
            function () {
                done({canvas: canvas, vrcanvas: vrcanvas});
              });
        };
        vrcanvas.init(setupVR);
      }

      QUnit.module("WebVR", function () {
        QUnit.test("makeWebVRCanvas", function (assert) {
          var done = assert.async();
          makeVRCanvas(function (o) {
            o.vrcanvas.draw();
            assert.pixelApprox(o.canvas, o.canvas.width/2, o.canvas.height/2,
                [256/2, 256/2, 255], "middle coords", 3); // large epsilon
            // because left eye matrix is used (introducing an offset)

            if (o.vrcanvas.vrDisplay) {
              o.vrcanvas.enterVR().then(function () {
                o.vrcanvas.draw();

                var centerNDC = vec3.fromValues(0.0, 0.0, -100); // Eye coordinates
                // to normalized device coordinates
                var vr = o.vrcanvas.vr;
                vec3.transformMat4(centerNDC, centerNDC, vr.pMatrix);

                assert.pixelApprox(o.canvas,
                    (centerNDC[0] + 1) * vr.viewportWidth()/2 + vr.viewportWidth(),
                    vr.viewportHeight() - (centerNDC[1] + 1) * vr.viewportHeight()/2,
                    [256/2, 256/2, 255], "middle coords VR", 10); // large epsilon
                // to compensate for eye transform
                o.vrcanvas.exitVR().then(function () {
                  o.vrcanvas.draw();
                  assert.pixelApprox(o.canvas, o.canvas.width/2, o.canvas.height/2,
                      [256/2, 256/2, 255], "middle coords exit VR", 3);
                  done();
                },
                function () {
                  assert.ok(false, "exitVR() failed");
                  done();
                });
              },
              function () {
                assert.ok(true, "enterVR() failed");
                done();
              });
            } else {
              done();
            }

          });
        });
      });
    });
