/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "util/utils", "volume/mpr", "volume/oblique",
    "tests/volume-mock", "lib/gl-matrix", "volume/basis-transform",
    "volume/axis-numbers"],
    function(QUnit, utils, mpr, ObliqueMPRVolume, mock, glm, BasisTransform, AxisNumbers) {
      "use strict";

      var vec3 = glm.vec3;

      function reformatOptions (axis) {
        if (undefined === axis) {
          axis = AxisNumbers.tra;
        }
        return {
          patternFill: true,
          zIncrement: 3,
          axis: axis
        };
      }

      QUnit.module("MPR", function () {
        QUnit.test("reformatExtends", function (assert) {
          var reformat = mpr.reformatExtends;
          var extend = [1, 2, 3];
          var tra = AxisNumbers.tra;
          var cor = AxisNumbers.cor;
          var sag = AxisNumbers.sag;


          utils.forEach([tra, cor, sag], function (a) {
            assert.deepEqual(reformat(extend, a, a), extend, "from = to axis: " + a);
          });
          assert.deepEqual(reformat(reformat(extend, tra, cor), cor, tra),
              extend, "idendity tra => cor => tra");

          assert.deepEqual(reformat(reformat(extend, tra, sag), sag, tra),
              extend, "idendity tra => sag => tra");

          assert.deepEqual(reformat(reformat(extend, cor, sag), sag, cor),
              extend, "idendity cor => sag => cor");
        });

        function testWorldCoordsReformat (assert, volume) {
          assert.deepEqual(volume.getBasis().from(vec3.create(), [0,0,0]),
              volume.getOrigin(), "toWorld origin");
          assert.deepEqual(volume.getBasis().to(vec3.create(),
                volume.getOrigin()), [0,0,0], "toVolume origin");

          var vec = vec3.fromValues(1,2,3);
          assert.deepEqual(
              BasisTransform.fromTo(vec3.create(),
                volume.getBasis(), volume.getBasis(), vec),
              vec, "to-world from-world invariant");

          assert.deepEqual(
              volume.getBasis().to(vec3.create(),
                volume.getBasis().from(vec3.create(), vec)),
              vec, "to-basis from-basis invariant");
        }
        function testWorldCoordsReformatAxis (axisNumber, fromAxis) {
          return function (assert) {
            var testVolume = mock.makeTestVolume(5, 6, 7,
                reformatOptions(fromAxis));
            var volume = new mpr.MPRVolume(testVolume, axisNumber);

            testWorldCoordsReformat(assert, volume);
          };
        }

        QUnit.module("to/from worldCoords", function (assert) {
          for (var to=0; to<3; to++) {
            for (var from=0; from<3; from++) {
              if (to !== from) {
                QUnit.test("from " + AxisNumbers.toName(from) +
                    " to " + AxisNumbers.toName(to),
                    testWorldCoordsReformatAxis(to, from));
              }
            }
          }
        });

        function testReformatVoxelEquivalence (baseVolume, mprVolume, assert) {
          function inVolumeBounds(coords, volume) {
            var size = volume.getSize();

            for (var i=0; i<3; i++) {
              if (coords[i] < 0 || coords[i] >= size[i]) {
                return false;
              }
            }
            return true;
          }
          var baseVolumeStride = baseVolume.getSize()[0];
          var mprVolumeStride = mprVolume.getSize()[0];

          var coords = vec3.create();

          for (var z=0; z<baseVolume.getSize()[2]; z++) {
            for (var y=0; y<baseVolume.getSize()[1]; y++) {
              for (var x=0; x<baseVolume.getSize()[0]; x++) {
                vec3.set(coords, x,y,z);
                var baseCoordsStr = "" + coords;

                utils.assert(inVolumeBounds(coords, baseVolume),
                    "coords: " + coords + " in volume bounds baseVolume");
                var baseVolumeVoxel = baseVolume.slice(coords[2])[coords[0] +
                  baseVolumeStride * coords[1]];

                BasisTransform.fromTo(coords,
                    baseVolume.getBasis(), mprVolume.getBasis(), coords);

                assert.ok(inVolumeBounds(coords, mprVolume),
                    "coords: " + coords + " in volume bounds mprVolume");

                var mprVolumeVoxel = mprVolume.slice(coords[2])[coords[0] +
                  mprVolumeStride * coords[1]];

                assert.equal(mprVolumeVoxel, baseVolumeVoxel,
                    "voxel equal for baseVolume at: " + baseCoordsStr +
                    ", mpr at: " + coords);
              }
            }
          }
        }
        (function testReformat () {
          function mkTestFunction (from, to) {
            return function (assert) {
              var baseVolume = mock.makeTestVolume(5, 6, 7,
                  reformatOptions(from));
              var mprVolume = new mpr.MPRVolume(baseVolume, to);
              testReformatVoxelEquivalence(baseVolume, mprVolume, assert);
            };
          }
          for (var from=0; from<3; from++) {
            for (var to=0; to<3; to++) {
              if (from !== to) {
                QUnit.test("Voxel Equivalence from " +
                    AxisNumbers.toName(from) + " to " +
                    AxisNumbers.toName(to),
                    mkTestFunction(from, to));
              }
            }
          }
        })();

        QUnit.test("reformat tra->cor", function (assert) {
          var zIncrement = 3;
          var testVolume = mock.makeTestVolume(5, 6, 7, reformatOptions());
          var testVolumeSize = testVolume.getSize();

          var corVolume = new mpr.MPRVolume(testVolume, AxisNumbers.cor);
          var corVolumeSize = corVolume.getSize();

          assert.deepEqual(corVolume.getAxisNumber(), AxisNumbers.cor, "coronal axis");
          assert.equal(corVolumeSize[0], testVolumeSize[0], "x axis");
          assert.equal(corVolumeSize[1], testVolumeSize[2], "y axis");
          assert.equal(corVolumeSize[2], testVolumeSize[1], "z axis");
        });

        var testSIMD = QUnit.skip.bind(QUnit);
        if (window.Uint32Array) {
          testSIMD = QUnit.test.bind(QUnit);
        } else {
          testSIMD = QUnit.skip.bind(QUnit);
        }
        testSIMD("SIMD tra->cor", function (assert) {
          var testVolume = mock.makeTestVolume(64, 64, 8, reformatOptions());
          var corVol = new mpr.MPRVolume(testVolume, AxisNumbers.cor);

          var noSIMD = new mpr.MPRVolume(
              mock.makeTestVolume(3, 3, 3, reformatOptions()), AxisNumbers.cor);
          assert.notEqual(corVol.rotate, noSIMD.rotate,
              "different internal reformat function (<= +- SIMD");
        });


        QUnit.test("reformat tra->sag", function (assert) {
          var testVolume = mock.makeTestVolume(5, 6, 7, reformatOptions());

          var sagVol = new mpr.MPRVolume(testVolume, AxisNumbers.sag);
          assert.deepEqual(sagVol.getAxisNumber(), AxisNumbers.sag, "sag axis");
          assert.equal(sagVol.getSize()[0], testVolume.getSize()[1], "x axis");
          assert.equal(sagVol.getSize()[1], testVolume.getSize()[2], "y axis");
          assert.equal(sagVol.getSize()[2], testVolume.getSize()[0], "z axis");

          var slice = sagVol.slice(3);
          assert.equal(slice.length, 6*7, "slice length");
        });


        QUnit.test("reformat sag->cor", function (assert) {
          var testVolume = mock.makeTestVolume(5, 6, 7,
              reformatOptions(AxisNumbers.sag));
          var testVolumeSize = testVolume.getSize();

          var corVolume = new mpr.MPRVolume(testVolume, 1);
          var corVolumeSize = corVolume.getSize();

          assert.deepEqual(corVolume.getAxisNumber(), AxisNumbers.cor, "coronal axis");

          assert.equal(corVolumeSize[0], testVolumeSize[2], "x axis");
          assert.equal(corVolumeSize[1], testVolumeSize[1], "y axis");
          assert.equal(corVolumeSize[2], testVolumeSize[0], "z axis");

          var slice = corVolume.slice(3);
          assert.equal(slice.length, 6*7, "slice length");
        });
        QUnit.test("reformat sag->tra", function (assert) {
          var testVolume = mock.makeTestVolume(5, 6, 7,
              reformatOptions(AxisNumbers.sag));
          var testVolumeSize = testVolume.getSize();

          var traVolume = new mpr.MPRVolume(testVolume, AxisNumbers.tra);
          var traVolumeSize = traVolume.getSize();

          assert.deepEqual(traVolume.getAxisNumber(), AxisNumbers.tra, "transverse axis");

          assert.equal(traVolumeSize[0], testVolumeSize[2], "x axis");
          assert.equal(traVolumeSize[1], testVolumeSize[0], "y axis");
          assert.equal(traVolumeSize[2], testVolumeSize[1], "z axis");

          var slice = traVolume.slice(3);
          assert.equal(slice.length, 5*7, "slice length");
        });

        QUnit.test("ObliqueMPRVolume", function (assert) {
          var testVolume = mock.makeTestVolume(8, 10, 3, reformatOptions());
          var vol1 = new ObliqueMPRVolume(testVolume, 0);
          var vol2 = new ObliqueMPRVolume(testVolume, 1);

          var assertVolume = function (volume, name) {
            name += ": ";
            assert.equal(volume.getSize()[1], testVolume.getSize()[2], name + "size_z equal");
            assert.equal(volume.getVoxelSize()[1], -testVolume.getVoxelSize()[2], name + "voxelsize_z equal");

            var slice = volume.slice(0);
            if (undefined === slice[0]) {
              if (console && console.warn) {
                console.warn("ObliqueMPRVolume Unit Test:"  + name +
                    "not using TypedArrays. not zero initialized but works anyhow (ie9)");
              }
            } else {
              assert.equal(slice[0], 0, name + "out of volume = empty");
            }

            testWorldCoordsReformat(assert, volume, testVolume);
          };

          assertVolume(vol1, "obl0");
          assertVolume(vol2, "obl1");

          assert.deepEqual(vol1.getSize(), vol2.getSize(), "oblique same size");

          assert.equal(vol1.getVoxelSize()[0], -vol2.getVoxelSize()[0],
              "voxelSizeX LAO = - RAO");
          assert.equal(vol1.getVoxelSize()[1], vol2.getVoxelSize()[1],
              "equal voxelSizeY");
          assert.equal(vol1.getVoxelSize()[2], vol2.getVoxelSize()[2],
              "equal voxelSizeZ");
        });
      });
    });
