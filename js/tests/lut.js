/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "volume/lut", "util/array"],
    function(QUnit, luts, typedArray) {
      "use strict";

      QUnit.module("LUT");

      var dataLength = 256;
      function makeLinearArray() {
        var data = new Array(dataLength);
        for (var i=0; i<data.length; i++) {
          data[i] = i;
        }
        return data;
      }

      function makePixelsArray() {
        return new Array(4*dataLength);
      }

      QUnit.test("identity", function (assert) {
        var lut = new luts.GrayLUT();
        var data = makeLinearArray();
        var pixels = makePixelsArray();

        lut.toPixels(data, pixels);

        var cTest = assert.combinedTests().begin("identity");
        for (var i=0, j=0; i<pixels.length; i+=4, j++) {
          cTest.equal(pixels[i], pixels[i+1], "gray");
          cTest.equal(pixels[i], pixels[i+2], "gray2");
          cTest.equal(pixels[i+3], 255, "alpha");

          cTest.equal(pixels[i], data[j], "equal");
        }
        cTest.end();
      });

      QUnit.test("invert", function (assert) {
        var lut = new luts.GrayLUT();
        var data = makeLinearArray();
        var pixels = makePixelsArray();

        lut.setWindow(255/2,-255);
        assert.equal(lut.getCenter(), 255/2, "getCenter");
        assert.equal(lut.getWidth(), -255, "getWidth");
        lut.toPixels(data, pixels);

        var cTest = assert.combinedTests().begin("invert");
        for (var i=0, j=data.length-1; i<pixels.length; i+=4, j--) {
          cTest.equal(pixels[i], data[j], "invert");
        }
        cTest.end();
      });

      QUnit.test("double invert", function (assert) {
        var lut = new luts.GrayLUT();
        var data = makeLinearArray();
        var pixels = makePixelsArray();

        var min = 0;
        var max = 100;
        lut.setWindow((min+max)/2, -(max-min));
        lut.toPixels(data, pixels);
        assert.equal(pixels[0], 255, "invert 1");

        lut.setWindow((min+max)/2, -(max-min));
        lut.toPixels(data, pixels);
        assert.equal(pixels[0], 255, "invert 2");
      });

      QUnit.test("small window", function (assert) {
        var lut = new luts.GrayLUT();
        var data = [2, 4, 5, 6, 8, 200];
        var pixels = makePixelsArray();

        lut.setWindow(5,5);
        lut.toPixels(data, pixels);

        assert.equal(pixels[0], 0, "clamp lower");

        var centerValue = 255/2;
        assert.equal(pixels[2*4], Math.round(centerValue), "center");
        assert.equal(centerValue - pixels[1*4], pixels[3*4] - centerValue,
            "symmetry");

        assert.equal(pixels[4*4], 255, "clamp upper");
        assert.equal(pixels[5*4], 255, "clamp upper2");
      });

      QUnit.test("NOOPLUT", function (assert) {
        var lut = new luts.NOOPLUT();
        var data = [2, 4, 5, 6, 8,  200, 255, 42, 0];
        var pixels = new typedArray.mkUint8Array(data.length);

        lut.toPixels(data, pixels);

        assert.deepEqual(pixels, data, "identity");
      });

      QUnit.test("ColorAlphaLUT", function (assert) {
        var data = [2, 4, 5, 6,  200, 255, 42, 0];
        var lutArray = typedArray.mkUint8Array(4*256);
        for (var i=0; i<256; i++) {
          lutArray[4*i   ] = i;
          lutArray[4*i +1] = i;
          lutArray[4*i +2] = i;
          lutArray[4*i +3] = i;
        }

        var identityLUT = new luts.ColorAlphaLUT(lutArray);

        var pixels = new typedArray.mkUint8Array(4*data.length);

        identityLUT.toPixels(data, pixels);

        var expected = new Array(4*data.length);
        for (var i=0; i<data.length; i++) {
          var v = data[i];
          expected[4*i   ] = v;
          expected[4*i +1] = v;
          expected[4*i +2] = v;
          expected[4*i +3] = v;
        }

        assert.deepEqual(pixels, expected, "identity");
      });

    });
