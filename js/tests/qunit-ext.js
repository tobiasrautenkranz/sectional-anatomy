/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "util/utils", "util/gl-utils"],
  function (QUnit, utils, glu) {
    "use strict";

    QUnit.assert.combinedTests = function () {
      var qAssert = this;

      var numberOfTests = 0;
      var failed = {
        actual: [],
        expected: [],
        message: ""
      };
      function addFailed(actual, expected, message) {
        failed.actual.pushResult(actual);
        failed.expected.pushResult(expected);
        failed.message += "; ";
        failed.message += message;
      }
      var pname;
      return {
        begin: function (name) {
          if (numberOfTests !== 0) {
            throw new Error ("combinedTests: missing end() for: " + pname);
          }
          pname = name;
          return this;
        },
        equal: function (actual, expected, message) {
          if (actual != expected) {
            addFailed(actual, expected, message);
          }
          numberOfTests++;
        },
        deepEqual: function (actual, expected, message) {
          if (QUnit.equiv(actual, expected)) {
            addFailed(actual, expected, message);
          }
          numberOfTests++;
        },

        pixelEqual: function (canvas, x,y, expected, message) {
          var pixel = getPixel(canvas, x, y);
          if (!pixelEqual(pixel, expected)) {
            addFailed(pixel, expected, message);
          }

          numberOfTests++;
        },
        floatAprox: function (actual, expected, message, epsilon) {
          epsilon = epsilon || 1e-4;
          if (Math.abs(actual - expected) > epsilon) {
            addFailed(actual, expected, message);
          }
          numberOfTests++;
        },
        end: function () {
          qAssert.pushResult({
            result: failed.actual.length === 0,
            actual: failed.actual,
            expected: failed.expected,
            message: pname + " (" + failed.actual.length + "/" + numberOfTests + ")\n" +
              failed.message
          });

          failed.actual = [];
          failed.expected = [];
          failed.message = "";
          numberOfTests = 0;
        }
      };
    };

    QUnit.assert.approxEqual = function(actual, expected, message, epsilon) {
      epsilon = epsilon || 1e-4;
      this.pushResult({
        result: Math.abs(actual - expected) <= epsilon,
        actual: actual,
        expected: expected,
        message: message
      });
    };

    function floatArrayEqual(a, b, epsilon) {
      if (a.length != b.length) {
        return false;
      }
      for (var i=0; i<a.length; i++) {
        if (Math.abs(a[i] - b[i]) > epsilon) {
          return false;
        }
      }
      return true;
    }

    QUnit.assert.floatArrayEqual = function(actual, expected, message, epsilon) {
      epsilon = epsilon || 1e-4;
      this.pushResult({
        result: floatArrayEqual(actual, expected, epsilon),
        actual: actual,
        expected: expected,
        message: message
      });
    };

    var isTypedArray = function () {return false;};
    if (window.Uint8Array) {
      isTypedArray = function (o) {
        // FIXME add all
        return o instanceof Uint8Array ||
               o instanceof Uint8ClampedArray ||
               o instanceof Float32Array;
      };
    }

    // patch deep equal to recognise typed arrays as arrays
    var deepEqual = QUnit.assert.deepEqual;
    QUnit.assert.deepEqual = function (a, b, m) {
      if (isTypedArray(a)) {
        a = utils.arrayFromTypedArray(a);
      }
      if (isTypedArray(b)) {
        b = utils.arrayFromTypedArray(b);
      }
      deepEqual.call(this, a, b, m);
    };

    function getPixel(canvas, x, y) {
      var context = canvas.getContext("2d");
      if (context) {
        utils.assert(0 <= x && x < canvas.width, "x out of range: " + x);
        utils.assert(0 <= y && y < canvas.height, "y out of range: " + y);

        var p = canvas.getContext("2d").getImageData(x,y,1,1).data;
        return [p[0], p[1], p[2], p[3]]; // make a real Array
      } else {
        var webglOptions = {preserveDrawingBuffer: true};
        var gl = canvas.getContext("webgl", webglOptions) ||
          canvas.getContext("experimental-webgl", webglOptions);
        utils.assert(gl, "webgl context creation failed");

        var onContextLost = function (e) {
          throw "qunit pixel test: webgl context is lost: can't read pixel!";
        };
        canvas.addEventListener("webglcontextlost", onContextLost);

        var pixel = new Uint8Array(4);
        utils.assert(0 <= x && x < gl.drawingBufferWidth, "x out of range: " + x);
        utils.assert(0 <= y && y < gl.drawingBufferHeight, "y out of range: " + y);
        // gl: origin lower left corner
        gl.readPixels(x, gl.drawingBufferHeight-1 - y,  1, 1,
            gl.RGBA, gl.UNSIGNED_BYTE, pixel);
        glu.checkGLError(gl, "pixelTest: readPixels");

        canvas.removeEventListener("webglcontextlost", onContextLost);

        // use js array to make deepEqual work
        return utils.arrayFromTypedArray(pixel);
      }
    }
    function pixelEqual(pixel, color) {
      if (!Array.isArray(color)) {
        return pixelEqual(pixel, [color, color, color]);
      }

      switch (color.length) {
        case 1:
          return pixelEqual(pixel, color[0]);
        case 4:
          return pixel[0] === color[0] &&
                 pixel[1] === color[1] &&
                 pixel[2] === color[2] &&
                 pixel[3] === color[3];

        case 3:
          return pixel[0] === color[0] &&
                 pixel[1] === color[1] &&
                 pixel[2] === color[2];
        default:
          return false;
      }
    }

    QUnit.assert.pixelEqual = function (canvas, x, y, expected, message) {
      var pixel = getPixel(canvas, x, y);
      this.pushResult({
        result: pixelEqual(pixel, expected),
        actual: pixel,
        expected: expected,
        message: message
      });
    };

    QUnit.assert.pixelApprox = function (canvas, x, y, expected, message, delta) {
      delta = delta || 2;

      var pixel = getPixel(canvas, x, y);

      var ok=true;
      for (var i=0; i<expected.length; i++) {
        if (Math.abs(expected[i] - pixel[i]) > delta) {
          ok = false;
          break;
        }
      }
      this.pushResult({
        result: ok,
        actual: pixel,
        expected: expected,
        message: message
      });
    };

    function glCanvasColorMismatch(canvas, color, epsilon) {
      epsilon = 0 || epsilon;

      var options = {preserveDrawingBuffer: true};
      var gl = canvas.getContext("webgl", options) ||
        canvas.getContext("experimental-webgl", options);
      utils.assert(gl, "gl context creation failed");

      var width = gl.drawingBufferWidth;
      var height = gl.drawingBufferHeight;

      var pixels = new Uint8Array(4 * width * height);
      gl.readPixels(0, 0, width, height, gl.RGBA, gl.UNSIGNED_BYTE, pixels);
      glu.checkGLError(gl, "glCanvasColorEqual: readPixels");

      var equalAt;
      if (0 === epsilon) {
        equalAt = function(index) {
          return pixels[i  ] === color[0] &&
                 pixels[i+1] === color[1] &&
                 pixels[i+2] === color[2];
        };
      } else {
        equalAt = function(index) {
          return Math.abs(pixels[i  ] - color[0]) <= epsilon &&
                 Math.abs(pixels[i+1] - color[1]) <= epsilon &&
                 Math.abs(pixels[i+2] - color[2]) <= epsilon;
        };
      }

      for (var i=0; i<pixels.length; i+=4) {
        if (!equalAt(i)) {
          return {
            color: [pixels[i], pixels[i+1], pixels[i+2]], // no slice for typedarray
            x: (i/4) % width,
            y: Math.floor(i/4 / width),
          };
        }
      }

      return null;
    }

    QUnit.assert.canvasColor = function (canvas, color, message, epsilon) {
      var delta = glCanvasColorMismatch(canvas, color, epsilon);
      this.pushResult({
        result: !delta,
        actual: delta,
        expected: color,
        message: message
      });
    };

    return QUnit;
  });
