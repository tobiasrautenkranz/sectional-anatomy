/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "util/utils", "view/view", "tests/volume-mock", "view/redraw"],
  function(QUnit, utils, View, mock, draw) {
    "use strict";

    QUnit.module("view");

    QUnit.test("draw slice", function (assert) {
      var fixture = document.getElementById("qunit-fixture");
      fixture.style.width = "500px";
      fixture.style.height = "500px";

      var z=5;
      var index = [0,0,z];
      var volume0 = mock.makeTestVolume(10,10,10);
      var volume = mock.makeTestVolume();

      var view = new View(fixture, index, {interactive: false});

      view.setVolumes([volume0]);
      assert.equal(view.getVolume(), volume0, "getVolume volume0");

      assert.equal( view.sliceViews[0].canvas.width, 10, "canvas width");
      assert.equal( view.sliceViews[0].canvas.height, 10, "canvas height");

      view.setVolumes([volume]);
      assert.equal(view.getVolume(), volume, "getVolume volume");
      assert.deepEqual(view.getNativeSize(), volume.getSize().slice(0,2),
          "native size");

      view.draw();
      var size = volume.getSize();

      var cassert = assert.combinedTests().begin("image equal");
      for (var y=0; y<size[1]; y++) {
        for (var x=0; x<size[0]; x++) {
          cassert.pixelEqual(view.sliceViews[0].canvas, x, y,
              volume.slice(z)[x+y*size[0]], "" + x + "x" + y);
        }
      }
      cassert.end();
    });

    function makeKeyEvent(code) {
      return { // not using event constructor because IE9
        keyCode: code,
        preventDefault: function() {}
      };
    }

    QUnit.test("scroll", function (assert) {
      // disable redraw for this test
      var redrawOrig = draw.requestRedraw;
      draw.requestRedraw = function () {};

      try {
        var fixture = document.getElementById("qunit-fixture");
        fixture.style.width = "500px";
        fixture.style.height = "500px";

        var z=5;
        var index = [0, 0, z];
        var volume0 = mock.makeTestVolume(20,20,20);

        var view = new View(fixture, index, {interactive: true});
        view.setVolumes([volume0]);

        var up = makeKeyEvent(38);
        view.keydown(up);
        assert.deepEqual(index, [0, 0, z-1], "scroll one slice");
        for (var i=z-1; i>1; i--) {
          view.keydown(up);
        }
        assert.deepEqual(index, [0, 0, 1], "scroll");
        view.keydown(up);
        assert.deepEqual(index, [0, 0, 0], "scroll");

        view.keydown(up);
        assert.deepEqual(index, [0, 0, 0], "scroll clamp");

        var pageDown = makeKeyEvent(34);
        view.keydown(pageDown);
        assert.deepEqual(index, [0, 0, 10], "scroll pageDown");

      } finally {
        draw.requestRedraw = redrawOrig;
      }
    });

    QUnit.test("draw slice zoom", function (assert) {
      var fixture = document.getElementById("qunit-fixture");
      fixture.style.width = "500px";
      fixture.style.height = "500px";

      var volume = mock.makeTestVolume(10,10,10);
      var z = 5;
      var index = [5,5,z];
      var view = new View(fixture, index, {interactive: false});

      view.setVolumes([volume]);
      view.setZoom(1, [5,5]);
      assert.equal( view.sliceViews[0].canvas.width, 10, "canvas width zoom 1");
      assert.equal( view.sliceViews[0].canvas.height, 10, "canvas height zoom 1");
      assert.deepEqual(view.getImageOffset(), [0,0], "no offset zoom 1");

      view.setZoom(2, [5,5]);
      assert.equal( view.sliceViews[0].canvas.width, 5, "canvas width zoom 2");
      assert.equal( view.sliceViews[0].canvas.height, 5, "canvas height zoom 2");
      assert.deepEqual(view.getImageOffset(), [3,3], "offset zoom 2 center");

      view.setZoom(1, [0,0]);
      assert.deepEqual(view.getImageOffset(), [0,0], "no offset zoom 1 reset");

      view.setZoom(2, [10,10]);
      assert.deepEqual(view.getImageOffset(), [5,5], "offset");

      view.draw();

      var size = volume.getSize();
      var cassert = assert.combinedTests().begin("image equal");
      for (var y=5; y<size[1]; y++) {
        for (var x=5; x<size[0]; x++) {
          cassert.pixelEqual(view.sliceViews[0].canvas, x-5, y-5,
              volume.slice(z)[x+y*size[0]], "" + x + "x" + y);
        }
      }
      cassert.end();

      view.setZoom(1, [0,0]);
      fixture.style.width = "1000px";
      fixture.style.height = "500px";

      view.resize();
      assert.equal(view.sliceViews[0].canvas.width, 20,
          "canvas width zoom 1 double width");

      assert.equal(view.sliceViews[0].canvas.height, 10,
          "canvas height zoom 1 double width");
    });

    function rgba(array, index) {
      var i=4*index;
      return utils.map(function (a) {return a || 0;},
        [array[i], array[i+1], array[i+2], array[i+3]]);
    }

    QUnit.test("draw labelmap", function (assert) {
      var fixture = document.getElementById("qunit-fixture");
      fixture.style.width = "500px";
      fixture.style.height = "500px";

      var z = 2;
      var index = [0,0,z];
      var volume = mock.makeTestVolume().getLabelMap();
      var lut = volume.getLUT();

      var size = volume.getSize();
      var colorAt = function (x,y) {
        var label = volume.slice(z)[x+y*size[0]];
        return rgba(lut.getArray(),label);
      };

      var view = new View(fixture, index, {interactive: false});

      view.setVolumes([volume]);
      assert.equal(view.getVolume(), volume);
      assert.deepEqual(view.getNativeSize(), volume.getSize().slice(0,2),
          "native size");

      view.draw();
      for (var y=0; y<size[1]; y++) {
        for (var x=0; x<size[0]; x++) {
          assert.pixelEqual(view.sliceViews[0].canvas, x, y, colorAt(x,y),
              "" + x + "x" + y);
        }
      }
    });

    QUnit.test("draw labelmap 16bit", function (assert) {
      var fixture = document.getElementById("qunit-fixture");
      var z = 3;
      var index = [0,0,z];
      var volume = mock.makeTestLabelMapVolume16();
      var lut = volume.getLUT();

      assert.ok(lut.getArray().length >=256, "lut needs 16 bit");

      var size = volume.getSize();
      var colorAt = function (x,y) {
        var label = volume.slice(z)[x+y*size[0]];
        return rgba(lut.getArray(),label);
      };

      var view = new View(fixture, index, {interactive: false});

      view.setVolumes([volume]);
      assert.equal(view.getVolume(), volume);
      assert.deepEqual(view.getNativeSize(), volume.getSize().slice(0,2),
          "native size");

      view.draw();
      for (var y=0; y<size[1]; y++) {
        for (var x=0; x<size[0]; x++) {
          // FIXME why approx / equal does not always work!
          assert.pixelApprox(view.sliceViews[0].canvas, x, y, colorAt(x,y),
              "" + x + "x" + y);
        }
      }
    });

  });
