/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["volume/lut", "util/array", "volume/basis-transform", "volume/axis-numbers"],
  function (luts, array, BasisTransform, AxisNumbers) {
    "use strict";

    function makeTestVolume (x,y,z, options) {
      x = x || 10;
      y = y || 11;
      z = z || 12;
      options = options || {};
      var sliceThickness = options.sliceThickness || 1;
      var axis = options.axis;
      if (undefined === axis) {
        axis = AxisNumbers.tra;
      }


      var slices = new Array(z);
      if (options.patternFill) {
        for (var s=0; s<z; s++) {
          var slice = array.mkUint8Array(x*y);
          for (var i=0; i<x*y; i++) {
            slice[i] = Math.floor((i+s)%256);
          }
          slices[s] = slice;
        }
      } else {
        for (var s=0; s<z; s++) {
          var slice = array.mkUint8Array(x*y);
          for (var i=0; i<x*y; i++) {
            slice[i] = Math.floor(255.0*(i%y)/(x));
          }
          slices[s] = slice;
        }
      }

      return {
        type: "testVolume",
        slices: slices,
        getSize: function() {
          return [x, y, z];
        },
        slice: function (index) {
          return slices[index];
        },
        getSlice: function(cont, index) {
          var slice = this.slice(index);

          if (cont) cont(slice);

          return slice;
        },
        getVoxelSize: function () {
          return [1, 1, sliceThickness];
        },
        getDepth: function () {
          return 8;
        },
        getAxis: function () {
          return setAxis([0, 0, 0], axis, sliceThickness);
        },
        getSourceVolume: function () {
          return this;
        },
        isComplete: function () {
          return true;
        },
        getAxisNumber: function () {
          return axis;
        },
        getAxes: function () {
          return AxisNumbers.toAxes(this.getAxisNumber(), this.getVoxelSize());
        },
        getOrigin: function () {
          return [0,0,0];
        },
        hasCompleteSlice: function (index) {
          return true;
        },
        lut: new luts.GrayLUT(),
        getLUT: function () {
          return this.lut;
        },
        getLabelMap: function () {
          return makeTestLabelMapVolume(x, y, z, options);
        },
        getReformatedLabelMap: function () {
          return this.getLabelMap();
        },
        getBasis: function () {
          return new BasisTransform(this.getAxes(), this.getOrigin());
        }
      };
    }
    function makeLUT(length) {
      var lut = new array.mkUint8Array(4*length);
      for (var l=0; l<length; l+=4) {
        lut[l  ] = l % 256;
        lut[l+1] = Math.abs(length -l) % 256;
        lut[l+2] = 255;
        lut[l+3] = 250;
      }
      return new luts.ColorAlphaLUT(lut);
    }

    function makeTestLabelMapVolume(x, y, z, options) {
      var volume = makeTestVolume(x, y, z, options);
      volume.type = "labelmap";

      volume.lut = makeLUT(256);

      return volume;
    }

    function makeTestLabelMapVolume16(x, y, z, options) {
      var volume = makeTestVolume(x, y, z, options);
      volume.type = "labelmap";

      var lutLength = 300;
      volume.lut = makeLUT(lutLength);

      for (var s=0; s<z; s++) {
        var slice = array.mkUint16Array(x*y);
        for (var i=0; i<x*y; i++) {
          slice[i] = (s*(x*y)+i)%lutLength;
        }
        volume.slices[s] = slice;
      }

      return volume;
    }

    return {
      makeTestVolume: makeTestVolume,
      makeTestLabelMapVolume: makeTestLabelMapVolume,
      makeTestLabelMapVolume16: makeTestLabelMapVolume16
    };
  });
