/* Sectional Anatomy
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "vr/vr-canvas", "tests/volume-mock", "lib/gl-matrix"],
    function(QUnit, VRCanvas, mock, glm) {
      "use strict";

      //var devicePixelRatio = window.devicePixelRatio || 1;
      var devicePixelRatio = 1;

      QUnit.module("VR", function () {
        function makeVRCanvas(cubeDistance) {
          var canvas = document.createElement("canvas");

          if (false) {
            document.documentElement.appendChild(canvas);
          } else {
            var fixture = document.getElementById("qunit-fixture");
            fixture.appendChild(canvas);
          }

          var vrcanvas = new VRCanvas(canvas);

          if (undefined === cubeDistance) {
            cubeDistance = -1.8;
          }

          var rotation = glm.quat.create();
          glm.mat4.fromRotationTranslation(vrcanvas.mvMatrix(),
              rotation,  glm.vec3.fromValues(0, 0, cubeDistance));

          return vrcanvas;
        }


        QUnit.test("makeVRCanvas", function (assert) {
          var vrcanvas = makeVRCanvas();
          var canvas = vrcanvas.canvas;
          assert.ok(vrcanvas);

          canvas.style.width = "300px";
          canvas.style.height = "300px";

          var done = assert.async();
          vrcanvas.init(function () {
            vrcanvas.setAnatomyVisible(false);
            vrcanvas.draw();
            assert.ok(canvas.width>0 && canvas.height>0,
                "canvas area is > zero");
            assert.pixelEqual(canvas, 0, 0, [0,0,0], "top left corner");
            assert.pixelEqual(canvas, canvas.width-1, canvas.height-1, [0,0,0],
                "bottom right corner");
            assert.pixelApprox(canvas, canvas.width/2, canvas.height/2,
                [256/2,256/2,255], "middle");
            done();
          });
        });

        QUnit.test("resize VRCanvas", function (assert) {
          var vrcanvas = makeVRCanvas();
          var canvas = vrcanvas.canvas;
          assert.ok(vrcanvas);

          var done = assert.async();
          vrcanvas.init(function () {
            canvas.style.width = "300px";
            canvas.style.height = "300px";
            vrcanvas.draw();

            var oldWidth = canvas.width;
            var oldHeight = canvas.height;
            // resize factor 2
            // cache to compensate for width resize
            var clientHeight = canvas.clientHeight;
            canvas.style.width = 2*canvas.clientWidth+"px";
            canvas.style.height = 2*clientHeight+"px";

            vrcanvas.draw();

            assert.equal(canvas.width, 2*oldWidth, "canvas width resize 2x");
            assert.equal(canvas.height, 2*oldHeight, "canvas height resize 2x");

            assert.pixelEqual(canvas, 0, 0, [0,0,0], "top left corner");

            canvas.style.width = "1px";
            canvas.style.height = "1px";
            vrcanvas.draw();
            assert.equal(vrcanvas.vr.viewportWidth(), 1, "viewportWidth");

            // Test drawingbufferSize
            canvas.style.width = "0";
            canvas.style.height = "0";
            vrcanvas.draw();

            assert.equal(vrcanvas.vr.viewportWidth(), 1, "viewportWidth");

            done();
          });
        });
        QUnit.test("testvolume VRCanvas", function (assert) {
          var vrcanvas = makeVRCanvas();
          var canvas = vrcanvas.canvas;

          canvas.style.width = "512px";
          canvas.style.height = "512px";

          var done = assert.async();
          vrcanvas.init(function () {
            var volume = mock.makeTestVolume(10, 11, 12, 4);
            vrcanvas.setVolume(volume);
            vrcanvas.setLUT(volume.getLUT());
            vrcanvas.setRenderer({directory: "/shaders/coords/"},
                function () {
                  vrcanvas.setAnatomyVisible(false);
                  vrcanvas.draw();
                  assert.pixelEqual(canvas, 0, 0, [0,0,0], "top left corner");
                  assert.pixelApprox(canvas, canvas.width/2, canvas.height/2,
                      [255/2,255/2,255], "middle");
                  done();
                });
          });
        });


        function testInGLCoords (options, tests) {
          var size = options.size || 512;
          function toDeviceCoords(x, y) {
            var m = (devicePixelRatio*size-1);
            return {x: Math.round(m*x), y: Math.round(m*y)};
          }

          QUnit.test(options.name, function (assert) {
            var vrcanvas = makeVRCanvas();
            var canvas = vrcanvas.canvas;

            if (options.showAnatomy) {
              canvas.style.width = 2*size+"px";
            } else {
              canvas.style.width = size+"px";
            }
            canvas.style.height = size+"px";

            var done = assert.async();

            vrcanvas.init(function () {
              vrcanvas.setAnatomyVisible(false || options.showAnatomy);
              if (options.subSample) {
                vrcanvas.vr.setSubSample(options.subSample);
                size /= options.subSample;
              }

              vrcanvas.draw();

              var assertEqualGLandNormalizedCoords =
                function (x, y, nx, ny, message) {
                  var glCoords = vrcanvas.inGLCoords(x, y);
                  assert.deepEqual(glCoords, toDeviceCoords(nx, ny), message);
                };

              tests(assertEqualGLandNormalizedCoords);
              done();
            });
          });
        }

        QUnit.module("GLCoords",
            function testGLCoords () {
              var options = [
                {name: "default"},
                {name: "subsample 2", subSample: 2},
                {name: "anatomy", showAnatomy: true},
                {name: "anatomy subsample 2", showAnatomy: true, subSample: 2}
              ];
              options.forEach(function (option) {
                testInGLCoords(option, function (test) {
                  test(0, 0,  0, 1, "coords top left corner");

                  var lastPixel = 1 * 512 -1;
                  test(lastPixel, lastPixel,  1, 0, "coords bottom right corner");

                  test(lastPixel/2, lastPixel/2, 0.5, 0.5, "coords middle");
                });
              });
            });

        function testAnatomy(assert, scale, approxEqual) {
          var vrcanvas = makeVRCanvas();
          var canvas = vrcanvas.canvas;

          var done = assert.async();
          var x,y;
          var approx = false;

          var renderer = {directory: "/shaders/tests/frontcoords/",
            pickFunction: function (pixel) {
              // test that the output from first pass is equal to vr output
              // (for coords shader)
              var glc = vrcanvas.inGLCoords(x,y);
              var canvasX = Math.round(devicePixelRatio*x);
              var canvasY = Math.round(devicePixelRatio*y);
              if (approx) {
                assert.pixelApprox(canvas, canvasX, canvasY,
                    [pixel[0], pixel[1], pixel[2]],
                    "pick approx coords " + x + " " + y +
                    " gl: " + glc.x + " " + glc.y);
              } else {
                assert.pixelEqual(canvas, canvasX, canvasY,
                    [pixel[0], pixel[1], pixel[2]],
                    "pick equal coords " + x + " " + y +
                    " gl: " + glc.x + " " + glc.y);
              }
            }
          };
          var pick = function (px,py, useApprox) {
            approx = useApprox || approxEqual;
            x = Math.round(px*(canvas.clientWidth-1));
            y = Math.round(py*(canvas.clientHeight-1));

            vrcanvas.pick(x,y);
          };

          vrcanvas.init(function () {
            // size should not be to small to allow for precise pixel comparison
            canvas.style.width = "1024px";
            canvas.style.height = "512px";

            var volume = mock.makeTestVolume();
            vrcanvas.setVolume(volume);
            vrcanvas.setLUT(volume.getLUT());
            vrcanvas.setRenderer(renderer, function () {
              vrcanvas.vr.setSubSample(scale);
              vrcanvas.setAnatomyVisible(true);
              vrcanvas.draw();

              assert.pixelEqual(canvas, canvas.width/2, canvas.height/2,
                  [0,0,0], "middle"); // between scan and anatomy

              var fracScale = (scale % 1) !== 0 || devicePixelRatio !== 1; // fractional scale

              pick(0, 0);
              pick(1, 1);
              pick(1/2, 1);
              pick(1/4, 1/2, fracScale);
              pick(3/4, 1/2, fracScale); // pick in anatomy
              pick(1/5, 1/2, fracScale);
              pick(1/4, 1/3, fracScale);
              done();
            });
          });
        }
        QUnit.module("picking", function () {
          QUnit.test("default", function (assert) {
            testAnatomy(assert, 1);
          });
          QUnit.test("scaled 2", function (assert) {
            testAnatomy(assert, 2, true);
          });
          QUnit.test("scaled 1.5", function (assert) {
            testAnatomy(assert, 1.5);
          });
        });


        function testContextLost (vrcanvas, cont) {
          var loseExt = vrcanvas.gl.getExtension("WEBGL_lose_context");
          if (!loseExt) {
            cont();
            return;
          }

          var count = 0;
          var redraw = function () {
            vrcanvas.draw();
            if (count++ < 600) {
              window.requestAnimationFrame(redraw);
            }
          };
          redraw(); // test redrawing during context lost and restoring

          loseExt.loseContext();
          window.setTimeout(function () {
            loseExt.restoreContext();
            // Wait for context restore
            window.setTimeout(cont, 300);
          }, 200);
        }

        QUnit.module("contextLost", function () {
          QUnit.test("contextLost", function (assert) {
            var vrcanvas = makeVRCanvas();
            var canvas = vrcanvas.canvas;
            canvas.style.width = "80px";
            canvas.style.height = "80px";

            var done = assert.async();
            vrcanvas.init(function () {
              vrcanvas.draw();
              testContextLost(vrcanvas, function () {
                assert.pixelEqual(canvas, 0, 0, [0,0,0], "top left corner");
                done();
              });
            });
          });

          QUnit.test("contextLost complete", function (assert) {
            console.group("contextlostteest...");
            var vrcanvas = makeVRCanvas();
            var canvas = vrcanvas.canvas;
            canvas.style.width = "80px";
            canvas.style.height = "80px";

            var done = assert.async();
            vrcanvas.init(function () {
              var volume = mock.makeTestVolume();
              vrcanvas.setVolume(volume);
              vrcanvas.setLUT(volume.getLUT());
              vrcanvas.setRenderer({directory: "/shaders/coords/"},
                  function () {
                    vrcanvas.draw();
                    testContextLost(vrcanvas, function () {
                      assert.pixelEqual(canvas, 0, 0, [0,0,0],
                          "top left corner");
                      console.log("contextlostteest...done");
                      console.groupEnd();
                      done();
                    });
                  });
            });
          });

        });

        QUnit.test("mip", function (assert) {
          var vrcanvas = makeVRCanvas(-4.0);
          var canvas = vrcanvas.canvas;

          canvas.style.width = 800+"px";
          canvas.style.height = 800+"px";

          var done = assert.async();
          vrcanvas.init(function () {
            var volume = mock.makeTestVolume(256, 256, 256);
            vrcanvas.setVolume(volume);
            vrcanvas.setLUT(volume.getLUT());
            vrcanvas.setAnatomyVisible(false);
            vrcanvas.setRenderer({directory: "/shaders/mip/"}, function () {
              vrcanvas.draw();

              var w = canvas.width;
              var h = canvas.height;

              assert.pixelEqual(canvas, 0, 0, [0,0,0], "top left corner");
              assert.pixelApprox(canvas, w/2,   h/2,   255/2, "middle");
              assert.pixelApprox(canvas, w/4,   h/2,   20, "left");
              assert.pixelApprox(canvas, w*3/4, h/2, 254, "right");
              done();
            });
          });
        });

        QUnit.test("makeVRCanvas HQ", function (assert) {
          var vrcanvas = makeVRCanvas();
          var canvas = vrcanvas.canvas;
          canvas.style.width = 800+"px";
          canvas.style.height = 800+"px";

          var done = assert.async();
          vrcanvas.init(function () {
            vrcanvas.setAnatomyVisible(false);
            vrcanvas.vr.setSubSample(4);
            vrcanvas.vr.beginHQDraw();
            vrcanvas.draw();

            var w = canvas.width;
            var h = canvas.height;
            assert.pixelEqual(canvas, w/4, h/4, [0,0,0], "tile0");
            assert.pixelEqual(canvas, w/4, h*3/4, [0,0,0], "tile2");

            vrcanvas.draw();
            vrcanvas.draw();
            vrcanvas.draw();
            assert.pixelEqual(canvas, 0, 0, [0,0,0], "top left corner");
            assert.pixelEqual(canvas, w-1, h-1, [0,0,0], "bottom right corner");
            assert.pixelApprox(canvas, w/2, h/2, [256/2,256/2,255], "middle");
            done();
          });
        });

        /* should not call resize with invalid dimensions.
         * i.e. before they are set.  */
        QUnit.test("setAnatomyVisible vs resize", function (assert) {
          var vrcanvas = makeVRCanvas(0.0);

          var done = assert.async();

          vrcanvas.setAnatomyVisible(true);
          vrcanvas.init(function () {
            vrcanvas.setAnatomyVisible(true);
            assert.equal(vrcanvas.vr.showAnatomy, true, "anatomy is visible");
            done();
          });
        });

        QUnit.test("setRenderer cache", function (assert) {
          var vrcanvas = makeVRCanvas(0.0);

          var done = assert.async();

          var renderer;

          vrcanvas.init(function () {
            var volume = mock.makeTestVolume(256, 256, 256);
            vrcanvas.setVolume(volume);
            vrcanvas.setLUT(volume.getLUT());

            vrcanvas.setRenderer({directory: "/shaders/mip/"}, function () {
              renderer = vrcanvas.vr.renderer;
              vrcanvas.setRenderer({directory: "/shaders/mip/"}, function () {
                assert.equal(vrcanvas.vr.renderer, renderer, "renderer cached");
                done();
              });
            });
          });
        });

        QUnit.test("hq draw", function (assert) {
          var vrcanvas = makeVRCanvas(0.0);

          var done = assert.async();

          vrcanvas.init(function () {
            var volume = mock.makeTestVolume(256, 256, 256);
            vrcanvas.setVolume(volume);
            vrcanvas.setLUT(volume.getLUT());
            var vr = vrcanvas.vr;
            vr.setSubSample(3);

            vrcanvas.setRenderer({directory: "/shaders/mip/"}, function () {
              vr.beginHQDraw();
              var drawCount = 0;
              do {
                vrcanvas.draw();
                drawCount++;
              } while(vr.isHQDraw());

              assert.equal(drawCount, 5, "High quality draw");
              done();
            });
          });
        });

        QUnit.test("previous stage access", function (assert) {
          var vrcanvas = makeVRCanvas(-3.0);
          vrcanvas.canvas.style.width = "80px";
          vrcanvas.canvas.style.height = "80px";

          var done = assert.async();

          vrcanvas.init(function () {
            var volume = mock.makeTestVolume(256, 256, 256);
            vrcanvas.setVolume(volume);
            vrcanvas.setLUT(volume.getLUT());
            vrcanvas.setAnatomyVisible(false);
            var vr = vrcanvas.vr;

            vrcanvas.setRenderer({directory: "/shaders/tests/previous-diff/"}, function () {
              vrcanvas.draw();
              assert.canvasColor(vrcanvas.canvas, [0,0,0], "no difference", 1);

              done();
            });
          });
        });

        QUnit.test("nearClipPlane", function (assert) {
          var nearClipPlane = 0.1;
          var vrcanvas = makeVRCanvas(-nearClipPlane);
          var canvas = vrcanvas.canvas;
          canvas.style.width = "256px";
          canvas.style.height = "256px";

          var done = assert.async();

          vrcanvas.init(function () {
            var volume = mock.makeTestVolume(10, 10, 10);
            volume.getVoxelSize = function () {
              return [10, 10, 10]; // [mm]; make volume 1m³
            };
            vrcanvas.setVolume(volume);
            vrcanvas.setAnatomyVisible(false);

            vrcanvas.setRenderer({directory: "/shaders/coords/"}, function () {
              vrcanvas.draw();
              assert.pixelApprox(canvas, canvas.width/2, canvas.height/2,
                  [127, 127, 127], "middle is origin coordinates");

              done();
            });
          });
        });

      });
    });
