/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "util/utils", "volume/volume", "volume/mip", "volume/avg",
    "volume/mpr", "volume/axis-numbers", "tests/volume-mock"],
    function(QUnit, utils, vol, MIPVolume, AVGVolume, mpr, AxisNumbers, mock) {
      "use strict";

    function reformatOptions(axis) {
      if (axis !== undefined) {
        var o = reformatOptions();
        o.axis = axis;

        return o;
      }

      return {
        patternFill: true,
        zIncrement: 3
      };
    }

    QUnit.module("Projections", function () {
      function testMIP(fromAxis, toAxis) {
        return function (assert) {
          var baseVolume = mock.makeTestVolume(4,4,8, reformatOptions(fromAxis));
          var testVolume = mpr.reformatVolume(baseVolume, toAxis);
          var vol = new MIPVolume(testVolume, testVolume.getSize()[2], "MIP");

          var labelMap = vol.getReformatedLabelMap();
          assert.deepEqual(vol.getSize(), labelMap.getSize());

          var index = testVolume.getSize()[2]/2;

          var expectedSlice = new Array(vol.getSize()[0] * vol.getSize()[1]);
          for (var pos=0; pos<expectedSlice.length; pos++) {
            var max = 0;
            for (var i=0; i<testVolume.getSize()[2]; i++) {
              var pixel = testVolume.slice(i)[pos];
              if (pixel > max) {
                max = pixel;
              }
            }
            expectedSlice[pos] = max;
          }

          assert.deepEqual(vol.slice(index), expectedSlice, "mip slice");
          assert.deepEqual(labelMap.slice(index), expectedSlice, "mip slice labelMap");

          var done = assert.async();
          vol.getSlice(function (slice) {
            assert.deepEqual(slice, expectedSlice, "mip slice async");
            done();
          }, index);
        };
      }

      QUnit.module("MIP", function () {
        for (var from=0; from<3; from++) {
          for (var to=0; to<3; to++) {
            if (from !== to) {
              QUnit.test("from " + AxisNumbers.toName(from) + " to " +
                  AxisNumbers.toName(to),
                  testMIP(from, to));
            }
          }
        }
      });

      QUnit.test("MinIP", function (assert) {
        var testVolume = mock.makeTestVolume(4,4,8, reformatOptions());

        var vol = new MIPVolume(testVolume, testVolume.getSize()[2], "MinIP");

        var labelMap = vol.getReformatedLabelMap();
        assert.deepEqual(vol.getSize(), labelMap.getSize());

        var index = vol.getSize()[2]/2;

        var expectedSlice = new Array(vol.getSize()[0] * vol.getSize()[1]);
        for (var pos=0; pos<expectedSlice.length; pos++) {
          var max = 255;
          for (var i=0; i<testVolume.getSize()[2]; i++) {
            var pixel = testVolume.slice(i)[pos];
            if (pixel < max) {
              max = pixel;
            }
          }
          expectedSlice[pos] = max;
        }

        assert.deepEqual(vol.slice(index), expectedSlice, "MinIP slice");
        assert.deepEqual(labelMap.slice(index), expectedSlice, "MinIP slice labelMap");
      });

      QUnit.test("AvgIP", function (assert) {
        var testVolume = mock.makeTestVolume(4,4,8, reformatOptions());

        var vol = new AVGVolume(testVolume, testVolume.getSize()[2]);

        var done = assert.async();
        vol.getSlice(function (slice) {
          for (var pos=0; pos<slice.length; pos++) {
            var pixelAcc = 0;
            for (var i=0; i<testVolume.getSize()[2]; i++) {
              pixelAcc += testVolume.slice(i)[pos];
            }
            pixelAcc = Math.round(pixelAcc/testVolume.getSize()[2]);
            assert.equal(slice[pos], pixelAcc, "pos " + pos);
          }
          done();
        },
        testVolume.getSize()[2]/2);
      });

      QUnit.test("AvgIP LabelMap", function (assert) {
        var testVolume = mock.makeTestVolume(4,4,8, reformatOptions());

        var vol = new AVGVolume(testVolume, testVolume.getSize()[2]);

        var labelMap = vol.getReformatedLabelMap();
        assert.deepEqual(vol.getSize(), labelMap.getSize());

        var done1 = assert.async();
        labelMap.getAnatomyId(function (id) {
          assert.ok(id >= 0, "valid labelmap id");
          done1();
        },
        testVolume.getSize()[2]/2);

        var done2 = assert.async();
        labelMap.getSlice(function (slice) {
          // FIXME better tests, but what?
          assert.equal(slice.length, 4 * vol.getSize()[0] * vol.getSize()[1]);
          done2();
        },
        testVolume.getSize()[2]/2);
      });
    });

    });
