/* Sectional Anatomy
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "ui/widgets"],
  function(QUnit, widgets) {
    "use strict";

    QUnit.module("ui", function () {
        QUnit.test("slider-1", function (assert) {
          var done = assert.async();
          var expected = 0.7;
          var slider = widgets.makeSlider(function onchange (value)
              {
                assert.equal(value, expected, "onchange");
                done();
              });

          slider.setValue(0.5);
          assert.equal(slider.getValue(), 0.5, "middle");

          slider.setValue(1.5);
          assert.equal(slider.getValue(), 1.0, "max");

          slider.setValue(-1.5);
          assert.equal(slider.getValue(), 0.0, "min");

          slider.updateValue(expected);
        });

        QUnit.test("interval-slider-1", function (assert) {
          var slider = widgets.makeIntervalSlider();

          slider.setValue([0, 1]);
          assert.deepEqual(slider.getValue(), [0, 1], "full range");

          slider.setValue([0.5, 0]);
          assert.deepEqual(slider.getValue(), [0.5, 0.5], "clamp asccending");
        });
    });
  });
