/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2015 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "ui/views", "view/grid-view", "tests/volume-mock"],
    function(QUnit, views, GridView, mock) {
      "use strict";

      QUnit.module("views");

      QUnit.test("showSingleView", function (assert) {
        var fixture = document.getElementById("qunit-fixture");
        fixture.style.width = "1000px";
        fixture.style.height = "500px";

        var div = document.createElement("div");
        div.style.width = "100%";
        div.style.height = "100%";
        fixture.appendChild(div);

        var gridView = new GridView(div, {});
        var volume = mock.makeTestVolume(10,10,10);

        views.showSingleView(gridView, volume, false);

        assert.deepEqual(gridView.getSize(), [1, 2], "gridView size");

        var view = gridView.getView(0,0).sliceViews[0].canvas;
        assert.equal(view.width, 10, "canvas width");
        assert.equal(view.height, 10, "canvas height");
      });

      QUnit.test("showMPRView", function (assert) {
        var fixture = document.getElementById("qunit-fixture");
        fixture.style.width = "1000px";
        fixture.style.height = "500px";

        var div = document.createElement("div");
        div.style.width = "100%";
        div.style.height = "100%";
        fixture.appendChild(div);

        var gridView = new GridView(div, {});
        var volume = mock.makeTestVolume(10,10,10);

        views.showMPRView(gridView, volume, false);

        assert.deepEqual(gridView.getSize(), [2, 3], "gridView size");
      });
    });

