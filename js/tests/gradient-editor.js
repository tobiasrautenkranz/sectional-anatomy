/* Sectional Anatomy
 *
 * Copyright (C) 2015 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["QUnit", "util/utils", "ui/gradient-editor"],
    function(QUnit, utils, GradientEditor) {
      "use strict";
      QUnit.module("gradient-editor");

      var fixture = document.getElementById("qunit-fixture");

      QUnit.test("Luminance", function (assert) {
        var ge = new GradientEditor(fixture, "L");

        var grad = utils.arrayFromTypedArray(ge.toArray());
        assert.deepEqual(grad.slice(0, 4), [0,0,0,255], "gradient start");
        assert.deepEqual(grad.slice(4*255, 4*255+4), [255,255,255,255],
            "gradient end");

        ge.changeWindow(-1, -0.5);
        grad = utils.arrayFromTypedArray(ge.toArray());
        assert.deepEqual(grad.slice(4*250, 4*250+4), [255,255,255,255],
            "gradient change window");
        assert.deepEqual(grad.slice(4*255, 4*255+4), [255,255,255,255],
            "gradient end change window");
      });

      QUnit.test("Luminance Alpha", function (assert) {
        var ge = new GradientEditor(fixture, "LA");

        var grad = utils.arrayFromTypedArray(ge.toArray());
        assert.deepEqual(grad.slice(0, 4), [0,0,0,0], "gradient start");
        assert.deepEqual(grad.slice(4*255, 4*255+4), [255,255,255,255],
            "gradient end");
      });

      QUnit.test("RGBA", function (assert) {
        var ge = new GradientEditor(fixture, "RGBA");

        var grad = utils.arrayFromTypedArray(ge.toArray());
        assert.deepEqual(grad.slice(0, 4), [0,0,0,0], "gradient start");
        assert.deepEqual(grad.slice(4*255, 4*255+4), [255,255,255,255],
            "gradient end");

        ge.changeWindow(0, -0.5);
        grad = utils.arrayFromTypedArray(ge.toArray());
        assert.deepEqual(grad.slice(0, 4), [0,0,0,0],
            "gradient start change window");
        assert.deepEqual(grad.slice(4*255, 4*255+4), [255,255,255,255],
            "gradient end change windo change windoww");
      });


      QUnit.test("restore", function (assert) {
        var ge = new GradientEditor(fixture, "L");
        var ge2 = new GradientEditor(fixture, "RGBA");

        var l = ge.save();
        var lGrad = ge.toArray();

        var rgba = ge2.save();
        var rgbaGrad = ge2.toArray();

        ge.setChannels("RGBA", rgba);
        assert.deepEqual(ge.toArray(), rgbaGrad, "restore RGBA");

        ge2.setChannels("L", l);
        assert.deepEqual(ge2.toArray(), lGrad, "restore L");
      });

      QUnit.test("addStop invariance", function (assert) {
        var ge = new GradientEditor(fixture, "RGBA");
        var grad = ge.toArray();

        var positions = [0.5, 0.1, 0.75, 0, 1];
        positions.forEach(function (p) {
          var stop = ge.addColorStop(p);
          assert.deepEqual(ge.toArray(), grad, "colorStop position " + p);
          ge.removeStop(stop);
          assert.deepEqual(ge.toArray(), grad, "colorStop removed");
        });

        positions.forEach(function (p) {
          var stop = ge.addAlphaStop(p);
          assert.deepEqual(ge.toArray(), grad, "alphaStop position " + p);
          ge.removeStop(stop);
          assert.deepEqual(ge.toArray(), grad, "alphaStop removed");
        });
      });
      QUnit.test("toArray", function (assert) {
        var ge = new GradientEditor(fixture, "RGBA");
        var array = ge.toArray();
        assert.equal(array.length, 256*4, "toArray() length");
        assert.ok(array instanceof Uint8Array,
            "toArray() returns a Uint8Array");
      });
    });
