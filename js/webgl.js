/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["view/gl-view", "ui/gl-view-tools", "ui/views"],
  function(GLView, GLViewTools, views) {
    "use strict";
    var glViewTools;
    function showGLView(viewer) {
      var gridView = viewer.gridView;
      if (!glViewTools) {
        glViewTools = new GLViewTools(gridView.parentElement, viewer);
        viewer.tabs.addOverlay(glViewTools);
      }
      var volume = gridView.getSelectedView().getVolume().getSourceVolume();
      gridView.setSize(1,1, {viewTools: glViewTools}, GLView);

      var glView = gridView.getView(0, 0);
      glView.setVolumes([volume]);
      glView.setRenderer(glViewTools.renderers[2]);

      var setAnatomyHidden = function (small) {
        glView.setAnatomyHidden(small);
      };
      views.setViewProperties(viewer.gridView, {
        setAnatomyHidden: setAnatomyHidden,
        isAnatomyVisible: function () {return glView.isAnatomyVisible();}
      });
      viewer.gridView.onResize();
    }

    return {
      showGLView: showGLView
    };
  });
