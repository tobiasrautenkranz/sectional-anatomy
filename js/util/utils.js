/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2018 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** Utility & shared functions  */
define(function () {
  "use strict";

  var debug = false;
  //>>includeStart("debug", pragmas.debug);
    debug = !urlParam("nodebug");
  //>>includeEnd("debug");

  /** Calls CONT with the object resulting from the JSON file at URL. */
  function loadJSONFile(URL, cont, version) {
    loadFile(URL, function (content) { cont(JSON.parse(content)); }, version);
  }
  function loadFile(URL, cont, version) {
    var request = new XMLHttpRequest();
    if (version) {
      request.open("GET", URL + "?v=" + version, true);
    } else {
      request.open("GET", URL, true);
    }
    request.addEventListener("error", function () {
      console.log("transfer failed", URL);
    });
    request.addEventListener("abort", function () {
      console.log("transfer canceled", URL);
    });
    request.onreadystatechange = function (rEvent) {
      if (request.readyState === 4) {
        if (request.status === 200) {
          cont(request.responseText, URL);
        } else {
          throw new Error("Error loading " + URL + ": " + request.statusText);
        }
      }
    };
    request.send();
  }

  /** applies f to arrays */
  function map(f, array) {
    function mkAccessor(index) {
      return function(array) { return array[i]; };
    }

    var args = Array.prototype.slice.call(arguments).slice(1);
    var result = new Array(array.length);
    for (var i=0, n=result.length; i<n; i++) {
      result[i] = f.apply(undefined, args.map(mkAccessor(i)));
    }
    return result;
  }

  function forEach(array, f) {
    for (var i=0; i<array.length; i++) {
      f(array[i]);
    }
  }

  function reduce(array, f, init) {
    for (var i=0; i<array.length; i++) {
      init = f(init, array[i]);
    }
    return init;
  }

  /** Returns value such that min <= value <= max. */
  function clamp(value, min, max) {
    if (undefined === min) {
      min = 0;
    }
    if (undefined === max) {
      max = 1;
    }

    if (Array.isArray(value)) {
      return value.map(function(v) {
        return Math.max(min, Math.min(max, v));
      });
    }

    return Math.max(min, Math.min(max, value));
  }

  /** Returns value such that min(a,b) <= value <= max(a,b) */
  function clampToRange(value, a, b) {
    if (a > b) {
      return clamp(value, b, a);
    }
    return clamp(value, a, b);
  }

  /** Returns the indices of data for which searchString matches. */
  function findSearchString(searchStrings, data, accessorFkt) {
    var ids = [];
    var searchStringsRegex =  [];

    if (searchStrings.length !== 0) {
      accessorFkt = accessorFkt || function (d) {return d;};

      for (var i=0; i<searchStrings.length; i++) {
        searchStringsRegex[i] = new RegExp("\\b"+searchStrings[i], "i");
      }

      for (var i=0, length=data.length; i<length; i++) {
        var index=0;
        while (index < searchStringsRegex.length &&
            searchStringsRegex[index].test(accessorFkt(data[i])))
        {
          index++;
        }
        if (index === searchStrings.length) {
          ids.push(i);
        }
      }
    }

    return {
      ids: ids,
      regexs: searchStringsRegex
    };
  }

  /** Returns the search terms of searchString. */
  function splitSearchString(searchString) {
    var search = searchString.trim();
    return 0 === search.length ?
      [] : search.split(/\s+/);
  }

  function sign(number) {
    return number >= 0 ? 1 : -1;
  }

  /** Throws error if EXPR is false */
  var assert;
  if (!debug) {
    assert = function assertNOOP() { };
  } else {
    if (console && console.assert && console.assert.bind) {
      assert = console.assert.bind(console);
    } else {
      assert = function (expr, messages) {
        if (!expr) {
          var message = "";
          for (var i=1; i<arguments.length; i++) {
            message += " " + arguments[i];
          }
          throw new Error("assert:" + message);
        }
      };
    }
  }


  function urlParam(parameter) {
    var href = window.location.hash;
    var index = href.indexOf("#" + parameter + "=");
    if (-1 === index) {
      index = href.indexOf("&" + parameter + "=");
    }

    if (-1 === index) {
      return undefined;
    } else {
      index += 2 + parameter.length;
      var endIndex = href.substr(index).indexOf("&");
      if (-1 === endIndex)
        return href.substr(index);
      else
        return href.substr(index, endIndex);
    }
  }

  var hashSeparator="&";
  function makeURLHash(parameters) {
    var isFirst = true;
    var result = "#";

    for (var p in parameters) {
      if (!isFirst) {
        result += hashSeparator;
      }

      result += p;
      result += "=";
      result += encodeURIComponent(parameters[p]);

      isFirst = false;
    }

    return result;
  }

  function clearURLHash() {
    if (window.history && window.history.replaceState) {
      window.history.replaceState(null, null, document.location.pathname);
    }
  }

  function getAbsoluteOffset(element) {
    // getBoundingClientRect works differently
    // (probably respects CSS transforms?)
    var left = 0;
    var top = 0;
    while (element) {
      left += element.offsetLeft;
      top += element.offsetTop;

      element = element.offsetParent;
    }
    return [left, top];
  }

  // do not use onwheel event because of deltaMode madness and maybe softscroll
  var mouseWheelEvent = window.onmousewheel !== undefined ?
    "mousewheel" :
    "DOMMouseScroll"; // Firefox/Iceweasel

  function mouseWheelClicks(mouseEvent) {
    if (mouseEvent.wheelDelta) {
      return mouseEvent.wheelDelta/120;
    } else {
      return mouseEvent.detail/-3;
    }
  }

  function isRightClick(mouseClickEvent) {
    if (mouseClickEvent.which) {
      return (mouseClickEvent.which == 3);
    } else if (mouseClickEvent.button) {
      return (mouseClickEvent.button == 2);
    }

    return false;
  }

  /* Version of the studies, update to invalidate browser cache. */
  var studiesDataVersion = "1.4";
  var version = "1.4.7";
  //>>includeStart("debugInclude", pragmas.debug);
  version += "-dev-" + Date.now();
  //>>includeEnd("debugInclude");

  /* Returns the version of the studies data;
   * use to invalidate the browser cache. */
  function studyDataVersion(study) {
    return study.version || studiesDataVersion;
  }

  function majorVersion(version) {
    var point = version.indexOf(".");
    if (-1 === point) {
      return version;
    } else {
      return version.substring(0, point);
    }
  }
  function minorVersion(version) {
    var point = version.indexOf(".");
    if (-1 === point) {
      return "0";
    } else {
      return version.substring(point+1) || "0";
    }
  }

  function makeEventFromTouch(e, touch) {
    touch.preventDefault = function () { e.preventDefault(); };
    touch.stopPropagation = function () { e.stopPropagation(); };
    return touch;
  }

  function addMouseAndTouchListener(element, type, onEvent) {
    var eventTranslations = {
      "mousedown":  {
        name: "touchstart",
        touchList: "changedTouches",
      },
      "mouseup":  {
        name: "touchend",
        touchList: "changedTouches",
      },
      "mousemove":  {
        name: "touchmove",
        touchList: "touches",
      }
    };

    touchEvent = eventTranslations[type];

    if (!touchEvent) {
      throw new Error("unknown mouse eventy type: " + type);
    }

    element.addEventListener(type, onEvent);

    var touchListener = function (e) {
      onEvent(makeEventFromTouch(e, e[touchEvent.touchList][0]));
    };
    element.addEventListener(touchEvent.name, touchListener);

    return [onEvent, touchListener];
  }

  function makeDragEventListener(element, onStart, onDrag, onEnd) {
    element.addEventListener("mousedown", function (e) {
      window.addEventListener("mouseup", function mouseup () {
        window.removeEventListener("mouseup", mouseup);
        window.removeEventListener("mousemove", onDrag);
        if (onEnd) {
          onEnd(e);
        }
      });
      window.addEventListener("mousemove", onDrag, { passive: false });

      onStart(e);
    });

    var onTouchDrag = function (e) {
      onDrag(makeEventFromTouch(e, e.touches[0]));
      e.preventDefault();
    };

    element.addEventListener("touchstart", function (e) {

      window.addEventListener("touchend", function touchend () {
        window.removeEventListener("touchend", touchend);
        window.removeEventListener("touchmove", onTouchDrag);
        if (onEnd) {
          onEnd(makeEventFromTouch(e, e.changedTouches[0]));
        }
      });
      window.addEventListener("touchmove", onTouchDrag, { passive: false });

      onStart(makeEventFromTouch(e, e.changedTouches[0]));
    });
  }


  function hasWebGL (options) {
    var webgl = false;
    try {
      var canvas = document.createElement("canvas");
      webgl = !!(canvas.getContext("webgl", options) ||
          canvas.getContext("experimental-webgl", options));
    } catch (e) {
      return false;
    }
    return webgl;
  }

  function hasFastWebGL () {
    var options = {failIfMajorPerformanceCaveat: true};
    return hasWebGL(options);
  }

  function setContext2DImageSmoothing(context, value) {
    if (context.imageSmoothingEnabled !== undefined) {
      context.imageSmoothingEnabled = value;
      return;
    }

    context.mozImageSmoothingEnabled = value;
    context.webkitImageSmoothingEnabled = value;
    context.msImageSmoothingEnabled = value;
    context.imageSmoothingEnabled = value;
  }

  function getOffscreenContext2D(canvas) {
    var context =  canvas.getContext("2d", {
      alpha: false,

      // FireFox OS: do not use HW accel (because of getImageData)
      willReadFrequently: true
    });

    return context;
  }

  function arrayFromTypedArray (a) {
    var array = new Array(a.length);
    for (var i=0; i<a.length; i++) {
      array[i] = a[i];
    }
    return array;
  }

  function roundArray (out, array) {
    for (var i=0; i<array.length; i++) {
      out[i] = Math.round(array[i]);
    }
    return out;
  }

  return {
    forEach: forEach,
    map: map,
    reduce: reduce,
    clamp: clamp,
    clampToRange: clampToRange,
    sign: sign,

    assert: assert,

    loadFile: loadFile,
    loadJSONFile: loadJSONFile,
    splitSearchString: splitSearchString,
    findSearchString: findSearchString,

    getAbsoluteOffset: getAbsoluteOffset,
    mouseWheelEvent: mouseWheelEvent,
    mouseWheelClicks: mouseWheelClicks,
    isRightClick: isRightClick,

    addMouseAndTouchListener: addMouseAndTouchListener,
    makeDragEventListener: makeDragEventListener,

    urlParam: urlParam,
    makeURLHash: makeURLHash,
    clearURLHash: clearURLHash,

    studiesDataVersion: studiesDataVersion,
    studyDataVersion: studyDataVersion,
    majorVersion: majorVersion,
    minorVersion: minorVersion,
    versionNumber: version,

    hasWebGL: hasWebGL,
    hasFastWebGL: hasFastWebGL,

    setContext2DImageSmoothing: setContext2DImageSmoothing,
    getOffscreenContext2D: getOffscreenContext2D,
    arrayFromTypedArray: arrayFromTypedArray,

    roundArray: roundArray,


    debug: debug
  };
});
