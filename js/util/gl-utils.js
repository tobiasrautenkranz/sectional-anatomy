/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/console"],
  function (utils, console) {
    "use strict";

    function checkFramebufferStatus(gl) {
      var s = gl.checkFramebufferStatus(gl.FRAMEBUFFER);

      switch(s) {
        case gl.FRAMEBUFFER_COMPLETE:
          break;
        case gl.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
          throw new Error("Error: GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT");
        case gl.FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
          throw new Error("Error: GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS");
        case gl.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
          throw new Error("Error: GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT");
        case gl.FRAMEBUFFER_UNSUPPORTED:
          if (!gl.isContextLost()) {
            throw new Error("Error: GL_FRAMEBUFFER_UNSUPPORTED");
          }
          break;
        default:
          throw new Error("Unknown Framebufferstatus: " + s);
      }
    }


    function makeFramebuffer(gl, width, height, format, numberOfAttachments) {
      var framebuffer = gl.createFramebuffer();
      gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);

      var maxTextureSize = gl.getParameter(gl.MAX_TEXTURE_SIZE);
      utils.assert(width <= maxTextureSize && height <= maxTextureSize,
          "requested framebuffer texture to big. " + width + "x" + height);

      numberOfAttachments = numberOfAttachments || 1;

      var attachment = gl.COLOR_ATTACHMENT0;
      // is equal to draw_buffers_EXT.COLOR_ATTACHMENT0_WEBGL

      var textures = new Array(numberOfAttachments);
      for (var i=0; i<numberOfAttachments; i++, attachment++) {
        var texture = gl.createTexture();
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        gl.texImage2D(gl.TEXTURE_2D, 0, format, width, height, 0,
            format, gl.UNSIGNED_BYTE, null);

        gl.framebufferTexture2D(gl.FRAMEBUFFER, attachment,
            gl.TEXTURE_2D, texture, 0);

        textures[i] = texture;
      }

      if (utils.debug) checkFramebufferStatus(gl);


      gl.bindTexture(gl.TEXTURE_2D, null);
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);

      if (utils.debug) checkGLError(gl, "makeFramebuffer");

      return {
        framebuffer: framebuffer,
        texture: 1 === numberOfAttachments ? textures[0] : textures
      };
    }

    /** Checks if a WebGL error occurred (see webgl.getError()). */
    function checkGLError(gl, message) {
      var error = gl.getError();
      if (error !== gl.NO_ERROR && error !== gl.CONTEXT_LOST_WEBGL) {
        if (message) {
          throw new Error(message + ": WebGL error: " + enumToString(gl, error));
        } else {
          throw new Error("WebGL error: " + enumToString(gl, error));
        }
      }
    }


    // ------------------------------
    // Shader stuff
    // -----------------------------

    function shaderType(gl, extension) {
      if (".vert" === extension)
        return gl.VERTEX_SHADER;
      if (".frag" === extension)
        return gl.FRAGMENT_SHADER;
      else
        throw new Error("Unknow shader extension: " + extension);
    }

    var loadShaderSources = function(urls, cont, version) {
      function loadNextShaderSource(urls, shaders, cont) {
        if (urls.length > 0) {
          var URL = urls.pop();

          utils.loadFile(URL, function (shaderSource) {
            shaders[URL] = shaderSource;
            loadNextShaderSource(urls, shaders, cont);
          }, version);
        } else {
          cont(shaders);
        }
      }

      loadNextShaderSource(urls, {}, cont);
    };


    function createShaderWithSource(gl, source, type, name) {
      utils.assert(gl.VERTEX_SHADER === type || gl.FRAGMENT_SHADER === type,
          "Invalid shader type:", type);

      var shader = gl.createShader(type);
      gl.shaderSource(shader, source);
      gl.compileShader(shader);

      if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS) &&
          !gl.isContextLost()) {
        var error = gl.getShaderInfoLog(shader);
        gl.deleteShader(shader);
        if (name) {
          error = name + ":\n" + error;
        }
        throw new Error(error);
      }
      if (utils.debug) {
        checkGLError(gl, "createShaderWithSource");
        var log = gl.getShaderInfoLog(shader);
        if (log && log.length > 0) {
          console.log(name, ":\n", log);
        }
      }

      return shader;
    }

    function concatShaderSources(sources) {
      if (!Array.isArray(sources)) {
        return sources;
      }

      var source = sources[0];
      for (var i=1; i<sources.length; i++) {
        source += "#line 1 " + i + "\n";
        source += sources[i];
      }
      return source;
    }

    function createShaderWithSources(gl, sources, type, name) {
      return createShaderWithSource(gl, concatShaderSources(sources), type, name);
    }

    function enumToString(object, enumValue) {
      for (var p in object) {
        if (object[p] === enumValue) return p;
      }
      return undefined;
    }

    function linkProgram(gl, program) {
      gl.linkProgram(program);

      if(!gl.getProgramParameter(program, gl.LINK_STATUS) &&
          !gl.isContextLost()) {
        var error =  gl.getProgramInfoLog(program);
        gl.deleteProgram(program);
        throw new Error("Error Linking: " + error);
      }
      if (utils.debug) checkGLError(gl, "linkProgram");
    }

    var createProgramWithShaders =
      function (gl, vertexShader, fragmentShader, attributes) {
        var program = gl.createProgram();

        utils.assert(gl.isShader(vertexShader),
            "not a shader: shaders[" + i + "]: " + vertexShader);
        gl.attachShader(program, vertexShader);

        utils.assert(gl.isShader(fragmentShader),
            "not a shader: shaders[" + i + "]: " + fragmentShader);
        gl.attachShader(program, fragmentShader);

        if (attributes) {
          for (var i=0; i<attributes.length; i++) {
            gl.bindAttribLocation(program, i, attributes[i]);
          }
        }

        linkProgram(gl, program);

        return program;
      };

    function validateProgram(gl, program) {
      utils.assert(gl.isProgram(program), "not a GLSL program: " + program);
      gl.validateProgram(program);

      if(!gl.getProgramParameter(program, gl.VALIDATE_STATUS) &&
          !gl.isContextLost()) {
        var error =  gl.getProgramInfoLog(program);
        throw new Error("Validation failed for shader program: \"" +
            (program.name || program) + "\" with error: \"" + error + "\"");
      }

      if (utils.debug) {
        var log = gl.getProgramInfoLog(program);
        if (log.length > 0 &&
            log.charCodeAt(0) !== 0 // fix for chrome
            ) {
          console.log("WebGL: programInfoLog: ", program.name || program, log);
        }
      }
    }

    function getWebGLContext(canvas, options) {
      return canvas.getContext("webgl", options) ||
        canvas.getContext("experimental-webgl", options);
    }

    var prefixes = ["", "MOZ", "WEBKIT"];
    function getExtension(gl, extensionName) {
      var extension;
      for (var i=0; i<prefixes.length; i++) {
        var prefix = prefixes[i];
        if (prefix) {
          prefix += '_';
        }
        extension = gl.getExtension(prefix + extensionName);
        if (extension) {
          break;
        }
      }

      return extension;
    }

    function createBuffer(gl, type, data, usage) {
      var buffer = gl.createBuffer();

      gl.bindBuffer(type, buffer);
      gl.bufferData(type, data, usage);

      return buffer;
    }

    return {
      getContext: getWebGLContext,

      getExtension: getExtension,

      checkGLError: checkGLError,
      checkFramebufferStatus: checkFramebufferStatus,
      makeFramebuffer: makeFramebuffer,

      shaderType: shaderType,
      createProgramWithShaders: createProgramWithShaders,
      loadShaderSources: loadShaderSources,
      createShaderWithSources: createShaderWithSources,
      linkProgram: linkProgram,
      validateProgram: validateProgram,

      createBuffer: createBuffer
    };

  });
