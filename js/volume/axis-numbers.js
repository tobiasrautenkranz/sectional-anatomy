/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "lib/gl-matrix"],
    function (utils, glm) {
      "use strict";

      var vec3 = glm.vec3;

      /*
       * The transverse, coronal and sagittal plane are identified by their normal
       * vector in the transverse coordinate system. The normal vector is associated
       * with the number of the coordinate system axis along the same direction.
       * Where 0 identifies the x axis, 1 the y and 2 the z axis.
       *
       * i.e. coronal slices are stacked along the y axis of a transverse volume.
       */
      var AxisNumbers = {
        sag: 0,
        cor: 1,
        tra: 2,
        other: undefined, // e.g. oblique (not useable for reformat)
      };

      function isValidAxisNumber(a) {
        switch(a) {
          case AxisNumbers.sag:
          case AxisNumbers.cor:
          case AxisNumbers.tra:
            return true;

          default:
            return false;
        }
      }

      var axisNames = ["sagittal", "coronal", "transverse"];
      function axisNameToNumber(name) {
        for (var i=0; i<3; i++) {
          if (axisNames[i] === name) {
            return i;
          }
        }
        return undefined;
      }

      function axisNumberToName(number) {
        if (number < 0 || number > 2) {
          return "other (" + number + ")";
        }
        return axisNames[number];
      }

      function axisNumberToAxes(axisNumber, voxelSize) {
        var x = voxelSize[0];
        var y = voxelSize[1];
        var z = voxelSize[2];
        var axis;
        switch (axisNumber) {
          case AxisNumbers.sag:
            axis = [[0,y,0], [0,0,z], [x,0,0]];
            break;
          case AxisNumbers.cor:
            axis = [[x,0,0], [0,0,z], [0,y,0]];
            break;
          case AxisNumbers.tra:
            axis = [[x,0,0], [0,y,0], [0,0,z]];
            break;
          default:
            throw new Error("unknown axisNumber: " + axisNumber);
        }
        return axis;
      }


      function axisNumberToVolumeOrigin(axisNumber, volumeSize) {
        var origin;
        switch (axisNumber) {
          case AxisNumbers.sag:
            return vec3.fromValues(
                volumeSize[0] - 1,
                0,
                volumeSize[2] - 1);

          case AxisNumbers.cor:
            return vec3.fromValues(0, 0, volumeSize[2] - 1);

          case AxisNumbers.tra:
            return vec3.fromValues(0, 0, 0);
        }
        utils.assert(false, "not reached");
      }

      return {
        sag: AxisNumbers.sag,
        cor: AxisNumbers.cor,
        tra: AxisNumbers.tra,
        isValid: isValidAxisNumber,
        fromName: axisNameToNumber,
        toName: axisNumberToName,

        toAxes: axisNumberToAxes,
        toVolumeOrigin: axisNumberToVolumeOrigin
      };
    });
