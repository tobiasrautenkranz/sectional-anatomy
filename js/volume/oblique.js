/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["./derived-volume", "util/array", "util/utils", "lib/gl-matrix",
"./basis-transform", "./axis-numbers"],
  function (DerivedVolume, array, utils, glm, BasisTransform, AxisNumbers) {
    "use strict";

    var vec3 = glm.vec3;

    // Oblique (45°) reformat
    var ObliqueMPRVolume = function (baseVolume, direction) {
      DerivedVolume.call(this, baseVolume);
      direction = direction || "Oblique1";
      if (typeof direction === "number") {
        this.direction = direction;
      } else {
        for (var i=0; i<ObliqueMPRVolume.directions.length; i++) {
          if (ObliqueMPRVolume.directions[i] === direction) {
            this.direction = i;
            break;
          }
        }
        if (undefined === this.direction) {
          throw new Error("ObliqueMPRVolume: unknown direction \"" +
              direction + "\"");
        }
      }

      this.basis = new BasisTransform(this.getAxes(), this.getOrigin());
    };
    ObliqueMPRVolume.prototype = Object.create(DerivedVolume.prototype);

    ObliqueMPRVolume.directions = ["Oblique1", "Oblique2"];

    ObliqueMPRVolume.prototype.getAxisNumber = function () {
      return AxisNumbers.other;
    };


    ObliqueMPRVolume.prototype.getSize = function () {
      var s = this.baseVolume.getSize();
      var l = Math.floor((s[0]+s[1])/2);
      return [l, s[2], l];
    };

    ObliqueMPRVolume.prototype.getVoxelSize = function () {
      var s = this.baseVolume.getVoxelSize();
      var l = Math.sqrt(s[0]*s[0] + s[1]*s[1]);
      var l2 = s[0]*s[1]/l;
      return [this.direction===0 ? l:-l, -s[2], l2];
    };

    ObliqueMPRVolume.prototype.getAxisName = function () {
      return ObliqueMPRVolume.directions[this.direction];
    };

    ObliqueMPRVolume.prototype.getAxes = function () {
      var basis = this.baseVolume.getBasis();
      var toWorld = function (v) {
        return basis.from(vec3.create(), v);
      };
      var axes = this.baseVolume.getAxes();
      var x = axes[0], y = axes[1], z = axes[2];
      if (0 === this.direction) {
        return [vec3.fromValues(1, -1, 0),
          vec3.fromValues(0, 0, 1),
          vec3.fromValues(1, 1, 0)
          ].map(toWorld);
      } else {
        return [vec3.fromValues(-1, -1, 0),
          vec3.fromValues(0, 0, 1),
          vec3.fromValues(1, -1, 0)
          ].map(toWorld);
      }
      /*
      var v = this.baseVolume.getVoxelSize();
      var x = v[0], y = v[1], z = v[2];

      if (0 === this.direction) {
        return [
          [x, -y, 0],
          [0, 0, z],
          [x, y, 0]];
      } else {
        return [
          [-x, -y, 0],
          [0, 0, z],
          [x, -y, 0]];
      }
      */
    };

    ObliqueMPRVolume.prototype.getOrigin = function ()  {
      var s = this.baseVolume.getSize();
      var origin;
      if (0 === this.direction) {
        origin = vec3.fromValues(-s[1]/2, s[1]/2, 0);
        return this.baseVolume.getBasis().from(origin, origin);
      } else {
        origin = vec3.fromValues(s[0]/2, s[0]/2 + s[1], 0);
        return this.baseVolume.getBasis().from(origin, origin);
      }
    };

    /*
     * oblique 45° injective reformat (i.e. 1:1 voxel mapping). Thus every voxel
     * of the oblique volume can be unambiguously identified with one in the
     * source volume. This is important for labelmap access. The space outside
     * of the souece Volume is filled with empty / black voxels.
     *
     *
     * To prevent small left-right offset flicker when scrolling only the
     * even diagonals are used as slices.
     *
     *       y                           y    volume_z (normal)
     *       ^     /                     ^     /
     *       |------                     |------
     *      \|   / |                    /|   / |
     * yStart|  /  |           origin₀ / s_y/  |
     *       |\/   |                   \ | /   |
     *       |/\   |                  ly\|/    |
     *       +---------> x yEnd          +-s_x-+---> x
     *           \                        \   /
     *         slice                     lx\ /lx
     *                                   origin₁
     *
     * (note that volume_z does not intersect the opposite corner if the
     * source volume is not square)
     *
     *  s_x² = lx² + lx²
     *
     *  =>
     *  lx = 1/√2 s_x
     *  ly = 1/√2 s_y
     *
     *  volume_z_max = lx + ly
     *
     *  origin₀_x = -½ s_y      origin₁_x = ½ s_x
     *  origin₀_y = ½ s_y       origin₁_y = -½ s_x
     *
     *
     *  z-axis of the source volume is assumed to be superior to inferior;
     *  Contrary to the convention used.
     *
     * -- magic -----
     */
    ObliqueMPRVolume.prototype.slice = function (index) {
      /** Returns the X coordinate of the point x,y in the slice projected
       * onto the oblique one. */
      function projectToOblique(x,y) {
        return xObliqueOrigin - Math.floor(1/2*(y-x));
      }

      var baseSize = this.baseVolume.getSize();
      var slice = array.mkUint8Array(this.getSize()[0]*this.getSize()[1]);

      index *= 2;

      // end is the last pixel
      var xStart = Math.max(0, index-baseSize[1]+1);
      var xEnd = Math.min(index, baseSize[0]-1);
      var yStart = Math.min(baseSize[1]-1, index);
      var yEnd = Math.max(0, index - baseSize[0]+1);

      var xMax = baseSize[0];
      var xObliqueMax = this.getSize()[0];

      var xObliqueOrigin = Math.ceil(baseSize[1]/2);
      var xObliqueStart =  projectToOblique(xStart, yStart);
      var xObliqueEnd =  projectToOblique(xEnd,yEnd);

      // fix rounding error
      if (xObliqueEnd-xObliqueStart > Math.abs(yEnd-yStart)) {
        if (xObliqueStart > 0) {
          xObliqueStart++;
        } else {
          xObliqueEnd--;
        }
      }
      if (xObliqueEnd-xObliqueStart > Math.abs(yEnd-yStart)) {
        if (xObliqueStart > 0) {
          xObliqueStart++;
        } else {
          xObliqueEnd--;
        }
      }

      // RAO
      if (1 === this.direction) {
        yStart = baseSize[1]-1 - yStart;
        yEnd = baseSize[1]-1 - yEnd;

        // swapocalypse
        var tmp;
        tmp = xStart; xStart = xEnd; xEnd = tmp;
        tmp = yStart; yStart = yEnd; yEnd = tmp;
        tmp = xObliqueStart; xObliqueStart = xObliqueEnd; xObliqueEnd = tmp;

        xObliqueStart = this.getSize()[2] - xObliqueStart;
        xObliqueEnd = this.getSize()[2] - xObliqueEnd;
      }

      //assert(Math.abs(xObliqueEnd-xObliqueStart) === Math.abs(yEnd-yStart),
      //"obl start end");

      var baseIndexStart = yStart*xMax + xStart; // (x++,y--) [y*xMax +x]
      var baseIndexD = (0 === this.direction) ?
        1 - xMax: // (x++,y--) LAO
        -1 - xMax; // (x--,y--) RAO

      for (var z=0, zMax = baseSize[2]; z < zMax; z++) {
        var yObliqueIndex = z*xObliqueMax;
        var baseSlice = this.baseVolume.slices[z];
        for (var xOblique = xObliqueStart, baseIndex=baseIndexStart;
            xOblique <= xObliqueEnd; xOblique++, baseIndex += baseIndexD) {
          slice[yObliqueIndex + xOblique] = baseSlice[baseIndex];
        }
      }
      return slice;
    };

    // FIXME copy from MPRVolume
    ObliqueMPRVolume.prototype.getSlice = function (cont, index) {
      if (!cont) {
        if (!this.baseVolume.isComplete()) {
          this.baseVolume.loadAllSlices(function (){});
        }
        return this.slice(index);
      }
      if (this.baseVolume.loading ||
          (this.baseVolume.notLoaded && this.baseVolume.notLoaded())) {
        var mprvol = this;
        var update = function update() {
          if (mprvol.baseVolume.loading && !cont(mprvol.slice(index))) {
            window.setTimeout(update, 1000);
          }
        };
        window.setTimeout(update, 500);
        this.baseVolume.loadAllSlices(function (){mprvol.getSlice(cont, index);});
      } else {
        cont(this.slice(index));
      }
    };

    ObliqueMPRVolume.prototype.hasCompleteSlice = function (index) {
      return this.baseVolume.isComplete();
    };

    ObliqueMPRVolume.prototype.getLabelMap = function () {
      var labelmap = new ObliqueMPRVolume(this.getSourceVolume().getLabelMap(),
          ObliqueMPRVolume.directions[this.direction]);
      labelmap.getAnatomyId = function (cont, index) {
        var sliceWidth = this.getSize()[0];
        this.getSlice(function(slice) {
          cont(slice[index[0] + sliceWidth*index[1]]);
        }, index[2]);
      };
      return labelmap;
    };

    return ObliqueMPRVolume;
  });
