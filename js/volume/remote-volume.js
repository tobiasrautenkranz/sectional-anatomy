/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "./lut", "util/array", "lib/gl-matrix", "./axis-numbers",
"./basis-transform"],
  function (utils, luts, array, glm, AxisNumbers, BasisTransform) {
    "use strict";

    var vec3 = glm.vec3;

    function voxelSizeFromAxes(axes) {
      var sqrt1_3 = 1 / Math.sqrt(3);
      var positiveVec = vec3.fromValues(sqrt1_3, sqrt1_3, sqrt1_3);
      return axes.map(function(a) {
        var l = vec3.length(a);
        var sign = utils.sign(vec3.dot(a, positiveVec));
        return 1 * l;
      });
    }

    // A Volume based on data (CT, MR, etc)
    // The volume is loaded slicewise (slices are jpeg images)
    var RemoteVolume = function (properties) {
      function setProperties(vol, properties) {
        if (properties.axes) {
          vol.axes = properties.axes;
          vol.fullAxes = vol.axes;
          vol.voxelSize = voxelSizeFromAxes(vol.axes);
        } else {
          vol.voxelSize = properties.voxelSize;
        }

        var names = ["size", "orientation", "url",
        "type"];
        for (var i in names) {
          var n = names[i];
          vol[n] = properties[n];
        }
        vol.name = properties.name;

        var optional_names = ["HUMinMax", "version", "depth"];
        for (i in optional_names) {
          var n = optional_names[i];
          if (properties.hasOwnProperty(n)) {
            vol[n] = properties[n];
          }
        }

        vol.defaultWindow = properties.defaultWindow || [120, 200];

        vol.fullSize = vol.size;
        vol.fullVoxelSize = vol.voxelSize.slice(0);
      }
      function imageToSliceFunction(depth) {
        switch (depth) {
          case 8:
            return imageToSlice8Bit;
          case 16:
            return imageToSlice16Bit;
        }
        throw new Error("unsupported bit depth: " + depth);
      }
      this.extension = ".jpeg";

      setProperties(this, properties);

      this.clearSlices();

      this.imageToSlice = imageToSliceFunction(this.getDepth());

      this.lut = new luts.GrayLUT();
      if (this.size) {
        this.missingSlices = this.size[2];
      }

      this.axisNumber = AxisNumbers.fromName(this.orientation);

      this.basis = new BasisTransform(this.getAxes(), this.getOrigin());
    };

    RemoteVolume.prototype.constructor = RemoteVolume;

    RemoteVolume.prototype.getDepth = function () {
      return this.depth || 8;
    };

    RemoteVolume.prototype.getAxisNumber = function () {
      return this.axisNumber;
    };

    RemoteVolume.prototype.isLabelMap = function () {
      return false;
    };

    RemoteVolume.prototype.getBasis = function () {
      return this.basis;
    };
    RemoteVolume.prototype.isLabelMap = function () {
      return false;
    };

    /** Set the quality level. e.g.: 0 */
    RemoteVolume.prototype.setQuality = function (study, quality) {
      if (this.quality != quality) {
        this.quality = quality;
        this.qualityName = study.qualities[this.quality].name;

        var scale = study.qualities[quality].scale || vec3.fromValues(1,1,1);
        this.size = vec3.create();
        vec3.multiply(this.size, this.fullSize, scale);
        utils.roundArray(this.size, this.size);

        if (this.fullAxes) {
          this.axes =
            utils.map(function(axis, scale) {
              return vec3.scale(axis, axis, 1/scale);
            }, this.fullAxes, scale);
          this.voxelSize = voxelSizeFromAxes(this.axes);
        } else {
          this.voxelSize = vec3.create();
          vec3.divide(this.voxelSize, this.fullVoxelSize, scale);
          this.axes = undefined;
        }

        this.clearSlices();

        this.basis = new BasisTransform(this.getAxes(), this.getOrigin());
      }
    };

    RemoteVolume.prototype.clearSlices = function () {
      this.slices = new Array(this.size[2]);
      for (var i=0, n=this.slices.length; i<n; i++) {
        this.slices[i] = [];
      }

      this.images = new Array(this.size[2]);
    };

    /** Returns a slice of the volume. */
    RemoteVolume.prototype.slice = function (index) {
      return this.slices[index];
    };

    /** Returns the size along the 3 axes of the volume in voxels. */
    RemoteVolume.prototype.getSize = function () {
      return this.size;
    };
    /** Returns the size of a voxel in some arbitrary unit. */
    RemoteVolume.prototype.getVoxelSize = function () {
      return this.voxelSize;
    };


    /** The axes in world coordinates. The length of a axis is equal to the
     * voxel size in that direction. */
    RemoteVolume.prototype.getAxes = function () {
      if (this.axes) {
        return this.axes;
      } else {
        this.axes = AxisNumbers.toAxes(this.getAxisNumber(), this.getVoxelSize());
        return this.axes;
      }
    };
    /** THe origin of the volume coordinate system in world coordinates.
     * i.e.(0,0,0) in volume coordinates. */
    RemoteVolume.prototype.getOrigin = function ()  {
      return this.origin || [0,0,0];//;AxisNumbers.toVolumeOrigin(this.getAxisNumber(), this.getSize());
    };



    /** Returns the LabelMap for the Volume. */
    RemoteVolume.prototype.getLabelMap = function () {
      return this.labelMap;
    };

    /** Returns the LabelMap of volume with the same axis. */
    RemoteVolume.prototype.getReformatedLabelMap = function () {
      return this.getLabelMap();
    };

    /** Returns the LUT for the Volume. */
    RemoteVolume.prototype.getLUT = function () {
      return this.lut;
    };

    RemoteVolume.prototype.getSourceVolume = function () {
      return this;
    };

    function makeOffscreenContext() {
      var canvas = document.createElement("canvas");

      // for debugging only
      /*
      var showOffscreenCanvas = true;
      if (showOffscreenCanvas) {
        var div = document.createElement("div");
        div.style.position="absolute";
        div.style.bottom = "0px";
        div.appendChild(canvas);
        document.body.appendChild(div);
      }
      */

      canvas.mozOpaque = true; // optimized rendering for Gecko

      canvas.width = 1;
      canvas.height = 1;
      var context = utils.getOffscreenContext2D(canvas);

      // deprecated
      var backingStoreRatio = context.webkitBackingStorePixelRatio ||
        context.mozBackingStorePixelRatio || context.msBackingStorePixelRatio ||
        context.oBackingStorePixelRatio   || context.backingStorePixelRatio   || 1;
      utils.assert(1 === backingStoreRatio,
          "unsupported CanvasBackingStorePixelRation unequal to one");
      utils.assert(4 === context.getImageData(0, 0, 1, 1).data.length,
          "Canvas: getImageData unexpected pixel size");

      /* we do not scale
       * (something went wrong with FirefoxOS, scales nevertheless?
       * (devicePixelRatio?)
       * => NN scaling is acceptable)
       * also should be faster :) http://jsperf.com/imagesmoothingenabled
       */
       utils.setContext2DImageSmoothing(context, false);

      return context;
    }

    var offScreenContext = makeOffscreenContext();

    function getOffScreenContext(size) {
      var canvas = offScreenContext.canvas;
      if (size[0] !== canvas.width ||
          size[1] !== canvas.height) {
        canvas.width = size[0];
        canvas.height = size[1];
        // race with onload when using different sized Images at once
        // (not the case)
      }
      return offScreenContext;
    }

    // Continuation Passing Style
    /* Passes the slice at INDEX (z-axis) as argument to CONT. */
    RemoteVolume.prototype.getSlice = function (cont, index) {
      if (!cont) {
        if (this.slices[index].length > 0) {
          return this.slices[index];
        }
        this.loadSlice(function(){}, index);

        return [];
      }

      if (this.slices[index] && this.slices[index].length > 0) {
        cont(this.slices[index]);
      } else {
        this.loadSlice(cont, index);
      }
    };

    /** Returns true when getSlice can return the final loaded slice. */
    RemoteVolume.prototype.hasCompleteSlice = function (index) {
      //    return this.slices[index].length > 0;
      //  FIXME (used to handle invalid index on load
      return this.slices[index] && this.slices[index].length > 0;
    };

    /* Returns IMAGE converted to an array by using canvas CONTEXT. */
    var imageToSlice8Bit = function (image, context) {
      context.drawImage(image, 0, 0);

      var pixels = context.getImageData(0, 0, image.width, image.height).data;
      var slice = array.mkUint8Array(Math.floor(pixels.length/4));

      utils.assert(slice.length === image.width*image.height,
          "Image", image, "dimension mismatch");

      // gray scale only.
      for (var i = 0, n = slice.length; i < n; i++){
        slice[i] = pixels[4*i];
      }

      return slice;
    };

    var hasUint16Array = !!window.Uint16Array;

    var imageToSlice16Bit;
    if (hasUint16Array) {
      imageToSlice16Bit = function (image, context) {
        context.drawImage(image, 0, 0);

        var imageData = context.getImageData(0, 0, image.width, image.height).data;
        var pixels = new Uint16Array(imageData.buffer);
        var slice = new Uint16Array(Math.floor(imageData.length/4));

        utils.assert(slice.length === image.width*image.height,
            "Image", image, "dimension mismatch");

        for (var i = 0, n = slice.length; i < n; i++){
          slice[i] = pixels[2*i];
        }

        return slice;
      };
    } else {
      /* Returns IMAGE converted to an array by using canvas CONTEXT. */
      imageToSlice16Bit = function (image, context) {
        context.drawImage(image, 0, 0);

        var pixels = context.getImageData(0, 0, image.width, image.height).data;
        var slice = array.mkUint16Array(Math.floor(pixels.length/4));

        utils.assert(slice.length === image.width*image.height,
            "Image", image, "dimension mismatch");

        for (var i = 0, n = slice.length; i < n; i++){
          // little endian
          slice[i] = pixels[4*i] + (pixels[4*i + 1] << 8);
        }

        return slice;
      };
    }

    var Tasks = (function () {
      var taskWorkerTimer = null;
      var tasks = [];
      function run () {
        var task = tasks.shift();
        while (task && task.cancel && task.cancel()) {
          task=tasks.shift();
        }	

        if (task) {
          var cont = task.cont();
          if (cont && task.cancel) {
            task.cont = cont;
            tasks.push(task);
          }	
        }

        if (tasks.length > 0) {
          taskWorkerTimer = null;
          maybeRunTasks();
        } else {
          taskWorkerTimer = null;
        }
      }
      function maybeRunTasks() {
        if (!taskWorkerTimer) {
          taskWorkerTimer = window.setTimeout(run, 10);
        }
      }

      return {
        add: function (cont, cancel) {
          tasks.push(cancel ?
              {"cont": cont, "cancel": cancel} :
              {"cont": cont});

          maybeRunTasks();
        },
      };
    })();

    function loadImage(url, cont) {
      var image = new Image();
      image.onload = function () {
        // hack: in IE9 the image is sometimes not ready when onload is called,
        // thus wait for 0 secs ;)
        window.setTimeout(function() { cont(image); }, 0);
      };
      image.src = url;
    }

    /* Passes the image of the slice at INDEX to CONT. */
    RemoteVolume.prototype.loadSlice = function (cont, index) {
      utils.assert(index >= 0, "index must be >= 0");

      loadImage(this.sliceURL(index), function(image) {
        this.images[index] = image;

        var addSlice = function () {
          if (this.slices[index].length === 0) {
            this.slices[index] = this.imageToSlice(image,
                getOffScreenContext(this.getSize()));
            this.images[index] = undefined;
            this.missingSlices--;
          }
          cont(this.slices[index]);
        }.bind(this);

        Tasks.add(addSlice);
      }.bind(this));
    };

    /* Returns the URL to access image of the slice at INDEX. */
    RemoteVolume.prototype.sliceURL = function (index) {
      var maxDigits = this.getSize()[2].toString().length;
      var zeroPaddedIndex = ("0000" + index).slice(-maxDigits);
      return this.baseURL + this.qualityName + "/"  + this.url + "/" +
        zeroPaddedIndex + this.extension + "?v=" + utils.majorVersion(this.version);
    };

    RemoteVolume.prototype.notLoaded = function (hint) {
      var i;
      for (i=hint||0;
          i < this.getSize()[2] && this.slices[i] && this.slices[i].length>0;
          i++) {
        // do nothing / just find the slice
      }
      if (i != this.getSize()[2])
        return [true,i];
      else
        return false;
    };

    /** Loads all slices of the volume and calls cont when done. */
    RemoteVolume.prototype.loadAllSlices = function (cont) {
      function loadNext(volume, slice, cont) {
        var s = volume.notLoaded(slice);
        if (s) {
          var vol = volume;
          volume.loading=true;
          if (!volume.onallload)
            volume.onallload = [];
          Tasks.add(function (){
            vol.loadSlice(function (){loadNext(vol, s[1], cont);}, s[1]);
          });
        } else {
          volume.loading=false;
          cont(volume);
          if (volume.onallload) {
            for(var i=0; i<volume.onallload.length; i++) {
              volume.onallload[i]();
            }
            volume.onallload = undefined;
          }
        }
      }
      if (this.loading) {
        this.onallload.push(cont);
      } else {
        loadNext(this, 0, cont);
      }
    };

    RemoteVolume.prototype.isComplete = function () {
      return (this.missingSlices <= 0);
    };


    return RemoteVolume;
  });
