/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "lib/gl-matrix"],
    function (utils, glm) {
      "use strict";

      var vec3 = glm.vec3;
      var mat3 = glm.mat3;


      function matrixFromBasisVectors (basisVectors) {
        function setBasisVector(mat, e, index) {
          var offset = 3*index;
          for (var i=0; i<3; i++) {
            mat[offset + i] = e[i];
          }
        }
        var mat = mat3.create();

        for (var i=0; i<3; i++) {
          setBasisVector(mat, basisVectors[i], i);
        }
        utils.assert(vec3.sqrLen(basisVectors[0]) > 0, "validbasis");


        return mat;
      }

      // transformation to / from the supplied basis
      var BasisTransform = function (axes, origin) {
        this.origin = origin || vec3.fromValues(0, 0, 0);

        this.fromBasisMatrix = matrixFromBasisVectors(axes);

        this.toBasisMatrix = mat3.invert(mat3.create(), this.fromBasisMatrix);
        utils.assert(this.toBasisMatrix, "matrix invert failed");
      };

      BasisTransform.prototype.from = function (worldCoordsOut, basisCoords) {
        return vec3.add(worldCoordsOut,
            vec3.transformMat3(worldCoordsOut, basisCoords, this.fromBasisMatrix),
            this.origin);
      };

      BasisTransform.prototype.to = function (basisCoordsOut, worldCoords) {
          return vec3.transformMat3(basisCoordsOut,
              vec3.subtract(basisCoordsOut, worldCoords, this.origin),
              this.toBasisMatrix);
      };

      BasisTransform.fromTo = function (out, fromBasis, toBasis, vector) {
        fromBasis.from(out, vector);
        return toBasis.to(out, out);
      };
      BasisTransform.to = function (out, toBasis, vector) {
        return toBasis.to(out, vector);
      };
      BasisTransform.from = function (out, fromBasis, vector) {
        return fromBasis.from(out, vector);
      };

      return BasisTransform;
    });
