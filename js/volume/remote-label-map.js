/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "./lut", "util/array", "./remote-volume"],
  function (utils, luts, array, RemoteVolume) {
    "use strict";

  // A LabelMap volume containing a segmentation.
  var LabelMap = function (properties) {
    RemoteVolume.call(this, properties);
    this.extension = ".png";
  };
  LabelMap.prototype =  Object.create(RemoteVolume.prototype);
  LabelMap.prototype.constructor = LabelMap;

  LabelMap.unselectedAlpha = 150;
  LabelMap.multiselectAlpha = 200;

  /** Loads the LUT and label description. */
  LabelMap.prototype.load = function (cont, study) {
    var labelMap = this;
    this.loadLabels(function () {
      loadLabelColors(labelMap, study.baseURL, cont);
    }, study);
  };
  LabelMap.prototype.isLabelMap = function () {
    return true;
  };
  LabelMap.prototype.getAnatomyId = function (cont, index) {
    var sliceWidth = this.getSize()[0];
    this.getSlice(function(slice) {
      cont(slice[index[0] + sliceWidth*index[1]]);
    }, index[2]);
  };

  LabelMap.prototype.loadLabels = function (cont, study, lang) {
      function convertUnderscores(label) {
        return label.replace(/_/g, " ");
      }

      lang = lang || study.labels.languages[study.language];
      var baseURL = study.baseURL;

      var labelMap = this;

      utils.loadJSONFile(baseURL + "labels_" + lang + ".json",
          function (labels) {
            labelMap.labels = labels.map(convertUnderscores);
            if(cont) cont();
          }, labelMap.version);
  };


  /* Loads the Color LUT for the labels. */
  function  loadLabelColors (labelMapVolume, baseURL, cont) {
    utils.loadJSONFile(baseURL + "labels_colors.json",
        function (colors) {
          var lut = array.mkUint8Array(colors);
          lut[3] = 0; // None transparent
          for(var i=7; i<lut.length; i+=4) {
            lut[i] = LabelMap.unselectedAlpha;
          }
          labelMapVolume.lut = new luts.ColorAlphaLUT(lut);
          if (cont) cont();
        }, labelMapVolume.version);
  }

  return LabelMap;
  });
