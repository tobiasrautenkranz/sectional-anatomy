/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/array"],
  function (typedArray) {
    "use strict";

    // gray scale LUT
    var GrayLUT = function (center, width) {
      // the distance covered by an array of size 256 is
      // 255; i.e.: the number of spaces between elements
      this.center = center || 255/2;
      this.width = width || 255;

      this.setWindow(this.center, this.width);
    };

    GrayLUT.prototype.setWindow = function (center, width) {
      this.center = center;
      this.width = width;

      this.LUT = mkGrayLUT(this.center, this.width, this.LUT);
    };

    GrayLUT.prototype.getArray = function () {
      return this.LUT;
    };

    GrayLUT.prototype.getCenter = function () {
      return this.center;
    };
    GrayLUT.prototype.getWidth = function () {
      return this.width;
    };

    function fill (array, value, start, end) {
      for (var i=start; i<end; i++) {
        array[i] = value;
      }
    }

    function mkGrayLUT (center, width, LUT) {
      function addWindowMinMax(min, max, LUT) {
        var delta = 1/(max-min);
        for (var i=Math.max(0, min), n=Math.min(LUT.length,max+1); i<n; i++) {
          var v = delta*(i-min);
          LUT[i] = Math.round(255*v);
        }

        return LUT;
      }
      function addWindow(center, width, LUT) {
        return addWindowMinMax(Math.floor(center-(width)/2),
            Math.ceil(center+width/2), LUT);
      }
      // Invert
      if (width < 0) {
        var inverseLUT = mkGrayLUT(center, -width, LUT);
        for (var i=0; i<LUT.length; i++) {
          inverseLUT[i] = 255-inverseLUT[i];
        }
        return inverseLUT;
      }

      LUT = LUT || typedArray.mkUint8Array(256);
      fill(LUT, 0, 0, Math.min(LUT.length, Math.ceil(center-width/2)));

      addWindow(center, width, LUT);

      fill(LUT, 255, Math.max(0, Math.floor(center+width/2)), LUT.length);

      return LUT;
    }

    GrayLUT.prototype.toPixels = function (data, pixels) {
      var lut = this.getArray();
      for (var i = 0, j=0, n = pixels.length; i < n; i += 4, j++) {
        var value = lut[data[j]];
        pixels[i  ] = value;
        pixels[i+1] = value;
        pixels[i+2] = value;
        pixels[i+3] = 255;
      }

      return pixels;
    };

    // no operation LUT; returns pixels unchanged
    var NOOPLUT = function () { };

    if (typedArray.hasUint32Array) {
      // using TypedArrays
      NOOPLUT.prototype.toPixels = function (data, pixels) {
        pixels.set(data);
      };
    } else {
      NOOPLUT.prototype.toPixels = function (data, pixels) {
        for (var i = 0, n = pixels.length; i < n; i++) {
          pixels[i] = data[i];
        }
      };
    }


    /** LUT that maps to RGBA. */
    var ColorAlphaLUT = function (array) {
      this.array = array;
      if (typedArray.hasUint32Array) {
        this.array32 = new Uint32Array(array.buffer);
      }
    };

    ColorAlphaLUT.prototype.getArray = function () {
      return this.array;
    };

    if (typedArray.hasUint32Array) {
      ColorAlphaLUT.prototype.toPixels = function (data, pixels) {
        var lut32 = this.array32;
        var pixels32 = new Uint32Array(pixels.buffer);
        for (var i = 0, n = pixels32.length; i < n; i++) {
          pixels32[i] = lut32[data[i]];
        }
      };
    } else {
      /** Maps DATA to PIXELS using the LUT. */
      ColorAlphaLUT.prototype.toPixels = function (data, pixels) {
        var lut = this.getArray();
        for (var i = 0, j=0, n = pixels.length; i < n; i += 4, j++) {
          var index = data[j]*4;
          pixels[i  ] = lut[index  ];
          pixels[i+1] = lut[index+1];
          pixels[i+2] = lut[index+2];
          pixels[i+3] = lut[index+3];
        }
      };
    }

    return {
      GrayLUT: GrayLUT,
      NOOPLUT: NOOPLUT,
      ColorAlphaLUT: ColorAlphaLUT,
    };
  });
