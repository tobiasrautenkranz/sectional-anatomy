/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["./derived-volume", "util/array", "util/utils", "./lut", "./oblique"],
  function (DerivedVolume, array, utils, luts, ObliqueMPRVolume) {
    "use strict";

    /** (thin) Maximum / Minimum Intensity Projection. */
    var MIPVolume = function (baseVolume, thickness, type) {
      DerivedVolume.call(this, baseVolume);
      this.setThickness(thickness);

      if ("MinIP" === type) {
        this.minip = true;
      } else if (type && type !== "MIP") {
        new Error("unknow intensity projection type: " + type);
      }

      var arraySize = this.getSize()[0] * this.getSize()[1];

      this.sliceArray = array.mkUint8Array(arraySize);

      this.zBuffer = this.getSize()[2] > 256 ? // Make index fit the array element
        array.mkUint16Array(arraySize) :
        array.mkUint8Array(arraySize);

      this.isPreview = false;
    };

    MIPVolume.prototype = Object.create(DerivedVolume.prototype);
    MIPVolume.prototype.constructor = MIPVolume;

    MIPVolume.prototype.getLabelMap = function () {
      if (this.baseVolume instanceof ObliqueMPRVolume) {
        throw new Error("Can't create labelmap for Oblique MIP volume" +
            " (not implemented)."); // FIXME
      }
      return new ZOffsetVolume(this.getSourceVolume().getLabelMap(), this);
    };

    MIPVolume.prototype.getZBuffer = function (index) {
      this.getSlice(null, index);
      return this.zBuffer;
    };

    // FIXME SIMD
    function mip(outSlice, zBuffer, volume, start, end, step) {
      for (var z=start; z<=end; z+=step) {
        var slice = volume.slice(z);
        for (var i=0, l=slice.length; i<l; i++) {
          if (slice[i] > outSlice[i]) {
            outSlice[i] = slice[i];
            zBuffer[i] = z;
          }
        }
      }
    }
    function minip(outSlice, zBuffer, volume, start, end, step) {
      for (var z=start; z<=end; z+=step) {
        var slice = volume.slice(z);
        for (var i=0, l=slice.length; i<l; i++) {
          if (slice[i] < outSlice[i]) {
            outSlice[i] = slice[i];
            zBuffer[i] = z;
          }
        }
      }
    }

    MIPVolume.prototype.slice = function (index, preview) {
      var outSlice = this.sliceArray;
      var zBuffer = this.zBuffer;

      var initialValue = this.minip ? 255 : 0;
      array.fill(outSlice, initialValue);
      array.fill(zBuffer, index);

      var start = Math.max(0, index - Math.floor(this.thickness/2));
      var end = Math.min(this.baseVolume.getSize()[2]-1,
          index + this.thickness - (index - start));

      var step = 1;
      if (preview) {
        step = Math.max(1,Math.floor(this.thickness/10));
      }

      if (this.minip) {
        minip(outSlice, zBuffer, this.baseVolume, start, end, step);
      } else {
        mip(outSlice, zBuffer, this.baseVolume, start, end, step);
      }

      this.lastindex = index;

      return outSlice;
    };

    MIPVolume.prototype.getSlice = function (cont, index) {
      if (this.lastindex === index && !this.isPreview) { // cache
        if (cont) {
          return cont(this.sliceArray, this.zBuffer);
        } else {
          return this.sliceArray;
        }
      }
      if (cont && !this.getSourceVolume().isComplete()) {
        var t = this;
        return this.getSourceVolume().loadAllSlices(function () {
          t.getSlice(cont,index);
        });
      }

      if (this.isPreview && this.lastindex === index &&
          this.getSourceVolume().isComplete()) {
        this.isPreview = false;
      } else {
        this.isPreview = true;
      }

      var outSlice = this.slice(index, this.isPreview);

      if (cont) {
        cont(outSlice, this.zBuffer);
      }
      return outSlice;
    };

    MIPVolume.prototype.hasCompleteSlice = function (index) {
      return this.getSourceVolume().isComplete() &&
        this.lastindex === index && !this.isPreview;
    };

    MIPVolume.prototype.setThickness = function (thickness) {
      this.thickness = thickness;
      this.isPreview = false;
      this.lastindex = undefined;
    };

    /** Constructs slices based on a per pixel z-offset.
     * Used for the labelmap of a MIPVolume. */
    var ZOffsetVolume = function (baseVolume, mipVolume) {
      utils.assert(!baseVolume.baseVolume,
          "Supply the sourceVolume, not an MPR one (preformance reasons).");

      DerivedVolume.call(this, baseVolume);
      this.mipVolume = mipVolume;

      this.outSlice = array.mkUint8Array(this.getSize()[0]*this.getSize()[1]);
      this.toSlice = getSliceFunction(this.baseVolume.getAxisNumber(), this.getAxisNumber());
    };

    ZOffsetVolume.prototype = Object.create(DerivedVolume.prototype);
    ZOffsetVolume.prototype.constructor = ZOffsetVolume;

    ZOffsetVolume.prototype.getAxes = function () {
      return this.mipVolume.getAxes();
    };
    ZOffsetVolume.prototype.getOrigin = function () {
      return this.mipVolume.getOrigin();
    };
    ZOffsetVolume.prototype.getVoxelSize = function () {
      return this.mipVolume.getVoxelSize();
    };
    ZOffsetVolume.prototype.getSize = function () {
      return this.mipVolume.getSize();
    };
    ZOffsetVolume.prototype.getAxisNumber = function () {
      return this.mipVolume.getAxisNumber();
    };
    ZOffsetVolume.prototype.getBasis = function () {
      return this.mipVolume.getBasis();
    };
    ZOffsetVolume.prototype.getAnatomyId = function (cont, index) {
      var sliceWidth = this.getSize()[0];
      this.getSlice(function (slice) {
        var id = slice[index[0] + sliceWidth * index[1]];
        cont(id);
      }, index[2]);
    };

    function getSliceFunction(fromAxis, toAxis) {
      function cor2tra(slice, volume, zOffset) {
        var baseVolume = volume.baseVolume;
        //coronal: (x,z,y)
        var xMax = baseVolume.getSize()[0];
        for (var z=0, zMax = baseVolume.getSize()[2]; z < zMax; z++) {
          var yCoronar = z*xMax;
          var baseSlice = baseVolume.slices[z];
          for (var x=0; x<xMax; x++) {
            slice[yCoronar+x] = baseSlice[xMax*zOffset[yCoronar+x] + x];
          }
        }
      }
      function sag2cor(slice, volume, zOffset) {
        var baseVolume = volume.baseVolume;
        //sagittal: (z,y,x)
        var xMax = baseVolume.getSize()[0];
        var yMax = baseVolume.getSize()[1];
        for (var z=0, zMax = baseVolume.getSize()[2]; z < zMax; z++) {
          //var xSagittal = zMax-1-z;
          var xSagittal = z;
          var baseSlice = baseVolume.slices[z];
          for (var y=0; y<yMax; y++) {
            slice[y*zMax + xSagittal] =
              baseSlice[y*xMax + zOffset[y*zMax+xSagittal]];
          }
        }
      }
      function tra2sag(slice, volume, zOffset) {
        var baseVolume = volume.baseVolume;
        //sagittal: (y,z,x)
        var xMax = baseVolume.getSize()[0];
        var yMax = baseVolume.getSize()[1];
        var xSagittalMax = volume.getSize()[0];
        for (var z=0, zMax = baseVolume.getSize()[2]; z < zMax; z++) {
          var ySagittal = z*xSagittalMax;
          var baseSlice = baseVolume.slices[z];
          for (var y=0; y<yMax; y++) {
            var xSagittal = y;
            slice[ySagittal + xSagittal] =
              baseSlice[y*xMax + zOffset[ySagittal + xSagittal]];
          }
        }
      }
      function sag2tra(slice, volume, zOffset) {
        var baseVolume = volume.baseVolume;
        //transverse:   (z,x,y)
        var xMax = baseVolume.getSize()[0];
        var xTransverseMax = volume.getSize()[0];
        for (var z=0, zMax = baseVolume.getSize()[2]; z < zMax; z++) {
          var xTransverse = z;
          var baseSlice = baseVolume.slices[z];
          for (var x=0; x<xMax; x++) {
            var yTransverse = x*xTransverseMax;
            slice[yTransverse + xTransverse] =
              baseSlice[xMax*zOffset[yTransverse + xTransverse] + x];
          }
        }
      }
      function noMPR(slice, volume, zOffset) {
        var baseVolume = volume.baseVolume;
        for (var i=0, n = slice.length; i < n; i++) {
          slice[i] = baseVolume.slice(zOffset[i])[i];
        }
      }

      var rotation = (fromAxis + toAxis) % 3;
      if (2 === rotation && 2 === toAxis) {
        // special case for inverse s->a
        rotation = 3;
      } else if (toAxis === fromAxis) {
        rotation = -1; // special case no mpr
      }

      switch (rotation) {
        case -1:
          return noMPR;
          // in baseVolume Coords
        case 0: // coronal <-> transverse
          return cor2tra;
        case 1: // sagittal <-> coronal
          return sag2cor;
        case 2: // transverse -> sagittal
          return tra2sag;
        case 3: // sagittal -> transverse
          // inverse (different from transverse -> sagittal, since two rotations)
          return sag2tra;
      }
      utils.assert(false, "not reached");
    }

    ZOffsetVolume.prototype.sliceZOffset = function (zOffset) {
      this.toSlice(this.outSlice, this, zOffset);

      return this.outSlice;
    };

    ZOffsetVolume.prototype.slice = function (index) {
        return this.sliceZOffset(this.mipVolume.getZBuffer(index));
    };

    ZOffsetVolume.prototype.getSlice = function (cont, index) {
      if (!cont) {
        return this.slice(index);
      }

      var t = this;
      this.mipVolume.getSlice(function (rslice, zOffset) {
        cont(t.sliceZOffset(zOffset));
      },
        index);
    };

    ZOffsetVolume.prototype.hasCompleteSlice = function (index) {
      return this.getSourceVolume().isComplete() &&
        this.mipVolume.hasCompleteSlice(index);
    };

    return MIPVolume;
  });
