/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "lib/gl-matrix", "./axis-numbers", "./basis-transform"],
  function (utils, glm, AxisNumbers, BasisTransform) {
    "use strict";

    var vec3 = glm.vec3;

/* coordinate system:
 *
 * world coordinates: right handed with axes pointing to patient left,
 * posterior and superior in that order (also named x,y,z)
 * (ie. l-p-s)
 *
 * screen:
 * The origin is in the top left corner with the x axis horizontally to the
 * right and the y axis vertically downwards. The z axis is pointing behind the
 * screen.
 *
 *
 * For the default transverse slices, the slices start at the feet of the
 * patient and higher slice numbers, i.e. higher z values go towards the head.
 *
 * As usual the patients right side is on the left on the screen.

/*
 * getSize return a array indicating the extend (number of voxels) along the
 * three axes identified by their axis number
 *
 * Similar voxelSize represents the extends of a voxel along the three axes
 * in arbitrary units (only their relative magnitude is important).
 * The voxelSize of a Volume might be negative indicating a flipped axis.
 */

    var axes = [
    {
      axis: vec3.fromValues(1, 0, 0),
      labelStart: "R",
      labelEnd: "L"
    },
    {
      axis: vec3.fromValues(0, 1, 0),
      labelStart: "A",
      labelEnd: "P"
    },
    {
      axis: vec3.fromValues(0, 0, 1),
      labelStart: "I",
      labelEnd: "S"
    },
    {
      axis: vec3.fromValues(Math.SQRT1_2, Math.SQRT1_2, 0),
      labelStart: "RAO",
      labelEnd: "LPO"
    },
      {
        axis: vec3.fromValues(-Math.SQRT1_2, Math.SQRT1_2, 0),
        labelStart: "LAO",
        labelEnd: "RPO"
      }
    ];

    var normalizedAxis = vec3.create();

    function labelForAxis (axis) {
      vec3.normalize(normalizedAxis, axis);
      var bestAxis;
      var dot = 0;
      for (var i=0; i<axes.length; i++) {
        var d = vec3.dot(normalizedAxis, axes[i].axis);
        if (Math.abs(d) > Math.abs(dot)) {
          dot = d;
          bestAxis = axes[i];
        }
      }
      if (dot > 0) {
        return {
          start: bestAxis.labelStart,
          end: bestAxis.labelEnd
        };
      } else {
        return {
          start: bestAxis.labelEnd,
          end: bestAxis.labelStart
        };
      }
    }

    return {
      getAxisName: function (volume) {
        if (volume.getAxisName) {
          return volume.getAxisName();
        } else {
          return AxisNumbers.toName(volume.getAxisNumber());
        }
      },

      hasAxisNumber: function (axis) {
        return (0 === axis[0] && 0 === axis[1] && 0 !== axis[2]) ||
          (0 === axis[0] && 0 !== axis[1] && 0 === axis[2]) ||
          (0 !== axis[0] && 0 === axis[1] && 0 === axis[2]);
      },

      isRightHandCoordinateSystem: function (volume) {
        var d = volume.getVoxelSize();
        return d[0]*d[1]*d[2] > 0;
      },

      volumeRangeInWorldCoordinates: function (volume) {
        var begin = vec3.fromValues(0,0,0);
        var end = vec3.create();
        var range = {
          begin: BasisTransform.from(begin, volume.getBasis(), begin),
          end: BasisTransform.from(end, volume.getBasis(), volume.getSize())
        };
        return range;
      },

      isSameGeometry: function (vol1, vol2) {
        function vec3Equal (a, b) {
          return a[0] == b[0] &&
            a[1] == b[1] &&
            a[2] == b[2];
        }
        function axesEqual (a, b) {
          return vec3Equal(a[0], b[0]) &&
            vec3Equal(a[1], b[1]) &&
            vec3Equal(a[2], b[2]);
        }
        return vol1.getAxisNumber() == vol1.getAxisNumber() && // fail early (not strictly needed
          vec3Equal(vol1.getOrigin(), vol2.getOrigin()) &&
          axesEqual(vol1.getAxes(), vol2.getAxes());
      },

      isLabelMap: function (volume) {
        return volume.isLabelMap();
      },

      labelForAxis: labelForAxis
    };
  });
