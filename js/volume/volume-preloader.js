/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(function (utils) {
  "use strict";

  var PreloadBatch = function (volumePreloader, batch) {
    this.preloader = volumePreloader;

    this.batch = batch;
    this.volumeIndex = 0;
    this.sliceIndex = batch;

    this.loadNext = this.load.bind(this);
  };

  PreloadBatch.prototype.load = function () {
    if (this.batch === 0) { // only first batch for monotonic progress
      var ratio = this.sliceIndex / this.preloader.study.volumes[0].getSize()[2];
      this.preloader.batchProgress(ratio);
    }

    if (this.volumeIndex >= this.preloader.study.volumes.length) {
      this.volumeIndex = 0;
      this.sliceIndex += this.preloader.batches;
    }
    var volume = this.preloader.study.volumes[this.volumeIndex];
    this.volumeIndex++;

    // find next slice that needs loading
    for (; this.sliceIndex < volume.getSize()[2]; this.sliceIndex+=this.preloader.batches) {
      if (!volume.hasCompleteSlice(this.sliceIndex)) {
        volume.getSlice(this.loadNext, this.sliceIndex);
        return;
      }
    }

    this.preloader.batchComplete(this.batch);
  };

  var VolumePreloader = function (study) {
    this.study = study;
    this.batches = 1;

    this.completedBatches = 0;
  };

  VolumePreloader.prototype.run = function () {
    for (var i=0; i<this.batches; i++) {
      var p = new PreloadBatch(this, i);
      p.load();
    }
  };

  VolumePreloader.prototype.batchProgress = function (ratio) {
    if (this.onprogress) {
      this.onprogress(ratio);
    }
  };

  VolumePreloader.prototype.batchComplete = function (batch) {
    this.completedBatches++;
    if (this.completedBatches === this.batches && this.oncomplete) {
      this.oncomplete();
    }
  };

  return VolumePreloader;
});

