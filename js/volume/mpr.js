/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["./derived-volume", "util/array", "util/utils", "lib/gl-matrix",
"./basis-transform", "./axis-numbers"],
  function (DerivedVolume, array, utils, glm, BasisTransform, AxisNumbers) {
    "use strict";

    var vec3 = glm.vec3;
    /** Multi Planar Reformat along the three axes. */
    var MPRVolume = function (baseVolume, axis) {
      DerivedVolume.call(this, baseVolume);
      this.axis = axis;

      // array to return as slice
      // can be reused since it is no longer used by the previous caller
      // on the next request. (only used to draw to canvas)
      this.sliceArray = array.mkUintArray(baseVolume.getSourceVolume().getDepth(),
          this.getSize()[0] * this.getSize()[1]);

      var rotation = (baseVolume.getAxisNumber() + this.axis) % 3;

      if (2 == rotation && this.axis == 2) {
        // special case for inverse s->a
        rotation = 3;
      }

      switch (rotation) {
        // in baseVolume Coords
        // coronal <-> transverse
        case 0:
          if (array.hasUint32Array && 0 === (this.baseVolume.getSize()[0] % 4)) {
            this.rotate = corTraSIMD32;
          } else {
            this.rotate = corTra;
          }
          break;

          // sagittal <-> coronal
        case 1:
          this.rotate = sagCor;
          break;

          // transverse -> sagittal
        case 2:
          this.rotate = tra2sag;
          break;

          // sagittal -> transverse
          // inverse (different from transverse -> sagittal, since two rotations)
        case 3:
          this.rotate = sag2tra;
          break;
      }
      utils.assert(this.rotate, "MPRVolume no rotate function set.");

      this.basis = new BasisTransform(this.getAxes(), this.getOrigin());
    };

    MPRVolume.prototype = Object.create(DerivedVolume.prototype);


    MPRVolume.prototype.getAxisNumber = function () {
      return this.axis;
    };

    MPRVolume.prototype.getSize = function () {
      return reformatExtendsAbs(this.baseVolume.getSize(),
          this.baseVolume.getAxisNumber(), this.axis);
    };
    MPRVolume.prototype.getVoxelSize = function () {
      return reformatExtends(this.baseVolume.getVoxelSize(),
          this.baseVolume.getAxisNumber(), this.axis);
    };

    function reformatAxes (axes, fromAxis, toAxis) {
      utils.assert(AxisNumbers.isValid(fromAxis) &&
          AxisNumbers.isValid(toAxis), "invalid axis", fromAxis, toAxis);
      if (fromAxis == toAxis) {
        return axes;
      }
      var n = AxisNumbers;
      switch (fromAxis) {
        case n.tra:
              switch (toAxis) {
                case n.cor:
                  return [axes[0], axes[2], axes[1]];
                case n.sag:
                  return [axes[1], axes[2], axes[0]];
              }
              break;

        case n.cor:
              switch (toAxis) {
                case n.tra:
                  return [axes[0], axes[2], axes[1]];
                case n.sag:
                  return [axes[2], axes[1], axes[0]];
              }
              break;

        case n.sag:
              switch (toAxis) {
                case n.tra:
                  return [axes[2], axes[0], axes[1]];
                case n.cor:
                  return [axes[2], axes[1], axes[0]];
              }
              break;

         default:
              utils.assert(false, "invalid axis reformat");
      }
    }

    MPRVolume.prototype.getAxes = function () {
      if (!this.axes) {
        this.axes = reformatAxes(this.baseVolume.getAxes(),
            this.baseVolume.getAxisNumber(), this.axis);
      }
      return this.axes;
    };

    function getOffset(from, to, fromSize) {
      switch (from) {
        case AxisNumbers.tra:
          switch (to) {
            case AxisNumbers.cor:
              return vec3.fromValues(0, 0, fromSize[2] -1);

            case AxisNumbers.sag:
              return vec3.fromValues(fromSize[0]-1, 0, fromSize[2] -1);
          }
          break;
        case AxisNumbers.sag:
          if (to === AxisNumbers.cor) {
              return vec3.fromValues(0, 0, fromSize[2] -1);
          }
          break;
        case AxisNumbers.cor:
          if (to === AxisNumbers.tra) {
            return vec3.fromValues(0, fromSize[2]-1, 0);
          }
      }
      var inverseOffset = getOffset(to, from, fromSize);
      return vec3.scale(inverseOffset, inverseOffset, -1);
    }

    MPRVolume.prototype.getOrigin = function () {
      return [0,0,0];
    };

    MPRVolume.prototype.getReformatedLabelMap = function () {
      return new MPRVolume(this.getLabelMap(), this.axis);
    };

    function reformatExtends (extend, fromAxis, toAxis) {
      utils.assert(extend.length == 3, "invalid extend");
      utils.assert(AxisNumbers.isValid(fromAxis) &&
          AxisNumbers.isValid(toAxis), "invalid axis", fromAxis, toAxis);
      if (fromAxis == toAxis) {
        return extend;
      }
      var n = AxisNumbers;
      switch (fromAxis) {
        case n.tra:
              switch (toAxis) {
                case n.cor:
                  return vec3.fromValues(extend[0], -extend[2], extend[1]);
                case n.sag:
                  return vec3.fromValues(extend[1], -extend[2], -extend[0]);
              }
              break;

        case n.cor:
              switch (toAxis) {
                case n.tra:
                  return vec3.fromValues(extend[0], extend[2], -extend[1]);
                case n.sag:
                  return vec3.fromValues(extend[2], extend[1], -extend[0]);
              }
              break;

        case n.sag:
              switch (toAxis) {
                case n.tra:
                  return vec3.fromValues(-extend[2], extend[0], -extend[1]);
                case n.cor:
                  return vec3.fromValues(-extend[2], extend[1], extend[0]);
              }
              break;

         default:
              utils.assert(false, "invalid axis reformat");
      }
    }
    function reformatExtendsAbs (extend, fromAxis, toAxis) {
      var out = reformatExtends(extend, fromAxis, toAxis);

      for (var i=0; i<3; i++) {
        out[i] = Math.abs(out[i]);
      }
      return out;
    }

    /**
     * For easier index access we assume a z axis to posterior for transverse
     * volumes (not anterior as usual). Thus we have to flip the y axis for the
     * coronal and sagittal MPR.
     */
    function corTra(slice, index, baseVolume) {
      //coronal: (x,z,y)
      var xMax = baseVolume.getSize()[0];
      var y = index*xMax;

      for (var z=0, zMax = baseVolume.getSize()[2]; z < zMax; z++) {
        var yCoronar = z*xMax;
       var baseSlice = baseVolume.slices[z];
        for (var x=0; x<xMax; x++) {
          slice[yCoronar + x] = baseSlice[y + x];
        }
      }
      return slice;
    }
    function corTraSIMD32(slice, index, baseVolume) {
      //coronal: (x,z,y)
      var n = 4;
      var xMax = baseVolume.getSize()[0];
      var y = Math.floor(index*xMax/n);

      var slice32 = new Uint32Array(slice.buffer);

      for (var z=0, zMax = baseVolume.getSize()[2]; z < zMax; z++) {
        var yCoronar = Math.floor(z*xMax/n);
        var baseSlice = baseVolume.slices[z];
        if (baseSlice.buffer) { // slice loaded
          var baseSlice32 = new Uint32Array(baseSlice.buffer);
          for (var x=0; x<xMax/n; x++) {
            slice32[yCoronar + x] = baseSlice32[y + x];
          }
        }
      }
      return slice;
    }

    function sagCor(slice, index, baseVolume) {
      //sagittal: (z,y,x)
      var size = baseVolume.getSize();
      var xMax = size[0], yMax = size[1], zMax = size[2];
      var x = index;
      for (var z=0; z < zMax; z++) {
        //var xSagittal = zMax-1-z;
        var xSagittal = z;
        var baseSlice = baseVolume.slices[z];
        for (var y=0; y<yMax; y++) {
          slice[y*zMax + xSagittal] = baseSlice[y*xMax + x];
        }
      }
      return slice;
    }
    function tra2sag(slice, index, baseVolume, volume) {
      //sagittal: (y,z,x)
      var xMax = baseVolume.getSize()[0];
      var yMax = baseVolume.getSize()[1];
      var xSagittalMax = volume.getSize()[0];
      var x = index;
      for (var z=0, zMax = baseVolume.getSize()[2]; z < zMax; z++) {
        var ySagittal = z*xSagittalMax;
        var baseSlice = baseVolume.slices[z];
        for (var y=0; y<yMax; y++) {
          var xSagittal = y;
          slice[ySagittal + xSagittal] = baseSlice[y*xMax + x];
        }
      }
      return slice;
    }
    function sag2tra(slice, index, baseVolume, volume) {
      //transverse:   (z,x,y)
      var xMax = baseVolume.getSize()[0];
      var xTransverseMax = volume.getSize()[0];
      var y = xMax*index;
      for (var z=0, zMax = baseVolume.getSize()[2]; z < zMax; z++) {
        var xTransverse= z;
        var baseSlice = baseVolume.slices[z];
        for (var x=0; x<xMax; x++) {
          var yTransverse = x*xTransverseMax;
          slice[yTransverse + xTransverse] = baseSlice[y + x];
        }
      }
      return slice;
    }

    /** Returns the slice at INDEX. */
    MPRVolume.prototype.slice = function (index) {
      return this.rotate(this.sliceArray, index, this.baseVolume, this);
    };

    /** Passes the slice at INDEX to CONT. Calls CONT while loading
     * with partial slices and aborts when CONT returns true. */
    MPRVolume.prototype.getSlice = function (cont, index) {
      if (!cont) { // FIXME
        if (!this.baseVolume.isComplete()) {
          this.baseVolume.loadAllSlices(function (){});
        }
        return this.slice(index);
      }

      if (this.baseVolume.loading || this.baseVolume.notLoaded()) {
        var mprvol = this;
        var update = function update() {
          if (mprvol.baseVolume.loading && !cont(mprvol.slice(index))) {
            window.setTimeout(update, 1000);
          }
        };
        window.setTimeout(update, 500);
        this.baseVolume.loadAllSlices(function () {
          mprvol.getSlice(cont, index);
        });
      } else {
        cont(this.slice(index));
      }
    };

    MPRVolume.prototype.hasCompleteSlice = function (index) {
      return this.baseVolume.isComplete();
    };

    var reformatVolume = function (volume, axis) {
      if (volume.getAxisNumber() === undefined) {
        return reformatVolume(volume.getSourceVolume(), axis);
      }
      if (volume.getAxisNumber() === axis) {
        return volume;
      }
      if (volume.getSourceVolume().getAxisNumber() === axis) {
        return volume.getSourceVolume();
      }

      return new MPRVolume(volume.getSourceVolume(), axis);
    };

    return {
      MPRVolume: MPRVolume,
      reformatVolume: reformatVolume,

      reformatExtendsAbs: reformatExtendsAbs,
      reformatExtends: reformatExtends
    };
  });
