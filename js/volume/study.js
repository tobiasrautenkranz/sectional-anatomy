/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/cont", "volume/remote-volume",
    "volume/remote-label-map", "volume/volume"],
  function (utils, CollectiveContinuation, RemoteVolume, LabelMap, volUtil) {
    "use strict";

    // load a study descition
    var loadStudy = function (cont, progress, language, baseURL) {
      baseURL = baseURL || "";
      language = language || "en";

      utils.loadJSONFile(baseURL + "study.json", function (study) {
        function findLanguageId(languages, language) {
          for (var i=0; i< languages.length; i++) {
            if (languages[i] === language) {
              return i;
            }
          }
          return 0;
        }

        study.language = findLanguageId(study.labels.languages, language);
        study.baseURL = baseURL;

        var volumes = study.volumes;
        var orientations = ["sagittal", "coronal", "transverse"];
        var allCont = new CollectiveContinuation(function () { cont(study); });
        var done = allCont.makeContinue();
        var quality = parseInt(utils.urlParam("quality"), 10) || 0;
        quality = utils.clamp(quality, 0, study.qualities.length-1);

        for (var i=0; i<volumes.length; i++) {
          var v = volumes[i];

          var remoteVolume = "labelmap" === v.type ?
            new LabelMap(v) :
            new RemoteVolume(v);

          volumes[i] = remoteVolume;
          remoteVolume.baseURL = baseURL;
          remoteVolume.index = i;
          if (!remoteVolume.version) {
            remoteVolume.version = utils.studyDataVersion(study);
          }

          if (volUtil.isLabelMap(remoteVolume)) {
            study.labelMap = remoteVolume;
            remoteVolume.load(allCont.makeContinue(progress), study);
          } else {
            var w = remoteVolume.defaultWindow;
            remoteVolume.getLUT().setWindow(w[0], w[1]);
          }
          remoteVolume.setQuality(study, quality);
        }
        for (var i=0; i<volumes.length; i++) {
          var v = volumes[i];
          if (!volUtil.isLabelMap(v)) {
            v.labelMap = study.labelMap;
          }
        }

        utils.assert(study.labelMap, "No labelMap in study.");

        done();
      },
      utils.studiesDataVersion);
    };

    return {
      loadStudy: loadStudy
    };
  });
