/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define([],
  function () {
    "use strict";

    // -- post-processing ---
    var DerivedVolume = function (baseVolume) {
      this.baseVolume = baseVolume;
    };

    DerivedVolume.prototype.getAxisNumber = function () {
      return this.baseVolume.getAxisNumber();
    };
    DerivedVolume.prototype.getSize = function () {
      return this.baseVolume.getSize();
    };
    DerivedVolume.prototype.getBasis = function () {
      return this.basis || this.baseVolume.getBasis();
    };
    DerivedVolume.prototype.getVoxelSize = function () {
      return this.baseVolume.getVoxelSize();
    };
    DerivedVolume.prototype.getAxes = function () {
      return this.baseVolume.getAxes();
    };
    DerivedVolume.prototype.getOrigin = function () {
      return this.baseVolume.getOrigin();
    };
    DerivedVolume.prototype.getLUT = function () {
      return this.baseVolume.lut || this.baseVolume.getLUT();
    };
    DerivedVolume.prototype.getLabelMap = function () {
      return this.baseVolume.getLabelMap();
    };
    DerivedVolume.prototype.getReformatedLabelMap = function () {
      return this.getLabelMap();
    };
    DerivedVolume.prototype.isLabelMap = function () {
      return this.baseVolume.isLabelMap();
    };
    DerivedVolume.prototype.getDepth = function () {
      return this.baseVolume.getDepth();
    };

    /** Returns the Volume that contains the Data; i.e.: a RemoteVolume. */
    DerivedVolume.prototype.getSourceVolume = function () {
      return this.baseVolume.getSourceVolume();
    };

    DerivedVolume.prototype.hasCompleteSlice = function (index) {
      return this.baseVolume.hasCompleteSlice(index);
    };

    return DerivedVolume;
  });
