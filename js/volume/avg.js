/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["./derived-volume", "util/array", "util/utils", "./lut"],
  function (DerivedVolume, array, utils, luts) {
    "use strict";

    // Average projection
    var AVGVolume = function (baseVolume, thickness) {
      DerivedVolume.call(this, baseVolume);
      this.setThickness(thickness);

      var arraySize = this.getSize()[0] * this.getSize()[1];

      this.sliceArray = array.mkFloat32Array(arraySize);

      this.isPreview = false;
    };

    AVGVolume.prototype = Object.create(DerivedVolume.prototype);
    AVGVolume.prototype.constructor = AVGVolume;

    AVGVolume.prototype.getLabelMap = function () {
      return new RGBAAvgVolume(this.baseVolume.getReformatedLabelMap(),
          this.thickness);
    };

    function avgip(outSlice, volume, start, end, step) {
      for (var z=start; z<=end; z+=step) {
        var slice = volume.slice(z);
        for (var i=0, l=slice.length; i<l; i++) {
          outSlice[i] += slice[i];
        }
      }
      var slices = (end-start+1)/step;
      for (var i=0, l=outSlice.length; i<l; i++) {
        outSlice[i] = Math.min(255,Math.round(outSlice[i]/slices));
      }
    }

    AVGVolume.prototype.getSlice = function (cont, index) {
      if (this.lastindex === index && !this.isPreview) { // cache
        if (cont) {
          return cont(this.sliceArray);
        } else {
          return this.sliceArray;
        }
      }

      if (cont && !this.getSourceVolume().isComplete()) {
        var t = this;
        return this.getSourceVolume().loadAllSlices(function () {
          t.getSlice(cont,index);
        });
      }

      var outSlice = this.sliceArray;

      array.fill(outSlice, 0);

      var start = Math.max(0, index - Math.floor(this.thickness/2));
      var end = Math.min(this.baseVolume.getSize()[2]-1,
          index + this.thickness - (index - start));

      var step = 1;
      if (!this.isPreview || !this.getSourceVolume().isComplete()) {
        step = Math.max(1,Math.floor(this.thickness/10));
      }

      avgip(outSlice, this.baseVolume, start, end, step);

      this.lastindex = index;

      if (step === 1) {
        this.isPreview = false;
      } else {
        this.isPreview = true;
      }

      if (cont) {
        cont(outSlice);
      }

      return outSlice;
    };

    AVGVolume.prototype.hasCompleteSlice = function (index) {
      return this.getSourceVolume().isComplete() &&
        this.lastindex === index && !this.isPreview;
    };

    AVGVolume.prototype.setThickness = function (thickness) {
      this.thickness = thickness;
      this.isPreview = false;
      this.lastindex = undefined;
    };

    //
    // Labelmap for AVGVolume
    //
    //
    var RGBAAvgVolume = function (baseVolume, thickness) {
      DerivedVolume.call(this, baseVolume);

      this.thickness = thickness;
      this.sliceArray = new array.mkFloat32Array(4*(this.getSize()[0]*this.getSize()[1]));
      this.isPreview = false;
    };
    RGBAAvgVolume.prototype = Object.create(DerivedVolume.prototype);
    RGBAAvgVolume.prototype.constructor = RGBAAvgVolume;

    RGBAAvgVolume.prototype.getLUT = function () {
      return new luts.NOOPLUT();
    };
    RGBAAvgVolume.prototype.getSlice = function (cont, index) {
      // FXIME disable since lut change does not invalidate cache
      /*
         if (this.lastindex === index && !this.isPreview) { // cache
         if (cont) {
         return cont(this.sliceArray);
         } else {
         return this.sliceArray;
         }
         }
         */

      if (cont && !this.getSourceVolume().isComplete()) {
        var t = this;
        return this.getSourceVolume().loadAllSlices(function () {
          t.getSlice(cont,index);
        });
      }

      var out = this.sliceArray;

      array.fill(out, 0);

      var start = Math.max(0, index - Math.floor(this.thickness/2));
      var end = Math.min(this.baseVolume.getSize()[2]-1,
          index + this.thickness - (index - start));

      var step = 1;
      if (!this.isPreview || !this.getSourceVolume().isComplete()) {
        step = Math.max(1,Math.floor(this.thickness/10));
      }

      var lut = this.baseVolume.getLUT().getArray();
      var threshold = 252;
      for (var z=start; z<=end; z+=step) {
        var slice= this.baseVolume.slice(z);
        for (var i=0, l=slice.length; i<l; i++) {
          var lutIndex = 4*slice[i];

          var alpha = lut[lutIndex + 3];
          if (alpha >= threshold) {
            out[4*i + 3] = alpha;
          }
          if (alpha > 0) {
            out[4*i    ] += lut[lutIndex    ];
            out[4*i + 1] += lut[lutIndex + 1];
            out[4*i + 2] += lut[lutIndex + 2];
          }
        }
      }
      var rayLength = this.baseVolume.getSize()[2];
      var slices = (end-start+1)/step;
      var fillingFactor = 1 - 0.3*(this.thickness - 1) / rayLength;
      slices = fillingFactor*slices;
      for (var i=0, l=out.length; i<l; i+=4) {
        out[i    ] = Math.round(out[i    ] / slices);
        out[i + 1] = Math.round(out[i + 1] / slices);
        out[i + 2] = Math.round(out[i + 2] / slices);
        if (out[i + 3] > threshold) {
          out[i + 3] = 255;
        } else {
          out[i + 3] = 100 + 100*(this.thickness - 1) / rayLength;
        }
      }

      this.lastindex = index;
      if (step === 1) {
        this.isPreview = false;
      } else {
        this.isPreview = true;
      }

      if (cont) {
        cont(out);
      } else {
        return out;
      }
    };

    RGBAAvgVolume.prototype.getAnatomyId = function (cont, index) {
      var anatomyId = 0;
      var anatomySize = 0;
      var curAnatomyId = 0;
      var curAnatomySize = 0;

      var slicePosition = index[0] + this.getSize()[0] * index[1];

      var start = Math.max(0, index[2] - Math.floor(this.thickness/2));
      var end = Math.min(this.baseVolume.getSize()[2]-1,
          index[2] + this.thickness - (index[2] - start));
      for (var z=start; z<=end; z++) {
        var id = this.baseVolume.slice(z)[slicePosition];
        if (curAnatomyId === id) {
          curAnatomySize++;
        } else {
          if (curAnatomySize > anatomySize && curAnatomyId !== 0) {
            anatomyId = curAnatomyId;
            anatomySize = curAnatomySize;
          }
          curAnatomyId = id;
          curAnatomySize = 1;
        }
      }
      if (curAnatomySize > anatomySize && curAnatomyId !== 0) {
        anatomyId = curAnatomyId;
      }
      cont(anatomyId);
    };

    RGBAAvgVolume.prototype.hasCompleteSlice = function (index) {
      return this.getSourceVolume().isComplete() &&
        this.lastindex === index && !this.isPreview;
    };

    return AVGVolume;
  });
