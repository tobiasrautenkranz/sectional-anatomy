/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["./derived-volume"],
  function (DerivedVolume) {
    "use strict";

    // use an other LUT for a volume
    var ExtraLUTVolume = function (baseVolume, lut) {
      DerivedVolume.call(this, baseVolume);
      this.lut = lut;
    };
    ExtraLUTVolume.prototype = Object.create(DerivedVolume.prototype);

    ExtraLUTVolume.prototype.getLUT = function () {
      return this.lut;
    };
    ExtraLUTVolume.prototype.getSlice = function (cont, index) {
      return this.baseVolume.getSlice(cont, index);
    };

    return ExtraLUTVolume;
  });
