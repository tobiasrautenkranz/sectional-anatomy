/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2015 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

requirejs.config({
  baseUrl: "/js",
  //>>includeStart("debugInclude", pragmas.debug);
  urlArgs: "bust=" +  (new Date()).getTime(),
  //>>includeEnd("debugInclude");
});

requirejs(["vr/vr-canvas", "tests/volume-mock", "ui/gradient-editor", "lib/gl-matrix"],
    function(VRCanvas, mock, GradientEditor, glm) {
      "use strict";
      console.log("devicePixelRatio", window.devicePixelRatio);
      var canvas = document.getElementById("test-canvas");
      canvas.style.width = "300px";
      canvas.style.height = "300px";

      var vrcanvas = new VRCanvas(canvas);

      function doTests() {
        console.log(vrcanvas.vr.tile);
        vrcanvas.draw();
      }
      function setup() {
        var cubeDistance = -4.0;
        var rotation = glm.quat.create();
        glm.mat4.fromRotationTranslation(vrcanvas.mvMatrix(),
            rotation,  glm.vec3.fromValues(0, 0, cubeDistance));

        var volume = mock.makeTestVolume(256, 256, 256);

        vrcanvas.setAnatomyVisible(false);
        vrcanvas.setVolume(volume);
        vrcanvas.setLUT(volume.getLUT());
        vrcanvas.setRenderer({directory: "/shaders/mip/"}, function () {
        //  vrcanvas.vr.setSubSample(2.0);
          vrcanvas.draw();
        });
      }

      vrcanvas.init(setup);

      var draw = document.createElement("button");
      draw.appendChild(document.createTextNode("rerun"));
      document.documentElement.appendChild(draw);
      draw.addEventListener("click", doTests);

      var gl = vrcanvas.gl;
      var loseExt = vrcanvas.gl.getExtension("WEBGL_lose_context");
      if (loseExt) {
        var loseContext = document.createElement("button");
        loseContext.appendChild(document.createTextNode("lose Context"));
        document.documentElement.appendChild(loseContext);
        loseContext.addEventListener("click",
            function () {
              loseExt.loseContext();
              window.setTimeout(function () {
                loseExt.restoreContext();
              }, 500);
            });
      }


      var redrawButton = document.createElement("button");
      redrawButton.appendChild(document.createTextNode("loop"));
      document.documentElement.appendChild(redrawButton);
      var redraw = function redraw () {
        vrcanvas.draw();
        window.requestAnimationFrame(redraw);
      };
      redrawButton.addEventListener("click", function () {
        window.requestAnimationFrame(redraw);
      });
    });
