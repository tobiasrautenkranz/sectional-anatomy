/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "lib/gl-matrix"],
  function (utils, glm)  {
    "use strict";

    var vec3 = glm.vec3;
    var vec4 = glm.vec4;
    var mat4 = glm.mat4;

    var uMat = mat4.create();
    var uVec = vec4.create();
    function unproject(out, vec, view, proj, viewport) {
      vec4.set(uVec,
          (vec[0] - viewport[0]) * 2.0 / viewport[2] - 1.0,
          (vec[1] - viewport[1]) * 2.0 / viewport[3] - 1.0,
          2.0 * vec[2] - 1.0,
          1.0);

      mat4.multiply(uMat, proj, view);
      mat4.invert(uMat, uMat);
      if (!uMat) { return null; }

      vec4.transformMat4(uVec, uVec, uMat);
      var w = uVec[3];
      if (0 === w) { return null; }

      return vec3.set(out, uVec[0]/w, uVec[1]/w, uVec[2]/w);
    }

    function inVolumeCoordinates(out, objectCoords, boundingBox) {
      boundingBox = boundingBox || [[0,1], [0,1], [0,1]];

      for (var i=0; i<3; i++) {
        var r = boundingBox[i];
        out[i] = r[0] + objectCoords[i] * (r[1] - r[0]);
      }

      return out;
    }

    function volumeDirection(x, y, viewProperties) {
      var coords = unproject(vec3.create(),
          vec3.fromValues(x , y, 1),
          viewProperties.mvMatrix, viewProperties.pMatrix, viewProperties.viewport);

      coords = inVolumeCoordinates(coords, coords, viewProperties.boundingBox);
      vec3.normalize(coords, coords);

      return coords;
    }

    return {
      unproject: unproject,
      volumeDirection: volumeDirection
    };
  });
