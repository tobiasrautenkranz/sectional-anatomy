/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2016 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["vr/volumerenderer", "lib/gl-matrix"],
  function (VolumeRenderer, glm)  {
    "use strict";

    var mat4 = glm.mat4;
    var vec3 = glm.vec3;
    var quat = glm.quat;

    var FoVYAngle = 70 * Math.PI / 180;

    var fsaaMax = 1.2;
    var VR = function () {
      VolumeRenderer.call(this);

      this.showAnatomy = false;

      this.fsaa = fsaaMax; // FIXME

      if (window.VRFrameData) {
        this.frameData = new VRFrameData();
      }
    };

    VR.prototype = Object.create(VolumeRenderer.prototype);

    VR.prototype.updateAndDraw = function () {
      if (!this.isInitialized) {
        return;
      }

      /*
       // need    EXT_disjoint_timer_query TODO FIXME
      if (this.lastDraw) {
        var now = window.performance.now();
        var timeForLastFrame = now - this.lastDraw;
        if (timeForLastFrame > 1/60*1000) {
          this.fsaa = Math.max(this.fsaa-0.2, 0.5);
        console.log(this.fsaa, timeForLastFrame);
        } else if (timeForLastFrame < 7.8) {
          this.fsaa = Math.min(this.fsaa+0.1, fsaaMax);
        console.log(this.fsaa);
        }
        this.lastDraw = now;
      } else  {
        this.lastDraw = window.performance.now();
      }
      */


      var gl = this.gl;

      this.updateSize();

      gl.disable(this.gl.SCISSOR_TEST);
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
      gl.enable(this.gl.SCISSOR_TEST);


      if (this.vrDisplay) {
        this.vrDisplay.getFrameData(this.frameData);
        if (this.vrDisplay.isPresenting) {
          this.setViewport("left");
          this.updateAndDrawEye("left");

          this.setViewport("right");
          this.updateAndDrawEye("right");

          this.vrDisplay.submitFrame();
        } else {
          this.setViewport();
          this.updateAndDrawEye();
        }
      } else {
        this.setViewport();
        mat4.perspective(this.pMatrix, FoVYAngle,
            gl.canvas.clientWidth / gl.canvas.clientHeight, 0.01, 100);

        if (this.extraUpdateAndDrawEye) {
          //gl.enable(gl.DEPTH_TEST);
          var VMatrix = mat4.create();
          this.extraUpdateAndDrawEye(VMatrix);
          //gl.disable(gl.DEPTH_TEST);
        }

        this.updateAndDrawFrontCoords();

        gl.enable(gl.BLEND);
        this.updateAndDrawCommonPass();
        gl.disable(gl.BLEND);

        if (this.overlay) {
          gl.disable(gl.DEPTH_TEST);
          var VMatrix = mat4.create();
          for (var i=0; i<this.overlay.length; ++i) {
            this.overlay[i](VMatrix);
          }
        }
      }
    };

    function poseToVMatrix (VMatrix, pose, eyeOffset) {
      var rotation = pose.orientation;
      //mat4.fromRotationTranslation(VMatrix, rotation,
      //    vec3.fromValues(0, 0, 0));
      var o = vec3.create();
      mat4.fromTranslation(VMatrix, vec3.negate(o, eyeOffset));
    }

    VR.prototype.updateAndDrawEye = function (laterality) {
      var gl = this.gl;

      var VMatrix = mat4.create();

      switch (laterality) {
        case "left":
          mat4.copy(this.pMatrix, this.frameData.leftProjectionMatrix);
          var eyeOffset = this.vrDisplay.getEyeParameters(laterality).offset;
          poseToVMatrix(VMatrix, this.frameData.pose, eyeOffset);

          break;

        case "right":
          mat4.copy(this.pMatrix, this.frameData.rightProjectionMatrix);
          var eyeOffset = this.vrDisplay.getEyeParameters(laterality).offset;
          poseToVMatrix(VMatrix, this.frameData.pose, eyeOffset);


          break;

        case undefined:
          mat4.perspective(this.pMatrix, FoVYAngle,
              gl.canvas.clientWidth / gl.canvas.clientHeight,
              0.01, 100);
          var eyeOffset = vec3.fromValues(0,0,0);
          poseToVMatrix(VMatrix, this.frameData.pose, eyeOffset);
          break;

        default:
          throw "unknown laterality: " + laterality;
      }

      var MMatrix = mat4.clone(this.MVMatrix);

      mat4.multiply(this.MVMatrix, VMatrix, MMatrix);


      if (this.extraUpdateAndDrawEye) {
        this.setViewport(laterality);
        this.extraUpdateAndDrawEye(VMatrix);
      }

      this.setViewport(); // set non offset viewport for render to texture
      this.updateAndDrawFrontCoords();

      var x = 0;
      if (laterality === "right") {
        this.setViewport(laterality);
        x = this.viewportWidth();
      }
      gl.useProgram(this.renderer.common);
      gl.uniform2f(gl.getUniformLocation(this.renderer.common, "uViewportOrigin"), x, 0);
      gl.uniform2f(gl.getUniformLocation(this.renderer.common, "uViewportSize"),
          this.viewportWidth(), this.viewportHeight());

      gl.enable(gl.BLEND);
      gl.enable(gl.DEPTH_TEST);
      this.updateAndDrawCommonPass();
      gl.disable(gl.DEPTH_TEST);
      gl.disable(gl.BLEND);

      if (this.overlay) {
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.disable(gl.DEPTH_TEST);
        gl.disable(gl.CULL_FACE);
        this.setViewport(laterality);
        gl.enable(gl.BLEND);
        for (var i=0; i<this.overlay.length; ++i) {
          this.overlay[i](VMatrix);
        }
        gl.disable(gl.BLEND);
      }

      this.MVMatrix = MMatrix;
      this.VMatrix = VMatrix;
    };

    VR.prototype.viewportWidth = function () {
      var width = this.fsaa/fsaaMax * VolumeRenderer.prototype.viewportWidth.call(this);

      if (this.vrDisplay && this.vrDisplay.isPresenting) {
        return width / 2;
      }

      return width;
    };
    VR.prototype.viewportHeight = function () {
      return this.fsaa/fsaaMax * this.gl.drawingBufferHeight;
    };


    VR.prototype.updateSize = function () {
      if (this.vrDisplay && this.vrDisplay.isPresenting) {
        var left = this.vrDisplay.getEyeParameters("left");
        var right = this.vrDisplay.getEyeParameters("right");
        var recommendedWidth = Math.max(left.renderWidth, right.renderWidth) * 2;
        var recommendedHeight = Math.max(left.renderHeight, right.renderHeight);

        this.setSize(Math.floor(fsaaMax * recommendedWidth), Math.floor(fsaaMax * recommendedHeight));

        var boundsWidth = this.fsaa / fsaaMax /2;
        var boundsHeight = this.fsaa / fsaaMax;
        var boundsTop = 1 - boundsHeight; // texcoord different from viewport coords!?!

        this.vrDisplay.requestPresent([ {
          source: this.gl.canvas,
          leftBounds: [0, boundsTop, boundsWidth, boundsHeight],
          rightBounds: [boundsWidth, boundsTop, boundsWidth, boundsHeight]
            } ]);
      } else {
        VolumeRenderer.prototype.updateSize.call(this);
      }
    };

    VR.prototype.setViewport = function (eye) {
      var width = this.viewportWidth();
      var height = this.viewportHeight();
      switch (eye) {
        case "left":
          this.gl.viewport(0, 0, width, height);
          this.gl.scissor(0, 0, width, height);
          break;
        case "right":
          this.gl.viewport(width, 0, width, height);
          this.gl.scissor(width, 0, width, height);
          break;
        default:
          this.gl.viewport(0, 0, width, height);
          this.gl.scissor(0, 0, width, height);
      }
    };

    VR.prototype.setVolumeTexture = function (volume, labelMap) {
      VolumeRenderer.prototype.setVolumeTexture.call(this, volume, labelMap);
    };

    return VR;
  });
