/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils", "volume/volume"],
  function (utils, glu, vol)  {
    "use strict";
    function getScaleNoopFunction(size, sourceSize, depth) {
      utils.assert(size[0] === sourceSize[0] &&
                   size[1] === sourceSize[1] &&
                   size[2] === sourceSize[2]);
      return {
        scale: function dontScale (slice) {
          return new Uint8Array(slice.buffer);
          // for 16 bit we use 2x channels so nothing changes.
        },
        pushToTexture: function (gl, row, column, textureFormat, slice) {
          gl.texSubImage2D(gl.TEXTURE_2D, 0, row*size[0], column*size[1],
              size[0], size[1], textureFormat, gl.UNSIGNED_BYTE, slice);
        }
      };
    }

    /* Default (linear) scaling */
    function getScaleFunction(size, sourceSize, depth) {
      utils.assert(depth === 8, "Scaling slices of depth " + depth +
          " not supported.");
      var canvas = document.createElement("canvas");
      canvas.width = sourceSize[0];
      canvas.height = sourceSize[1];

      var sliceContext = utils.getOffscreenContext2D(canvas);
      var imageData = sliceContext.createImageData(sourceSize[0],
          sourceSize[1]);

      var scaledSliceCanvas = document.createElement("canvas");
      scaledSliceCanvas.width = size[0];
      scaledSliceCanvas.height = size[1];
      var scaledSliceContext =
        scaledSliceCanvas.getContext("2d", {alpha: false});

      return {
        scale: function scaleSlice (slice) {
          var pixels = imageData.data;
          for (var i=0, n=slice.length; i<n; i++) {
            pixels[4*i  ] = slice[i];
            pixels[4*i+1] = slice[i];
            pixels[4*i+2] = slice[i];
            pixels[4*i+3] = 255;
          }
          sliceContext.putImageData(imageData, 0, 0);

          scaledSliceContext.drawImage(sliceContext.canvas, 0, 0, size[0], size[1]);
          return scaledSliceContext.canvas;
        },
        pushToTexture: function (gl, row, column, textureFormat, sliceCanvas) {
          gl.texSubImage2D(gl.TEXTURE_2D, 0, row*size[0], column*size[1],
              textureFormat, gl.UNSIGNED_BYTE, sliceCanvas);
        }
      };
    }

    /** Nearest Neighbour scaling for Labelmap */
    function getScaleNNFunction(size, sourceSize, depth) {
      var scaledSlice;
      var scaledSliceInTextureFormat;
      switch (depth) {
        case 8:
          scaledSlice = new Uint8Array(size[0]*size[1]);
          scaledSliceInTextureFormat = scaledSlice;
          break;
        case 16:
          scaledSlice = new Uint16Array(size[0]*size[1]);
          scaledSliceInTextureFormat = new Uint8Array(scaledSlice.buffer);
          break;
        default:
          throw new Error("unsupported depth for labelmap", depth);
      }
      var xLength = size[0];
      var yLength = size[1];
      var scaleX = sourceSize[0]/size[0];
      var scaleY = sourceSize[1]/size[1];
      var sourceWidth = sourceSize[0];

      return {
        scale: function (slice) {
          for (var y=0; y<yLength; y++) {
            for (var x=0; x<xLength; x++) {
              scaledSlice[y*xLength + x] =
                slice[Math.round(scaleY*y)*sourceWidth + Math.floor(scaleX*x)];
            }
          }
          return scaledSliceInTextureFormat;
        },

        pushToTexture: function (gl, row, column, textureFormat, slice) {
          gl.texSubImage2D(gl.TEXTURE_2D, 0, row*size[0], column*size[1],
              size[0], size[1], textureFormat, gl.UNSIGNED_BYTE, slice);
        }
      };
    }

    function getTextureTiling(volume, gl, options, scale) {
      scale = scale || 1;
      var isotropicDownsampling = options.isotropicDownsampling || false;

      var size = utils.map(function (x) { return Math.floor(x/scale); }, volume.getSize());
      var voxelSize = utils.map(Math.abs, volume.getVoxelSize());

      if (scale > 1) {
        isotropicDownsampling = true;
      }

      var doScaling = false;

      if (isotropicDownsampling) {
        // only support downsampling inplane
        // (i.e. Slice thickness > inplane pixel size)
        if (voxelSize[0] < voxelSize[2] && voxelSize[1] < voxelSize[2]) {
          size = [
            voxelSize[0]/voxelSize[2]*size[0],
            voxelSize[1]/voxelSize[2]*size[1],
            size[2]
          ].map(Math.abs).map(Math.round);
          doScaling = true;
        } else if (scale > 1 &&
            voxelSize[0] == voxelSize[1] && voxelSize[1] == voxelSize[2]) {
          doScaling = true;
        }
      } else {
        utils.assert(voxelSize[0] === voxelSize[1],
            "inpane non square pixels not supported");
      }

      // tile 2D texture with slices
      var maxTextureSize = gl.getParameter(gl.MAX_TEXTURE_SIZE);
      var rows = Math.floor(maxTextureSize / size[0]);
      var columns = Math.ceil(size[2] / rows);
      if (columns === 1) {
        rows = size[2];
      }

      var volumeVoxelSize = [voxelSize[0], voxelSize[1], voxelSize[2]];
      if (doScaling) {
        volumeVoxelSize[0] = voxelSize[2];
        volumeVoxelSize[1] = voxelSize[2];
      }

      // check if tiling fits texture
      if (columns * size[1] <= maxTextureSize) {
        return {
          size: size,
          rows: rows,
          columns: columns,
          voxelSize: volumeVoxelSize,

          textureWidth: function() { return this.size[0] * this.rows; },
          textureHeight: function() { return this.size[1] * this.columns; },

          row: function(z) { return z % this.rows; },
          column: function(z) { return Math.floor(z / this.rows); },

          scale: scale,
          isotropicDownsampling: doScaling
        };
      }

      if (size[0] <= 2 || size[1] <= 2 || size[2] <= 2) {
        throw Error("can not fit volume in texture"); // should never happen;
        // MAX_TEXTURE_SIZE must be at least 1024
      }

      return getTextureTiling(volume, gl, options, ++scale);
    }

    // Returns a texture containing the slices of VOLUME.
    // This is needed since WebGL1 does not support 3D textures.
    // See also the corresponding shader functions.
    function makeTextureVolume3D (gl, volume, isotropicDownsampling) {
      /** Nearest neighbour scaling for labelmap. */
      /** Default canvas scaling (linear or whatever) */
      var depth = volume.getDepth();
      utils.assert(depth === 8 || depth === 16, "unsupported depth", depth);
      var textureType = gl.UNSIGNED_BYTE;

      gl.pixelStorei(gl.UNPACK_ALIGNMENT, 1);
      gl.pixelStorei(gl.UNPACK_COLORSPACE_CONVERSION_WEBGL, gl.NONE);
      var flipY = !vol.isRightHandCoordinateSystem(volume);

      var texture = gl.createTexture();
      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(gl.TEXTURE_2D, texture);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

      var textureFilter = gl.LINEAR;
      if (volume.type === "labelmap") {
        textureFilter = gl.NEAREST;
      }
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, textureFilter);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, textureFilter);

      var tiling = getTextureTiling(volume, gl,
          { isotropicDownsampling: isotropicDownsampling });

      var textureFormat = depth === 8 ? gl.LUMINANCE : gl.LUMINANCE_ALPHA;

      glu.checkGLError(gl, "texVolume3D");
      gl.texImage2D(gl.TEXTURE_2D, 0, textureFormat,
          tiling.textureWidth(), tiling.textureHeight(),
          0, textureFormat, textureType, null);
      glu.checkGLError(gl, "Error creating volume texture: ");

      var scaleFunction;
      if (tiling.isotropicDownsampling) {
        if (volume.type === "labelmap") {
          scaleFunction = getScaleNNFunction(tiling.size, volume.getSize(), depth);
        } else {
          scaleFunction = getScaleFunction(tiling.size, volume.getSize(), depth);
        }
      } else {
        scaleFunction = getScaleNoopFunction(tiling.size, volume.getSize(), depth);
      }

      if (tiling.scale > 1) {
        console.warn("insufficient MAX_TEXTURE_SIZE: downscaling x" + tiling.scale);
      }
      var sliceStride = tiling.scale;

      var z = 0;
      var zMax = tiling.size[2];

      var pushSlice = function pushSlice (slice) {
        if (gl.isContextLost()) return;

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, flipY);

        scaleFunction.pushToTexture(gl, tiling.row(z), tiling.column(z),
            textureFormat, scaleFunction.scale(slice));

        if (utils.debug) glu.checkGLError(gl, "texSubImage2D z:" + z + " " + volume.type);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 0);
        gl.bindTexture(gl.TEXTURE_2D, null);

        ++z;
        if (z < zMax) {
          volume.getSlice(pushSlice, z*sliceStride);
        }
      };

      volume.getSlice(pushSlice, z*sliceStride);
      gl.bindTexture(gl.TEXTURE_2D, null);

      glu.checkGLError(gl, "texVolume3D");

      var textureData = {
        size: tiling.size,
        voxelSize: tiling.voxelSize,
        texture: texture,
        rows: tiling.rows,
        columns: tiling.columns
      };
      return textureData;
    }

    function makeLabelMapLUTTexture(gl, lut) {
      var lutTexture = {};
      gl.pixelStorei(gl.UNPACK_ALIGNMENT, 1);
      gl.pixelStorei(gl.UNPACK_COLORSPACE_CONVERSION_WEBGL, gl.NONE);

      gl.activeTexture(gl.TEXTURE0);
      lutTexture.texture = gl.createTexture();
      gl.bindTexture(gl.TEXTURE_2D, lutTexture.texture);

      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

      updateLabelMapLUTTexture(gl, lut, lutTexture);

      return lutTexture;
    }

    function updateLabelMapLUTTexture(gl, lut, lutTexture) {
      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(gl.TEXTURE_2D, lutTexture.texture);

      var lutArray = lut.getArray();
      if (lutTexture.length) {
        utils.assert(lutTexture.length == lutArray.length/4,
            "updateLabelMapLUTTexture texture width mismatch");
      } else {
        lutTexture.length = lutArray.length/4;
      }

      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, lutTexture.length, 1,
          0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array(lutArray));

      gl.bindTexture(gl.TEXTURE_2D, null);
    }

    return {
      makeTextureVolume3D: makeTextureVolume3D,
      makeLabelMapLUTTexture: makeLabelMapLUTTexture,
      updateLabelMapLUTTexture: updateLabelMapLUTTexture
    };
  });
