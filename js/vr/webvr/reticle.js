/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils", "vr/gl-renderer", "lib/gl-matrix"],
  function (utils, glu, Renderer, glm)  {
    "use strict";

    var mat4 = glm.mat4;

    var Reticle = function (options) {
      utils.assert(options.radius && options.distance, options.width);

      this.vertexShaderURL = "/shaders/webvr/reticle.vert";
      this.fragmentShaderURL = "/shaders/webvr/reticle.frag";

      this.options = options;
    };

    Reticle.prototype.init = function (gl, cont) {
      Renderer.init.call(this, gl, function () {
        var options = this.options;
        var vertices = [
          -1, -1, 0,
           1, -1, 0,
          -1,  1, 0,
           1,  1, 0
        ];

        for (var i=0; i<vertices.length; ++i) {
          vertices[i] *= options.radius;
        }
        for (var i=2; i<vertices.length; i+=3) {
          vertices[i] = options.distance;
        }

        this.positions = glu.createBuffer(gl, gl.ARRAY_BUFFER,
            new Float32Array(vertices), gl.STATIC_DRAW);

        glu.checkGLError(gl, "makeReticle");

        gl.useProgram(this.program);

        var radiusLocation = gl.getUniformLocation(this.program, "uRadius");
        utils.assert(radiusLocation);
        gl.uniform1f(radiusLocation, options.radius);

        var widthLocation = gl.getUniformLocation(this.program, "uWidth");
        utils.assert(widthLocation);
        gl.uniform1f(widthLocation, options.width);


        this.pMatrixUniform = gl.getUniformLocation(this.program, "uPMatrix");
        this.vMatrixUniform = gl.getUniformLocation(this.program, "uVMatrix");

        gl.enableVertexAttribArray(Renderer.vertexPositionAttribute);

        glu.checkGLError(gl, "reticle.init");
        if (cont) cont();
      }.bind(this));
    };

    var MVMatrix = mat4.create();
    Reticle.prototype.draw = function (gl, pMatrix, vMatrix) {
      if (this.program && !this.disabled) {
        gl.useProgram(this.program);
        gl.disableVertexAttribArray(Renderer.vertexColorAttribute);
        gl.uniformMatrix4fv(this.pMatrixUniform, false, pMatrix);
        gl.uniformMatrix4fv(this.vMatrixUniform, false, vMatrix);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.positions);
        gl.vertexAttribPointer(Renderer.vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
        gl.enableVertexAttribArray(Renderer.vertexColorAttribute);

      }
      glu.checkGLError(gl, "Reticle.draw");
    };

    return Reticle;
  });
