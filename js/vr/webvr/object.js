/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils", "vr/gl-renderer", "lib/gl-matrix"],
  function (utils, glu, Renderer, glm)  {
    "use strict";

    var mat4 = glm.mat4;
    var vec3 = glm.vec3;

    var vertexNormalAttribute = 2;

    function setVertex(array, index, vertex) {
      for (var i=0; i<vertex.length; i++) {
        array[index * vertex.length + i] = vertex[i];
      }
    }

    // Cylinder around the z axis. edges +- height/2 from the xy plane.
    // Every upper vertex is equidistant to its the neighboring lower vertices.
    // In that manner the cylinder is made up of two times the subdivision count
    // triangles.
    function makeCylinder(gl, options) {
      utils.assert(options.subdivisions && options.radius && options.height);

      var isClosed = !options.sectionAngle;

      var numVertices = options.subdivisions * 2 + 2;
      var vertices = new Float32Array(3 * numVertices);
      var normals = new Float32Array(3 * numVertices);

      var sectionAngle = options.sectionAngle || 2*Math.PI;

      var radius = options.radius;
      var z = options.height/2;
      var angleOffset = sectionAngle / options.subdivisions / 2;

      var i = 0;
      if (!isClosed) {
        // right angled begin edge
        var angle = 0;
        setVertex(vertices, 0, [radius * Math.cos(angle), -z, radius * Math.sin(angle)]);
        setVertex(normals, 0, [-Math.cos(angle), 0, -Math.sin(angle)]); // face inward
        i++;

        numVertices--;
      }

      for (; i<numVertices; i+=2) {
        var angle = Math.floor(i/2) * sectionAngle / options.subdivisions;
        setVertex(vertices, i, [radius * Math.cos(angle), z, radius * Math.sin(angle)]);
        //setVertex(vertices, i, [radius * Math.cos(angle), radius * Math.sin(angle), z]);
        setVertex(normals, i, [-Math.cos(angle), 0, -Math.sin(angle)]); // face inward

        angle += angleOffset;
        setVertex(vertices, i+1, [radius * Math.cos(angle), -z, radius * Math.sin(angle)]);
        //setVertex(vertices, i, [radius * Math.cos(angle), z, radius * Math.sin(angle)]);
        setVertex(normals, i+1, [-Math.cos(angle), 0, -Math.sin(angle)]);
      }

      if (!isClosed) {
        var angle = Math.floor((i-2)/2) * sectionAngle / options.subdivisions + angleOffset;
        setVertex(vertices, i, [radius * Math.cos(angle), z, radius * Math.sin(angle)]);
        setVertex(normals, 0, [-Math.cos(angle), 0, -Math.sin(angle)]); // face inward
        numVertices++;
      }

      var vertexBuffer = glu.createBuffer(gl, gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
      vertexBuffer.itemSize = 3;
      vertexBuffer.numItems = numVertices;
      utils.assert(vertices.length == vertexBuffer.numItems * vertexBuffer.itemSize);

      var normalBuffer = glu.createBuffer(gl, gl.ARRAY_BUFFER, normals, gl.STATIC_DRAW);

      var color = options.color || [1, 1, 1];
      var colors = new Float32Array(3*numVertices);
      for (var i=0; i < 3*numVertices; i+=3) {
        colors[i  ] = color[0];
        colors[i+1] = color[1];
        colors[i+2] = color[2];
      }
      utils.assert(colors.length == vertices.length);
      var colorBuffer = glu.createBuffer(gl, gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);

      glu.checkGLError(gl, "makeCylinder");

      return {
        positions: vertexBuffer,
        colors: colorBuffer,
        normals: normalBuffer,
        draw: function (gl) {
          gl.bindBuffer(gl.ARRAY_BUFFER, this.positions);
          gl.vertexAttribPointer(Renderer.vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

          gl.bindBuffer(gl.ARRAY_BUFFER, this.colors);
          gl.vertexAttribPointer(Renderer.vertexColorAttribute, 3, gl.FLOAT, false, 0, 0);

          gl.bindBuffer(gl.ARRAY_BUFFER, this.normals);
          gl.vertexAttribPointer(vertexNormalAttribute, 3, gl.FLOAT, false, 0, 0);

          gl.drawArrays(gl.TRIANGLE_STRIP, 0, this.positions.numItems);
        }
      };
    }

    var Scene = function () {
      this.vertexShaderURL = "/shaders/webvr/scene.vert";
      this.fragmentShaderURL = "/shaders/webvr/scene.frag";

      this.light = vec3.fromValues(2, 8, 0);

      this.objects = [];
    };

    Scene.prototype.init = function (gl, cont) {
      Renderer.init.call(this, gl, function () {
        gl.useProgram(this.program);
        this.pMatrixUniform = gl.getUniformLocation(this.program, "uPMatrix");
        this.mvMatrixUniform = gl.getUniformLocation(this.program, "uMVMatrix");
        this.lightUniform = gl.getUniformLocation(this.program, "uLightPosition");

        gl.bindAttribLocation(this.program, vertexNormalAttribute, "aVertexNormal");

        gl.enableVertexAttribArray(Renderer.vertexPositionAttribute);
        gl.enableVertexAttribArray(Renderer.vertexColorAttribute);
        //gl.enableVertexAttribArray(vertexNormalAttribute);

        glu.checkGLError(gl, "scene");

        if (cont) cont();
      }.bind(this));
    };

    Scene.prototype.add = function (object) {
      this.objects.push(object);
    };

    //var normalMatrix = mat4.create();
    var lightPosition = vec3.create();
    Scene.prototype.update = function (gl, pMatrix, mvMatrix) {
      this.mvMatrix = mvMatrix;
      gl.useProgram(this.program);
      gl.uniformMatrix4fv(this.pMatrixUniform, false, pMatrix);
      gl.uniformMatrix4fv(this.mvMatrixUniform, false, mvMatrix);

      vec3.transformMat4(lightPosition, this.light, mvMatrix);
      gl.uniform3fv(this.lightUniform, lightPosition);

      /*
      mat4.transpose(normalMatrix, mat4.inverse(normalMatrix, mvMatrix));
      gl.uniformMatrix4fv(this.NormalMatrixUniform, false, normalMatrix);
      */

      gl.disable(gl.CULL_FACE);
    };

    var MVMatrix = mat4.create();
    Scene.prototype.draw = function (gl) {
      if (this.program) {
        gl.useProgram(this.program);
        gl.enableVertexAttribArray(vertexNormalAttribute);
        for (var i=0; i<this.objects.length; i++) {
          var object = this.objects[i];
          if (object.MMatrix) {
            mat4.multiply(MVMatrix, this.mvMatrix, object.MMatrix);
            gl.uniformMatrix4fv(this.mvMatrixUniform, false, MVMatrix);
            object.draw(gl);
          } else {
            gl.uniformMatrix4fv(this.mvMatrixUniform, false, this.mvMatrix);
            object.draw(gl);
          }
        }
      }
      gl.disableVertexAttribArray(vertexNormalAttribute);
      glu.checkGLError(gl, "Scene.draw");
    };

    return {
      makeCylinder: makeCylinder,
      Scene: Scene
    };
  });
