/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils", "vr/gl-renderer", "lib/gl-matrix"],
  function (utils, glu, Renderer, glm)  {
    "use strict";

    var mat4 = glm.mat4;

    var Overlay = function (options) {
      this.vertexShaderURL = "/shaders/webvr/overlay.vert";
      this.fragmentShaderURL = "/shaders/webvr/overlay.frag";

      this.options = options;
    };

    Overlay.prototype.init = function (gl, cont) {
      Renderer.init.call(this, gl, function () {
        var options = this.options;

        var t = options.top;
        var l = options.left;
        var r = options.right;
        var b = options.bottom;

        var vertices = [
           l, b, 0,
           r, b, 0,
           l, t, 0,
           r, t, 0
        ];

        for (var i=2; i<vertices.length; i+=3) {
          vertices[i] = options.distance;
        }

        this.positions = glu.createBuffer(gl, gl.ARRAY_BUFFER,
            new Float32Array(vertices), gl.STATIC_DRAW);

        var textureCoordinates = [
          0, 1,
          1, 1,
          0, 0,
          1, 0
          ];

        this.textureCoordinatesBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordinatesBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordinates),
                          gl.STATIC_DRAW);

        gl.useProgram(this.program);
        glu.checkGLError(gl, "overlay p");

        var texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.bindTexture(gl.TEXTURE_2D, null);
        this.texture = texture;
        glu.checkGLError(gl, "overlay t");

        this.textureLocation = gl.getUniformLocation(this.program, "uSampler");
        utils.assert(this.textureLocation);
        gl.uniform1i(this.textureLocation, 0);

        this.pMatrixUniform = gl.getUniformLocation(this.program, "uPMatrix");
        this.vMatrixUniform = gl.getUniformLocation(this.program, "uVMatrix");

        gl.enableVertexAttribArray(Renderer.vertexPositionAttribute);

        var textureCoordAttribute = gl.getAttribLocation(this.program, 'aTextureCoord');
        gl.enableVertexAttribArray(textureCoordAttribute);

        glu.checkGLError(gl, "overlay");
        if (cont) cont();
      }.bind(this));
    };

    Overlay.prototype.setTexture = function (image) {
      this.image = image;
    };

    var MVMatrix = mat4.create();
    Overlay.prototype.draw = function (gl, pMatrix, vMatrix) {
      if (this.program) {
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
        if (this.image) {
          gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
          gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.image);
          this.image = null;
        }

        gl.useProgram(this.program);
        gl.disableVertexAttribArray(Renderer.vertexColorAttribute);
        gl.uniformMatrix4fv(this.pMatrixUniform, false, pMatrix);
        gl.uniformMatrix4fv(this.vMatrixUniform, false, vMatrix);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.positions);
        gl.vertexAttribPointer(Renderer.vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordinatesBuffer);
        var textureCoordAttribute = gl.getAttribLocation(this.program, 'aTextureCoord');
        gl.enableVertexAttribArray(textureCoordAttribute);
        gl.vertexAttribPointer(textureCoordAttribute, 2, gl.FLOAT, false, 0, 0);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.texture);

        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
        gl.enableVertexAttribArray(Renderer.vertexColorAttribute);

        gl.bindTexture(gl.TEXTURE_2D, null);

      }
      glu.checkGLError(gl, "Overlay.draw");
    };

    return Overlay;
  });
