/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils"],
  function (utils, glu)  {
    "use strict";

    var vertexPositionAttribute = 0;

    var vertices = [
       0, 0, 0, 1,
       1, 0, 0, 0,
       0, 0, 1, 0,
      -1, 0, 0, 0,
       0, 0,-1, 0
    ];

    var indices = [
      0, 1, 2,
      0, 2, 3,
      0, 3, 4,
      0, 4, 1
    ];

    function makeFloor(gl) {
      var vertexBuffer = glu.createBuffer(gl, gl.ARRAY_BUFFER,
          new Float32Array(vertices), gl.STATIC_DRAW);
      vertexBuffer.itemSize = 4;
      vertexBuffer.numItems = 5;
      console.assert(vertices.length = vertexBuffer.numItems * vertexBuffer.itemSize);

      var indexBuffer = glu.createBuffer(gl, gl.ELEMENT_ARRAY_BUFFER,
          new Uint16Array(indices), gl.STATIC_DRAW);
      indexBuffer.itemSize = 1;
      indexBuffer.numItems = 3*4;

      console.assert(indices.length = indexBuffer.numItems * indexBuffer.itemSize);

      var vertexShaderURL = "/shaders/webvr/floor.vert";
      var fragmentShaderURL = "/shaders/webvr/floor.frag";

      return {
        positions: vertexBuffer,
        indices: indexBuffer,

        init: function(gl, cont) {
          glu.loadShaderSources([vertexShaderURL, fragmentShaderURL],
              function (shaders) {
                this.vertexShaderSource = shaders[vertexShaderURL];

                this.program = gl.createProgram();

                gl.enableVertexAttribArray(vertexPositionAttribute);

                var vertexShader = glu.createShaderWithSources(gl,
                    this.vertexShaderSource, gl.VERTEX_SHADER, "floor vertex shader");
                gl.attachShader(this.program, vertexShader);
                gl.bindAttribLocation(this.program, vertexPositionAttribute, "aVertexPosition");

                var fragmentShader = glu.createShaderWithSources(gl,
                    shaders[fragmentShaderURL],
                    gl.FRAGMENT_SHADER, fragmentShaderURL);
                gl.attachShader(this.program, fragmentShader);

                glu.linkProgram(gl, this.program);

                this.program.name = fragmentShaderURL;
                if (utils.debug) glu.validateProgram(gl, this.program);

                this.program.pMatrixUniform = gl.getUniformLocation(this.program, "uPMatrix");
                this.program.mvMatrixUniform = gl.getUniformLocation(this.program, "uMVMatrix");

                glu.checkGLError(gl, "floor.init");

                cont();
              }.bind(this), utils.versionNumber);
        },
        updateProgram: function (gl, pMatrix, mvMatrix) {
          gl.useProgram(this.program);
          gl.uniformMatrix4fv(this.program.pMatrixUniform, false, pMatrix);
          gl.uniformMatrix4fv(this.program.mvMatrixUniform, false, mvMatrix);

          gl.bindBuffer(gl.ARRAY_BUFFER, this.positions);
          gl.vertexAttribPointer(vertexPositionAttribute, this.positions.itemSize, gl.FLOAT, false, 0, 0);

          gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indices);
          glu.checkGLError(gl, "floor.update");
        },
        draw: function (gl) {
          gl.useProgram(this.program);
          gl.disableVertexAttribArray(1);
          gl.disable(gl.CULL_FACE);
          gl.drawElements(gl.TRIANGLES, this.indices.numItems, gl.UNSIGNED_SHORT, 0);
          glu.checkGLError(gl, "floor.draw");
          gl.enableVertexAttribArray(1);
        }
      };
    }

    return makeFloor;
  });
