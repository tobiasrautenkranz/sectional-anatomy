/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["lib/gl-matrix"],
  function (glm)  {
    "use strict";

    var quat = glm.quat;
    var vec3 = glm.vec3;
    var vec2 = glm.vec2;

    /*
     * ARCBALL:
     * A User Interface for Specifying Three-Dimensional Orientation Using a Mouse
     * Ken Shoemak
     * https://www.talisman.org/~erlkonig/misc/shoemake92-arcball.pdf
     */
    var ArcBall = function () {
      this.startPosition = undefined;
      this.currentPosition = undefined;
      this.startRotation = quat.create();

      this.dragDistance2 = 0.0;
    };

    ArcBall.prototype.setStartPosition = function (position) {
      this.endDrag();

      this.startPosition = position;
    };
    ArcBall.prototype.setPosition = function (position) {
      var lastPosition = this.currentPosition || this.startPosition;

      if (lastPosition) {
        this.dragDistance2 += vec2.squaredDistance(lastPosition, position);
        this.currentPosition = position;
      } else {
        this.setStartPosition(position);
      }
    };

    // sets vector. returns true when position is inside the arcball
    function positionToVector(position, vector) {
      var radius2 = vec2.squaredLength(position);

      vector[0] = position[0];
      vector[1] = position[1];

      if (radius2 > 1) {
        var scale = 1.0/Math.sqrt(radius2);
        vector[0] *= scale;
        vector[1] *= scale;
        vector[2] = 0;

        return false;
      }

      vector[2] = Math.sqrt(1.0 - radius2);

      return true;
    }

    // Additional rotation for positions outside the Arcball.
    function positionToExtraAngle(position) {
      var radius2 = vec2.squaredLength(position);

      if (radius2 <= 1) {
        return 0;
      }

      // no need to consider the sign (e.g.: left <-> right); it works out with
      // setting the quaternion using the cross and dot product. (The angle is
      // always positive)
      return 0.5 * Math.PI * (Math.sqrt(radius2)-1);
    }

    var v1 = vec3.create(); // preallocate
    var v2 = vec3.create();
    ArcBall.prototype.rotation = function (rot) {
      if (undefined === this.currentPosition) {
        return this.startRotation;
      }
      var startInside = positionToVector(this.startPosition, v1);
      var endInside = positionToVector(this.currentPosition, v2);

      rot = rot || quat.create();
      var vecAngle = Math.acos(vec3.dot(v1, v2));
      if (vecAngle === 0) {
        quat.identity(rot);
        return rot;
      }
      var angle = vecAngle;
      if (startInside && !endInside) {
        angle += positionToExtraAngle(this.currentPosition);
      }

      var axis = vec3.cross(v1, v1, v2);
      vec3.scale(axis, axis, 1/Math.sin(vecAngle)); // normalize

      quat.setAxisAngle(rot, axis, 2 * angle); // 2x angle for path independence

      quat.multiply(rot, rot, this.startRotation);
      return rot;
    };

    ArcBall.prototype.setRotation = function (rotation) {
      quat.copy(this.startRotation, rotation);
    };
    ArcBall.prototype.rotate = function (rotation) {
      quat.multiply(this.startRotation, rotation, this.startRotation);
    };

    ArcBall.prototype.endDrag = function () {
      this.currentPosition = undefined;

      this.startPosition = undefined;

      this.dragDistance2 = 0.0;
    };

    ArcBall.prototype.isRotating = function () {
      return !!this.startPosition;
    };

    return ArcBall;
  });
