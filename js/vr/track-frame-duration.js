/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["view/redraw", "util/gl-utils", "util/utils"], function (draw, glu, utils) {
  "use strict";
  function noop() {}

  function makeDisjointQuery () {
    return {
      init: function (gl) {
        if (!gl) {
          this.ext = null;
          return;
        }

        this.gl = gl;
        this.ext = this.gl.getExtension("EXT_disjoint_timer_query");
        if (this.ext) {
          if (utils.debug) console.log("EXT_disjoint_timer_query found");
          this.query = this.ext.createQueryEXT();

          // reset disjoint parameter
          this.getDisjoint();
        }
        if (utils.debug) glu.checkGLError(this.gl, "makeDisjointQuery");
      },
      hasExt: function () {
        return !!this.ext;
      },

      isAvailable: function () {
        var result = this.ext &&
          this.ext.isQueryEXT(this.query) &&
          this.ext.getQueryObjectEXT(this.query,
            this.ext.QUERY_RESULT_AVAILABLE_EXT);

        if (utils.debug) glu.checkGLError(this.gl, "disjointQuery available");

        return result;
      },
      // checks that the result can be used for performance measurements.
      // true when e.g. the GPU entered power saving while measuring. The value
      // is reset to false when called.
      getDisjoint: function () {
        var result = this.gl.getParameter(this.ext.GPU_DISJOINT_EXT);
        if (utils.debug) glu.checkGLError(this.gl, "disjointQuery disjoint");
        return result;
      },

      // Time for GL commands between begin() and end() in nanoseconds.
      elapsedTime: function () {
        var result = this.ext.getQueryObjectEXT(this.query, this.ext.QUERY_RESULT_EXT);
        if (utils.debug) glu.checkGLError(this.gl, "disjointQuery elapsed");
        return result;
      },

      begin: function () {
        this.ext.beginQueryEXT(this.ext.TIME_ELAPSED_EXT, this.query);
        if (utils.debug) glu.checkGLError(this.gl, "disjointQuery begin");
      },
      end: function () {
        this.ext.endQueryEXT(this.ext.TIME_ELAPSED_EXT);
        if (utils.debug) glu.checkGLError(this.gl, "disjointQuery end");
      }
    };
  }

  // Track frame per second (FPS)
  function makeTracker () {
    var avgDelta; // ms frame duration per area

    var area = 1; // draw area in e.g. pixels

    // disabled since it underestimates the true fps
    var disjointQuery = makeDisjointQuery();

    var frameDuration = function () {
      if (disjointQuery.isAvailable()) {
        if (!disjointQuery.getDisjoint()) {
          return disjointQuery.elapsedTime() * 1e-6;
        }
      } else if (draw.validFrameDuration()) {
        return draw.frameDuration();
      }

      return null;
    };

    var skip = false;

    var tracker = {
      init: function (gl) {
       disjointQuery.init(gl);
       if (disjointQuery.hasExt()) {
         this.begin = disjointQuery.begin.bind(disjointQuery);
         this.end = disjointQuery.end.bind(disjointQuery);
       } else {
         this.begin = noop;
         this.end = noop;
       }
      },
      setSize: function (width, height) {
        area = width*height;
      },
      update: function () {
        if (skip) {
          skip = false;
          return;
        }

        var duration = frameDuration();
        if (duration) {
          var delta = Math.max(1000/60, duration) / area;
          avgDelta = (delta + avgDelta) / 2;
        }
      },
      fps: function () {
        return Math.min(60, 1000/(avgDelta * area));
      },
      reset: function () {
        avgDelta = 1000/20;
      },
      skipNext: function () {
        skip = true;
      },

      begin: noop,
      end: noop
    };

    tracker.reset();

    return tracker;
  }

  return makeTracker;
});
