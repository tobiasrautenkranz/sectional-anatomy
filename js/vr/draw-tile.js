/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["vr/gl-renderer", "lib/gl-matrix"],
  function (Renderer, glm)  {
    "use strict";

    var mat4 = glm.mat4;
    // Draw the tile from a texture (rendered to texture) to the canvas.
    function makeDrawTile (gl, textureId) {
      var drawTile = {
        toModelMatrix:  mat4.create(),
        vertexShaderURL: "/shaders/scale.vert",
        fragmentShaderURL: "/shaders/scale.frag",
        init2: function (gl) {
          gl.useProgram(this.program);
          gl.uniform1i(gl.getUniformLocation(this.program, "uTile"), textureId);
          this.uViewportScale = gl.getUniformLocation(this.program, "uViewportScale");
        },
        init: function (gl, cont) {
          Renderer.init.call(this, gl,
              function () {
                this.init2(gl);
                cont();
              }.bind(this));
        }
      };

      var vertexBuffer = gl.createBuffer();
      gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
      var vertices = [
        -1, -1,
         1, -1,
        -1,  1,
         1,  1,
      ];
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
      drawTile.positions = vertexBuffer;

      drawTile.update = function (gl, scale) {
        gl.useProgram(this.program);
        gl.uniform1f(this.uViewportScale, scale);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.positions);
        gl.vertexAttribPointer(Renderer.vertexPositionAttribute, 2, gl.FLOAT, false, 0, 0);
      };

      drawTile.draw = function (gl) {
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
      };

      return drawTile;
    }

    return makeDrawTile;
  });
