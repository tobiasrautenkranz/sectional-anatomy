/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils", "volume/volume", "vr/volumerenderer-tile",
    "vr/tex-volume", "vr/program-cache",
    "vr/coords",
    "lib/gl-matrix", "util/console"],
  function (utils, glu, vol, VolumeRenderer, texVol, ProgramCache,
    coords,
    glm, console)  {
    "use strict";

    var vec3 = glm.vec3;
    var vec4 = glm.vec4;

    // Volume Rendering in a canvas element using webgl
    var VRCanvas = function VRCanvas(canvasElement, options) {
      options = options || {};
      this.canvas = canvasElement;

      this.canvas.addEventListener("webglcontextcreationerror", function (e) {
        throw new Error("WebGL context creation error: " +
            (e.statusMessage || "Unknown"));
      });

      this.canvas.addEventListener("webglcontextlost", function (e) {
        e.preventDefault(); // we can handle context lost
        if (utils.debug) {
          console.group("context lost");
        }

        this.invalidateWebGLObjects();
      }.bind(this), false);
      this.canvas.addEventListener("webglcontextrestored", function (e) {
        if (utils.debug) console.log("context regained");

        this.init(function () {
          this.draw();
          if (utils.debug) {
            console.log("restore done");
            console.groupEnd();
          }
        }.bind(this), true);
      }.bind(this), false);

      this.programCache = new ProgramCache(function (renderer, cont) {
        this.vr.makeRenderer(renderer,
            {quality: this.quality,
              depth: this.volume.getLabelMap().getDepth()
            },
            cont);
      }.bind(this));
      this.invalidateWebGLObjects();

      this.vr = new VolumeRenderer();

      if (options.constantFPS) {
        this.vr.fpsTarget = {min: 28, max: 40};
      }

      this.quality = 1;
    };

    VRCanvas.prototype.invalidateWebGLObjects = function () {
      this.glObjects = {};
      this.programCache.clear();
    };

    VRCanvas.prototype.init = function (cont, restoreContext) {
      var gl = glu.getContext(this.canvas, {
        antialias: false,
        depth: false,
        alpha: false
      });
      if (!gl) {
        throw new Error("Error creating webgl context");
      }

      if (gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.HIGH_FLOAT).precision === 0) {
        console.error("'highp float' fragmet shader precision not supported. " +
            "rendering artifacts may occur!");
      }

      this.gl = gl;


      var init1 = function () {
        if (restoreContext) {
          if (this.volume) {
            this.setVolume(this.volume);
          }
          if (this.lut) {
            this.setLUT(this.lut);
          }
          if (this.renderer) {
            this.setRenderer(this.renderer, init2);
          } else {
            init2();
          }
        } else {
          init2();
        }
      }.bind(this);

      var init2 = function () {
        glu.checkGLError(gl, "VRCanvas.init");

        cont();
      }.bind(this);

      this.vr.init(gl, init1);
    };

    VRCanvas.prototype.mvMatrix = function() {
      return this.vr.MVMatrix;
    };

    VRCanvas.prototype.draw = function draw() {
      if (this.gl.isContextLost()) {
        return;
      }

      if (this.updateMVMatrix) {
        this.updateMVMatrix(this.vr.MVMatrix);
        this.vr.updateMVMatrix();
      }

      this.updateLabelMapLUT(); // FIXME only when needed

      this.vr.updateAndDraw();
    };


    VRCanvas.prototype.setUpdateMVMatrix = function (updateMVMatrix) {
      this.updateMVMatrix = updateMVMatrix;
    };

    VRCanvas.prototype.setVolume = function (volume) {
      this.volume = volume;
      var isotropicDownsampling = true;
      var voxelSize = volume.getVoxelSize();
      if (Math.abs(voxelSize[2] / voxelSize[1]) >= 4 &&
          volume.getSize()[2] < 100) {
        // prevent killing to much data on low z resolution due to isotropic
        // downscaling (e.g. ct-wrist)
        isotropicDownsampling = false;
        if (utils.debug) {
          console.warn("high slice thickness for data; using anisotropic voxels");
        }
      }

      var textureVolume = texVol.makeTextureVolume3D(this.gl, this.volume,
          isotropicDownsampling);

      var labelMap = this.volume.getLabelMap();
      var labelMapVolume = texVol.makeTextureVolume3D(this.gl, labelMap,
          isotropicDownsampling);
      labelMapVolume.depth = labelMap.getDepth();

      this.vr.setVolumeTexture(textureVolume, labelMapVolume);

      this.glObjects.labelMapLUT =
        texVol.makeLabelMapLUTTexture(this.gl, labelMap.getLUT());
      this.vr.setLabelMapLUT(this.glObjects.labelMapLUT);
    };

    VRCanvas.prototype.updateLabelMapLUT = function () {
      if (!this.glObjects.labelMapLUT) {
        return;
      }
      texVol.updateLabelMapLUTTexture(this.gl,
          this.volume.getLabelMap().getLUT(), this.glObjects.labelMapLUT);
    };

    VRCanvas.prototype.setLUT = function (lut) {
      this.lut = lut;

      var gl = this.gl;

      this.glObjects.lutTexture = gl.createTexture();
      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(gl.TEXTURE_2D, this.glObjects.lutTexture);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
      gl.bindTexture(gl.TEXTURE_2D, null);
      this.vr.setLUTTexture(this.glObjects.lutTexture);

      if (typeof this.lut !== "function") {
        this.lut = function () {return new Uint8Array(lut.getArray());};
      }

      this.updateLUT();
    };

    VRCanvas.prototype.updateLUT = function () {
      if (!this.lut) {
        return;
      }

      var gl = this.gl;

      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(gl.TEXTURE_2D, this.glObjects.lutTexture);

      var textureArray = this.lut();

      var channels = Math.floor(textureArray.length / 256);
      if (textureArray.length % 256 !== 0) {
        throw new Error("unsupported lut array size: " + textureArray.length);
      }
      var textureFormat;
      switch (channels) {
        case 1:
          textureFormat = gl.LUMINANCE;
          break;
        case 2:
          textureFormat = gl.LUMINANCE_ALPHA;
          break;
        case 4:
          textureFormat = gl.RGBA;
          break;
      }
      gl.texImage2D(gl.TEXTURE_2D, 0, textureFormat, 256, 1,
          0, textureFormat, gl.UNSIGNED_BYTE, textureArray);
      gl.bindTexture(gl.TEXTURE_2D, null);
    };

    VRCanvas.prototype.setQuality = function (quality, cont) {
      this.quality = quality;
      this.programCache.setQuality(quality);
      this.setRenderer(this.renderer, cont);
    };

    VRCanvas.prototype.setRenderer = function (renderer, cont) {
      this.renderer = renderer;
      this.programCache.get(renderer,
          function (rendererProgram) {
            this.vr.setRenderer(rendererProgram);
            this.vr.updateRenderer(renderer.config);
            if (cont) cont();
          }.bind(this));
    };

    VRCanvas.prototype.volumeDirection = function (x, y) {
      var subSample = this.vr.subSample();
      var viewport = vec4.fromValues(0, 0,
          this.vr.viewportWidth()/subSample,
          this.vr.gl.drawingBufferHeight/subSample);
      return coords.volumeDirection(x, y, {
            mvMatrix: this.vr.MVMatrix,
            pMatrix: this.vr.pMatrix,
            viewport: viewport,
            boundingBox: this.vr.cube.range
          });
    };

    VRCanvas.prototype.inGLCoords = function (x, y) {
      var w = this.canvas.clientWidth;
      if (this.vr.showAnatomy) {
        w /= 2;
      }
      var h = this.canvas.clientHeight;

      // normalize [0 1];
      x /= (w-1);
      y /= (h-1);


      var subSample = this.vr.subSample();

      // to gl window coordinates
      var windowWidth = this.vr.viewportWidth() / subSample;
      var windowHeight = this.vr.gl.drawingBufferHeight / subSample;

      x *= windowWidth-1;
      x = x % windowWidth; // warp for anatomy

      y *= windowHeight-1;
      y = windowHeight-1 - y; // flip Y

      return {x: Math.round(x), y: Math.round(y)};
    };

    var pixel = new Uint8Array(4);
    VRCanvas.prototype.pick = function (x, y) {
      var gl = this.gl;
      var rendererDescription = this.renderer;

      var c = this.inGLCoords(x, y);

      var picked = false;
      // try picking using label ids stored in the last stage.
      if (rendererDescription.pickFunction) {
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.vr.framebuffers[1].framebuffer);
        gl.readPixels(c.x, c.y, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, pixel);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        picked = rendererDescription.pickFunction(pixel);
      }

      // general picking (i.e.: ray casting the selected pixel)
      if (!picked && rendererDescription.pickFunctionJs) {
        //FIXME do not hardcode framebuffer index
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.vr.framebuffers[0].framebuffer);
        gl.readPixels(c.x, c.y, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, pixel);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        var direction = this.volumeDirection(c.x, c.y);
        var start = vec3.fromValues(pixel[0]/255, pixel[1]/255, pixel[2]/255);
        if (vol.isRightHandCoordinateSystem(this.volume)) {
          // flipY
          direction[1] = -direction[1];
          start[1] = 1 - start[1];
        }
        rendererDescription.pickFunctionJs(start, direction, this.volume,
            this.lut());
      }
    };

    VRCanvas.prototype.setAnatomyVisible = function (visible) {
      this.vr.setAnatomyVisible(visible);
    };

    return VRCanvas;
  });
