/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils"],
  function (utils, glu)  {
    "use strict";

    // shader program helper.
    // the shaders and some vertices
    var Renderer = {
      vertexPositionAttribute: 0,
      vertexColorAttribute: 1,
      attributes: ["aVertexPosition", "aVertexColor"],
      init: function(gl, cont) {
        if (this.vertexShaderURL && this.fragmentShaderURL) {
          glu.loadShaderSources([this.vertexShaderURL, this.fragmentShaderURL],
              function (shaders) {
                var vertexShader = glu.createShaderWithSources(gl,
                    shaders[this.vertexShaderURL],
                    gl.VERTEX_SHADER, this.vertexShaderURL);
                var fragmentShader = glu.createShaderWithSources(gl,
                    shaders[this.fragmentShaderURL],
                    gl.FRAGMENT_SHADER, this.fragmentShaderURL);
                this.program = glu.createProgramWithShaders(gl,
                    vertexShader, fragmentShader, Renderer.attributes);
                this.program.name = "Renderer: " + this.fragmentShaderURL;
                if (utils.debug) glu.validateProgram(gl, this.program);

                cont();
              }.bind(this), utils.versionNumber);
        }
      }
    };

    return Renderer;
  });
