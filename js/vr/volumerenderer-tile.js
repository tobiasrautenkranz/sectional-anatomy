/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils", "vr/volumerenderer", "vr/cube",
    "vr/draw-tile", "vr/track-frame-duration"],
  function (utils, glu, VolumeRenderer, cube, makeDrawTile, track)  {
    "use strict";

    // Volume Rendering with adaptive resolution to hit a FPS target
    // and a high quality mode (increased resolution).
    //
    // To prevent a lookup in the main thread, the high quality render is
    // drawn in parts using several tiles.
    var VR = function () {
      VolumeRenderer.call(this);

      /* subsample for current draw */
      this.drawSubSample = 1;
      /* subsample for interactive drawing */
      this.interactiveSubSample = 1;
      //this.fpsTarget= {min: 20, max: 35};
      this.track = track();

      /* tile to draw */
      this.tile = 0;
      /* number of tiles in x and y direction. */
      this.tilesLength = 1;
      this.tileTextureID = 7;
      this.hqdrawCount = null;

      // FIMXE MAKE THIS
      //this.interactiveTilesLength =16;
    };

    VR.subSampleMax = 32;

    VR.prototype = Object.create(VolumeRenderer.prototype);

    VR.prototype.init = function (gl, cont) {
      VolumeRenderer.prototype.init.call(this, gl,
          function () {
            this.isInitialized = false; // set again
            this.drawTile = makeDrawTile(gl, this.tileTextureID);
            this.drawTile.init(gl, function () {
              this.isInitialized = true;
              this.track.init(gl);
              cont();
            }.bind(this));
          }.bind(this));
    };

    VR.prototype.makeFramebuffers = function () {
      VolumeRenderer.prototype.makeFramebuffers.call(this);
      var gl = this.gl;

      var width = this.viewportWidth();
      var height = this.gl.drawingBufferHeight;

      this.framebuffers.push(glu.makeFramebuffer(gl, width, height, gl.RGBA));
      this.framebuffers.push(glu.makeFramebuffer(gl, width, height, gl.RGBA));

      var l = this.framebuffers.length;
      this.vrFramebuffer = this.framebuffers[l-2];
      this.labelFramebuffer = this.framebuffers[l-1];

      for (var i=l-2; i<l; i++) {
        this.framebuffers[i].hasAlpha = true;
        // tile texture linear scaling
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.framebuffers[i].texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      }
    };

    VR.prototype.beginHQDraw = function () {
      this.interactiveSubSample = this.drawSubSample;

      if (this.drawSubSample >= 2) {
        // tile 0: draw preview
        // start with tile 1; to allow the interactive draw to be reused for
        // not yet draw tiles;
        // lastly draw tile 0
        this.hqdrawCount = 0;
        this.setHQTilesLength(2);
      } else {
        // no tiling when interactive preview does not fit one tile
        this.setSubSample(1);
        this.hqdrawCount = 1; // no preview, because it would be the same
        this.setHQTilesLength(1);
      }
    };
    VR.prototype.isHQDraw = function () {
      return this.hqdrawCount !== null && this.hqdrawCount <= this.HQTiles;
    };
    VR.prototype.endHQDraw = function () {
      this.tilesLength = 1;
      this.tile = 0;
      this.setSubSample(this.interactiveSubSample);
      this.hqdrawCount = null;
    };

    VR.prototype.updateAndDraw = function () {
      if (!this.isInitialized) {
        return;
      }
      this.updateSize();

      this.track.update();

      var gl = this.gl;

      if (this.isHQDraw()) {
        this.track.skipNext();
        if (1 === this.tile) {
          // first preview (tile 0) then tile 1
          this.tilesLength = this.HQTilesLength;
          this.setSubSample(this.drawSubSample/this.HQTiles);
        }

        this.setViewport(); // set viewport for tile
      } else if (this.fpsTarget) {
        this.adjustSubSample();
      }

      this.track.begin();

      this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.vrFramebuffer.framebuffer);
      this.gl.clear(this.gl.COLOR_BUFFER_BIT);

      VolumeRenderer.prototype.updateAndDraw.call(this);

      // draw to canvas
      if (this.renderer &&
          (null === this.hqdrawCount || // draw non HQ
           0 === this.hqdrawCount || // draw HQ preview
           this.HQTiles === this.hqdrawCount)) // draw finished HQ
      {
        this.drawTiles(false);
        if (this.showAnatomy) {
          this.drawTiles(true);
        }

        this.setViewport(); // reset viewport (set from drawTiles)
      }

      if (this.hqdrawCount !== null) {
        // HQ Draw tile order:
        // 0: preview (like interactive)
        // then, 4 tiles: 1,2,3,0 in that order;
        // layout like:
        //
        // ---------
        // | 0 | 1 |
        // ----+----
        // | 2 | 3 |
        // ---------
        //
        this.hqdrawCount++;
        this.tile = (this.tile + 1) % this.HQTiles;
      }
      this.track.end();
    };

    VR.prototype.setViewport = function () {
      var width = this.viewportWidth() / this.drawSubSample;
      var height = this.gl.drawingBufferHeight / this.drawSubSample;
      this.gl.viewport(0, 0, width, height);

      var sWidth = Math.ceil(width / this.tilesLength);
      var sHeight = Math.ceil(height / this.tilesLength);
      var x = Math.floor((this.tile % this.tilesLength) * sWidth);
      var y = Math.floor(Math.floor(this.tile / this.tilesLength) * sHeight);

      if (2 == this.tilesLength &&
          this.renderer && this.renderer.overlapTiles) {
        // overlap tiles in drawing order to prevent border interpolation issues
        var border = 10; // guestimate
        switch (this.tile) {
          // last two tiles cover borders between all 4 tiles
          case 3:
            sWidth += border;
            x -= border; sWidth += border;
            y -= border; sHeight += border;
            break;
          case 0:
            sWidth += border;
            sHeight += border;
            break;
        }
      }
      this.gl.scissor(x, y, sWidth, sHeight);

      this.gl.enable(this.gl.SCISSOR_TEST);
    };

    function fracRound(x, delta) {
      // round scale to fractions; denser towards 1
      return x < 1.1 ? 1 : Math.max(1, delta/Math.round(delta/x));
    }

    VR.prototype.adjustSubSample = function () {
      var fps = this.track.fps();
      if (fps < this.fpsTarget.min || this.fpsTarget.max < fps) {
        var fpsTargetMean = (this.fpsTarget.min + this.fpsTarget.max) / 2.0;
        var scale = this.interactiveSubSample * Math.sqrt(fpsTargetMean/fps);

        scale = fracRound(scale, 4*VR.subSampleMax);

        this.interactiveSubSample = scale;
        this.setSubSample(scale);
      }
    };

    VR.prototype.clearBorder = function() {
      // clear bigger than scissor box to prevent artifacts at the border from
      // linear scaling
      // FIXME some artefacts left

      var gl = this.gl;
      var framebuffers = [this.vrFramebuffer, this.labelFramebuffer];
      var clearFramebuffers = function () {
        for (var i=0; i<framebuffers.length; i++) {
          gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffers[i].framebuffer);
          gl.clear(gl.COLOR_BUFFER_BIT);
        }
      };
      var width = this.viewportWidth() / this.drawSubSample-1;
      var height = this.gl.drawingBufferHeight / this.drawSubSample-1;
      var border = this.renderer.overlapTiles ? 14 : 3;

      gl.scissor(0, height, width+border, border);
      clearFramebuffers();

      gl.scissor(width, 0, border, height);
      clearFramebuffers();
    };

    VR.prototype.setSubSample = function (scale) {
      scale = utils.clamp(scale, 1, VR.subSampleMax);
      if (scale !== this.drawSubSample) {
        this.track.setSize(this.tilesLength/scale, this.tilesLength/scale);
        this.drawSubSample = scale;

        if (this.renderer) {
          this.clearBorder();

          this.setViewport();
          this.updateProgramsUniforms(this.updateProgramUniformsViewport);
        }
      }
    };


    VR.prototype.subSample = function () {
      return this.currentSubSample || this.drawSubSample;
    };

    VR.prototype.drawTiles = function (label) {
      var gl = this.gl;
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);

      var width = this.viewportWidth();
      var framebuffer = this.vrFramebuffer;
      var offsetX = 0;
      if (label) {
        offsetX = width;
        framebuffer = this.labelFramebuffer;
      }
      gl.viewport(offsetX, 0, width, gl.drawingBufferHeight);

      gl.disable(gl.CULL_FACE);
      gl.activeTexture(gl.TEXTURE0 + this.tileTextureID);
      gl.bindTexture(gl.TEXTURE_2D, framebuffer.texture);

      this.drawTile.update(gl, this.drawSubSample);
      this.currentSubSample = this.drawSubSample;

      gl.disable(gl.SCISSOR_TEST);
      this.drawTile.draw(gl);
      gl.enable(gl.SCISSOR_TEST);

      gl.bindTexture(gl.TEXTURE_2D, null);
    };

    VR.prototype.setRenderer = function (renderer) {
      VolumeRenderer.prototype.setRenderer.call(this, renderer);

      if (this.fpsTarget) {
        this.track.reset();
        this.interactiveSubSample = VR.subSampleMax/4;
        this.setSubSample(this.interactiveSubSample);
      }
    };

    VR.prototype.setHQTilesLength = function (l) {
      this.HQTilesLength = l;
      this.HQTiles = l*l;
    };

    return VR;
  });
