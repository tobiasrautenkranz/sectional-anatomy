/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils", "vr/gl-renderer", "lib/gl-matrix"],
  function (utils, glu, Renderer, glm)  {
    "use strict";

    var vec3 = glm.vec3;
    var vec4 = glm.vec4;
    var mat4 = glm.mat4;

    // vertices of a cube with the origin (0,0,0) as center and a side length
    // of 1.
    // Use counter-clockwise orientation for front faces (use by gl.cullFace)
    function cubeVertices () {
      return [
        // front
        -0.5, -0.5,  0.5,
         0.5, -0.5,  0.5,
         0.5,  0.5,  0.5,
        -0.5,  0.5,  0.5,
        // back
        -0.5, -0.5, -0.5,
         0.5, -0.5, -0.5,
         0.5,  0.5, -0.5,
        -0.5,  0.5, -0.5
      ];
    }

    // draw a cube where the volume is to be rendered.
    // the color at a specific point on the cube surface represents the volume
    // coordinates: rgb -> xyz
    function makeCube(gl) {
      var vertices = cubeVertices();
      var vertexBuffer = glu.createBuffer(gl, gl.ARRAY_BUFFER,
          new Float32Array(vertices), gl.STATIC_DRAW);
      vertexBuffer.itemSize = 3;
      vertexBuffer.numItems = 8;
      if (utils.debug)
        utils.assert(vertices.length == vertexBuffer.itemSize * vertexBuffer.numItems,
            "vertexBuffer size");

      // Colors are the coordinates (rgb = xyz [0:1]) of the volume
      var colors = vertices.map(function (c) {return c < 0 ? 0.0 : 1.0;});
      var colorBuffer = glu.createBuffer(gl, gl.ARRAY_BUFFER,
          new Float32Array(colors), gl.STATIC_DRAW);
      colorBuffer.itemSize = 3;
      colorBuffer.numItems = 8;
      if (utils.debug)
        utils.assert(colors.length == colorBuffer.itemSize * colorBuffer.numItems,
            "colorBuffer size");

      var indices = [
        // front
        0, 1, 2,
        2, 3, 0,
        // top
        1, 5, 6,
        6, 2, 1,
        // back
        7, 6, 5,
        5, 4, 7,
        // bottom
        4, 0, 3,
        3, 7, 4,
        // left
        4, 5, 1,
        1, 0, 4,
        // right
        3, 2, 6,
        6, 7, 3
      ];

      var indexBuffer = glu.createBuffer(gl, gl.ELEMENT_ARRAY_BUFFER,
          new Uint16Array(indices), gl.STATIC_DRAW);
      indexBuffer.itemSize = 1;
      indexBuffer.numItems = 3*2*6;
      if (utils.debug)
        utils.assert(indices.length == indexBuffer.itemSize * indexBuffer.numItems,
            "indexBuffer size");

      return {
        vertexShaderURL: "/shaders/cube.vert",
        fragmentShaderURL:"/shaders/cube.frag",
        positions: vertexBuffer,
        colors: colorBuffer,
        indices: indexBuffer,
        vertices: vertices,//FIXME hack for cubecolors
        /** Bounding box for the Volume */
        range: [[0.0, 1.0], [0.0, 1.0], [0.0, 1.0]],
        scale: vec3.fromValues(1, 1, 1),

        /** Transforms coords [0; 1] to fit the bounding box of the volume */
        normalizedCoordsToRange: function (coords, dest) {
          for (var i=0; i<3; i++) {
            var r = this.range[i];
            dest[i] = r[0] + coords[i] * (r[1] - r[0]);
          }
          return dest;
        },
        init: function(gl, cont) {
          glu.loadShaderSources([this.vertexShaderURL, this.fragmentShaderURL],
              function (shaders) {
                this.vertexShaderSource = shaders[this.vertexShaderURL];

                this.program = gl.createProgram();
                this.attachVertexShader(gl, this.program);

                var fragmentShader = glu.createShaderWithSources(gl,
                    shaders[this.fragmentShaderURL],
                    gl.FRAGMENT_SHADER, this.fragmentShaderURL);
                gl.attachShader(this.program, fragmentShader);

                glu.linkProgram(gl, this.program);

                this.program.name = "Renderer: " + this.fragmentShaderURL;
                if (utils.debug) glu.validateProgram(gl, this.program);

                this.setProgramUniforms(this.program);

                glu.checkGLError(gl, "cube.init");

                cont();
              }.bind(this), utils.versionNumber);
        },

        update: function (gl, pMatrix, mvMatrix) {
          this.updateProgram(gl, this.program, pMatrix, mvMatrix);

          gl.bindBuffer(gl.ARRAY_BUFFER, this.positions);
          gl.vertexAttribPointer(Renderer.vertexPositionAttribute,
              this.positions.itemSize, gl.FLOAT, false, 0, 0);
          gl.bindBuffer(gl.ARRAY_BUFFER, this.colors);
          gl.vertexAttribPointer(Renderer.vertexColorAttribute,
              this.colors.itemSize, gl.FLOAT, false, 0, 0);
          gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indices);
        },
        updateRange: function(gl) {
          var range = this.range;
          var colors = new Float32Array(3*24);
          for (var i=0; i<colors.length; i+=3) {
            for (var j=0; j<3; j++) {
              colors[i+j] = vertices[i+j] < 0 ? range[j][0] : range[j][1];
            }
          }
          gl.bindBuffer(gl.ARRAY_BUFFER, this.colors);
          gl.bufferData(gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);

          for (i=0; i<3; i++) {
            this.scale[i] = this.range[i][1] - this.range[i][0];
          }
        },
        rangeScale: function () {
          return this.scale;
        },
        updateProgram: function (gl, program, pMatrix, mvMatrix) {
          gl.uniformMatrix4fv(program.pMatrixUniform, false, pMatrix);
          gl.uniformMatrix4fv(program.mvMatrixUniform, false, mvMatrix);
        },

        draw: function drawCube(gl) {
          gl.drawElements(gl.TRIANGLES, this.indices.numItems,
              gl.UNSIGNED_SHORT, 0);
        },

        attachVertexShader: function (gl, program) {
          if (!this.vertexShader) {
            this.vertexShader = glu.createShaderWithSources(gl,
                this.vertexShaderSource, gl.VERTEX_SHADER, "cube vertex shader");
          }
          gl.attachShader(program, this.vertexShader);
          for (var i=0; i<Renderer.attributes.length; i++) {
            gl.bindAttribLocation(program, i, Renderer.attributes[i]);
          }
        },

        setProgramUniforms: function (program) {
          program.pMatrixUniform = gl.getUniformLocation(program, "uPMatrix");
          program.mvMatrixUniform = gl.getUniformLocation(program, "uMVMatrix");
        }
      };
    }

    // Draw the volume coordinates as color like for the cube, but now for the
    // near clipping plane. This is needed to prevent a hole when the cube is clipped:
    function makeNearClipPlane (gl, cube) {
      var ncp = {
        toModelMatrix:  mat4.create(),
        cubeOrigin: vec3.fromValues(-1, -1, -1),
        cube: cube,
        vertexShaderURL: "/shaders/quad.vert",
        fragmentShaderURL: "/shaders/quad.frag",
        colorArray: new Float32Array(4 * 3),
        init: Renderer.init
      };

      var vertexBuffer = gl.createBuffer();
      gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
      // normalized device coordinates [-1; 1] of a full screen quad on the near
      // clipping plane.
      var vertices = [
        -1, -1, -1,
         1, -1, -1,
        -1,  1, -1,
         1,  1, -1
      ];
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
      ncp.positions = vertexBuffer;

      var colorBuffer = gl.createBuffer();
      gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
      gl.bufferData(gl.ARRAY_BUFFER, 4 * 3, gl.DYNAMIC_DRAW);
      ncp.colors = colorBuffer;

      var vertexCoords = []; // same as vertices but an array of vectors
      for (var i=0; i<4; i++) {
        var j=3*i;
        vertexCoords[i] = vec3.fromValues(vertices[j], vertices[j+1], vertices[j+2]);
      }

      var vCoords = vec3.create();
      var cubeOrigin = vec3.fromValues(-0.5, -0.5, -0.5); // cube side length = 1
      var nearClipPlaneColor = function (deviceCoords, toModelMatrix) {
        // to cube coordinates;
        vec3.transformMat4(vCoords, deviceCoords, toModelMatrix);

        // from cube to volume coordinates
        vec3.subtract(vCoords, vCoords, cubeOrigin); // [0, 1] (if valid)
        return ncp.cube.normalizedCoordsToRange(vCoords, vCoords);
      };

      ncp.update = function (gl, pMatrix, mvMatrix) {
        mat4.multiply(this.toModelMatrix, pMatrix, mvMatrix);
        mat4.invert(this.toModelMatrix, this.toModelMatrix);

        for (var i=0; i<vertexCoords.length; i++) {
          var color = nearClipPlaneColor(vertexCoords[i], this.toModelMatrix);
          this.colorArray.set(color, 3*i);
        }

        gl.useProgram(this.program);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.positions);
        gl.vertexAttribPointer(Renderer.vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.colors);
        gl.bufferData(gl.ARRAY_BUFFER, this.colorArray, gl.DYNAMIC_DRAW);
        gl.vertexAttribPointer(Renderer.vertexColorAttribute, 3, gl.FLOAT, false, 0, 0);
      };

      ncp.draw = function (gl) {
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
      };

      return ncp;
    }

    return {
      makeCube: makeCube,
      makeNearClipPlane: makeNearClipPlane,

      vertexPositionAttribute: Renderer.vertexPositionAttribute,
      vertexColorAttribute: Renderer.vertexColorAttribute
    };
  });
