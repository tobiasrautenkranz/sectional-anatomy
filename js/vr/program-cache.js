/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(function () {
  "use strict";

  function key(renderer) {
    return renderer.directory;
  }

  // cache shader programs to prevent expensive recompilation
  var C = function (update) {
    this.cache = {};
    this.update = update;

    this.quality = undefined;
  };

  C.prototype.get = function (renderer, cont) {
    if (this.hasProgram(renderer)) {
      cont(this.cache[key(renderer)]);
    } else {
      this.make(renderer, cont);
    }
  };

  C.prototype.setQuality = function (quality) {
    if (this.quality !== quality) {
      this.quality = quality;
      this.clear();
    }
  };

  C.prototype.clear = function () {
    this.cache = {};
  };

  C.prototype.make = function (renderer, cont) {
    this.update(renderer, function (program) {
      this.cache[key(renderer)] = program;
      cont(program);
    }.bind(this));
  };

  C.prototype.hasProgram = function (renderer) {
    return this.cache[key(renderer)];
  };

  return C;
});
