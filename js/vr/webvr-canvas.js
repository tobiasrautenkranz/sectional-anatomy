/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils", "volume/volume", "vr/volumerenderer-webvr",
    "vr/tex-volume", "vr/program-cache",
    "vr/coords",
    "vr/webvr/reticle", "vr/webvr/overlay",
    "lib/gl-matrix", "util/console",
"vr/webvr/floor", "vr/webvr/object"],
  function (utils, glu, vol, VolumeRenderer, texVol, ProgramCache, coords,
    Reticle, Overlay,
    glm, console,
    floor, object)  {
    "use strict";

    var vec2 = glm.vec2;
    var vec3 = glm.vec3;
    var vec4 = glm.vec4;
    var mat4 = glm.mat4;
    var quat = glm.quat;

    function bestDisplay(displays) {
      if (displays.length === 0) {
        return null;
      }

      for (var i=0; i<displays.length; i++) {
        if (displays[i].capabilities.canPresent) {
          return displays[i];
        }
      }

      return displays[0];
    }

    // Volume Rendering in a canvas element using webgl
    var VRCanvas = function VRCanvas(canvasElement, options) {
      options = options || {};
      this.canvas = canvasElement;

      this.canvas.addEventListener("webglcontextcreationerror", function (e) {
        throw new Error("WebGL context creation error: " +
            (e.statusMessage || "Unknown"));
      });

      this.canvas.addEventListener("webglcontextlost", function (e) {
        e.preventDefault(); // we can handle context lost
        if (utils.debug) {
          console.group("context lost");
        }

        this.invalidateWebGLObjects();
      }.bind(this), false);
      this.canvas.addEventListener("webglcontextrestored", function (e) {
        if (utils.debug) console.log("context regained");

        this.init(function () {
          this.draw();
          if (utils.debug) {
            console.log("restore done");
            console.groupEnd();
          }
        }.bind(this), true);
      }.bind(this), false);

      window.addEventListener("vrdisplayactivate", function (e) {
        // FIXME untested
        console.log("vrdisplayactivate", e.reason);
        if (e.display && e.display !== this.vrDisplay) {
          console.error("multiple VRDisplays");
        }
        if (this.vrDisplay.capabilities.canPresent) {
          this.enterVR();
        }
      }.bind(this));
      window.addEventListener("vrdisplaydeactivate",function (e) {
        // FIXME untested
        console.log("vrdisplaydeactivate", e.reason);
        if (e.display && e.display !== this.vrDisplay) {
          console.error("multiple VRDisplays");
        }
        if (this.vrDisplay.capabilities.canPresent) {
          this.exitVR();
        }
      }.bind(this));

      window.addEventListener("vrdisplaypresentchange", function (e) {
        if (e.display && e.display !== this.vrDisplay) {
          console.error("multiple VRDisplays");
        }
        var presenting = this.vrDisplay.isPresenting;

        this.buttons.enterVR.style.display = presenting ? "none" : "";
        this.buttons.exitVR.style.display = presenting ? "" : "none";
      }.bind(this));

      this.programCache = new ProgramCache(function (renderer, cont) {
        this.vr.makeRenderer(renderer,
            {quality: this.quality,
              depth: this.volume.getLabelMap().getDepth()
            },
            cont);
      }.bind(this));
      this.invalidateWebGLObjects();

      this.vr = new VolumeRenderer(this.canvas);

      if (options.constantFPS) {
        this.vr.fpsTarget = {min: 20, max: 40};
      }

      this.quality = 1;
    };

    VRCanvas.prototype.invalidateWebGLObjects = function () {
      this.glObjects = {};
      this.programCache.clear();
    };

    var cubeDistance = -2;

    VRCanvas.prototype.init = function (cont, restoreContext) {
      var gl = glu.getContext(this.canvas, {
        antialias: true,
        depth: true,
        alpha: false
      });
      if (!gl) {
        throw new Error("Error creating webgl context");
      }

      this.gl = gl;


      this.MMatrix = mat4.create();

      var init1 = function () {
        var reticle = new Reticle({
          radius: 0.03,
          width: 0.01,
          distance: -1.5});
        reticle.init(gl);
        reticle.disabled = true;
        this.reticle = reticle;

        var label = new Overlay({
          top: -1.1, bottom: -1.4,
          left: -2.1, right: 2.1,
          distance: -3
        });
        label.init(gl);
        var canvas = document.createElement("canvas");
        var ctx = canvas.getContext("2d");
        label.setTexture(canvas);
        this.label = label;
        this.labelContext = ctx;

        this.vr.overlay = [
          function (VMatrix) {
            reticle.draw(this.gl, this.vr.pMatrix, VMatrix);
          }.bind(this),
          function (VMatrix) {
            label.draw(this.gl, this.vr.pMatrix, VMatrix);
          }.bind(this),
        ];

        if (restoreContext) {
          if (this.volume) {
            this.setVolume(this.volume);
          }
          if (this.lut) {
            this.setLUT(this.lut);
          }
          if (this.renderer) {
            this.setRenderer(this.renderer, init2);
          } else {
            init2();
          }
        } else {
          init2();
        }
      }.bind(this);

      this.buttons = {};

      var buttons = document.createElement("div");
      buttons.style.position = "absolute";
      buttons.style.width = "100%";
      buttons.style.padding = 0;
      buttons.style.magin = 0;
      buttons.style.left = 0;
      buttons.style.bottom = "3%";
      buttons.style.zIndex = "100";
      buttons.style.display = "flex";
      buttons.style.justifyContent = "center";
      buttons.style.alignItems = "center";
      if (this.canvas.parentNode) {
        this.canvas.parentNode.insertBefore(buttons, this.canvas);
      }


      var enterVR = document.createElement("span");
      enterVR.className = "vr-icon";
      enterVR.style.fontSize = "1.5em";
      enterVR.style.display = "none";

      enterVR.addEventListener("click", function () {
        this.enterVR();
      }.bind(this));

      buttons.appendChild(enterVR);
      this.buttons.enterVR = enterVR;

      var exitVR = document.createElement("button");
      exitVR.appendChild(document.createTextNode("exitVR"));
      exitVR.style.left = "50%";
      exitVR.style.display = "none";
      exitVR.style.fontSize = "1.5em";
      buttons.appendChild(exitVR);

      exitVR.addEventListener("click", function () {
        this.exitVR();
      }.bind(this));
      this.buttons.exitVR = exitVR;

      var setVRDisplay = function (displays) {
        if (0 === displays.length ) {
          console.log("no VR displays available");
          return;
        }

        enterVR.style.display = "";
        exitVR.style.display = "none";

        this.vrDisplay = bestDisplay(displays);
        console.log("VR Display \"" + this.vrDisplay.displayName + "\" found.");

        this.vr.vrDisplay = this.vrDisplay;
      }.bind(this);

      var initVRDisplay = function () {
        if (navigator.getVRDisplays) {
          navigator.getVRDisplays().then(function (displays) {
            setVRDisplay(displays);

            window.addEventListener("vrdisplayconnect", function (e) {
              console.log("vrdisplayconnet", e.display);
              if (!this.vrDisplay) {
                initVRDisplay();
              }
            }.bind(this));

            window.addEventListener("vrdisplaydisconnect", function (e) {
              console.log("vrdisplaydisconnet", e.display);
              if (e.display === this.vrDisplay) {
                this.exitVR();
                initVRDisplay();
              }
            }.bind(this));

            cont();
          },
          function () { console.error("getVRDisplays"); });
        } else {
          console.log("no WebVR 1.0 support");
          cont();
        }
      };

      var init2 = function () {
        initVRDisplay();

        glu.checkGLError(gl, "VRCanvas.init");

      }.bind(this);

      this.vr.init(gl, init1);

    };
    VRCanvas.prototype.enterVR = function () {
      var enterVR = this.buttons.enterVR;
      var exitVR = this.buttons.exitVR;


      if (this.vrDisplay && !this.vrDisplay.isPresenting) {
        if (this.vrDisplay.capabilities.canPresent) {
          // using defaults: leftBounds  rightBounds
          var layer = { source: this.canvas };
          utils.assert(this.vrDisplay.capabilities.maxLayers >= 1, "vrDisplay maxLayers >= 1");

          return this.vrDisplay.requestPresent([layer]).then(
              function () {
                enterVR.style.display = "none";
                exitVR.style.display = "";
              }.bind(this));
        }
      }

      return Promise.reject();
    };

    VRCanvas.prototype.exitVR = function () {
      var enterVR = this.buttons.enterVR;
      var exitVR = this.buttons.exitVR;

      if (this.vrDisplay && this.vrDisplay.isPresenting) {
        return this.vrDisplay.exitPresent().then(function () {
          enterVR.style.display = "";
          exitVR.style.display = "none";
        });
      }

      return Promise.resolve();
    };
    VRCanvas.prototype.resetPose = function () {
      if (this.vrDisplay) {
        this.vrDisplay.resetPose();
      }
    };


    VRCanvas.prototype.draw = function draw() {
      if (this.gl.isContextLost()) {
        return;
      }

      this.updateLabelMapLUT(); // FIXME only when needed

      if (this.vr.vrDisplay &&
          this.vr.vrDisplay.getFrameData(this.vr.frameData)) {
        var rot = quat.create();
        quat.invert(rot, this.vr.frameData.pose.orientation);

        mat4.fromRotationTranslation(this.MMatrix,
            rot,
          vec3.fromValues(0,0, cubeDistance));
      } else {
        mat4.fromTranslation(this.MMatrix, vec3.fromValues(0,0, cubeDistance));
      }

      mat4.multiply(this.MMatrix, this.MMatrix, this.vr.MVMatrix);

      var tmp = this.vr.MVMatrix;
      this.vr.MVMatrix = this.MMatrix;
      this.vr.updateAndDraw();

      this.vr.MVMatrix = tmp;
    };

    VRCanvas.prototype.requestAnimationFrame = function (callback) {
      if (this.vrDisplay && this.vrDisplay.requestAnimationFrame) {
        return this.vrDisplay.requestAnimationFrame(callback);
      }

      return window.requestAnimationFrame(callback);
    };

    VRCanvas.prototype.setVolume = function (volume) {
      this.volume = volume;
      var isotropicDownsampling = true;
      var voxelSize = volume.getVoxelSize();
      if (Math.abs(voxelSize[2] / voxelSize[1]) >= 4 &&
          volume.getSize()[2] < 100) {
        // prevent killing to much data on low z resolution due to isotropic
        // downscaling (e.g. ct-wrist)
        isotropicDownsampling = false;
        if (utils.debug) {
          console.warn("high slice thickness for data; using anisotropic voxels");
        }
      }

      var textureVolume = texVol.makeTextureVolume3D(this.gl, this.volume,
          isotropicDownsampling);

      var labelMap = this.volume.getLabelMap();
      var labelMapVolume = texVol.makeTextureVolume3D(this.gl, labelMap,
          isotropicDownsampling);
      labelMapVolume.depth = labelMap.getDepth();

      this.vr.setVolumeTexture(textureVolume, labelMapVolume);

      this.glObjects.labelMapLUT =
        texVol.makeLabelMapLUTTexture(this.gl, labelMap.getLUT());
      this.vr.setLabelMapLUT(this.glObjects.labelMapLUT);
    };

    VRCanvas.prototype.updateLabelMapLUT = function () {
      if (!this.glObjects.labelMapLUT) {
        return;
      }
      texVol.updateLabelMapLUTTexture(this.gl,
          this.volume.getLabelMap().getLUT(), this.glObjects.labelMapLUT);
    };

    VRCanvas.prototype.setLUT = function (lut) {
      this.lut = lut;

      var gl = this.gl;

      this.glObjects.lutTexture = gl.createTexture();
      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(gl.TEXTURE_2D, this.glObjects.lutTexture);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
      gl.bindTexture(gl.TEXTURE_2D, null);
      this.vr.setLUTTexture(this.glObjects.lutTexture);

      if (typeof this.lut !== "function") {
        this.lut = function () {return new Uint8Array(lut.getArray());};
      }

      this.updateLUT();
    };

    VRCanvas.prototype.updateLUT = function () {
      if (!this.lut) {
        return;
      }

      var gl = this.gl;

      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(gl.TEXTURE_2D, this.glObjects.lutTexture);

      var textureArray = this.lut();

      var channels = Math.floor(textureArray.length / 256);
      if (textureArray.length % 256 !== 0) {
        throw new Error("unsupported lut array size: " + textureArray.length);
      }
      var textureFormat;
      switch (channels) {
        case 1:
          textureFormat = gl.LUMINANCE;
          break;
        case 2:
          textureFormat = gl.LUMINANCE_ALPHA;
          break;
        case 4:
          textureFormat = gl.RGBA;
          break;
      }
      gl.texImage2D(gl.TEXTURE_2D, 0, textureFormat, 256, 1,
          0, textureFormat, gl.UNSIGNED_BYTE, textureArray);
      gl.bindTexture(gl.TEXTURE_2D, null);
    };

    VRCanvas.prototype.setQuality = function (quality, cont) {
      this.quality = quality;
      this.programCache.setQuality(quality);
      this.setRenderer(this.renderer, cont);
    };

    VRCanvas.prototype.setRenderer = function (renderer, cont) {
      this.renderer = renderer;
      this.programCache.get(renderer,
          function (rendererProgram) {
            this.vr.setRenderer(rendererProgram);
            this.vr.updateRenderer(renderer.config);
            if (cont) cont();
          }.bind(this));
    };
    VRCanvas.prototype.mvMatrix = function () {
      return this.vr.MVMatrix;
    };

    VRCanvas.prototype.volumeDirection = function (start, viewport) {
      return coords.volumeDirection(start[0], start[1], {
            mvMatrix: this.MMatrix,
            pMatrix: this.vr.pMatrix,
            viewport: viewport,
            boundingBox: this.vr.cube.range
          });
    };

    var pixel = new Uint8Array(4);
    VRCanvas.prototype.pickAtCenter = function() {
      var gl = this.gl;
      var rendererDescription = this.renderer;

      // general picking (i.e.: ray casting the selected pixel)
      if (rendererDescription.pickFunctionJs) {
        var centerNDC = vec3.fromValues(0.0, 0.0, -100); // Eye coordinates
        // to normalized device coordinates
        vec3.transformMat4(centerNDC, centerNDC, this.vr.pMatrix);

        var viewport = vec4.fromValues(0, 0, this.vr.viewportWidth(), this.vr.viewportHeight());

        // to window coordinates
        // (ndc + 1) * (width/2) + viewport_offset(=0)
        var center = vec2.fromValues(
          (centerNDC[0] + 1) * viewport[2]/2,
          (centerNDC[1] + 1) * viewport[3]/2);

        gl.bindFramebuffer(gl.FRAMEBUFFER, this.vr.frontCoordsFramebuffer.framebuffer);
        gl.readPixels(center[0], center[1], 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, pixel);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);

        var direction = this.volumeDirection(center, viewport);
        var start = vec3.fromValues(pixel[0]/255, pixel[1]/255, pixel[2]/255);
        if (! // FIXME why?!?
            vol.isRightHandCoordinateSystem(this.volume)) {
          // flipY
          direction[1] = -direction[1];
          start[1] = 1 - start[1];
        }

        var id = rendererDescription.pickFunctionJs(start, direction, this.volume,
            this.lut());

        var aspect = this.canvas.width / this.canvas.height;

        var canvas = this.labelContext.canvas;
        var s = 0.8;
        canvas.height = s * viewport[2] * (this.label.options.top - this.label.options.bottom);
        canvas.width  = s * viewport[3]  * (this.label.options.right - this.label.options.left);

        var maxTextureSize = gl.getParameter(gl.MAX_TEXTURE_SIZE);
        var scale = Math.min(1.0,
            maxTextureSize / canvas.height, maxTextureSize / canvas.width);

        canvas.height *= scale;
        canvas.width *= scale;

        this.labelContext.clearRect(0, 0, canvas.width, canvas.height);
        var text = "";
        if (id > 0) {
          text = this.volume.getLabelMap().labels[id];
        }

        var ctx = this.labelContext;
        ctx.fillStyle = 'white';
        ctx.font = (canvas.height * 0.7) + "px serif";
        ctx.lineWidth = canvas.height * 0.1;
        ctx.strokeStyle = "black";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";

        ctx.strokeText(text, canvas.width/2, canvas.height/2);
        ctx.fillText(text, canvas.width/2, canvas.height/2);
        this.label.setTexture(canvas);
      }
    };


    return VRCanvas;
  });
