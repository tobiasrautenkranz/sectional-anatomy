/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2015 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "util/gl-utils", "util/cont", "vr/cube", "lib/gl-matrix"],
  function (utils, glu, CollectiveContinuation, cube, glm)  {
    "use strict";

    var vec3 = glm.vec3;
    var mat3 = glm.mat3;
    var mat4 = glm.mat4;

    var FoVYAngle = 60 * Math.PI / 180;

    function initRendererConfig(gl, renderer, programs) {
      programs.uniforms = {};

      for (var p in renderer.config) {
        var uniform = renderer.config[p].uniform ||
          "u" + p.charAt(0).toUpperCase() + p.slice(1);
        var loc = gl.getUniformLocation(programs.common, uniform);

        if (renderer.config[p].type === "normal") {
          utils.assert(!programs.normal, "only one normal supported");
          var pass = 0;
          if (!loc && programs.vr) {
            loc = gl.getUniformLocation(programs.vr, uniform);
            pass = 1;
          }
          if (loc) { // only set when used (e.g. disabled for low quality)
            programs.normalProgram = pass;
            var n = vec3.clone(renderer.config[p].value);
            vec3.normalize(n, n);
            programs.normal = {
              value: n,
              uniform: loc
            };
            programs.uniforms[p] = programs.normal;
          }
        } else {
          programs.uniforms[p] = loc;
        }
        if (utils.debug && !loc) console.warn("location", uniform,
            "not found (maybe disabled by quality level)");
      }
    }

    // Direct Volume Rendering using ray casting
    var VR = function ()  {
      this.MVMatrix = mat4.create();
      this.pMatrix = mat4.create();
      this.VMMatrix = mat3.create();
      this.modelViewScale = vec3.fromValues(1, 1, 1);

      this.normalLikeVector = vec3.create();

      this.textureVolume = {texture: null};
      this.labelMapVolume = {texture: null};
      this.isInitialized = false;
      this.showAnatomy = true;

      this.vrFramebuffer = {framebuffer: null};
      this.labelFramebuffer = {framebuffer: null};

    };

    VR.prototype.init = function (gl, cont) {
      // cleanup old state depending on gl for context restore
      this.renderer = null;
      this.vertexShader = null;
      this.lutTexture = null;
      this.isInitialized = false;
      this.needsResize = true;

      this.gl = gl;
      this.cube = cube.makeCube(gl);
      this.nearClipPlane = cube.makeNearClipPlane(gl, this.cube);

      this.drawBuffersExt = gl.getExtension("WEBGL_draw_buffers");

      this.makeFramebuffers();

      gl.enableVertexAttribArray(cube.vertexPositionAttribute);
      gl.enableVertexAttribArray(cube.vertexColorAttribute);

      var ccont = new CollectiveContinuation(function () {
        //gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
        gl.blendEquation(gl.FUNC_ADD);

        glu.checkGLError(gl, "initGL");
        this.isInitialized = true;
        if (cont) cont();
      }.bind(this));
      var queued = ccont.makeContinue();
      this.cube.init(gl, ccont.makeContinue());
      this.nearClipPlane.init(gl, ccont.makeContinue());
      queued();
    };

    VR.prototype.makeFramebuffers = function () {
      var gl = this.gl;
      var width = this.viewportWidth();
      var height = gl.drawingBufferHeight;

      this.frontCoordsFramebuffer = glu.makeFramebuffer(gl, width, height,
          gl.RGBA);
      this.frontCoordsFramebuffer.hasAlpha = true;

      if (this.drawBuffersExt) {
        this.commonPassFramebuffer = glu.makeFramebuffer(gl, width, height,
            gl.RGBA, 2, this.drawBuffersExt);
      } else {
        this.commonPassFramebuffer = glu.makeFramebuffer(gl, width, height,
            gl.RGBA);
      }
      this.commonPassFramebuffer.hasAlpha = true;

      this.framebuffers = [
        this.frontCoordsFramebuffer,
        this.commonPassFramebuffer
      ];


      this.bindFramebufferTextures();
    };
    VR.prototype.resizeFramebuffers = function () {
      var gl = this.gl;

      var width = this.viewportWidth() || 1;
      var height = gl.drawingBufferHeight;

      gl.activeTexture(gl.TEXTURE0);
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      for (var i=0; i<this.framebuffers.length; i++) {
        //gl.bindFramebuffer(gl.FRAMEBUFFER, this.framebuffers[i].framebuffer);

        var framebuffer = this.framebuffers[i];

        var textures = framebuffer.texture;
        var format = framebuffer.hasAlpha ? gl.RGBA : gl.RGB;

        if (!Array.isArray(textures)) {
          textures = [textures];
        }

        for (var j=0; j<textures.length; j++) {
          var texture = textures[j];

          gl.activeTexture(gl.TEXTURE0);
          gl.bindTexture(gl.TEXTURE_2D, texture);
          gl.texImage2D(gl.TEXTURE_2D, 0, format, width, height,
              0, format, gl.UNSIGNED_BYTE, null);
        }

      }

      gl.bindTexture(gl.TEXTURE_2D, null);
    };

    VR.prototype.viewportWidth = function () {
      var width = this.gl.drawingBufferWidth;

      if (this.showAnatomy) {
        return Math.ceil(width/2); // >= 1
      }

      return width;
    };


    VR.prototype.setViewport = function () {
      this.gl.viewport(0, 0, this.viewportWidth(), this.gl.drawingBufferHeight);
    };

    VR.prototype.updateMVMatrix = function () {
      var scale = vec3.multiply(vec3.create(),
          this.modelViewScale, this.cube.rangeScale());
      mat4.scale(this.MVMatrix, this.MVMatrix, scale);
    };

    VR.prototype.updateAndDrawFrontCoords = function () {
      var gl = this.gl;

      gl.bindFramebuffer(gl.FRAMEBUFFER, this.frontCoordsFramebuffer.framebuffer);

      if (utils.debug) glu.checkFramebufferStatus(gl);

      // no gl.clear needed, since we draw a fullscreen quad.
      this.nearClipPlane.update(gl, this.pMatrix, this.MVMatrix);
      this.nearClipPlane.draw(gl);

      gl.useProgram(this.cube.program);
      this.cube.update(gl, this.pMatrix, this.MVMatrix);

      gl.enable(gl.CULL_FACE);

      // Front face cube
      gl.cullFace(gl.BACK);
      this.cube.draw(gl);
      if (utils.debug) glu.checkGLError(gl, "drawFront");
    };

    VR.prototype.updateAndDrawCommonPass = function () {
      var gl = this.gl;

      gl.cullFace(gl.FRONT);
      var renderer = this.renderer;

      if (renderer.normal) {
        mat3.fromMat4(this.VMMatrix, this.MVMatrix);
        mat3.invert(this.VMMatrix, this.VMMatrix);
        vec3.transformMat3(this.normalLikeVector,
            renderer.normal.value, this.VMMatrix);
        vec3.normalize(this.normalLikeVector, this.normalLikeVector);
      }

      // common pass
      if (this.renderer.vr) {
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.commonPassFramebuffer.framebuffer);

        if (this.drawBuffersExt) {
          this.drawBuffersExt.drawBuffersWEBGL([
              this.drawBuffersExt.COLOR_ATTACHMENT0_WEBGL,
              this.drawBuffersExt.COLOR_ATTACHMENT1_WEBGL
          ]);
        }
        gl.clear(gl.COLOR_BUFFER_BIT);
      } else {
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      }

      gl.useProgram(renderer.common);
      if (0 === renderer.normalProgram) {
        gl.uniform3fv(renderer.normal.uniform, this.normalLikeVector);
      }

      this.cube.updateProgram(gl, renderer.common, this.pMatrix, this.MVMatrix);
      this.cube.draw(gl);

      if (this.drawBuffersExt && this.renderer.vr) {
        var ext = this.drawBuffersExt;
        ext.drawBuffersWEBGL([ext.COLOR_ATTACHMENT0_WEBGL]);
      }
    };

    VR.prototype.updateAndDrawVRPass = function () {
      var gl = this.gl;

      // Framebuffer is null when drawing directly to the canvas
      gl.bindFramebuffer(gl.FRAMEBUFFER, this.vrFramebuffer.framebuffer);


      if (this.drawBuffersExt) {
        gl.activeTexture(gl.TEXTURE0 + VR.textureIDs.previousStage);
        gl.bindTexture(gl.TEXTURE_2D, this.commonPassFramebuffer.texture[0]);
      }

      gl.useProgram(this.renderer.vr);
      //gl.clear(gl.COLOR_BUFFER_BIT);
      this.cube.updateProgram(gl, this.renderer.vr, this.pMatrix, this.MVMatrix);
      if (1 == this.renderer.normalProgram) {
        gl.uniform3fv(this.renderer.normal.uniform, this.normalLikeVector);
      }
      this.cube.draw(gl);
    };

    VR.prototype.updateAndDrawLabelPass = function () {
      var gl = this.gl;
      // Framebuffer is null when drawing directly to the canvas
      gl.bindFramebuffer(gl.FRAMEBUFFER, this.labelFramebuffer.framebuffer);

      if (this.drawBuffersExt) {
        gl.activeTexture(gl.TEXTURE0 + VR.textureIDs.previousStage);
        gl.bindTexture(gl.TEXTURE_2D, this.commonPassFramebuffer.texture[1]);
      }

      gl.useProgram(this.renderer.label);
      gl.clear(gl.COLOR_BUFFER_BIT);
      this.cube.updateProgram(gl, this.renderer.label,
          this.pMatrix, this.MVMatrix);
      this.cube.draw(gl);
    };

    VR.prototype.updateSize = function () {
      var canvas = this.gl.canvas;
      // not using devicePixelRatio since the resolution of the volume is low.
      // Thus there is nothing to be gained from rendering it in highDPI. Also
      // we would use more memory and that primarily on mobile devices.
      //var toPixels = window.devicePixelRatio || 1;
      var toPixels = 1;

      this.setSize(toPixels * canvas.clientWidth, toPixels * canvas.clientHeight);
    };

    VR.prototype.setSize = function (width, height) {
      var gl = this.gl;
      var canvas = gl.canvas;

      width = Math.round(width);
      height = Math.round(height);

      if (this.needsResize || canvas.width != width || canvas.height != height) {
        this.needsResize = undefined;

        canvas.width = width;
        canvas.height = height;

        var widthFactor = this.showAnatomy ? 0.5 : 1;

        mat4.perspective(this.pMatrix, FoVYAngle, width * widthFactor / height, 0.1, 100);
        this.setViewport();

        this.resizeFramebuffers();

        if (this.renderer) {
          this.updateProgramsUniforms(this.updateProgramUniformsViewport);
        }
      }
    };

    VR.prototype.updateAndDraw = function () {
      if (!this.isInitialized) {
        return;
      }
      var gl = this.gl;

      this.updateSize();

      this.updateAndDrawFrontCoords();

      if (!this.renderer) {
        // fallback for missing renderer
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.clear(gl.COLOR_BUFFER_BIT);
        this.cube.draw(gl);
        return;
      }


      this.updateAndDrawCommonPass();

      if (this.renderer.vr) {
        this.updateAndDrawVRPass();

        if (this.showAnatomy) {
          this.updateAndDrawLabelPass();
        }
      }

      gl.disable(gl.CULL_FACE);
      if (utils.debug) glu.checkGLError(gl, "vr.updateAndDraw");
    };

    VR.prototype.makeVRProgram = function(shaderURL, shaders, config) {
      var gl = this.gl;

      var program = gl.createProgram();
      this.cube.attachVertexShader(gl, program);

      var head = "#version 100\n" +
        "#define QUALITY " + config.quality + "\n" +
        "#define LABEL_DEPTH " + config.depth + "\n";

      if (this.drawBuffersExt) {
        head += "#define USE_DRAW_BUFFERS_EXT 1\n";
      }

      var fragmentShader = glu.createShaderWithSources(gl,
          [head, shaders["/shaders/utils.frag"], shaders[shaderURL]],
          gl.FRAGMENT_SHADER, shaderURL);
      gl.attachShader(program, fragmentShader);

      glu.linkProgram(gl, program);
      if (utils.debug) glu.validateProgram(gl, program);

      this.setRendererProgramUniforms(program);
      this.cube.setProgramUniforms(program);

      return program;
    };

    VR.prototype.makeRenderer = function (renderer, config, cont) {
      var shaderUtils = "/shaders/utils.frag";
      var shaderURLs = [shaderUtils];
      var shaderNames = ["stage0", "stage1", "stage1lbl"];
      if (renderer.isDirect) {
        shaderNames =  ["stage0"];
      }

      var shaderURL = function (name) {
        return renderer.directory + name + ".frag";
      };
      for (var i=0; i<shaderNames.length; i++) {
        shaderURLs.push(shaderURL(shaderNames[i]));
      }


      glu.loadShaderSources(shaderURLs, function (shaders) {
        // FIXME handle error (link failture)
        var makeVRProgram = function (name) {
          var program = this.makeVRProgram(shaderURL(name), shaders, config);
          program.name = name;
          return program;
        }.bind(this);

        var vr;
        if (renderer.isDirect) {
          vr = {
            common: makeVRProgram("stage0"),
          };
        } else {
          vr = {
            common: makeVRProgram("stage0"),
            vr: makeVRProgram("stage1"),
            label: makeVRProgram("stage1lbl"),
          };
        }

        if (renderer.overlapTiles) {
          vr.overlapTiles = true;
        }

        initRendererConfig(this.gl, renderer, vr);

        cont(vr);
      }.bind(this), utils.versionNumber);

    };

    VR.prototype.setRenderer = function (renderer) {
      this.renderer = renderer;
    };

    VR.prototype.updateRenderer = function (config) {
      var gl = this.gl;
      if (config) {
        gl.useProgram(this.renderer.common);
        for (var p in config) {
          var value = config[p].value || config[p];
          var loc = this.renderer.uniforms[p];
          if (loc) {
            if (loc.value) { // normal
              vec3.normalize(loc.value, value);
            } else {
              gl.uniform1f(loc, value);
            }
          }
        }
      }
    };

    // Used in fragment shader only
    // GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS The value must be at least 8
    // GL_MAX_TEXTURE_IMAGE_UNITS The value must be at least 8 (fragment shader)
    // ID 0 is used as temporary (e.g. to update textures)
    VR.textureIDs = {
      frontCoords: 1,
      volume: 2,
      labelMap: 3,
      labelMapLUT: 4,
      lut: 5,
      previousStage: 6,
    };

    VR.prototype.bindFramebufferTextures = function () {
      var gl = this.gl;
      gl.activeTexture(gl.TEXTURE0 + VR.textureIDs.frontCoords);
      gl.bindTexture(gl.TEXTURE_2D, this.frontCoordsFramebuffer.texture);

      if (!this.drawBuffersExt) {
        gl.activeTexture(gl.TEXTURE0 + VR.textureIDs.previousStage);
        gl.bindTexture(gl.TEXTURE_2D, this.commonPassFramebuffer.texture);
      }
    };
    VR.prototype.updateProgramUniformsViewport = function(program) {
      var gl = this.gl;
      gl.uniform2f(gl.getUniformLocation(program, "uViewportSize"),
          this.viewportWidth(), gl.drawingBufferHeight);

      gl.uniform2f(gl.getUniformLocation(program, "uViewportOrigin"),
          0, 0);

      gl.uniform1f(gl.getUniformLocation(program, "uScale"),
          this.drawSubSample || 1.0);
    };
    VR.prototype.updateProgramUniformsVolume = function (program) {
      var gl = this.gl;

      gl.uniform2fv(gl.getUniformLocation(program, "uVolumeTilingSize"),
          [this.textureVolume.rows, this.textureVolume.columns]);
      gl.uniform3fv(gl.getUniformLocation(program, "uVolumeSize"),
          this.textureVolume.size);
      gl.uniform1f(gl.getUniformLocation(program, "uLabelMapLUTlength"),
          this.labelMapLUT.length);
    };

    var t = VR.textureIDs;
    var uniformTextureIDs = {
      uFrontCoords:   t.frontCoords,
      uPreviousStage: t.previousStage,
      uVolume:        t.volume,
      uLabelMap:      t.labelMap,
      uLabelMapLUT:   t.labelMapLUT,
      uIntensityLUT:  t.lut
    };

    VR.prototype.setRendererProgramUniforms = function (program) {
      var gl = this.gl;
      gl.useProgram(program);

      for (var p in uniformTextureIDs) {
        gl.uniform1i(gl.getUniformLocation(program, p), uniformTextureIDs[p]);
      }
      this.updateProgramUniformsVolume(program);
      this.updateProgramUniformsViewport(program);

      if (utils.debug) {
        glu.validateProgram(gl, program);
        glu.checkGLError(gl, "uniforms");
      }
    };
    VR.prototype.updateProgramsUniforms = function (update) {
      var programs;
      if (this.renderer.vr) {
        programs = [this.renderer.common, this.renderer.vr, this.renderer.label];
      } else {
        programs = [this.renderer.common];
      }

      for (var i=0; i<programs.length; i++) {
        var program = programs[i];
        this.gl.useProgram(program);
        update.call(this, program);
      }
    };

    VR.prototype.setUniform = function(name, value) {
      this.gl.useProgram(this.renderer.common);
      var uniform = this.gl.getUniformLocation(this.renderer.common, name);
      this.gl.uniform1f(uniform, value);

      return !!uniform;
    };

    VR.prototype.setVolumeTexture = function (volume, labelMap) {
      var gl = this.gl;
      this.textureVolume = volume;
      this.labelMapVolume = labelMap;

      gl.activeTexture(gl.TEXTURE0 + VR.textureIDs.volume);
      gl.bindTexture(gl.TEXTURE_2D, this.textureVolume.texture);

      gl.activeTexture(gl.TEXTURE0 + VR.textureIDs.labelMap);
      gl.bindTexture(gl.TEXTURE_2D, this.labelMapVolume.texture);

      var scale = this.modelViewScale;
      vec3.copy(scale, volume.size);
      scale[0] *= 0.001 * volume.voxelSize[0]; // convert voxelSize [mm] to meters
      scale[1] *= 0.001 * volume.voxelSize[1];
      scale[2] *= 0.001 * volume.voxelSize[2];

      if (this.renderer) {
        this.updateProgramsUniforms(this.updateProgramUniformsVolume);
      }
    };

    VR.prototype.setLabelMapLUT = function (labelMapLUT) {
      var gl = this.gl;
      this.labelMapLUT = labelMapLUT;

      gl.activeTexture(gl.TEXTURE0 + VR.textureIDs.labelMapLUT);
      gl.bindTexture(gl.TEXTURE_2D, this.labelMapLUT.texture);

      if (this.renderer) {
        this.updateProgramsUniforms(this.updateProgramUniformsVolume);
      }
    };

    VR.prototype.setLUTTexture = function (lut) {
      this.lutTexture = lut;

      var gl = this.gl;
      gl.activeTexture(gl.TEXTURE0 + VR.textureIDs.lut);
      gl.bindTexture(gl.TEXTURE_2D, this.lutTexture);
    };

    VR.prototype.setAnatomyVisible = function (visible) {
      this.showAnatomy = visible;
      this.needsResize = true;
    };

    return VR;
  });
