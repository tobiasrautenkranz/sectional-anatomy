/* Sectional Anatomy Viewer WebGL
 *
 * Copyright (C) 2012 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "lib/gl-matrix"],
  function (utils, glm) {
    "use strict";

    var vec3 = glm.vec3;

    function pickId (start, direction, volume, chooseVoxel, finish, steps) {
      steps = steps || 30;

      var labelMap = volume.getLabelMap();
      var size = volume.getSize();

      var dDirection = vec3.scale(vec3.create(), direction, 1/steps);
      var position = vec3.clone(start);

      for (var i=0; i<steps; i++) {
        var indexZ = Math.round(position[2] * (size[2]-1));
        if (indexZ < 0 || indexZ >= size[2]) break;

        var indexX = Math.round(position[0] * (size[0]-1));
        if (indexX < 0 || indexX >= size[0]) break;

        var indexY = Math.round(position[1] * (size[1]-1));
        if (indexY < 0 || indexY >= size[1]) break;

        var slice = volume.slice(indexZ);
        var val = slice[indexX + size[0] * indexY];
        var id = labelMap.slice(indexZ)[indexX + size[0] * indexY];

        if (chooseVoxel(val, id)) {
          if (finish) {
            return finish(id);
          } else {
            return id;
          }
        }

        vec3.add(position, position, dDirection);
      }

      if (finish) {
        return finish(0);
      }
      return 0;
    }

    /* Choose the label that occupies the longest continuous interval on a ray.
     */
    function makeLongestLabelChooser() {
      var bestLength = 0;
      var bestId = 0;

      var curId = 0;
      var curLength = 0;

      var fkt = function longestLabelChooser (val, id) {
        if (id === curId) {
          curLength++;
        } else {
          if (curId !== 0 && curLength > bestLength) {
            bestId = curId;
            bestLength = curLength;
          }
          curId = id;
          curLength = 1;
        }
      };
      var reduceFkt = function () {
        if (bestId === 0 && curId !== 0) {
          return curId;
        } else {
          return bestId;
        }
      };
      return {
        iterate: fkt,
        choose: reduceFkt
      };
    }

    function makeMIPChooser() {
      var maxValue = 0;
      var maxId = 0;

      var fkt = function (val, id) {
        if (val > maxValue) {
          maxValue = val;
          maxId = id;
        }
      };
      var reduceFkt = function () {
          return maxId;
      };
      return {
        iterate: fkt,
        choose: reduceFkt
      };
    }

    function makeISOChooser(threshold) {
      var fkt = function (val, id) {
        return val > 255 * threshold;
      };
      return {
        iterate: fkt,
        choose: null
      };
    }


    function stepCorrectedOpacity(opacity, steps) {
      var referenceSteps = 64.0;
      return 1.0 - Math.pow(1.0 - opacity, referenceSteps/steps);
    }
    /* Choose the label with the most contribution to the final pixel along
     * a ray. */
    function makeVRchooser (lut, steps) {
      var bestOpacity = 0;
      var bestId = 0;

      var curId = 0;
      var curOpacity = 0;

      var opacityAcc = 0;

      function getOpacity(value) {
        return lut[4*value + 3] / 255.0;
      }

      var fkt = function vrRender (value, id) {
        var opacity = stepCorrectedOpacity(getOpacity(value), steps);
        var dOpacity = opacityAcc;
        opacityAcc = (1.0 - opacityAcc) * opacity;
        dOpacity = opacityAcc - dOpacity;
        if (id === curId) {
          curOpacity += dOpacity;
        } else {
          if (curId !== 0 && curOpacity > bestOpacity) {
            bestId = curId;
            bestOpacity = curOpacity;
          }
          curId = id;
          curOpacity = dOpacity;
        }
        if (opacityAcc >= 0.8) {
          return true;
        }
      };
      var selectLabel = function vrSelectLabel () {
        if (bestId === 0 && curId !== 0) {
          return curId;
        } else {
          return bestId;
        }
      };
      return {
        iterate: fkt,
        choose: selectLabel
      };
    }

    function fromHU(HU, HUMinMax) {
      var min = HUMinMax[0];
      var max = HUMinMax[1];

      var pos = (HU - min) / (max - min);
      return utils.clamp(pos, 0, 1);
    }

    function getLUTPresetHU(preset, HUMinMax) {
      //FIXME
      var stops = {};
      stops.colorStops = preset.colorStops.map(
          function (colorStop) {
            var c = colorStop.color;
            return {
              color: {r: c[0], g: c[1], b: c[2], a: 1},
              position: fromHU(colorStop.HU, HUMinMax)
            };
          });

      stops.alphaStops =  preset.alphaStops.map(
          function (alphaStop) {
            return {
              color: {r: 255, g: 255, b: 255, a: alphaStop.alpha},
              alpha: true,
              position: fromHU(alphaStop.HU, HUMinMax)
            };
          });

      return stops;
    }
    function getLUTPreset(preset) {
      //FIXME
      var stops = {};
      stops.colorStops = preset.colorStops.map(
          function (colorStop) {
            var c = colorStop.color;
            return {
              color: {r: c[0], g: c[1], b: c[2], a: 1},
              position: colorStop.value
            };
          });

      stops.alphaStops =  preset.alphaStops.map(
          function (alphaStop) {
            return {
              color: {r: 255, g: 255, b: 255, a: alphaStop.alpha},
              alpha: true,
              position: alphaStop.value
            };
          });

      return stops;
    }

    // The different renderer shader programs and their parameters
    function getRenderers(selectAnatomy) {
      selectAnatomy = selectAnatomy || function () {};
      var renderers = [
      {
        name: "Volume Rendering",
        vertexShader: "../cube.vert",
        directory: "/shaders/vr/",
        lut: {channels: "RGBA", // LA
          presetsCT: {
            "bone1": {
              "colorStops": [
                {"color": [177,74,6], "HU": 41},
                {"color": [209,209,204], "HU": 229}],
              "alphaStops": [
                {"alpha": 0, "HU": 65},
                {"alpha": 0.75, "HU": 190},
                {"alpha": 1, "HU": 461}]
            },
            "bone2": {
              "colorStops": [
                {"color": [248,139,36], "HU": 52},
                {"color": [255,255,255], "HU": 301}],
              "alphaStops": [
                {"alpha": 0, "HU": 141},
                {"alpha": 0.5, "HU": 430},
                {"alpha": 1,"HU": 1354}]
            },
            "soft": {
              "colorStops": [
                {"color": [225,129,15], "HU": -61},
                {"color": [255,255,255], "HU": 221}],
              "alphaStops":[
                {"alpha": 0, "HU": -159},
                {"alpha": 0.84, "HU": 179},
                {"alpha": 1, "HU": 437}]
            },
            "lung": {
              "colorStops": [
                {"color": [0,206,255], "HU": -883},
                {"color": [0,255,213], "HU": -584},
                {"color": [213,213,213], "HU": 453}],
              "alphaStops": [
                {"alpha": 0, "HU": -890},
                {"alpha": 0.29, "HU": -518},
                {"alpha": 0, "HU": -395},
                {"alpha": 0, "HU": 257},
                {"alpha": 0, "HU": 665}]
            }
          }
        },
        config: {
          light: {
            value: [1, 1, 1],
            uniform: "uToLight",
            type: "normal"
          }
        },
        pickFunctionJs: function (start, direction, volume, lut) {
          var steps = 128;
          var chooser = makeVRchooser(lut, steps);
          var id = pickId(start, direction, volume,
              chooser.iterate, chooser.choose, steps);
          selectAnatomy(id);
        }
      },
      {
        name: "ISO surface",
        directory: "/shaders/iso/",
        config: {
          threshold: {
            value: 0.3,
            valueCT: 150
          },
          light: {
            value: [1, 1, 1],
            uniform: "uToLight",
            type: "normal"
          }
        },
        pickFunction: function (pixel) {
          selectAnatomy(pixel[2] + 256*pixel[3]);
        }
      },
      {
        name: "MIP",
        directory: "/shaders/mip/",
        config: {
          threshold: {
            value: 1.0,
            uniform: "uThreshold"
          }
        },
        lut: {channels: "L"},
        pickFunction: function (pixel) {
          selectAnatomy(pixel[0] + 256*pixel[1]);
        }
      },
        {
          name: "MPR",
          directory: "/shaders/mpr/",
          config: {
            slice: 0.5,
            cutPlaneNormal: {
              value: [0, 0, 1],
              uniform: "uCutPlaneNormal",
              type: "normal"
            }
          },
          lut: {channels: "L"},
          pickFunction: function (pixel) {
            selectAnatomy(pixel[0] + 256*pixel[1]);
          }
        },
        {
          name: "Selected Anatomy",
          directory: "/shaders/lbl/",
          overlapTiles: true,
          lut: {channels: "LA"},
          config: {
            ligth: {
              value: [1, 1, 1],
              uniform: "uToLight",
              type: "normal"
            }
          },
          pickFunction: function (pixel) {
            var anatomyId = pixel[0] + 256 * pixel[1];
            selectAnatomy(anatomyId);
            return anatomyId > 0;
          },
          pickFunctionJs: function (start, direction, volume) {
            var longestLbl = makeLongestLabelChooser();
            var id = pickId(start, direction, volume,
                longestLbl.iterate, longestLbl.choose);
            selectAnatomy(id);
          },
          pickMode: "add"
        }
      ];

      if (utils.debug) {
        renderers.push({
          name: "Coordinates",
          directory: "/shaders/coords/"
        });
      }

      return renderers;
    }

    function getVolumeOnlyRenderers(selectAnatomy) {
      selectAnatomy = selectAnatomy || function () {};
      return [
      {
        name: "Volume Rendering",
        vertexShader: "../cube.vert",
        directory: "/shaders/direct/vr/",
        isDirect: true,
        lut: {channels: "RGBA", // LA
          presetsCT: {
            "bone1": {
              "colorStops": [
                {"color": [177,74,6], "HU": 41},
                {"color": [209,209,204], "HU": 229}],
              "alphaStops": [
                {"alpha": 0, "HU": 65},
                {"alpha": 0.75, "HU": 190},
                {"alpha": 1, "HU": 461}]
            },
            "bone2": {
              "colorStops": [
                {"color": [248,139,36], "HU": 52},
                {"color": [255,255,255], "HU": 301}],
              "alphaStops": [
                {"alpha": 0, "HU": 141},
                {"alpha": 0.5, "HU": 430},
                {"alpha": 1,"HU": 1354}]
            },
            "soft": {
              "colorStops": [
                {"color": [225,129,15], "HU": -61},
                {"color": [255,255,255], "HU": 221}],
              "alphaStops":[
                {"alpha": 0, "HU": -159},
                {"alpha": 0.84, "HU": 179},
                {"alpha": 1, "HU": 437}]
            },
            "lung": {
              "colorStops": [
                {"color": [0,206,255], "HU": -883},
                {"color": [0,255,213], "HU": -584},
                {"color": [213,213,213], "HU": 453}],
              "alphaStops": [
                {"alpha": 0, "HU": -890},
                {"alpha": 0.29, "HU": -518},
                {"alpha": 0, "HU": -395},
                {"alpha": 0, "HU": 257},
                {"alpha": 0, "HU": 665}]
            }
          },
          presetsMR: {
           "minmax": {
           "colorStops":[
             {"color":[0,0,0],"value":0},{"color":[255,255,255],"value":1}],
           "alphaStops":[{"alpha":0,"value":0},{"alpha":1,"value":1}]
           },
           "pca": {
           "colorStops":[{"color":[255,255,255],"value":0},{"color":[0,0,255],"value":0.4416243654822336}],
           "alphaStops":[{"alpha":0,"value":0.017766497461928946},{"alpha":1,"value":1}]
           },
           "tof": {
           "colorStops":[{"color":[255,255,255],"value":0.1751269035532995},{"color":[255,0,0],"value":0.4416243654822336}],
           "alphaStops":[{"alpha":0,"value":0.18781725888324874},{"alpha":1,"value":1}]
           }
          }
        },
        config: {
          light: {
            value: [1, 1, 1],
            uniform: "uToLight",
            type: "normal"
          },
          labelOppacity: {
            value: 0.0
          }
        },
        pickFunctionJs: function (start, direction, volume, lut) {
          var steps = 128;
          var chooser = makeVRchooser(lut, steps);
          var id = pickId(start, direction, volume,
              chooser.iterate, chooser.choose, steps);
          selectAnatomy(id);
          return id;
        }
      },
      {
        name: "ISO",
        vertexShader: "../cube.vert",
        directory: "/shaders/direct/mip/",
        isDirect: true,
        config: {
          labelOppacity: {
            value: 0.0
          }
        },
        pickFunctionJs: function (start, direction, volume, lut) {
          var steps = 128;
          var chooser = makeMIPChooser();
          var id = pickId(start, direction, volume,
              chooser.iterate, chooser.choose, steps);
          selectAnatomy(id);
          return id;
        }
      },
      {
        name: "MIP",
        vertexShader: "../cube.vert",
        directory: "/shaders/direct/iso/",
        isDirect: true,
        config: {
          light: {
            value: [1, 1, 1],
            uniform: "uToLight",
            type: "normal"
          },
          threshold: {
            value: 0.38
          },
          labelOppacity: {
            value: 0.0
          }
        },
        pickFunctionJs: function (start, direction, volume, lut) {
          var steps = 128;
          var chooser = makeISOChooser(0.38); // FIXME threshold
          var id = pickId(start, direction, volume,
              chooser.iterate, chooser.choose, steps);
          selectAnatomy(id);
          return id;
        }
      },
      {
        name: "coords",
        vertexShader: "../cube.vert",
        directory: "/shaders/direct/coords/",
        isDirect: true,
      }
      ];
    }

    function getShaderQualities() {
      return [
      {name: "low",     quality: 0},
      {name: "medium",  quality: 1},
      {name: "high",    quality: 2},
      {name: "highest", quality: 3}];
    }

    return {
      getRenderers: getRenderers,
      getVolumeOnlyRenderers: getVolumeOnlyRenderers,
      getShaderQualities: getShaderQualities,
      getLUTPresetHU: getLUTPresetHU,
      getLUTPreset: getLUTPreset,
      fromHU: fromHU
    };
  });
