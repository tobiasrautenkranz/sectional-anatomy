/* Sectional Anatomy Study Browser
 *
 * Copyright (C) 2016 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

requirejs.config({
  baseUrl: "/js",
  //>>includeStart("debugInclude", pragmas.debug);
  urlArgs: "bust=" +  (new Date()).getTime(),
  //>>includeEnd("debugInclude");
});

requirejs(["vr/webvr-canvas", "volume/study",
    "vr/volume-rendering", "ui/gradient-editor",
    "util/utils", "volume/volume", "volume/volume-preloader",
    "volume/axis-numbers",
    "lib/gl-matrix"],
    function(VRCanvas, studies, vr, GradientEditor, utils, vol, VolumePreloader,
      AxisNumbers, glm) {
      "use strict";

      var quat = glm.quat;
      var vec3 = glm.vec3;
      var mat4 = glm.mat4;

      function rotationToWorld(basis) {
        var rotation = quat.create();
        var corAxes =  [
          vec3.fromValues(0,1,0), // viewing direction
          vec3.fromValues(1,0,0), // right
          vec3.fromValues(0,0,1), // up (see gl-matrix docs)
        ];
        utils.map(function(axis) {
          basis.to(axis, axis);
          return vec3.normalize(axis, axis);
        }, corAxes);

        quat.setAxes(rotation, corAxes[0], corAxes[1], corAxes[2]);
        return rotation;
      }
      function initialRotation(volume) {
        // rotate volume to coronal position
        var rotation = rotationToWorld(volume.getBasis());
        if (!vol.isRightHandCoordinateSystem(volume)) {
          var rot = quat.create();
          // FIXME
          // only a hack to make it work (mostly)
          if (volume.getAxisNumber() == AxisNumbers.sag) {
            quat.rotateZ(rot, rot, Math.PI);
            quat.rotateY(rot, rot, Math.PI/2);
          } else {
            quat.rotateX(rot, rot, -Math.PI/2);
          }
          quat.multiply(rotation, rot, rotation);
        }
        return rotation;
      }

      var loading = document.createElement("div");
      var onLoaded = function () {
        document.documentElement.removeChild(loading);

        canvas.addEventListener("click", function (e)
            { vrcanvas.isPicking = !vrcanvas.isPicking; });
      };
      var progress = document.createElement("progress");
      function init() {
        loading.style.position = "absolute";
        loading.style.width = "50%";
        loading.style.height = "50%";
        loading.style.left = "25%";
        loading.style.top = "25%";

        var text = document.createElement("p");
        text.style.fontSize = "5em";
        text.style.color = "grey";
        text.style.marginBottom = 0;
        text.appendChild(document.createTextNode("loading"));
        loading.appendChild(text);

        progress.value = 0;
        progress.style.width = "100%";
        loading.appendChild(progress);

        document.documentElement.appendChild(loading);

        studies.loadStudy(onStudyLoad, function() {}, navigator.language, "mra-brain/");
      }

      var study;
      function onStudyLoad(s) {
        study = s;
        var preload = new VolumePreloader(study);
        preload.batches = 4;
        preload.onprogress = onVolumeLoading;
        preload.oncomplete = onVolumeLoaded;
        preload.run();
      }

      var vrcanvas;
      var canvas = document.getElementById("webvr-canvas");
      function onVolumeLoading (ratio) {
        progress.value = ratio;
      }
      function onVolumeLoaded () {
        progress.value = 1;
        vrcanvas = new VRCanvas(canvas);
        vrcanvas.init(setupVR);
      }

      var opacity = 0.2;
      var opacity_delta = 0.01;

      function redraw (time) {
        opacity = 0.2*Math.sin(time/400)+0.3;

        vrcanvas.reticle.disabled = ! vrcanvas.isPicking;

        vrcanvas.vr.updateRenderer({labelOppacity: opacity});
        vrcanvas.draw();
        if (vrcanvas.isPicking) {
          vrcanvas.pickAtCenter();
        }
        vrcanvas.requestAnimationFrame(redraw);
      }

      function urlIntParam(name, defaultValue) {
        var value = Number.parseInt(utils.urlParam(name));

        if (Number.isNaN(value)) {
          return defaultValue;
        }

        return value;
      }

      function setupVR() {
        var volumeIndex = urlIntParam("volume", 0);
        if (!(0 <= volumeIndex && volumeIndex < study.volumes.length-1)) {
          volumeIndex = 0;
        }
        var volume = study.volumes[volumeIndex];

        mat4.fromQuat(vrcanvas.mvMatrix(), initialRotation(volume));

        vrcanvas.setVolume(volume);

        var rendererIndex = urlIntParam("renderer", 1);

        var lblLUT = volume.getLabelMap().getLUT().array;
        // set alpha to 0 for all labels
        for (var i=0; i<lblLUT.length / 4; ++i) {
          lblLUT[4*i + 3] = 0;
        }

        var lastId = 0;
        var onPicked = function (id) {
          lblLUT[4* lastId + 3] = 0;
          lastId = id;

          if (id > 0) {
            lblLUT[4* id + 3] = 255;
          }
        };

        var renderers = vr.getVolumeOnlyRenderers(onPicked);
        if (!(0 <= rendererIndex && rendererIndex < renderers.length)) {
          rendererIndex = 0;
        }

        var gradient = new GradientEditor(document.createElement("div"), "RGBA");
        if (rendererIndex === 0 && volumeIndex === 0) {
          gradient.load(vr.getLUTPreset(vr.getVolumeOnlyRenderers()[0].lut.presetsMR.tof));
        }
        vrcanvas.setLUT(function () { return gradient.toArray(); });

        vrcanvas.setRenderer(renderers[rendererIndex],
            function () {
              vrcanvas.requestAnimationFrame(redraw);
              onLoaded();
              vrcanvas.setQuality(2);
        });
      }

      canvas.style.margin = "0";
      canvas.style.left = "0";
      canvas.style.top = "0";
      canvas.style.width = "100%";
      canvas.style.height = "100%";
      canvas.style.position = "absolute";

      init();
    });
