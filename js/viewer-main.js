/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2017 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

requirejs.config({
  urlArgs: "v=1.4.7", // viewer version
  //>>includeStart("debugInclude", pragmas.debug);
  urlArgs: "bust=" +  (new Date()).getTime(),
  //>>includeEnd("debugInclude");
  baseUrl: "/js/"
});

(function assertCanvasSupport () {
  "use strict";
  if (!document.createElement("canvas").getContext)  {
    var errorMsg = document.createElement("div");
    errorMsg.innerHTML = "<h1>Error</h1>"  +
      "<p>Your browser does not support the " +
      "<a href=\"http://en.wikipedia.org/wiki/Canvas_element\">" +
      "HTML5 canvas</a> element. " +
      "Try <a href=\"http://mozilla.com\">Firefox</a> or " +
      "<a href='http://google.com/chrome/'>Google Chrome</a>.</p>";
    errorMsg.className = "error";
    document.body.insertBefore(errorMsg, document.body.children[0]);

    throw new Error("HTML5 canvas is not supported by browser.");
  }
})();


requirejs(["viewer"],
    function (viewer) {
      "use strict";
      viewer.init();
    });
