/* Sectional Anatomy Viewer
 *
 * Copyright (C) 2012 - 2016 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define(["util/utils", "volume/volume", "ui/progress", "ui/tools",
    "ui/anatomy-info", "ui/views", "view/grid-view", "volume/study",
    "view/redraw", "ui/tabs", "volume/volume-preloader"],
  function (utils, volumes, progress, tools, AnatomyInfo, views, GridView,
    studies, draw, tabBar, VolumePreloader) {
    "use strict";

    function createGridView(parentElement, viewer) {
      var div = document.createElement("div");
      div.className = "views";
      parentElement.appendChild(div);

      var gridView = new GridView(div, viewer);
      gridView.setupEventHandlers();

      return gridView;
    }
    function setupDataInfo() {
      var dataInfo = document.getElementById("data-info");
      if (dataInfo) {
        // html5 details missing
        var dataInfoDetails = document.getElementById("data-info-detail");
        if (dataInfoDetails) {
          dataInfoDetails.style.display = "none";
          dataInfo.addEventListener("click", function () {
            if (dataInfoDetails.style.display) {
              dataInfoDetails.style.display = "";
              dataInfo.style.backgroundColor = "rgba(0,0,0,0.5)";
            } else {
              dataInfoDetails.style.display = "none";
              dataInfo.style.backgroundColor = "";
            }
          });
        }
      }
    }

    function setFixedViewport () {
      var fixedViewport = document.createElement("meta");
      fixedViewport.name = "viewport";
      fixedViewport.content =
        "width=device-width, initial-scale=1.0, user-scalable=no";
      document.head.appendChild(fixedViewport);

      document.body.addEventListener("touchmove", function (e) {
        //e.preventDefault();
      });
    }

    var init = function () {
      var progressString = "loading.";
      function updateLoadProgress () {
        progress.update(progressString);
        progressString += ".";
      }
      updateLoadProgress();

      var div = document.getElementsByClassName("sectional_anatomy_viewer")[0];
      if(!div)
        throw "error div";

      setFixedViewport();

      window.addEventListener(utils.mouseWheelEvent, function (e) {
        // prevent zoom with ctrl+mousewheel. does not work for chrome
        if (e.ctrlKey) {
          e.preventDefault();
        }
      });


      var viewer = {};
      viewer.gridView = createGridView(div, viewer);
      setupDataInfo();

      updateLoadProgress();

      var onStudyLoad = function (study) {
        viewer.study = study;

        viewer.anatomyInfo = createAnatomyInfo(div, study, viewer);

        var range = volumes.volumeRangeInWorldCoordinates(study.volumes[0]);

        for (var i=0; i<3; i++) {
          viewer.gridView.index[i] = (range.begin[i] + range.end[i]-1)/2;
        }

        views.showSingleView(viewer.gridView, study.volumes[0]);
        window.addEventListener("resize", function () {
          if (viewer.gridView.onResize) viewer.gridView.onResize();
        });
        if (viewer.gridView.onResize) viewer.gridView.onResize();

        var tabOverlays = createTabs(div, viewer);

        document.addEventListener("keydown", function (e) {
          if (!(document.activeElement instanceof HTMLInputElement) &&
              viewer.gridView.activeView.interactive) {
            switch (e.keyCode) {
              case 8: // Backspace
              case 46: // Delete
                viewer.anatomyTools.unselectAnatomy(viewer.anatomyInfo.anatomyID);
                draw.requestRedraw();
                break;
              default:
                viewer.gridView.activeView.keydown(e);
            }
          }
        });
        processURLArgs(viewer);

        draw.drawObjects.push(viewer.gridView);
        draw.drawObjects.push(tabOverlays.viewTools);
        draw.requestRedraw();

        progress.done();

        utils.clearURLHash();

        runVolumePreloader(viewer.study);
      };

      var language = utils.urlParam("lang") ||
        navigator.language || // Chrome & Firefox
        navigator.userLanguage; // IE

      studies.loadStudy(onStudyLoad, updateLoadProgress, language);
    };


    function createAnatomyInfo(parentElement, study, viewer) {
      var anatomyInfo = new AnatomyInfo(parentElement, study, viewer);
      return anatomyInfo;
    }

    function createTabBar(parentElement) {
      var tabsDiv = document.createElement("div");
      tabsDiv.className = "tab-bar";
      var tabs = new tabBar.Tabs(tabsDiv);
      parentElement.appendChild(tabsDiv);

      return tabs;
    }
    function createTabs(parentElement, viewer) {
      var tabs = createTabBar(parentElement);
      viewer.tabs = tabs;

      var overviewIcon = "#";
      var overviewButton =
        tabs.addButton(overviewIcon + " overview",
            function() {
              views.showOverview(viewer, function onSelect () {
                overviewButton.parentElement.removeAttribute("selected");
              },
              (document.body.clientWidth < 800));
              tabs.showOverlay(null);
              overviewButton.parentElement.removeAttribute("selected");
              // FIXME hack: always select when clicked, caller
              // toggles the state when the function returns so everything
              // works out, for now.
            });

      var anatomy = new tools.AnatomyTools(parentElement, viewer.anatomyInfo,
          viewer.study, viewer.gridView);
      //var anatomyIcon = "\u2695";
      viewer.anatomy = anatomy;
      tabs.addTab("<span class='anatomy'>anatomy</span>", anatomy);
      viewer.anatomyTools = anatomy;

      var setting = new tools.Settings(parentElement, {
        study: viewer.study,
        anatomy: anatomy,
        anatomyInfo: viewer.anatomyInfo
      });
      //var settingsIcon = "\u2699";
      tabs.addTab("<span class='settings'>settings</span>", setting);
      //tabs.addTab(settingsIcon + " settings", setting);

      var view = new tools.ViewTools(parentElement, viewer);
      //var viewIcon = "\u25d0";
      var viewButton = tabs.addTab("<span class='view'>view</span>", undefined,
          function () {
            var viewTools = viewer.gridView.getView(0,0).viewTools || view;
            if (viewTools.isVisible()) {
              viewTools.setHidden(true);
            } else {
              tabs.showOverlay(viewTools);
            }
          });
      tabs.addOverlay(view, viewButton);

      tabs.resize();

      return {
        anatomy: anatomy,
        viewTools: view
      };
    }

    function processURLArgs(viewer) {
      var query = utils.urlParam("q");
      var id = parseInt(utils.urlParam("id"), 10);

      if (query) {
        viewer.anatomy.search(query);
      } else if (id) {
        viewer.anatomyInfo.setAnatomyID(id);
        tools.findSlice(id, function (index) {
          if (index) {
            viewer.gridView.index[0] = index[0];
            viewer.gridView.index[1] = index[1];
            viewer.gridView.index[2] = index[2];
          }
          draw.requestRedraw();
        });
      }
    }

    function runVolumePreloader(study) {
      if (utils.debug) {
        console.log("readyState loading time [sec]: ",
            Math.round((Date.now() - performance.timing.navigationStart)/100)/10);
      }

      var progress = document.createElement("div");
      progress.id = "sliceProgress";
      progress.className = "progress-bar";
      var bar = document.createElement("span");
      progress.appendChild(bar);
      document.body.appendChild(progress);

      var text = document.createElement("p");
      text.appendChild(document.createTextNode("Loading Slices"));
      progress.appendChild(text);

      function updateProgress (completedRatio) {
        bar.style.width = Math.round(100*completedRatio) + "%";
      }
      function completed () {
        if (progress) {
          document.body.removeChild(progress);
          progress = null;
        }
        if (utils.debug) {
          console.log("total loading time [sec]: ",
              Math.round((Date.now() - performance.timing.navigationStart)/100)/10);
        }
      }
      var preload = new VolumePreloader(study);
      preload.batches = 4;
      preload.onprogress = updateProgress;
      preload.oncomplete = completed;
      preload.run();
    }

    return {
      init: init
    };
  });

